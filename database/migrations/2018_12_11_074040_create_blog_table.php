<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('autor_id')->unsigned()->comment('Автор ID');

            $table->string('title')->comment('Заголовок');

            $table->longText('content')->comment('Тело новости');

            $table->integer('prewiew_id')->unsigned()->comment('Превью');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
