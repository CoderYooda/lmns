<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraitCommerce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commerce', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
            $table->foreign('commerce_type_id')->references('id')->on('commerce_type');
            $table->foreign('readiness_id')->references('id')->on('readiness');
            $table->foreign('build_type_id')->references('id')->on('build_type');
            $table->foreign('doing_id')->references('id')->on('commerce_doing');
            $table->foreign('purpose_id')->references('id')->on('commerce_purpose');
            $table->foreign('kind_id')->references('id')->on('commerce_kind');
            $table->foreign('sklad_id')->references('id')->on('commerce_sklad');
            $table->foreign('gruz_id')->references('id')->on('commerce_gruz');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
