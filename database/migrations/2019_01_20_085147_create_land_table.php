<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('land', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id', false, 10)
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->integer('object_id', false, 10)
                ->unsigned()
                ->nullable()
                ->comment("ID Объекта");

            $table->string('house_number')
                ->nullable()
                ->comment("Номер дома");

            $table->float('total_square')
                ->nullable()
                ->comment("Общая площадь в сотках.");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('land');
    }
}
