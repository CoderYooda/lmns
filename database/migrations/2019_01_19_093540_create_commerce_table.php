<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommerceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commerce', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id', false, 10)
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->integer('object_id', false, 10)
                ->unsigned()
                ->nullable()
                ->comment("ID Объекта");

            $table->string('house_number')
                ->nullable()
                ->comment("Номер дома");

            $table->integer('floor')
                ->unsigned()
                ->nullable()
                ->comment("Этаж");

            $table->integer('n_of_floor')
                ->unsigned()
                ->nullable()
                ->comment("Этажность");


            $table->integer('commerce_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип коммерции ID");

            $table->integer('readiness_id')
                ->unsigned()
                ->nullable()
                ->comment("Статус готовности ID");


            $table->integer('build_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип здания ID");

            $table->integer('year')
                ->nullable()
                ->comment("Год постройки");


            $table->float('total_square')
                ->nullable()
                ->comment("Общая площадь в м. кв.");

            $table->float('sector_square')
                ->nullable()
                ->comment("Площадь участка в м. кв.");

            $table->integer('height')
                ->unsigned()
                ->nullable()
                ->comment("Высота");

            $table->integer('rooms')
                ->unsigned()
                ->nullable()
                ->comment("кол-во помещений");

            $table->integer('doing_id')
                ->unsigned()
                ->nullable()
                ->comment("Деятельность");

            $table->integer('purpose_id')
                ->unsigned()
                ->nullable()
                ->comment("Принадлежность");

            $table->integer('kind_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип");

            $table->integer('sklad_id')
                ->unsigned()
                ->nullable()
                ->comment("Склад");

            $table->integer('gruz_id')
                ->unsigned()
                ->nullable()
                ->comment("Груз");

            $table->integer('state_id')
                ->unsigned()
                ->nullable()
                ->comment("Состояние коммерции ID");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commerce');
    }
}
