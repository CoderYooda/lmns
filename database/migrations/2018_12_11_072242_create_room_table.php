<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id', false, 10)
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->integer('object_id', false, 10)
                ->unsigned()
                ->nullable()
                ->comment("ID Объекта");

            $table->string('house_number')
                ->nullable()
                ->comment("Номер дома");

            $table->integer('floor')
                ->unsigned()
                ->nullable()
                ->comment("Этаж");

            $table->integer('n_of_floor')
                ->unsigned()
                ->nullable()
                ->comment("Этажность");

            $table->integer('build_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип здания ID");

            $table->integer('year')
                ->nullable()
                ->comment("Год постройки");

            $table->integer('room_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип комнаты ID");

            $table->integer('rooms')
                ->nullable()
                ->comment("Кол - во комнат");

            $table->float('total_square')
                ->nullable()
                ->comment("Общая площадь в м. кв.");

            $table->float('room_square')
                ->nullable()
                ->comment("Площадь комнат в м. кв.");

            $table->float('count_people')
                ->nullable()
                ->comment("Рассчитано на человек");

            $table->integer('state_id')
                ->unsigned()
                ->nullable()
                ->comment("Состояние комнаты ID");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
