<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Добавление Foreign Key
 * Автор CoderYooda@gmail.com
 *
 * @return void
 */

class ForeignKeys extends Migration
{
    public function up()
    {

        Schema::table('objects', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('delited_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('area_id')->references('id')->on('area');
        });

        Schema::table('apartment', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
            $table->foreign('gk_id')->references('id')->on('gk');
            $table->foreign('build_type_id')->references('id')->on('build_type');
            $table->foreign('readiness_id')->references('id')->on('readiness');
            $table->foreign('hot_water_system_id')->references('id')->on('hot_water_system');
            $table->foreign('state_id')->references('id')->on('state');
            $table->foreign('rooms_isolation_id')->references('id')->on('rooms_isolation');
            $table->foreign('wc_id')->references('id')->on('wc');
        });

        Schema::table('room', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
            $table->foreign('build_type_id')->references('id')->on('build_type');
            $table->foreign('room_type_id')->references('id')->on('room_type');
            $table->foreign('state_id')->references('id')->on('state');
        });

        Schema::table('garage', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
            $table->foreign('gk_id')->references('id')->on('gk');
            $table->foreign('sgk_id')->references('id')->on('sgk');
            $table->foreign('garage_type_id')->references('id')->on('garage_type');
            $table->foreign('state_id')->references('id')->on('state');
        });


        Schema::table('wall_materials_to_apartment', function (Blueprint $table) {
            $table->foreign('wall_materials_id')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('apartment_comfort_to_apartment', function (Blueprint $table) {
            $table->foreign('apartment_comfort_id')->references('id')->on('apartment_comfort')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('repairs_to_apartment', function (Blueprint $table) {
            $table->foreign('repairs_id')->references('id')->on('repairs')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('build_comfort_to_apartment', function (Blueprint $table) {
            $table->foreign('build_comfort_id')->references('id')->on('build_comfort')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('layouts_to_apartment', function (Blueprint $table) {
            $table->foreign('layout_id')->references('id')->on('layout')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('heating_to_apartment', function (Blueprint $table) {
            $table->foreign('heating_id')->references('id')->on('heating')->onDelete('cascade');
            $table->foreign('apartment_id')->references('id')->on('apartment')->onDelete('cascade');
        });

        Schema::table('wall_materials_to_room', function (Blueprint $table) {
            $table->foreign('wall_materials_id')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('room')->onDelete('cascade');
        });

        Schema::table('room_special_to_room', function (Blueprint $table) {
            $table->foreign('room_special_id')->references('id')->on('room_special')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('room')->onDelete('cascade');
        });

        Schema::table('room_comfort_to_room', function (Blueprint $table) {
            $table->foreign('room_comfort_id')->references('id')->on('room_comfort')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('room')->onDelete('cascade');
        });

        Schema::table('build_comfort_to_room', function (Blueprint $table) {
            $table->foreign('build_comfort_id')->references('id')->on('build_comfort')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('room')->onDelete('cascade');
        });

        Schema::table('wall_materials_to_garage', function (Blueprint $table) {
            $table->foreign('wall_materials_id')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('garage_id')->references('id')->on('garage')->onDelete('cascade');
        });

        Schema::table('garage_special_to_garage', function (Blueprint $table) {
            $table->foreign('garage_special_id')->references('id')->on('garage_special')->onDelete('cascade');
            $table->foreign('garage_id')->references('id')->on('garage')->onDelete('cascade');
        });

        Schema::table('garage_comfort_to_garage', function (Blueprint $table) {
            $table->foreign('garage_comfort_id')->references('id')->on('garage_comfort')->onDelete('cascade');
            $table->foreign('garage_id')->references('id')->on('garage')->onDelete('cascade');
        });

        Schema::table('user_to_agency', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('agency_id')->references('id')->on('agencies')->onDelete('cascade');
        });

        Schema::table('agencies', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('logo_id')->references('id')->on('images');
        });

        Schema::table('blog', function (Blueprint $table) {
            $table->foreign('autor_id')->references('id')->on('users');
            $table->foreign('prewiew_id')->references('id')->on('images');
        });

        Schema::table('images', function (Blueprint $table) {
            $table->foreign('uploader_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('images_to_object', function (Blueprint $table) {
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
        });

        Schema::table('wall_materials_to_commerce', function (Blueprint $table) {
            $table->foreign('wall_materials_id')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('commerce_id')->references('id')->on('commerce')->onDelete('cascade');
        });

        Schema::table('build_comfort_to_commerce', function (Blueprint $table) {
            $table->foreign('build_comfort_id')->references('id')->on('build_comfort')->onDelete('cascade');
            $table->foreign('commerce_id')->references('id')->on('commerce')->onDelete('cascade');
        });

        Schema::table('commerce_special_to_commerce', function (Blueprint $table) {
            $table->foreign('commerce_special_id')->references('id')->on('commerce_special')->onDelete('cascade');
            $table->foreign('commerce_id')->references('id')->on('commerce')->onDelete('cascade');
        });

        Schema::table('commerce_adds_to_commerce', function (Blueprint $table) {
            $table->foreign('commerce_adds_id')->references('id')->on('commerce_adds')->onDelete('cascade');
            $table->foreign('commerce_id')->references('id')->on('commerce')->onDelete('cascade');
        });

        Schema::table('wall_materials_to_house', function (Blueprint $table) {
            $table->foreign('wall_materials_id')->references('id')->on('wall_materials')->onDelete('cascade');
            $table->foreign('house_id')->references('id')->on('house')->onDelete('cascade');
        });

        Schema::table('house_comfort_to_house', function (Blueprint $table) {
            $table->foreign('house_comfort_id')->references('id')->on('house_comfort')->onDelete('cascade');
            $table->foreign('house_id')->references('id')->on('house')->onDelete('cascade');
        });

        Schema::table('repairs_to_house', function (Blueprint $table) {
            $table->foreign('repairs_id')->references('id')->on('repairs')->onDelete('cascade');
            $table->foreign('house_id')->references('id')->on('house')->onDelete('cascade');
        });

        Schema::table('build_comfort_to_house', function (Blueprint $table) {
            $table->foreign('build_comfort_id')->references('id')->on('build_comfort')->onDelete('cascade');
            $table->foreign('house_id')->references('id')->on('house')->onDelete('cascade');
        });

        Schema::table('land_comfort_to_land', function (Blueprint $table) {
            $table->foreign('land_comfort_id')->references('id')->on('land_comfort')->onDelete('cascade');
            $table->foreign('land_id')->references('id')->on('land')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
