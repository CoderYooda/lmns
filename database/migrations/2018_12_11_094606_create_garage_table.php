<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id', false, 10)
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->integer('object_id', false, 10)
                ->unsigned()
                ->nullable()
                ->comment("ID Объекта");

            $table->string('house_number')
                ->nullable()
                ->comment("Номер дома");

            $table->integer('floor')
                ->unsigned()
                ->nullable()
                ->comment("Этаж");

            $table->integer('n_of_floor')
                ->unsigned()
                ->nullable()
                ->comment("Этажность");

            $table->integer('gk_id')
                ->unsigned()
                ->nullable()
                ->comment("Жилищьный комплекс ID");

            $table->integer('sgk_id')
                ->unsigned()
                ->nullable()
                ->comment("Строй Газ Консалтинг ID");

            $table->integer('garage_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип гаража ID");

            $table->integer('year')
                ->nullable()
                ->comment("Год постройки");

            $table->float('total_square')
                ->nullable()
                ->comment("Общая площадь в м. кв.");

            $table->float('sector_square')
                ->nullable()
                ->comment("Площадь участка в м. кв.");

            $table->integer('state_id')
                ->unsigned()
                ->nullable()
                ->comment("Состояние гаража ID");

            $table->integer('height')
                ->nullable()
                ->comment("Высота");

            $table->integer('width')
                ->nullable()
                ->comment("Ширина");

            $table->integer('length')
                ->nullable()
                ->comment("Длинна");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garage');
    }
}
