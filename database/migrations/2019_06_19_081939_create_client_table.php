<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('add_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('add_email')->nullable();
            $table->string('comment')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('rooms')->nullable();
            $table->integer('square')->nullable();
            $table->integer('to_agent')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
