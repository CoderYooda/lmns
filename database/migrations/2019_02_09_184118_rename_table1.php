<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('garage', function (Blueprint $table) {
            $table->float('height', 8, 2)->change();
            $table->float('width', 8, 2)->change();
            $table->float('length', 8, 2)->change();
        });

        Schema::table('land', function (Blueprint $table) {
            $table->renameColumn('total_square', 'land_square');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
