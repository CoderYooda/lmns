<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {

            $table->increments('id');

            $table->string('title')->comment('Наименование');

            $table->string('phone')->comment('Телефон');

            $table->string('email')->comment('Почта');

            $table->string('address')->comment('Адрес');

            $table->integer('owner_id')->unsigned()->comment('Владелец ID');

            $table->integer('logo_id')->nullable()->unsigned()->comment('Картинка ID');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
