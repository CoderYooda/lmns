<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')
                ->default('Без названия')
                ->comment("Название объекта");

            $table->integer('category_id')
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->boolean('published')
                ->default(1)
                ->comment("Статус публикации");

            $table->boolean('exlusive')
                ->default(0)
                ->comment("Эксклюзиный объект");

            $table->boolean('writed')
                ->default(0)
                ->comment("Эксклюзиный объект");

            $table->date('exlusive_date')
                ->useCurrent()
                ->nullable()
                ->comment("Дата статуса Эксклюзиный");

            $table->string('part')
                ->default('1-1')
                ->comment("Доля объекта");

            $table->integer('agency_id')
                ->unsigned()
                ->nullable()
                ->comment("Принадлежность к агентству");

            $table->integer('created_id')
                ->unsigned()
                ->nullable()
                ->comment("Кем был создан ID");

            $table->integer('delited_id')
                ->unsigned()
                ->nullable()
                ->comment("Кем был удален ID");

            $table->integer('city_id')
                ->unsigned()
                ->nullable()
                ->comment("Город ID");

            $table->integer('area_id')
                ->unsigned()
                ->nullable()
                ->comment("Район ID");

            $table->string('street')
                ->nullable()
                ->comment("Улица");

            $table->bigInteger('price')
                ->unsigned()
                ->nullable()
                ->comment("Стоимость");

            $table->boolean('rented')
                ->nullable()
                ->comment('Аренда?');

            $table->integer('rent_price')
                ->nullable()
                ->comment('Стоимость аренды в месяц');

            $table->integer('price_m')
                ->unsigned()
                ->nullable()
                ->comment("Стоимость за м.кв.");

            $table->boolean('notary')
                ->default(0)
                ->comment('Нотариус');

            $table->boolean('custody')
                ->default(0)
                ->comment('Опека');

            $table->boolean('burden')
                ->default(0)
                ->comment('Обремененный');

            $table->text('description')
                ->nullable()
                ->comment('Описание');

            $table->string('geopoint')
                ->nullable()
                ->comment('Яндекс координаты');

            $table->integer('mls')
                ->unsigned()
                ->nullable()
                ->comment("Неведомая числовая хуета");

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
