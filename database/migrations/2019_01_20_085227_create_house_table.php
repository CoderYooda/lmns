<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id', false, 10)
                ->unsigned()
                ->default(1)
                ->comment("ID Категории");

            $table->integer('object_id', false, 10)
                ->unsigned()
                ->nullable()
                ->comment("ID Объекта");

            $table->string('house_number')
                ->nullable()
                ->comment("Номер дома");

            $table->integer('n_of_floor')
                ->unsigned()
                ->nullable()
                ->comment("Этажность");

            $table->integer('gk_id')
                ->unsigned()
                ->nullable()
                ->comment("Жилищьный комплекс ID");

            $table->integer('build_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип здания ID");

            $table->integer('readiness_id')
                ->unsigned()
                ->nullable()
                ->comment("Статус готовности ID");

            $table->integer('year')
                ->nullable()
                ->comment("Год постройки");

            $table->integer('qr')
                ->nullable()
                ->comment("Квартал");

            $table->integer('hot_water_system_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип водоснабжения ID");

            $table->integer('apartment_type_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип квартиры ID");

            $table->integer('rooms')
                ->nullable()
                ->comment("Кол-во комнат");

            $table->float('total_square')
                ->nullable()
                ->comment("Общая площадь в м. кв.");

            $table->float('live_square')
                ->nullable()
                ->comment("Жилая площадь в м. кв.");

            $table->float('kitchen_square')
                ->nullable()
                ->comment("Площадь кухни в м. кв.");

            $table->float('land_square')
                ->nullable()
                ->comment("Площадь кухни в м. кв.");

            $table->integer('state_id')
                ->unsigned()
                ->nullable()
                ->comment("Состояние квартиры ID");

            $table->integer('rooms_isolation_id')
                ->unsigned()
                ->nullable()
                ->comment("Изолирование комнат ID");

            $table->integer('wc_id')
                ->unsigned()
                ->nullable()
                ->comment("Тип санузла ID");

            $table->integer('wc_num')
                ->nullable()
                ->comment("Кол - во санузлов");

            $table->integer('balconies')
                ->nullable()
                ->comment("Кол - во балконов");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house');
    }
}
