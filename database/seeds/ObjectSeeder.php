<?php

use Illuminate\Database\Seeder;

class ObjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Сергей',
            'lastname' => 'Сенаторов',
            'midname' => 'Андреевич',
            'email' => 'CoderYooda@gmail.com',
            'password' => Hash::make('senatorov616322'),
        ]);
    }
}
