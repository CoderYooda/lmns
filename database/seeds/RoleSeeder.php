<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = new Role();
        $superadmin->name         = 'superadmin';
        $superadmin->display_name = 'Главный администратор'; // optional
        $superadmin->description  = 'Главнй администратор проекта'; // optional
        $superadmin->save();

        $headagent = new Role();
        $headagent->name         = 'headagent';
        $headagent->display_name = 'Глава агентства'; // optional
        $headagent->description  = 'Главный агент'; // optional
        $headagent->save();

        $agent = new Role();
        $agent->name         = 'agent';
        $agent->display_name = 'Агент'; // optional
        $agent->description  = 'Агент'; // optional
        $agent->save();

        $createPost = new Permission();
        $createPost->name         = 'remove-own-object';
        $createPost->display_name = 'Удаление своих обхектов'; // optional
        $createPost->description  = 'Позволяет удалять объекты созданные пользователем или переданных пользователю'; // optional
        $createPost->save();
    }
}
