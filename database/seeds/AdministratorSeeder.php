<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Agency;
use App\Permission;

use App\Role;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'firstname' => 'Сергей',
            'lastname' => 'Сенаторов',
            'midname' => 'Андреевич',
            'phone' => '315405',
            'email' => 'CoderYooda@gmail.com',
            'password' => Hash::make('senatorov616322'),
        ]);

        $agency = Agency::create([
            'title' => 'Администрация сайта',
            'phone' => '315405',
            'email' => 'CoderYooda@gmail.com',
            'address' => 'Ул. Сезам, дом 1',
            'owner_id' => $admin->id,
        ]);

        $admin->toAgency()->attach($agency);

        $superadmin = Role::where('name', 'superadmin')->first();

        $perm = Permission::where('name', 'remove-own-object')->first();

        $superadmin->perms()->sync($perm);
        $admin->roles()->attach($superadmin->id);
    }
}
