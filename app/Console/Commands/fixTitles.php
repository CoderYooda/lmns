<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Objects\BaseObject;

class fixTitles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'titles:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $objects = BaseObject::all();
        foreach($objects as $object){
            $title = str_replace('.00','', $object->title);
            $title = str_replace('м2',' м<sup>2</sup>', $title);
            $title = str_replace('.0','', $title);
            $title = str_replace('.10','.1', $title);
            $title = str_replace('.20','.2', $title);
            $title = str_replace('.30','.3', $title);
            $title = str_replace('.40','.4', $title);
            $title = str_replace('.50','.5', $title);
            $title = str_replace('.60','.6', $title);
            $title = str_replace('.70','.7', $title);
            $title = str_replace('.80','.8', $title);
            $title = str_replace('.90','.9', $title);
            $title = str_replace(' м<sup>','&nbsp;м<sup>', $title);
            $title = str_replace(' сот','&nbsp;сот', $title);

            $title = str_replace(' эт.','&nbsp;эт.', $title);

            $object->title = $title;
            $object->save();
        }
    }
}
