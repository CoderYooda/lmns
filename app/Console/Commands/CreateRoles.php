<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Role;
use App\Permission;
use App\Models\Agency;
use App\Models\User;
use App\Models\Stock\Images;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CreateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $superadmin = new Role();
//        $superadmin->name         = 'superadmin';
//        $superadmin->display_name = 'Главный администратор'; // optional
//        $superadmin->description  = 'Главнй администратор проекта'; // optional
//        $superadmin->save();
//
//        $headagent = new Role();
//        $headagent->name         = 'headagent';
//        $headagent->display_name = 'Глава агентства'; // optional
//        $headagent->description  = 'Главный агент'; // optional
//        $headagent->save();
//
//        $agent = new Role();
//        $agent->name         = 'agent';
//        $agent->display_name = 'Агент'; // optional
//        $agent->description  = 'Агент'; // optional
//        $agent->save();
//
//        $createPost = new Permission();
//        $createPost->name         = 'remove-own-object';
//        $createPost->display_name = 'Удаление своих обхектов'; // optional
//        $createPost->description  = 'Позволяет удалять объекты созданные пользователем или переданных пользователю'; // optional
//        $createPost->save();
        DB::table('user_to_agency')->update(['status' => 'agent']);

        $agencies = Agency::all();
        foreach($agencies as $agency){
            $user = User::where('id', $agency->owner_id)->first();
            $relation = DB::table('user_to_agency')->where('user_id', $user->id)->where('agency_id', $agency->id);
            $relation->update(['status' => 'manager']);

        }

        $images = Images::where('rank', NULL)->get();

        foreach($images as $key => $image){
            $image->rank = $key;
            $image->save();
        }

    }
}
