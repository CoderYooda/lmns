<?php

namespace App\Console\Commands;

use App\Permission;
use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Agency;
use Illuminate\Support\Facades\Hash;
use App\Role;

class CreateSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $admin = User::create([
//            'firstname' => 'Сергей',
//            'lastname' => 'Сенаторов',
//            'midname' => 'Андреевич',
//            'phone' => '315405',
//            'email' => 'CoderYooda@gmail.com',
//            'password' => Hash::make('senatorov616322'),
//        ]);
//
//        $agency = Agency::create([
//            'title' => 'Администрация сайта',
//            'phone' => '315405',
//            'email' => 'CoderYooda@gmail.com',
//            'address' => 'Ул. Сезам, дом 1',
//            'owner_id' => $admin->id,
//        ]);

        $admin = User::where('id', 1)->first();

        //$admin->toAgency()->attach($agency);

        $superadmin = Role::where('name', 'superadmin')->first();

        $perm = Permission::where('name', 'remove-own-object')->first();

        $superadmin->perms()->sync($perm);

        $admin->roles()->attach($superadmin->id);
    }
}
