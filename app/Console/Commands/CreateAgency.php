<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Agency;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Mail\AgentCreated;

class CreateAgency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agency:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $agencyes = [
            ['name' => 'ГАРМОНИЯ', 'email'                                      => 'AN.GARMONIA@MAIL.RU'],
            ['name' => 'ДОЛЬЧЕ-ВИТА', 'email'                                   => 'moiseev.serzh@inbox.ru'],
            ['name' => 'ЮЦ "Белгородское бюро недвижимости"', 'email'           => 'innsmir@mail.ru'],
            ['name' => 'Жильё Белогорья', 'email'                               => 'jilyo-belogorya@mail.ru'],
            ['name' => 'ДОМУС', 'email'                                         => 'domus-r@mail.ru'],
            ['name' => 'РИЭЛТОРСКИЙ ЦЕНТР', 'email'                             => 'belrealtor@yandex.ru'],
            ['name' => 'БУЛЬВАР 31', 'email'                                    => 'ostapenkoon@yandex.ru'],
            ['name' => 'ЖИЛИЩНАЯ ИНИЦИАТИВА', 'email'                           => 'jil-in31@mail.ru'],
            ['name' => 'ЦН ДОМ', 'email'                                        => 'domvbelgorode@mail.ru'],
            ['name' => 'АН КОМФОРТ', 'email'                                    => 'borisovbelgorod@mail.ru'],
            ['name' => 'АССОРТИ', 'email'                                       => 'natalya@anassorti.ru'],
            ['name' => 'ПЕРСПЕКТИВА 31', 'email'                                => 'tania31rus@yandex.ru'],
            ['name' => 'АВЕНЮ', 'email'                                         => 'avenue2014@bk.ru'],
            ['name' => 'ЭКСПРЕСС НЕДВЖИМОСТЬ', 'email'                          => 'diamanteivan@mail.ru'],
            ['name' => 'ГОРОД', 'email'                                         => 'an-gorod@inbox.ru'],
            ['name' => 'МЕТРАЖ', 'email'                                        => 'metraj31@bk.ru'],
            ['name' => 'СЕВЕРНОЕ СИЯНИЕ', 'email'                               => 'rk-shine@mail.ru'],
            ['name' => 'БОЦН', 'email'                                          => 'bosn-777@mail.ru'],
            ['name' => 'ПРЕСТИЖ', 'email'                                       => 'ndenezhko@yandex.ru'],
            ['name' => 'ВЫСОТА', 'email'                                        => 'cnvysota@mail.ru'],
            ['name' => 'ЖИЛТРЕСТ', 'email'                                      => 'info@zhiltrest10.ru'],
            ['name' => 'НЕДВИЖИМОСТЬ ДЛЯ ВАС', 'email'                          => 'belhouse@inbox.ru'],
            ['name' => 'АВАНГАРД', 'email'                                      => 'bel_avangard@mail.ru'],
            ['name' => 'СВОЙ УГОЛ', 'email'                                     => 'swoiugol@yandex.ru'],
            ['name' => 'ИМПУЛЬС', 'email'                                       => 'imp_ls@mail.ru'],
            ['name' => 'СОТА', 'email'                                          => 'belgorod@sota31.ru'],
            ['name' => 'ОКТАЙ', 'email'                                         => 'adres-31@mail.ru'],
            ['name' => 'ХОУМ ЛАЙН', 'email'                                     => 'lprisuhina@mail.ru'],
            ['name' => 'ПАЗЛ', 'email'                                          => 'bel-puzzle@mail.ru'],
            ['name' => 'София Недвижимость', 'email'                            => 's-n-888@yandex.ru'],
            ['name' => 'Академия Недвижимости', 'email'                         => 'academia.realty@yandex.ru'],
            ['name' => 'Белый Город', 'email'                                   => 'vp_3233@mail.ru'],
        ];
        $passwords = [];
        $this->output->progressStart(count($agencyes));
        foreach($agencyes as $agency){

            $pass = 'password';
            $agent = User::create([
                'firstname' => 'Иван',
                'lastname' => 'Иванов',
                'midname' => 'Иванович',
                'phone' => '88007008000',
                'email' => $agency['email'],
                'password' => Hash::make('password'),
            ]);

            $agency = Agency::create([
                'title' => $agency['name'],
                'phone' => '315405',
                'email' => $agency['email'],
                'address' => 'Ул. Сезам, дом 1',
                'owner_id' => $agent->id,
            ]);

            $agent->toAgency()->attach($agency);
            //Mail::to($agent->email)->send(new AgentCreated($agent));
            $passwords[$agent->id]['name'] = $agency->title;
            $passwords[$agent->id]['email'] = $agency['email'];
            $passwords[$agent->id]['pass'] = $pass;

            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        $srt = '';

        if (file_exists('passwords.txt')) unlink('passwords.txt');

        foreach($passwords as $pass){
            $srt .= 'Агентство ' . $pass['name'] . ' Логин ' . $pass['email'] . ' Пароль ' . $pass['pass'] . PHP_EOL;
        }
        $fp = fopen('passwords.txt', 'a');
        fwrite($fp, $srt);
        fclose($fp);

    }

}
