<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Objects\BaseObject;
use App\Models\Params\City;
use DB;

class ChangeCity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'city:fresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $objects = BaseObject::all();
        foreach($objects as $object){

            $city_old = DB::table('cities')->where('id', $object->city_id)->first();
            if($city_old){
                $city_new = City::where('name', $city_old->title)->first();
                if($city_new){
                    $object->city_id = $city_new->id;
                    echo $city_new->name . '<br>';
                    $object->save();
                }
            }
        }
    }
}
