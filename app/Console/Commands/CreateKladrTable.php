<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateKladrTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kladr:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = public_path('sql/kladr.sql');
        exec('mysql -u' .
            env('DB_USERNAME', 'homestead') . ' -p' .
            env('DB_PASSWORD', 'secret') . ' --default-character-set=utf8 ' .
            env('DB_DATABASE', 'mysql') . ' < '.$file);
    }
}
