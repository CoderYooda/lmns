<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class GarageSpecial extends Model
{
    protected $table = 'garage_special';
}
