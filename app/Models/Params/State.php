<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'state';
}
