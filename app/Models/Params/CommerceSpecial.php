<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceSpecial extends Model
{
    protected $table = 'commerce_special';
}
