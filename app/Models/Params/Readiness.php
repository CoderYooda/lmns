<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class Readiness extends Model
{
    protected $table = 'readiness';
}
