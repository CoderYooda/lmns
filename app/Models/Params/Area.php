<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
}
