<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class HouseComfort extends Model
{
    protected $table = 'house_comfort';
}
