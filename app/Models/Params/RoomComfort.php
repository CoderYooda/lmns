<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class RoomComfort extends Model
{
    protected $table = 'room_comfort';
}
