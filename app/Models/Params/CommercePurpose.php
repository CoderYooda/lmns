<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommercePurpose extends Model
{
    protected $table = 'commerce_purpose';
}
