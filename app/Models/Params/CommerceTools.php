<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceTools extends Model
{
    protected $table = 'commerce_tools';
}
