<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'kladr';

    public static function belgorod(){
        return self::where('gninmb', '>', 3100)->where('gninmb', '<', 3200)->orderBy('socr')->orderBy('status', 'DESC')->get();
    }
}
