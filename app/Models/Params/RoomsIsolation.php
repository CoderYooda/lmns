<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class RoomsIsolation extends Model
{
    protected $table = 'rooms_isolation';
}
