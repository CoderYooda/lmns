<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class HouseBuildType extends Model
{
    protected $table = 'house_build_type';
}
