<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceType extends Model
{
    protected $table = 'commerce_type';
}
