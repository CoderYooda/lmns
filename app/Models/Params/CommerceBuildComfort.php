<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceBuildComfort extends Model
{
    protected $table = 'commerce_build_comfort';
}
