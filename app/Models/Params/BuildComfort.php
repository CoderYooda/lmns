<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class BuildComfort extends Model
{
    protected $table = 'build_comfort';
}
