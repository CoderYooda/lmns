<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class GarageType extends Model
{
    protected $table = 'garage_type';
}
