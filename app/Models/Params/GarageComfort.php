<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class GarageComfort extends Model
{
    protected $table = 'garage_comfort';
}
