<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class ApartmentComfort extends Model
{
    protected $table = 'apartment_comfort';
}
