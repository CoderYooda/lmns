<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceGruz extends Model
{
    protected $table = 'commerce_gruz';
}
