<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class RoomSpecials extends Model
{
    protected $table = 'room_special';
}
