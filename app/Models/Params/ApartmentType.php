<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class ApartmentType extends Model
{
    protected $table = 'apartment_type';
}
