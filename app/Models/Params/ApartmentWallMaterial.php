<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class ApartmentWallMaterial extends Model
{
    protected $table = 'apartment_wall_material';
}
