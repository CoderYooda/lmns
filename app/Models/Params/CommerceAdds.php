<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceAdds extends Model
{
    protected $table = 'commerce_adds';
}
