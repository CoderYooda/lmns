<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceDoing extends Model
{
    protected $table = 'commerce_doing';
}
