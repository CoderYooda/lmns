<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceKind extends Model
{
    protected $table = 'commerce_kind';
}
