<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class BuildType extends Model
{
    protected $table = 'build_type';
}
