<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class CommerceSklad extends Model
{
    protected $table = 'commerce_sklad';
}
