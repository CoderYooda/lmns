<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class Heating extends Model
{
    protected $table = 'heating';
}
