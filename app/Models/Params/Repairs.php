<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class Repairs extends Model
{
    protected $table = 'repairs';
}
