<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class LandComfort extends Model
{
    protected $table = 'land_comfort';
}
