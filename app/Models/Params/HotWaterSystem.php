<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class HotWaterSystem extends Model
{
    protected $table = 'hot_water_system';
}
