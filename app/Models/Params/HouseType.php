<?php

namespace App\Models\Params;

use Illuminate\Database\Eloquent\Model;

class HouseType extends Model
{
    protected $table = 'house_type';
}
