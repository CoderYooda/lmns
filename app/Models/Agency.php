<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'agencies';
    protected $guarded = [];

    public $fields = [
        'title',
        'phone',
        'email',
        'address',
        ];

    public function object()
    {
        return $this->hasMany('App\Models\Objects\BaseObject', 'agency_id');
    }

    public function owner()
    {
        return $this->hasOne('App\Models\User', 'owner_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_to_agency', 'agency_id', 'user_id');
    }

    public function admins()
    {
        return $this->belongsTo('App\Models\User', 'owner_id');
    }

    public function agents()
    {
        return $this->belongsToMany('App\Models\User', 'user_to_agency', 'agency_id', 'user_id')->wherePivot('status', 'agent');
    }
}
