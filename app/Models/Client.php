<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';
    protected $guarded = [];


    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
