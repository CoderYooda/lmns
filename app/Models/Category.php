<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function generateTitle(){

        $type = NULL;

        if(request('rent') != NULL && request('rent') == 0){$type .= 'Продажа';}
        if(request('rent') != NULL && request('rent') == 1){$type .= 'Аренда';}
        if(request('rent') == NULL || request('rent') == 2){$type .= 'Продажа и аренда';}

        if(request('rooms_one') != NULL && request('rooms_one') == true) $type .= ' однокомнатных';
        if(request('rooms_two') != NULL && request('rooms_two') == true) $type .= ' двухкомнатных';
        if(request('rooms_three') != NULL && request('rooms_three') == true) $type .= ' трехкомнатных';
        if(request('rooms_four') != NULL && request('rooms_four') == true) $type .= ' четырехкомнатных';

        switch ($this->id) {
            case 1:$type .= " квартир";break;
            case 2:$type .= " комнат";break;
            case 3:$type .= " коттеджей";break;
            case 4:$type .= " земельных участков";break;
            case 5:$type .= " гаражей";break;
            case 6:$type .= " коммерческой недвижимости";break;
        }
        $type .= ' в Белгороде и Белгородской области.';

        return $type;
    }


    public function generateDescription(){

        $type = NULL;

        if(request('rent') != NULL && request('rent') == 0){$type .= 'Продажа';}
        if(request('rent') != NULL && request('rent') == 1){$type .= 'Аренда';}
        if(request('rent') == NULL || request('rent') == 2){$type .= 'Продажа и аренда';}

        if(request('rooms_one') != NULL && request('rooms_one') == true) $type .= ' однокомнатных';
        if(request('rooms_two') != NULL && request('rooms_two') == true) $type .= ' двухкомнатных';
        if(request('rooms_three') != NULL && request('rooms_three') == true) $type .= ' трехкомнатных';
        if(request('rooms_four') != NULL && request('rooms_four') == true) $type .= ' четырехкомнатных';

        switch ($this->id) {
            case 1:$type .= " квартир";break;
            case 2:$type .= " комнат";break;
            case 3:$type .= " коттеджей";break;
            case 4:$type .= " земельных участков";break;
            case 5:$type .= " гаражей";break;
            case 6:$type .= " коммерческой недвижимости";break;
        }

        if(request('square_from') != NULL) $type .= ' от ' . request('square_from') . ' м2';
        if(request('square_to') != NULL) $type .= ' до ' . request('square_to') . ' м2';

        if(request('square_sot_from') != NULL) $type .= ' от ' . request('square_sot_from') . ' соток';
        if(request('square_sot_to') != NULL) $type .= ' до ' . request('square_sot_to') . ' соток';


        $type .= ' в Белгороде и Белгородской области. Cвежие и актуальные объявления от собственника.';

        $type .= ' Ассоциация агентств недвижимости Недвижка 31';

        return $type;
    }
}
