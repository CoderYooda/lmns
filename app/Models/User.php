<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;
//    use EntrustUserTrait;

    public $fields = [
        'firstname',
        'lastname',
        'midname',
        'phone',
        'email',
    ];

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','midname','lastname', 'phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function activities(){
        return $this->hasMany('App\Models\Activity', 'actor')->orderBy('created_at', 'DESC');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function ownedAgency()
    {
        return $this->hasOne('App\Models\Agency', 'owner_id');
    }

    public function objects()
    {
        return $this->hasMany('App\Models\Objects\BaseObject', 'created_id');
    }

    public function toAgency()
    {
        return $this->belongsToMany('App\Models\Agency', 'user_to_agency');
    }

    public function hasAgency()
    {
        return $this->ownedAgency()->first() != null;
    }

    public function clients()
    {
        return $this->hasMany('App\Models\Client', 'to_agent');
    }

    public function Avatar()
    {
        return $this->BelongsTo('App\Models\Stock\Images', 'avatar');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Stock\Images', 'uploader_id');
    }

}
