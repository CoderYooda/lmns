<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class Commerce extends Model
{
    protected $table = 'commerce';
    protected $guarded = [];
    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'floor',
        'n_of_floor',
        'commerce_type_id',
        'build_type_id',
        'readiness_id',
        'year',
        'total_square',
        'sector_square',
        'doing_id',
        'purpose_id',
        'kind_id',
        'commerce_type_id',
        'sklad_id',
        'gruz_id',
        'rooms',
        'height',
        'state_id',
    ];

    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'object_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');//?
    }

    public function wall_materials()
    {
        return $this->belongsToMany('App\Models\Params\WallMaterial', 'wall_materials_to_commerce', 'commerce_id', 'wall_materials_id');
    }

    public function build_comforts()
    {
        return $this->belongsToMany('App\Models\Params\CommerceBuildComfort', 'build_comfort_to_commerce', 'commerce_id', 'build_comfort_id');
    }

    public function tools()
    {
        return $this->belongsToMany('App\Models\Params\CommerceTools', 'commerce_tools_to_commerce', 'commerce_id', 'commerce_tool_id');
    }

    public function specials()
    {
        return $this->belongsToMany('App\Models\Params\CommerceSpecial', 'commerce_special_to_commerce', 'commerce_id', 'commerce_special_id');
    }

    public function adds()
    {
        return $this->belongsToMany('App\Models\Params\CommerceAdds', 'commerce_adds_to_commerce', 'commerce_id', 'commerce_adds_id');
    }



    public function doing(){return $this->BelongsTo('App\Models\Params\CommerceDoing');}
    public function gruz(){return $this->BelongsTo('App\Models\Params\CommerceGruz');}
    public function kind(){return $this->BelongsTo('App\Models\Params\CommerceKind');}
    public function purpose(){return $this->BelongsTo('App\Models\Params\CommercePurpose');}
    public function sklad(){return $this->BelongsTo('App\Models\Params\CommerceSklad');}
    public function type(){return $this->BelongsTo('App\Models\Params\CommerceType', 'commerce_type_id');}
    public function state(){return $this->BelongsTo('App\Models\Params\State');}

    public function wmString(){

        $array = $this->wall_materials()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
}
