<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    protected $table = 'land';
    protected $guarded = [];

    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'land_square',
        'state_id',
        'height',
        'width',
        'length',
    ];
    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }


    public function comforts()
    {
        return $this->belongsToMany('App\Models\Params\LandComfort', 'land_comfort_to_land', 'land_id', 'land_comfort_id');
    }

    public function gk(){return $this->hasOne('App\Models\Params\Gk');}
}
