<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class Garage extends Model
{
    protected $table = 'garage';
    protected $guarded = [];

    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'floor',
        'n_of_floor',
        'gk_id',
        'sgk_id',
        'garage_type_id',
        'year',
        'total_square',
        'sector_square',
        'state_id',
        'height',
        'width',
        'length',
    ];

    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'object_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }

    public function wall_materials()
    {
        return $this->belongsToMany('App\Models\Params\WallMaterial', 'wall_materials_to_garage', 'garage_id', 'wall_materials_id');
    }

    public function comforts()
    {
        return $this->belongsToMany('App\Models\Params\GarageComfort', 'garage_comfort_to_garage', 'garage_id', 'garage_comfort_id');
    }

    public function specials()
    {
        return $this->belongsToMany('App\Models\Params\GarageSpecial', 'garage_special_to_garage', 'garage_id', 'garage_special_id');
    }

    public function gk(){return $this->BelongsTo('App\Models\Params\Gk', 'id');}
    public function garage_type(){return $this->BelongsTo('App\Models\Params\GarageType', 'id');}
    public function state(){return $this->BelongsTo('App\Models\Params\State', 'id');}

    public function wmString(){

        $array = $this->wall_materials()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }

}
