<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class BaseObject extends Model
{
    protected $table = 'objects';
    protected $guarded = [];
    public $fields = [
        'title',
        'category_id',
        'published',
        'exlusive',
        'writed',
        'rented',
        'exlusive_date',
        'part',
        'agency_id',
        'created_id',
        'delited_id',
        'city_id',
        'area_id',
        'street',
        'price',
        'price_m',
        'rent_price',
        'notary',
        'custody',
        'burden',
        'exclusive',
        'geopoint',
        'description',
        'mls',
        'export',
    ];


    public function isAutor($user)
    {
        return $this->belongsTo('App\Models\User', 'created_id')->first()->id == $user->id;
    }

    public function isAdminOfObject($user)
    {
        return $this->agency()->first()->owner_id == $user->id;
    }

    public function getAfiCategory(){
        switch ($this->category()->first()->type) {
            case 'apartment':
                if($this->rented){
                    return 208;
                } else{
                    return 175;
                }
            case 'room':
                if($this->rented){
                    return 234;
                } else{
                    return 189;
                }
            case 'house':
                if($this->rented){
                    return 50;
                } else{
                    return 102;
                }
            case 'land':
                if($this->rented){
                    return 200;
                } else{
                    return 31;
                }
            case 'garage':
                if($this->rented){
                    return 203;
                } else{
                    return 202;
                }
            case 'commerce':
                if($this->commerce()->first()->type()->first() != NULL) {
                    $slug = $this->commerce()->first()->type()->first()->slug;
                } else{
                    $slug = 'svobodnogo-naznacheniya';
                }
                if($this->rented){
                    if($slug == 'torgovoe-pomeshchenie' || $slug == 'torgovyj-tsentr'){
                        return 196;
                    }
                    elseif($slug == 'skladskoe-pomeshchenie'){
                        return 198;
                    }
                    elseif($slug == 'proizvodstvennoe-pomeshchenie'){
                        return 256;
                    }
                    elseif($slug == 'svobodnogo-naznacheniya'){
                        return 254;
                    }
                    elseif($slug == 'ofisnoe-pomeshchenie'){
                        return 180;
                    }
                } else{
                    if($slug == 'torgovoe-pomeshchenie' || $slug == 'torgovyj-tsentr'){
                        return 197;
                    }
                    elseif($slug == 'skladskoe-pomeshchenie'){
                        return 199;
                    }
                    elseif($slug == 'proizvodstvennoe-pomeshchenie'){
                        return 255;
                    }
                    elseif($slug == 'svobodnogo-naznacheniya'){
                        return 253;
                    }
                    elseif($slug == 'ofisnoe-pomeshchenie'){
                        return 191;
                    }
                }
        }
    }

    public function generateDescription(){
        $type = null;
        $text = null;
        $square = null;
        $cost = null;
        $address = ' Белгородская область';

        if(!$this->rented){
            $type = 'Объявление о продаже';
            $cost = ', стоимостью ' . $this->price . ' рублей';

        } else {
            $type = 'Объявление об аренде';
            $cost = ', стоимостью ' . $this->rent_price . ' рублей в месяц';

        }

        $cat = $this->category()->first()->type;

        if($cat == 'apartment'){
            $text = ' квартиры';
            if((int)$this->{$this->category()->first()->type}->rooms == 1){ $text = ' однокомнатной квартиры';}
            if((int)$this->{$this->category()->first()->type}->rooms == 2){ $text = ' двухкомнатной квартиры';}
            if((int)$this->{$this->category()->first()->type}->rooms == 3){ $text = ' трехкомнатной квартиры';}
            if((int)$this->{$this->category()->first()->type}->rooms == 4){ $text = ' четырехкомнатной квартиры';}
            if((int)$this->{$this->category()->first()->type}->rooms == 5){ $text = ' пятикомнатной квартиры';}


        }elseif($cat == 'room'){
            $text = ' комнаты';
        }elseif($cat == 'house'){
            $text = ' дома';
        }elseif($cat == 'land'){
            $text = ' участка';
        }elseif($cat == 'garage'){
            $text = ' гаража';
        }elseif($cat == 'commerce'){
            $text = ' комерческой недвижимости';
        }


        $square = ' площадью ' . (double)$this->{$this->category->type}->total_square . ' квадратных метров';

        if($this->city()->first() != null){
            $address .= ', ' . $this->city()->first()->name;
        }
        if($this->area()->first() != null){
            $address .= ', ' . ucfirst($this->area()->first()->title);
        }

        if($this->street != null){
            $address .= ', ' . mb_convert_case($this->street, MB_CASE_TITLE, "UTF-8") . ' ' . $this->{$this->category->type}->house_number;
        }


        return $type . $text . $square . $cost . $address . ' Актуальные объявления недвижимости Недвижка 31';
    }

    public function autor()
    {
        return $this->belongsTo('App\Models\User', 'created_id');
    }

    public function deliter()
    {
        return $this->belongsTo('App\Models\User', 'delited_id');
    }

    public function agency()
    {
        return $this->belongsTo('App\Models\Agency', 'agency_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Stock\Images', 'images_to_object', 'object_id', 'image_id')->orderBy('rank');
    }

    public function city()
    {
        return $this->BelongsTo('App\Models\Params\City', 'city_id');
    }

    public function area()
    {
        return $this->BelongsTo('App\Models\Params\Area', 'area_id');
    }

    public function apartment()
    {
        return $this->hasOne('App\Models\Objects\Apartment', 'object_id');
    }

    public function room()
    {
        return $this->hasOne('App\Models\Objects\Room', 'object_id');
    }

    public function garage()
    {
        return $this->hasOne('App\Models\Objects\Garage', 'object_id');
    }

    public function commerce()
    {
        return $this->hasOne('App\Models\Objects\Commerce', 'object_id');
    }

    public function house()
    {
        return $this->hasOne('App\Models\Objects\House', 'object_id');
    }

    public function land()
    {
        return $this->hasOne('App\Models\Objects\Land', 'object_id');
    }

}
