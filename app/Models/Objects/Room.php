<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $guarded = [];
    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'floor',
        'n_of_floor',
        'build_type_id',
        'year',
        'hot_water_system_id',
        'room_type_id',
        'rooms',
        'total_square',
        'room_square',
        'count_people',
        'kitchen_square',
        'state_id',
    ];
    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'object_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }

    public function wall_materials()
    {
        return $this->belongsToMany('App\Models\Params\WallMaterial', 'wall_materials_to_room', 'room_id', 'wall_materials_id');
    }

    public function comforts()
    {
        return $this->belongsToMany('App\Models\Params\RoomComfort', 'room_comfort_to_room', 'room_id', 'room_comfort_id');
    }

    public function specials()
    {
        return $this->belongsToMany('App\Models\Params\RoomSpecials', 'room_special_to_room', 'room_id', 'room_special_id');
    }

    public function build_comforts()
    {
        return $this->belongsToMany('App\Models\Params\BuildComfort', 'build_comfort_to_room', 'room_id', 'build_comfort_id');
    }

    public function gk(){return $this->BelongsTo('App\Models\Params\Gk');}
    public function build_type(){return $this->BelongsTo('App\Models\Params\BuildType');}
    public function room_type(){return $this->BelongsTo('App\Models\Params\RoomType');}
    //public function readiness(){return $this->BelongsTo('App\Models\Params\Readiness');}
    public function hot_water_system(){return $this->BelongsTo('App\Models\Params\HotWaterSystem');}
    public function apartment_type(){return $this->BelongsTo('App\Models\Params\ApartmentType');}
    public function state(){return $this->BelongsTo('App\Models\Params\State');}
    public function rooms_isoaltion(){return $this->BelongsTo('App\Models\Params\RoomsIsolation');}
    public function wc(){return $this->BelongsTo('App\Models\Params\Wc');}
    public function wmString(){

        $array = $this->wall_materials()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
}
