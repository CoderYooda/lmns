<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $table = 'house';
    protected $guarded = [];
    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'n_of_floor',
        'build_type_id',
        'readiness_id',
        'year',
        'qr',
        'heating_id',
        'hot_water_system_id',
        'apartment_type_id',
        'rooms',
        'total_square',
        'live_square',
        'land_square',
        'kitchen_square',
        'state_id',
        'rooms_isolation_id',
        'wc_id',
        'wc_num',
        'balconies',
    ];

    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }

    public function wall_materials()
    {
        return $this->belongsToMany('App\Models\Params\WallMaterial', 'wall_materials_to_house', 'house_id', 'wall_materials_id');
    }

    public function comforts()
    {
        return $this->belongsToMany('App\Models\Params\HouseComfort', 'house_comfort_to_house', 'house_id', 'house_comfort_id');
    }

    public function repairs()
    {
        return $this->belongsToMany('App\Models\Params\Repairs', 'repairs_to_house', 'house_id', 'repairs_id');
    }

    public function build_comforts()
    {
        return $this->belongsToMany('App\Models\Params\BuildComfort', 'build_comfort_to_house', 'house_id', 'build_comfort_id');
    }

    public function layouts()
    {
        return $this->belongsToMany('App\Models\Params\Layout', 'layouts_to_house', 'house_id', 'layout_id');
    }

    public function heatings()
    {
        return $this->belongsToMany('App\Models\Params\Heating', 'heating_to_house', 'house_id', 'heating_id');
    }

    public function build_type(){return $this->BelongsTo('App\Models\Params\HouseBuildType');}
    public function hot_water_system(){return $this->BelongsTo('App\Models\Params\HotWaterSystem');}
    public function house_type(){return $this->BelongsTo('App\Models\Params\HouseType');}
    public function state(){return $this->BelongsTo('App\Models\Params\State');}
    public function rooms_isolation(){return $this->BelongsTo('App\Models\Params\RoomsIsolation');}
    public function wc(){return $this->BelongsTo('App\Models\Params\Wc');}

    public function wmString(){

        $array = $this->wall_materials()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
    public function heatingString(){

        $array = $this->heatings()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
}
