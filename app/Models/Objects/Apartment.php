<?php

namespace App\Models\Objects;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    protected $table = 'apartment';
    protected $guarded = [];
    public $fields = [
        'category_id',
        'object_id',
        'house_number',
        'floor',
        'n_of_floor',
        'gk_id',
        'build_type_id',
        'readiness_id',
        'year',
        'qr',
        'heating_id',
        'hot_water_system_id',
        'apartment_type_id',
        'rooms',
        'total_square',
        'live_square',
        'kitchen_square',
        'state_id',
        'rooms_isolation_id',
        'wc_id',
        'wc_num',
        'balconies',
    ];

    public function object()
    {
        return $this->hasOne('App\Models\Objects\BaseObject', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id');
    }

    public function wall_materials()
    {
        return $this->belongsToMany('App\Models\Params\ApartmentWallMaterial', 'wall_materials_to_apartment', 'apartment_id', 'wall_materials_id');
    }

//    public function comforts()
//    {
//        return $this->belongsToMany('App\Models\Params\ApartmentComfort', 'apartment_comfort_to_apartment', 'apartment_id', 'apartment_comfort_id');
//    }

    public function repairs()
    {
        return $this->belongsToMany('App\Models\Params\Repairs', 'repairs_to_apartment', 'apartment_id', 'repairs_id');
    }

    public function layouts()
    {
        return $this->belongsToMany('App\Models\Params\Layout', 'layouts_to_apartment', 'apartment_id', 'layout_id');
    }

    public function heatings()
    {
        return $this->belongsToMany('App\Models\Params\Heating', 'heating_to_apartment', 'apartment_id', 'heating_id');
    }

    public function gk(){return $this->BelongsTo('App\Models\Params\Gk');}
    public function build_type(){return $this->BelongsTo('App\Models\Params\BuildType');}
    public function readiness(){return $this->BelongsTo('App\Models\Params\Readiness');}
    public function hot_water_system(){return $this->BelongsTo('App\Models\Params\HotWaterSystem');}
    public function apartment_type(){return $this->BelongsTo('App\Models\Params\ApartmentType');}
    public function state(){return $this->BelongsTo('App\Models\Params\State');}
    public function rooms_isolation(){return $this->BelongsTo('App\Models\Params\RoomsIsolation');}
    public function wc(){return $this->BelongsTo('App\Models\Params\Wc');}

    public function wmString(){

        $array = $this->wall_materials()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
    public function heatingString(){

        $array = $this->heatings()->get();
        $string = '';

        foreach($array as $wm){
            $string .= $wm->title . ',';
        }

        $string = substr($string, 0, -1);

        return $string;
    }
}
