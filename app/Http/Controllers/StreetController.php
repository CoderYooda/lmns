<?php

namespace App\Http\Controllers;

use App\Models\Params\City;
use App\Models\Params\Street;
use http\Env\Response;
use Illuminate\Http\Request;

class StreetController extends Controller
{
    public function getStreetsByCityId(Request $request){
        $city = City::where('id', $request['id'])->first();
        $streets = Street::where('ocatd', $city->ocatd)->get();
        $options = '';
        foreach ($streets as $street){
            if($request['current'] === $street->socr . ' ' . $street->name){
                $state = 'selected';
            } else {
                $state = '';
            }
            $options .= '<option value="'. $street->socr . ' ' . $street->name .'" ' . $state . ' >' . $street->socr . ' ' . $street->name . '</option>';
        }
        return response()->json([
            'data' => $options
        ]);
    }
}
