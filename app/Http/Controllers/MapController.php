<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use stdClass;

class MapController extends Controller
{


    public function loadObjects(Request $request)
    {

        $map_objects = BaseObject::select('id', 'geopoint', 'title', 'category_id', 'rented', 'price', 'rent_price')->with('category','images')
            ->where('category_id' , $request['category'])
            ->where(function($q) use ($request){
                if($request['type'] != NULL && $request['type'] != 2){

                    if ($request['type'] != NULL && $request['type'] == 0) {
                        $q->where('rented', 0);
                    }
                    if ($request['type'] != NULL && $request['type']) {
                        $q->where('rented', 1);
                    }
                }
            })
            ->get();
        if(count($map_objects) < 1){
            return response()->json(['status' => 'error'], 404);
        }

        $objects = collect();

        foreach($map_objects as $obj){
            $object = new stdClass();
            $object->point = explode(',', $obj->geopoint);
            $object->id = $obj->id;
            $object->link = route($obj->category->type, $obj->id);
            if($obj->images->first() == NULL){
                $object->thumb = asset('img/noimage.jpg');
            } else {
                $object->thumb = $obj->images->first()->thumb_url;
            }
            $object->title = $obj->title;
            if($obj->rented){
                $object->price = number_format($obj->rent_price, 0, '', ' '). ' ₽ / мес.';
            } else {
                $object->price = number_format($obj->price, 0, '', ' '). ' ₽';
            }
            $objects->push($object);
        }
        return response()->json(['status' => 'success', 'objects' => $objects]);
    }
}
