<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Objects\Apartment;


use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use App\Models\Agency;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        $obj = BaseObject::with('autor', 'deliter', 'agency', 'category', 'city', 'area')->first();
        dd($obj);
    }
    public function index2(Request $request)
    {

        DB::table('user_to_agency')->update(['status' => 'agent']);

        $agencies = Agency::all();
        foreach($agencies as $agency){
            $user = User::where('id', $agency->owner_id)->first();
            $relation = DB::table('user_to_agency')->where('user_id', $user->id)->where('agency_id', $agency->id);
            $relation->update(['status' => 'manager']);

        }
    }
}
