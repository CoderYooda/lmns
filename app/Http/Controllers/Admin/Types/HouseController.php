<?php

namespace App\Http\Controllers\Admin\Types;

use App\Models\Objects\House;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HouseController extends Controller
{
    public static function getForm()
    {
        return view('admin.object.type.house');
    }

    public static function store($request)
    {
        $house = House::firstOrNew([
            'object_id' => $request['object_id'],
        ]);

        $house->fill($request->only($house->fields));

        $house->save();

        $house->wall_materials()->sync($request['wall_materials']);

        $house->comforts()->sync($request['comforts']);

        $house->repairs()->sync($request['repairs']);

        $house->build_comforts()->sync($request['build_comforts']);

        $house->layouts()->sync($request['layouts']);

        $house->heatings()->sync($request['heatings']);

        return $house;
    }
}
