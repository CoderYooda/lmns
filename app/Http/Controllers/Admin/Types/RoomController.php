<?php

namespace App\Http\Controllers\Admin\Types;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Objects\Room;

class RoomController extends Controller
{
    public static function getForm()
    {
        // Форма создания агентства
        return view('admin.object.type.apartment');
    }

    public static function store($request)
    {

        //dd($request);

        $room = Room::firstOrNew([
            'object_id' => $request['object_id'],
        ]);

        $room->fill($request->only($room->fields));

        $room->save();

        $room->wall_materials()->sync($request['wall_materials']);

        $room->comforts()->sync($request['comforts']);

        $room->build_comforts()->sync($request['build_comforts']);

        $room->specials()->sync($request['specials']);

        return $room;
    }
}
