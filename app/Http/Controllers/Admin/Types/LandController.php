<?php

namespace App\Http\Controllers\Admin\Types;

use App\Models\Objects\Land;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandController extends Controller
{
    public static function getForm()
    {
        return view('admin.object.type.land');
    }

    public static function store($request)
    {
        $land = Land::firstOrNew([
            'object_id' => $request['object_id'],
        ]);

        $land->fill($request->only($land->fields));

        $land->save();

        $land->comforts()->sync($request['comforts']);

        return $land;
    }
}
