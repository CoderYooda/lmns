<?php

namespace App\Http\Controllers\Admin\Types;

use App\Models\Objects\Garage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GarageController extends Controller
{
    public static function getForm()
    {
        // Форма создания агентства
        return view('admin.object.type.garage');
    }

    public static function store($request)
    {

        $garage = Garage::firstOrNew([
            'object_id' => $request['object_id'],
        ]);

        $garage->fill($request->only($garage->fields));

        $garage->save();

        $garage->wall_materials()->sync($request['wall_materials']);

        $garage->comforts()->sync($request['comforts']);

        $garage->specials()->sync($request['specials']);

        return $garage;
    }

}
