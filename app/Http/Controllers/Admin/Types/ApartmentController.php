<?php

namespace App\Http\Controllers\Admin\Types;

use Illuminate\Http\Request;
use App\Models\Objects\Apartment;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ApartmentController extends Controller
{
    public static function getForm()
    {
        // Форма создания агентства
        return view('admin.object.type.apartment');
    }

    public static function store($request)
    {
        $apartment = Apartment::firstOrNew([
            'object_id' => $request['object_id'],
        ]);

        $apartment->fill($request->only($apartment->fields));

        $apartment->save();

        $apartment->wall_materials()->sync($request['wall_materials']);

        $apartment->repairs()->sync($request['repairs']);

        $apartment->layouts()->sync($request['layouts']);

        $apartment->heatings()->sync($request['heatings']);

        return $apartment;
    }

}
