<?php

namespace App\Http\Controllers\Admin\Types;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Objects\Commerce;

class CommerceController extends Controller
{
    public static function getForm()
    {
        // Форма создания агентства
        return view('admin.object.type.apartment');
    }

    public static function store($request)
    {
        $commerce = Commerce::firstOrNew([
            'object_id' => $request['object_id'],
        ]);
        $commerce->fill($request->only($commerce->fields));

        $commerce->save();


        $commerce->wall_materials()->sync($request['wall_materials']);

        $commerce->build_comforts()->sync($request['build_comforts']);

        $commerce->tools()->sync($request['tools']);

        $commerce->specials()->sync($request['specials']);

        $commerce->adds()->sync($request['adds']);

        return $commerce;
    }
}
