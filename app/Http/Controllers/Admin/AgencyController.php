<?php

namespace App\Http\Controllers\Admin;

use App\Mail\AgentCreated;
use App\Models\Agency;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Objects\BaseObject;
use App\Models\Category;
use App\Models\Params\Area;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Admin\ObjectController;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Admin\ExportController;

class AgencyController extends Controller
{
    public function index(Request $request)
    {

        if($request['type'] == null){
            $request['type'] = 'selded';
        }


        $agency = Auth::user()->ownedAgency()->with('users')->firstOrFail();

        $agency->objects_count = self::objectsCount($agency->id);
        $agency->agents_count = self::agentsCount($agency->id);
        $category = Category::where('id', $request['category'])->first();
        if ($agency == null) abort(404);

        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];
        $objects = BaseObject::where('agency_id', $agency->id)
            ->where(function($query){
                ObjectController::objectFilter($query);
            })
            ->where(function($query){
                if(request('agent') != null)
                $query->where('created_id', request('agent'));
            })
            ->where(function($q) use($request, $category){

                //self::rentSorting($request, $q);

                //self::citySorting($request, $q);

                if($request['category'] != NULL){//Квартиры

                    ObjectController::metaSorting($request, $q, $category->type);
                }
            })
            ->with($relations)
            ->orderBy('updated_at', 'DESC')
            ->paginate(15);
        $categories = Category::all();
        $areas = Area::all();

        $filter_params = ObjectController::collectFilterParams($agency, NULL, NULL);

        if(request()->ajax()){
            return response()->json([
                'status' => 'success',
                'destination' => '#filtered_objects',
                'mess' => false,
                'params' => $filter_params,
                'data' => view('template.admin.common.agency.objects.collection', compact('objects'))->render()], 200);
        }

        return view('template.admin.common.agency.index', compact('objects','categories', 'areas', 'agency', 'filter_params'));
    }



    public function exportPage()
    {
        $agency = Auth::user()->ownedAgency()->with('users')->firstOrFail();

        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];

        $agents = $agency->users()->get();

        $objects = BaseObject::where('agency_id', $agency->id)
            ->where(function($query){
                ObjectController::objectFilter($query);
            })
            ->where(function($query){
                if(request('agent') != null)
                    $query->where('created_id', request('agent'));
            })
            ->with($relations)
            ->orderBy('updated_at', 'DESC')
            ->get();

        return view('template.admin.common.agency.export.index', compact('objects', 'agents'));
    }

    public function createUser(Request $request){

        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'midname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|max:255|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => 'Ошибка при создании', $validator->errors()], 200);
        }


        $agency = Auth::user()->ownedAgency()->with('users')->first();

        if($agency == null){abort(403);}
        $pass = request('password');
        $agent = User::create([
            'firstname' => request('firstname'),
            'lastname' => request('lastname'),
            'midname' => request('midname'),
            'phone' => request('phone'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);

        $agent->rawpass = $pass;
        $agent->status = 'agent';
        Mail::to($agent->email)->send(new AgentCreated($agent));

        $agency->users()->attach([$agent->id => ['status' => 'agent']]);

        return response()->json(['status' => 'success', 'message' => 'Агент создан, логин и пароль отправлен агенту на почту'], 200);
    }

    public function removeUser(Request $request){
        $agent = User::where('id', $request['agent_id'])->first();
        $images = $agent->images()->where('type', NULL)->get();
        foreach($images as $image){
            $image->uploader_id = Auth::user()->id;
            $image->save();
        }

        foreach($agent->objects()->get() as $object){
            $object->created_id = Auth::user()->id;
            $object->save();
        }

        $agent->delete();
//        Artisan::call('cache:clear');
        return response()->json(['status' => 'success', 'agent' => $agent,  'message' => 'Агент удалён, все текущие объекты переданы Вам.'], 200);
    }

    public function getForm(){
        // Форма создания агентства
    }

    public function saveObject(){
        // сохранение агентства
    }

    public function remove(){
        // удаление агентства
    }

    public static function objectsCount($agency_id){
        return $objects = Agency::where('id', $agency_id)->first()->object()->count();
    }

    public static function agentsCount($agency_id){
        return $objects = Agency::where('id', $agency_id)->first()->users()->count();
    }

    public function settings(){
        $agency = Auth::user()->ownedAgency()->with('users')->first();
        return view('template.admin.common.agency.settings.index', compact('agency'));
    }

    public function saveSettings(Request $request){
        $agency = Auth::user()->ownedAgency()->with('users')->first();
        if($agency->id == $request['agency_id']){

            $agency->fill($request->only($agency->fields));
            $agency->save();

            return response()->json(['status' => 'success', 'message' => 'Настройки сохранены'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Попытка взлома'], 419);
        }

    }


}
