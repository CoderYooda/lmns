<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;

class ClientController extends Controller
{
    public static function getClientListByUser($user){

        $categories = Category::all();
        return view('template.admin.common.client.index', compact('user', 'categories'))->render();
    }
}
