<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use XMLWriter;
use App\Models\Objects\BaseObject;
use Auth;

class ExportController extends Controller
{
    public function exportGenerate(){

        return view('template.admin.common.agency.export.export');
    }

    public function generateExportFile(Request $request){

        $agency = Auth::user()->ownedAgency()->with('users')->firstOrFail();

        if($agency->id != $request['agency_id']){
            return response()->json(['status' => 'error', 'message' => 'У вас нет прав на выполнение этого действия']);
        }

        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];

        $objects = BaseObject::where('agency_id', $agency->id)
            ->where(function($query){
                ObjectController::objectFilter($query);
            })
            ->where(function($query){
                if(request('agent') != null)
                    $query->where('created_id', request('agent'));
            })
            ->where('export', true)
            ->with($relations)
            ->orderBy('updated_at', 'DESC')
            ->get();


        //$objects = BaseObject::all();

        $path = 'xml/'.$agency->id;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $method = $request['service'] . 'Export';



        if(method_exists(get_class(), $method)){
            $path = self::$method($path, $objects);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Метод ' . $method . ' не найден']);
        }

        if($path === false){
            return response()->json(['status' => 'error', 'message' => 'Внутренняя ошибка сервера 2930']);
        }

        return response()->json(['status' => 'success', 'message' => 'Файл сгенерирован успешно', 'url' => asset($path)]);
    }

    public static function afiExport($path, $objects){

        try
        {
            $xml = new XMLWriter();
            $xml->openURI(public_path($path.'/afi.xml'));
            $xml->startDocument('1.0');
            $xml->startElement('objects');
            foreach($objects as $object) {
                $xml->startElement('object');
                $xml->writeElement('original_id', $object->id);
                $xml->writeElement('original_url', route($object->category->type, $object->id));
                $xml->writeElement('time_add', strtotime($object->created_at));
                $xml->startElement('contact');
                $xml->writeElement('name', $object->autor->firstname);
                $xml->writeElement('telephone', $object->autor->phone);
                $xml->writeElement('email', $object->autor->email);
                $xml->writeElement('category', $object->getAfiCategory());
                $xml->writeElement('theme', strip_tags($object->title));
                $xml->writeElement('description', strip_tags($object->description));
                $xml->startElement('price');
                $xml->writeElement('currency', 'RUB');
                if($object->rented){
                    $xml->writeElement('value', $object->rent_price);
                    $xml->writeElement('unit', 'total');
                    $xml->writeElement('period', 'month');
                } else {
                    $xml->writeElement('value', $object->price);
                    $xml->writeElement('unit', 'total');
                }
                $xml->endElement();
                $xml->startElement('location');
                $xml->writeElement('country', 'Россия');
                $xml->writeElement('oblast', 'Белгородская область');
                $xml->writeElement('city', $object->city()->first()->title);
                $xml->writeElement('street', mb_convert_case($object->street, MB_CASE_TITLE, "UTF-8"));
                $house_num = $object->{$object->category->type}->house_number;
                if($house_num != NULL){
                    $xml->writeElement('house', $house_num);
                } else {
                    $xml->writeElement('house', '1');
                }
                if($object->geopoint != NULL){
                    $xml->writeElement('latitude', explode(',' , $object->geopoint)[0]);
                    $xml->writeElement('longitude', explode(',' , $object->geopoint)[1]);
                }

                $xml->endElement();
                $xml->startElement('square');
                if($object->category->type == 'land'){
                    $xml->writeElement('total', $object->{$object->category->type}->land_square);
                    $xml->writeElement('unit_sq', 'hundred_sq_m');
                } else {
                    $xml->writeElement('total', $object->{$object->category->type}->total_square);
                    $xml->writeElement('unit_sq', 'sq_m');
                }
                if($object->{$object->category->type}->live_square != NULL) {
                    $xml->writeElement('living', $object->{$object->category->type}->live_square);
                }
                if($object->{$object->category->type}->kitchen_square != NULL) {
                    $xml->writeElement('kitchen', $object->{$object->category->type}->kitchen_square);
                }
                if($object->{$object->category->type}->land_square != NULL) {
                    $xml->writeElement('land', $object->{$object->category->type}->land_square);
                }
                $xml->endElement();
                $xml->startElement('parameters');
                $xml->writeElement('story', $object->{$object->category->type}->floor);
                $xml->writeElement('story_count', $object->{$object->category->type}->n_of_floor);
                $xml->writeElement('rooms', (int)$object->{$object->category->type}->rooms);
                if($object->category->type == 'room'){
                    $xml->writeElement('rooms_sell', (int)$object->{$object->category->type}->rooms);
                }
                if($object->{$object->category->type}->balconies != NULL){
                    if($object->{$object->category->type}->balconies != 0) {
                        $xml->writeElement('balcony', 2);
                    }else{
                        $xml->writeElement('balcony', 1);
                    }
                } else{
                    $xml->writeElement('balcony', 1);
                }

                if($object->{$object->category->type}->balconies != NULL){
                    $xml->writeElement('repairs', 1);
                }
                if(method_exists($object->{$object->category->type}, 'state')){
                    $meta = $object->{$object->category->type}->state()->first();
                    if($meta != NULL){
                        $id = $meta->id;
                        if($id == 7 || $id == 5){
                            $xml->writeElement('repairs', 1);//Без ремонта
                        }
                        if($id == 7 || $id == 5){
                            $xml->writeElement('repairs', 2);//Косметический
                        }
                        if($id == 7 || $id == 5){
                            $xml->writeElement('repairs', 3);//Капитальный
                        }
                        if($id == 7 || $id == 5){
                            $xml->writeElement('repairs', 4);//Евроремонт
                        }
                    }
                }

                $xml->startElement('images');
                foreach($object->images()->get() as $image){
                    $xml->writeElement('image', asset($image->url));
                }
                $xml->endElement();

                $xml->endElement();
                $xml->endElement();
                $xml->endElement();
            }
            $xml->endElement();
            $xml->endDocument();

            $xml->flush();
        }
        catch(Exception $e)
        {
            return 0;
        }

        return $path.'/afi.xml';
    }

    public static function cianExport($path, $objects){

        try
        {
            $xml = new XMLWriter();
            $xml->openURI(public_path($path.'/cian.xml'));


            $xml->startDocument('1.0');
            $xml->startElement('feed');
                $xml->writeElement('feed_version', 2);
                foreach($objects as $object) {
                    $xml->startElement('object');

                    $type = NULL;
                    $category = NULL;

                    if ($object->rented) {
                        $type = 'Rent';
                    } else {
                        $type = 'Sale';
                    }

                    if ($object->category->type === 'apartment') {
                        $category = 'flat';
                    }
                    if ($object->category->type === 'room') {
                        $category = 'room';
                    }
                    if ($object->category->type === 'house') {
                        $category = 'house';
                    }
                    if ($object->category->type === 'land') {
                        $category = 'land';
                    }
                    if ($object->category->type === 'garage') {
                        $category = 'garage';
                    }
                    if ($object->category->type === 'commerce') {
                        if ($object->{$object->category->type}->type()->first() != null) {
                            $slug = $object->{$object->category->type}->type()->first()->slug;
                            if ($slug === 'torgovyj-tsentr') {
                                $category = 'shoppingArea';
                            }
                            if ($slug === 'ofisnoe-pomeshchenie') {
                                $category = 'office';
                            }
                            if ($slug === 'torgovoe-pomeshchenie') {
                                $category = 'shoppingArea';
                            }
                            if ($slug === 'skladskoe-pomeshchenie') {
                                $category = 'warehouse';
                            }
                            if ($slug === 'proizvodstvennoe-pomeshchenie') {
                                $category = 'industry';
                            }
                            if ($slug === 'svobodnogo-naznacheniya') {
                                $category = 'freeAppointmentObject';
                            }
                            if ($slug === 'zdanie') {
                                $category = 'building';
                            }
                            if ($slug === 'imushchestvennyj-kompleks') {
                                $category = 'industry';
                            }
                        }
                    }

                    if ($category == NULL || $type == NULL) {
                        break;
                    } else {
                        $xml->writeElement('Category', $category . $type);
                    }

                    $xml->writeElement('ExternalId', $object->id);
                    $xml->writeElement('Description', strip_tags($object->description));

                    $house_num = $object->{$object->category->type}->house_number;
                    if ($house_num == NULL) {
                        $house_num = 1;
                    }

                    $xml->writeElement('Address', $object->city()->first()->title . ', ' . mb_convert_case($object->street, MB_CASE_TITLE, "UTF-8") . ' ' . $house_num);

                    if ($object->geopoint != NULL) {
                        $xml->startElement('Coordinates');
                        $xml->writeElement('Lat', explode(',', $object->geopoint)[0]);
                        $xml->writeElement('Lng', explode(',', $object->geopoint)[1]);
                        $xml->endElement();
                    }

                    if ($object->{$object->category->type}->rooms != NULL) {
                        if ($object->category->type == 'apartment') {
                            $xml->writeElement('FlatRoomsCount', (int)$object->{$object->category->type}->rooms);
                        }
                        if ($object->category->type == 'house') {
                            $xml->writeElement('BedroomsCount', (int)$object->{$object->category->type}->rooms);
                        }
                    }

                    $string = $object->autor->phone;

                    $xml->writeElement('TotalArea', (double)$object->{$object->category->type}->total_square);

                    if($object->{$object->category->type}->floor != NULL){
                        $xml->writeElement('FloorNumber', $object->{$object->category->type}->floor);
                    } else {
                        $xml->writeElement('FloorNumber', 0);
                    }

                    if($object->{$object->category->type}->live_square != NULL){
                        $xml->writeElement('LivingArea', $object->{$object->category->type}->live_square);
                    }

                    if($object->{$object->category->type}->kitchen_square != NULL){
                        $xml->writeElement('KitchenArea', $object->{$object->category->type}->kitchen_square);
                    }

                    if($object->{$object->category->type}->balconies != NULL){
                        $xml->writeElement('BalconiesCount', $object->{$object->category->type}->balconies);
                    }

                    $xml->startElement('Building');
                        $xml->writeElement('FloorsCount', $object->{$object->category->type}->n_of_floor);
                        $xml->writeElement('BuildYear', $object->{$object->category->type}->year);
                    $xml->endElement();

                    if($object->{$object->category->type}->land_square != NULL) {
                        $xml->startElement('Land');
                        $xml->writeElement('Area', $object->{$object->category->type}->land_square);
                        $xml->writeElement('AreaUnitType', 'sotka');
                        $xml->endElement();
                    }

                    $xml->startElement('BargainTerms');

                        if($object->rented){
                            $xml->writeElement('Price', $object->rent_price);
                        } else {
                            $xml->writeElement('Price', $object->price);
                        }

                        $xml->writeElement('Currency', 'rur');
                        if(!$object->rented){
                            $xml->writeElement('SaleType', 'pdkp');
                        }

                    $xml->endElement();

                    preg_match('/(\+[0-9]+(?=\())/', $string, $prefix);
                    $number = preg_replace('/(\+[0-9]+\(|\D)/', '', $string);

                    $xml->startElement('Phones');
                    $xml->startElement('PhoneSchema');
                    $xml->writeElement('CountryCode', $prefix[1]);
                    $xml->writeElement('Number', $number);
                    $xml->endElement();
                    $xml->endElement();

                    if (method_exists($object->{$object->category->type}, 'rooms_isolation')) {

                        $isolation = $object->{$object->category->type}->rooms_isolation()->first();

                        if ($isolation != NULL) {
                            $roomtype = NULL;
                            if ($isolation->slug == 'vse-izolirovannye') {
                                $roomtype = 'combined';
                            }
                            if ($isolation->slug == 'est-prokhodnye') {
                                $roomtype = 'separate';
                            }
                            if ($roomtype != NULL) {
                                $xml->writeElement('RoomType', $roomtype);
                            }
                        }
                    }

                    if (method_exists($object->{$object->category->type}, 'state')) {

                        $state = $object->{$object->category->type}->state()->first();

                        if ($state != NULL) {
                            $state_out = NULL;
                            if ($state->slug == 'kosmeticheskij' || $state->slug == 'chistovaya-otdelka' || $state->slug == 'chernovaya-otdelka') {
                                $state_out = 'cosmetic';
                            }
                            if ($state->slug == 'evroremont' || $state->slug == 'pod-klyuch') {
                                $state_out = 'euro';
                            }
                            if ($state->slug == 'dizajnerskij') {
                                $state_out = 'design';
                            }
                            if ($state->slug == 'trebuet-remonta') {
                                $state_out = 'no';
                            }
                            if ($state_out != NULL) {
                                $xml->writeElement('RepairType', $state_out);
                            }
                        }
                    }

                    $xml->startElement('SubAgent');
                    $xml->writeElement('Email', $object->autor()->first()->email);
                    $xml->writeElement('Phone', $object->autor()->first()->phone);
                    $xml->writeElement('FirstName', $object->autor()->first()->firstname);
                    $xml->writeElement('LastName', $object->autor()->first()->lastname);

                    if ($object->autor()->first()->Avatar()->first() != null) {
                        $xml->writeElement('AvatarUrl', asset($object->autor()->first()->Avatar()->first()->url));
                    } else {
                        $xml->writeElement('AvatarUrl', asset('/img/noava.png'));
                    }

                    $xml->endElement();

                    $xml->startElement('Photos');
                    if ($object->images()->count() > 0){
                        foreach ($object->images()->get() as $key => $image) {
                            $xml->startElement('PhotoSchema');
                            $xml->writeElement('FullUrl', asset($image->url));
                            if ($key == 0) {
                                $xml->writeElement('IsDefault', 'true');
                            } else {
                                $xml->writeElement('IsDefault', 'false');
                            }
                            $xml->endElement();
                        }
                    }
                    $xml->endElement();
                    $xml->writeElement('Title', strip_tags($object->title));
                    $xml->endElement();
                }
            $xml->endElement();
            $xml->endDocument();

            $xml->flush();
        }
        catch(Exception $e)
        {
            return 0;
        }

        return $path.'/cian.xml';
    }

    public static function yandexExport($path, $objects){
        try
        {
            $xml = new XMLWriter();
            $xml->openURI(public_path($path.'/yandex.xml'));


            $xml->startDocument('1.0');

                $xml->startElement('realty-feed');
                    $xml->writeAttribute('xmlns', 'http://webmaster.yandex.ru/schemas/feed/realty/2010-06');
                    $xml->writeElement('generation-date', Carbon::now()->format("c"));
                    foreach ($objects as $object) {
                        $xml->startElement('offer');
                        $xml->writeAttribute('internal-id', $object->id);
                        if($object->rented){
                            $xml->writeElement('type', 'аренда');
                        } else {
                            $xml->writeElement('value', 'продажа');
                        }

                        if($object->category->type == 'apartament' ||
                            $object->category->type == 'house' ||
                            $object->category->type == 'room' ||
                            $object->category->type == 'garage' ||
                            $object->category->type == 'land'
                        ){
                            $xml->writeElement('property-type', 'жилая');
                        }

                        $category = null;

                        if ($object->category->type === 'apartment') {
                            $category = 'квартира';
                        }
                        if ($object->category->type === 'room') {
                            $category = 'комната';
                        }
                        if ($object->category->type === 'house') {
                            $category = 'дом с участком';
                        }
                        if ($object->category->type === 'land') {
                            $category = 'участок';
                        }
                        if ($object->category->type === 'garage') {
                            $category = 'гараж';
                        }
                        if($category != null){
                            $xml->writeElement('category', $category);
                        }

                        if($category === 'гараж') {
                            $xml->writeElement('garage-type', 'гараж');
                        }

                        $xml->writeElement('url', route($object->category->type, $object->id));

                        $xml->writeElement('creation-date', Carbon::parse($object->created_at)->format('c'));

                        $xml->endElement();
                    }
                $xml->endElement();
            $xml->endDocument();

            $xml->flush();
        }
        catch(Exception $e)
        {
            return 0;
        }

        return $path.'/yandex.xml';
    }

}
