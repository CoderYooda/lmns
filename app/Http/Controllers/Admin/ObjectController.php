<?php

namespace App\Http\Controllers\Admin;

use App\Models\Params\Area;
use App\Models\Params\HouseBuildType;
use Faker\Provider\Base;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Objects\BaseObject;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use stdClass;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\ActivityController;


class ObjectController extends Controller
{

    /**
     * Вывод своих добавленных обьектов
     *
     * @return void
     */
    public function index()
    {
    }

    public function getBaseForm()
    {
        // Форма добавления обьекта
        $categories = Category::all();

//        $params =  Cache::rememberForever('keyparams', function() {
//            return $this->collectObjectParams();
//        });
        $params = $this->collectObjectParams();

        return view('template.admin.common.object.index', compact('categories', 'params'));

    }

    public function switchExport(Request $request)
    {
        $object = BaseObject::where('id', $request['id'])->first();
        if($object != NULL){
            $object->export = $request['value'] === 'true';
            $object->save();

            if($object->export){
                $message = 'Объект ' . $object->id . ' назначен экспортируемым.';
            } else {
                $message = 'Объект ' . $object->id . ' снят с экспорта.';
            }

            return response()->json([
                'status'=>'success',
                'message'=>$message
            ]);
        } else {
            return response()->json([
                'status'=>'error'
            ]);
        }
    }

    public function editObject($id)
    {

        $user = Auth::user();

        $type = self::getObjectType($id);

        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area', $type];
        $object = BaseObject::where('id', $id)->with($relations)->firstOrFail();
        if($object->isAdminOfObject($user) || $object->isAutor($user)){
        } else {
            abort(403);
        }

        $params = $this->collectObjectParams();

        return view('template.admin.common.object.index', compact('object', 'params', 'type'));
    }

    private static function getObjectType($id){
        $base_object = BaseObject::where('id', $id)->with('category')->firstOrFail()->category->type;

        return $base_object;
    }

    public function saveObject(Request $request)
    {
        if(!isset($request['type']) || $request['type'] == null){
            return redirect()->back()->with('error', 'server error');
        }

        $controller = 'App\Http\Controllers\Admin\Types\\' . ucfirst($request['type']) . 'Controller';

        $object = $this->store($request);

        if($object->isAdminOfObject(Auth::user()) || $object->isAutor(Auth::user())){
        } else {
            abort(403);
        }

        if($object != null){
            $controller::store($request);
        } else {
            return redirect()->back()->with('error', 'server error');
        }

        return redirect()->route('Profile', Auth::user()->id);

    }

    public function removeObject($id)
    {
        $object = BaseObject::where('id', $id)->first();
        ActivityController::createActivity('delete_object', Auth::user(), $object->title);
        $object->delete();
        return response()->json(['status' => 'success', 'message' => 'Объект "'.$object->title.'" удалён'], 200);
    }

    public function store($request)
    {
        $object = BaseObject::firstOrNew(['id' => $request->input('id', 0)]);
        if(!isset($request['notary']))      $request['notary']      = 0;
        if(!isset($request['exlusive']))    $request['exlusive']    = 0;
        if(!isset($request['writed']))      $request['writed']      = 0;
        if(!isset($request['custody']))     $request['custody']     = 0;
        if(!isset($request['burden']))      $request['burden']      = 0;

        $category = Category::where('type', $request['type'])->first();

        $request['agency_id'] = Auth::user()->toAgency()->first()->id;

        $request['title'] = self::generateName($category, $request);

        if(!$request['rented'] && $request['total_square'] != null)
        $request['price_m'] =  $request['price'] / $request['total_square'];

        if ($object->wasRecentlyCreated) { //Если уже был

            if(isset($request['new_created_id'])){
                $request['created_id'] = $request['new_created_id'];
            } else {
                $request['created_id'] = $object->created_id;
            }
        } else {
            $request['created_id'] = Auth::user()->id;
        }

        $request['category_id'] = $category->id;

        $request['exlusive_date'] = Carbon::parse($request['exlusive_date'])->format('Y-m-d');
        $object->fill($request->only($object->fields));
        if($object->exists){
            $new = false;
        } else {
            $new = true;
        }
        $object->save();
        $object->images()->sync($request['pic']);
        $request['object_id'] = $object->id;

        if($new){
            self::sendActivity('create', $object);
        } else {
            self::sendActivity('update', $object);
        }

        return $object;
    }

    private static function sendActivity($type, $object){
        ActivityController::createActivity($type.'_object', Auth::user(), $object->title, route($object->category->type, $object->id));
    }

    public static function getMaxSquareByAgency($agency)
    {
        $categories = Category::all();
        $max_array = [];
        foreach( $categories as $category )
        {
            if($category->type != 'land') {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('agency_id', $agency->id)->get()->max($category->type . '.total_square')
                );
            } else {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('agency_id', $agency->id)->get()->max($category->type . '.land_square')
                );
            }
        }

        $max = 0;
        foreach( $max_array as $k => $v )
        {
            $max = max( array( $max, $v ) );
        }
        return $max;
    }

    public static function getMaxSquareByUser($user)
    {
        $categories = Category::all();
        $max_array = [];
        foreach( $categories as $category )
        {
            if($category->type != 'land') {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('created_id', $user->id)->get()->max($category->type . '.total_square')
                );
            } else {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('created_id', $user->id)->get()->max($category->type . '.land_square')
                );
            }
        }

        $max = 0;
        foreach( $max_array as $k => $v )
        {
            $max = max( array( $max, $v ) );
        }
        return $max;
    }

    public static function getMaxSquareByExclusive()
    {
        $categories = Category::all();
        $max_array = [];
        foreach( $categories as $category )
        {
            if($category->type != 'land') {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('exlusive', true)->get()->max($category->type . '.total_square')
                );
            } else {
                $max_array[$category->type] = (int)ceil(
                    BaseObject::where('exlusive', true)->get()->max($category->type . '.land_square')
                );
            }
        }

        $max = 0;
        foreach( $max_array as $k => $v )
        {
            $max = max( array( $max, $v ) );
        }
        return $max;
    }

    public static function list()
    {
        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];
        $objects = BaseObject::where('created_id', Auth::user()->id)
            ->orwhere(function($query){
                if (request('exlusive')) {
                    $query->orwhere('exlusive', true);
                }
            })
            ->where(function($query){
                self::objectFilter($query);
            })
            ->with($relations)->orderBy('created_at', 'DESC')->paginate(15);
        $categories = Category::all();
        $areas = Area::all();

        return view('admin.object.list', compact('objects','categories', 'areas'));
    }

    public function exclusiveList (){
        $request = request();
        if($request['type'] == null){
            $request['type'] = 'selded';
        }
        $category = Category::where('id', $request['category'])->first();
        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];
        $objects = BaseObject::where('exlusive', true)
            ->where(function($query){
                ObjectController::objectFilter($query);
            })
            ->where(function($q) use($request, $category){

                //self::rentSorting($request, $q);

                //self::citySorting($request, $q);

                if($request['category'] != NULL){//Квартиры

                    ObjectController::metaSorting($request, $q, $category->type);
                }
            })
            ->with($relations)->orderBy('created_at', 'DESC')->paginate(15);
        $categories = Category::all();
        $areas = Area::all();
        $filter_params = ObjectController::collectFilterParams(NULL, NULL, true);

        if(request()->ajax()){
            return response()->json([
                'status' => 'success',
                'destination' => '#filtered_objects',
                'mess' => false,
                'params' => $filter_params,
                'data' => view('template.admin.common.agency.objects.collection', compact('objects'))->render()], 200);
        }

        return view('template.admin.common.exclusive.index', compact('objects','categories', 'areas', 'filter_params'));
    }

    private static function generateName($category, $request)
    {
        if($category->type == 'apartment'){

            return $request['rooms'] . '-к квартира, ' . (float)$request['total_square']
                . '&nbsp;м<sup>2</sup>, ' . $request['floor'] . '/' . $request['n_of_floor'] . '&nbsp;эт.';

        } elseif ($category->type == 'room'){

            return 'Комната, ' . (float)$request['total_square'] . '&nbsp;м<sup>2</sup>, '
                . $request['floor'] . '/' . $request['n_of_floor'] . '&nbsp;эт.';
        } elseif ($category->type == 'garage'){

            return 'Гараж, ' . (float)$request['total_square'] . '&nbsp;м<sup>2</sup>';
        } elseif ($category->type == 'commerce'){

            return 'Комм. недвижимость, ' . (float)$request['total_square'] . '&nbsp;м<sup>2</sup>';
        } elseif ($category->type == 'house'){
            if($request['build_type_id'] != NULL){
                $type = HouseBuildType::where('id', $request['build_type_id'])->first();
                return $type->title.', ' . (float)$request['total_square'] . '&nbsp;м<sup>2</sup>';
            } else {
                return 'Дом, ' . (float)$request['total_square'] . '&nbsp;м<sup>2</sup>';
            }
        } elseif ($category->type == 'land'){

            return 'Участок, ' . (float)$request['land_square'] . '&nbsp;сот.';
        }

    }

    public static function objectFilter($query)
    {
        if (request('type') != null) {
            if (request('type') == 'rented') {
                $query->where('rented', true)
                ->where(function($query){
                    self::priceFilter($query,'rent_price');

                });

            }
            if (request('type') == 'selded') {
                $query->where('rented', false)
                ->where(function($query){
                    self::priceFilter($query,'price');
                });
            }
        }

        if (request('category') != null)
            $query->where('category_id', request('category'));

        if (request('area') != null)
            $query->where('area_id', request('area'));

        return $query;
    }

    public static function priceFilter($query, $table)
    {
        if(request('price_from') == NULL && request('price_to') == NULL) {
            return;
        }

        if(request('price_to') == NULL) {
            $query->whereBetween(
                $table, [request('price_from'), PHP_INT_MAX]
            );
        }

        if(request('price_from') == NULL) {
            $query->whereBetween(
                $table, [PHP_INT_MIN, request('price_to')]
            );
        }

        if(request('price_from') != NULL && request('price_to') != NULL){
            $query->whereBetween(
                $table, [request('price_from') , request('price_to')]
            );
        }
    }

    public static function metaSorting ($request, $q, $category){

        $q->whereHas($category, function($q) use ($request, $category){
            if($category == 'land') {
                self::squareSorting($q, 'land_square');
                self::squareSotSorting($q, 'land_square');

            } else {
                self::squareSorting($q, 'total_square');
                self::squareSotSorting($q, 'total_square');
            }

            self::readinessSorting($request, $q, $category);
            self::roomsSorting($request, $q, $category);
            self::commerceTypeSorting($request, $q, $category);
            self::floorSorting($q, 'floor');
        });
    }

    public static function collectFilterParams($agency = null, $user = null, $exclusive = null){

        $request = request();
        $filter_params = [];
        if($agency != null){
            $filter_params['price_max'] = BaseObject::where('agency_id', $agency->id)->get()->max('price');
            $filter_params['rent_price_max'] = BaseObject::where('agency_id', $agency->id)->get()->max('rent_price');
            $filter_params['square_max'] = ObjectController::getMaxSquareByAgency($agency);
        }

        if($user != null){
            $filter_params['price_max'] = BaseObject::where('created_id', $user->id)->get()->max('price');
            $filter_params['rent_price_max'] = BaseObject::where('created_id', $user->id)->get()->max('rent_price');
            $filter_params['square_max'] = ObjectController::getMaxSquareByUser($user);
        }

        if($exclusive != null){
            $filter_params['price_max'] = BaseObject::where('exlusive', true)->get()->max('price');
            $filter_params['rent_price_max'] = BaseObject::where('exlusive', true)->get()->max('rent_price');
            $filter_params['square_max'] = ObjectController::getMaxSquareByExclusive();
        }


        if($request['square_to'] == null){
            $request['square_to'] = $filter_params['square_max'];
        }

        if($request['type'] == null || $request['type'] == 'rented'){
            if($request['price_to'] == null){
                $request['price_to'] = $filter_params['rent_price_max'];
            }
        } else {
            if($request['price_to'] == null){
                $request['price_to'] = $filter_params['price_max'];
            }
        }

        return $filter_params;
    }

    private static function readinessSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'readiness')){
            return;
        }
        if($request['vtorich'] != NULL && $request['vtorich'] || $request['novostroi'] != NULL && $request['novostroi']){
            $q->where(function($q) use ($request){
                $q->where('id', NULL);

                $q->orWhereHas('readiness', function($q) use ($request){
                    $q->where('id', NULL);
                    if($request['vtorich'] != NULL && $request['vtorich']){
                        $q->orWhere('id', 1);
                    }
                    if($request['novostroi'] != NULL && $request['novostroi']){
                        $q->orWhere('id', 2);
                    }
                });
            });
        }
    }

    private static function commerceTypeSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'type')){
            return;
        }
        if($request['commece_type'] != NULL && $request['commece_type'] != 0){
            $q->whereHas('type', function($q) use ($request){
                $q->where('id', $request['commece_type']);
            });
        }
    }

    private static function squareSorting($q, $table){
        if(request('square_from') == NULL && request('square_to') == NULL) {
            return;
        }
        //dd(request('square_from'));
        $q->where(function($query) use ($table){



            if(request('square_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('square_from'), 999999999]
                );
            }

            if(request('square_from') == NULL) {
                $query->whereBetween(
                    $table, [0, (float)request('square_to')]
                );
            }

            if(request('square_from') != NULL && request('square_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('square_from') , (float)request('square_to')]
                );
            }

        });
    }

    private static function squareSotSorting($q, $table){
        if(request('square_sot_from') == NULL && request('square_sot_to') == NULL) {
            return;
        }

        $q->where(function($query) use ($table){

            if(request('square_sot_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('square_sot_from'), 999999999]
                );
            }

            if(request('square_sot_from') == NULL) {
                $query->whereBetween(
                    $table, [0, (float)request('square_sot_to')]
                );
            }

            if(request('square_sot_from') != NULL && request('square_sot_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('square_sot_from') , (float)request('square_sot_to')]
                );
            }
        });
    }

    private static function roomsSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'apartment_type')){
            return;
        }
        $q->where(function($q) use ($request){
            if($request['rooms_study'] != NULL && $request['rooms_study']){
                $q->whereHas('apartment_type', function($q) use ($request){
                    $q->where('id', 2);
                });
            }

            if($request['rooms_one'] != NULL || $request['rooms_two'] != NULL || $request['rooms_three'] != NULL || $request['rooms_four'] != NULL){
                $q->where('id', NULL);
                $q->orWhere(function($q) use ($request){
                    if($request['rooms_one'] != NULL && $request['rooms_one']){$q->orWhere('rooms', 1);}
                    if($request['rooms_two'] != NULL && $request['rooms_two']){$q->orWhere('rooms', 2);}
                    if($request['rooms_three'] != NULL && $request['rooms_three']){$q->orWhere('rooms', 3);}
                    if($request['rooms_four'] != NULL && $request['rooms_four']){$q->orWhere('rooms','>', 3);}
                });
            }
        });
    }

    private static function floorSorting($q, $table){

        if(request('floor_from') == NULL && request('floor_to') == NULL) {
            return;
        }

        $q->where(function($query) use ($table){

            if(request('floor_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('floor_from'), PHP_FLOAT_MAX]
                );
            }

            if(request('floor_from') == NULL) {
                $query->whereBetween(
                    $table, [PHP_FLOAT_MIN, (float)request('floor_to')]
                );
            }

            if(request('floor_from') != NULL && request('floor_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('floor_from') , (float)request('floor_to')]
                );
            }
        });
    }

    private function collectObjectParams()
    {
        $params = new stdClass();
        $params->apartment_comfort =        \App\Models\Params\ApartmentComfort::all();
        $params->apartment_type =           \App\Models\Params\ApartmentType::all();
        $params->area =                     \App\Models\Params\Area::all();
        $params->build_comfort =            \App\Models\Params\BuildComfort::all();
        $params->build_type =               \App\Models\Params\BuildType::all();
        $params->city =                     \App\Models\Params\City::belgorod();
        $params->garage_comfort =           \App\Models\Params\GarageComfort::all();
        $params->garage_special =           \App\Models\Params\GarageSpecial::all();
        $params->garage_type =              \App\Models\Params\GarageType::all();
        $params->gk =                       \App\Models\Params\Gk::all();
        $params->heating =                  \App\Models\Params\Heating::all();
        $params->hot_water_system =         \App\Models\Params\HotWaterSystem::all();
        $params->layout =                   \App\Models\Params\Layout::all();
        $params->readiness =                \App\Models\Params\Readiness::all();
        $params->repairs =                  \App\Models\Params\Repairs::all();
        $params->room_comfort =             \App\Models\Params\RoomComfort::all();
        $params->rooms_isolation =          \App\Models\Params\RoomsIsolation::all();
        $params->room_specials =            \App\Models\Params\RoomSpecials::all();
        $params->room_type =                \App\Models\Params\RoomType::all();
        $params->sgk =                      \App\Models\Params\Sgk::all();
        $params->state =                    \App\Models\Params\State::all();
        $params->wall_material =            \App\Models\Params\WallMaterial::all();
        $params->apartment_wall_material =  \App\Models\Params\ApartmentWallMaterial::all();
        $params->wc =                       \App\Models\Params\Wc::all();

        $params->commerce_adds =            \App\Models\Params\CommerceAdds::all();
        $params->commerce_doing =           \App\Models\Params\CommerceDoing::all();
        $params->commerce_gruz =            \App\Models\Params\CommerceGruz::all();
        $params->commerce_kind =            \App\Models\Params\CommerceKind::all();
        $params->commerce_purpose =         \App\Models\Params\CommercePurpose::all();
        $params->commerce_sklad =           \App\Models\Params\CommerceSklad::all();
        $params->commerce_special =         \App\Models\Params\CommerceSpecial::all();
        $params->commerce_tools =           \App\Models\Params\CommerceTools::all();
        $params->commerce_type =            \App\Models\Params\CommerceType::all();
        $params->commerce_build_comfort =   \App\Models\Params\CommerceBuildComfort::all();

        $params->house_comfort =            \App\Models\Params\HouseComfort::all();
        $params->house_type =               \App\Models\Params\HouseType::all();
        $params->house_build_type =         \App\Models\Params\HouseBuildType::all();

        $params->land_comfort =             \App\Models\Params\LandComfort::all();

        return $params;
    }
}
