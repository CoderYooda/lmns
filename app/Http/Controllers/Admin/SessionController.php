<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionController extends Controller
{
    public function saveToSession(Request $request)
    {
        session([$request['key'] => $request['value']]);
    }
    public function readFromSession($parametr)
    {
        if ($request->session()->exists($parametr)) {
            return session($parametr);
        }
    }
}
