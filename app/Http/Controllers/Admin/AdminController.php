<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function dashboard(){
        // Главная страница админки
        return redirect()->route('Profile', Auth::user()->id);
        return view('template.admin.common.index');
        //return redirect()->route('listObjects');
    }

    public function settings(){
        // Настройки
        $agent = Auth::user();
        return view('admin.settings', compact('agent'));
    }
}
