<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ActivityController;
use Auth;
use App\Models\Category;
use App\Models\Objects\BaseObject;
use Illuminate\Support\Facades\Cache;

class ProfileController extends Controller
{
    public function profile($id){
        $request = request();
        if($request['type'] == null){
            $request['type'] = 'selded';
        }
        $user = User::where('id', $id)->firstOrFail();

        $category = Category::where('id', $request['category'])->first();
        $user->objects_count = self::objectsCount($user);
        $relations = ['autor', 'deliter', 'agency', 'category', 'images', 'city', 'area'];
        $objects = BaseObject::where('created_id', $user->id)
            ->where(function($query){
                ObjectController::objectFilter($query);
            })
            ->where(function($q) use($request, $category){

                //self::rentSorting($request, $q);

                //self::citySorting($request, $q);

                if($request['category'] != NULL){//Квартиры

                    ObjectController::metaSorting($request, $q, $category->type);
                }
            })
            ->with($relations)
            ->orderBy('updated_at', 'DESC')
            ->paginate(15);

        $filter_params = ObjectController::collectFilterParams(NULL, $user, NULL);
        $categories = Category::all();
        if(request()->ajax()){
            return response()->json([
                'status' => 'success',
                'destination' => '#filtered_objects',
                'mess' => false,
                'params' => $filter_params,
                'data' => view('template.admin.common.agency.objects.collection', compact('objects'))->render()], 200);
        }

        return view('template.admin.common.profile.index', compact('user', 'objects', 'filter_params', 'categories'));
    }

    public static function objectsCount($user){

        return $user->objects()->count();
    }

    public function edit($id){
        if($id != Auth::user()->id){
            abort(404);
        }
        $user = User::where('id', $id)->first();
        return view('template.admin.common.profile.edit', compact('user'));
    }

    public function online(){
        $users = User::all();
        foreach($users as $user){

            if(Cache::get('user-is-online-' . $user->id) == 1){
                echo '<a href="'.route('Profile', $user->id).'">'.$user->lastname.' '.$user->firstname.'</a>';
                echo '<br>';
            }
        }
    }

    public function save(Request $request){
        $user = Auth::user();
        if($user->id == $request['user_id']){

            $user->fill($request->only($user->fields));
            $user->save();
            ActivityController::createActivity('edit_profile_personal', $user);
            //return redirect()->route('Profile',$user->id)->with(['status' => 'success'])->with(['message' => 'Настройки сохранены']);
            return response()->json(['status' => 'success', 'message' => 'Настройки сохранены'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Попытка взлома'], 419);
        }
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        if($user->id == $request['user_id']){

            if(!Hash::check($request['password'], $user->password)){
                return response()->json(['status' => 'error', 'message' => 'Введите верный пароль учетной записи'], 200);
            }

            $validator = Validator::make($request->all(), [
                'new_password' => 'required|string|min:6|confirmed',
            ]);

            if($validator->fails()){
                return response()->json(['status' => 'error', 'message' => $validator->errors()], 200);
            } else {
                $user->password = Hash::make($request['new_password']);
                $user->save();
                ActivityController::createActivity('edit_profile_password', $user);
                return response()->json(['status' => 'success', 'message' => 'Настройки сохранены'], 200);
            }

        } else {
            return response()->json(['status' => 'error', 'message' => 'Попытка взлома'], 419);
        }
    }
}
