<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;

class ActivityController extends Controller
{
    public static function createActivity($action, $user, $name = null, $link = null){
        $activities = self::getAllActivities();
        $activity = new Activity();
        $activity->title = sprintf($activities[$action], $link, $name);
        $activity->slug = $action;
        $activity->actor = $user->id;
        if($activity->save()){
            return true;
        } else {
            return false;
        }
    }

    private static function getAllActivities(){
        return [
            'edit_profile_personal' => 'Отредактировал(а) личную информацию',
            'edit_profile_password' => 'Сменил(а) пароль',
            'edit_profile_avatar' => 'Сменил(а) изображение профиля',

            'create_object' => 'Создал(а) объект <a href="%s">%s</a>',
            'update_object' => 'Обновил(а) объект <a href="%s">%s</a>',
            'delete_object' => 'Удалил(а) объект %s%s',
        ];
    }
}
