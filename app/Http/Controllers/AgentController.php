<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageToAgent;
use Illuminate\Support\Facades\Validator;
use stdClass;

class AgentController extends Controller
{
    public function messageToAgent(Request $request){

        $message = new stdClass();
        $message->email = $request['email'];
        $message->name = $request['name'];
        $message->phone = $request['phone'];
        $message->message = $request['message'];

        $agent = User::where('id', $request['agent'])->first();
        Mail::to($agent)->send(new MessageToAgent($message));
        return redirect()->back()->with('success', 'Сообщение отправлено!');
    }

    public function storeagent(Request $request){
        $agent = User::where('id', $request['id'])->first();
        if($agent == null){
            abort(404);
        }
        $agent->firstname = $request['firstname'];
        $agent->lastname = $request['lastname'];
        $agent->midname = $request['midname'];
        $agent->phone = $request['phone'];
        $agent->email = $request['email'];
        $agent->save();
        return redirect()->back()->with('message', 'Профиль обновлён');
    }

    public function storepassword(Request $request){
        $agent = User::where('id', $request['id'])->first();
        if($agent == null){
            abort(404);
        }
        $validator =  Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $agent->password = bcrypt($request['password']);
        $agent->save();
        return redirect()->back()->with('message', 'Новый пароль сохранён');
    }
}
