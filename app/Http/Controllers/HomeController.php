<?php

namespace App\Http\Controllers;

use App\Models\Params\City;
use App\Models\Params\CommerceType;
use App\Models\Params\Readiness;
use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Category;

class HomeController extends Controller
{
    public function index()
    {


        $browser = parent::checkAgent();
        $with = [
            'city', 'images', 'autor', 'apartment'
        ];

        $apartments = BaseObject::orderBy('id', 'DESC')

            ->where([
                ['rented', false],
                ['category_id', 1],
            ])
            ->with($with)
            ->paginate(12);

        $categories = Category::all();
        $readiness = Readiness::all();
        $cities = City::belgorod();
        $commerce_type = CommerceType::all();

        if($browser->isDesktop()) {
            $lastes = BaseObject::with('images')->limit(10)->orderBy('created_at', 'DESC')->get();
        }
        if($browser->isDesktop()){
            return view('template.front.common.home.desktop', compact('apartments', 'categories', 'readiness', 'cities', 'commerce_type', 'lastes')); //TODO переделать шаблон десктопа
        } else {
            return view('template.common.index.mobile', compact('apartments', 'categories', 'readiness', 'cities', 'commerce_type'));
        }

    }
}
