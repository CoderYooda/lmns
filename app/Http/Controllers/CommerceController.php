<?php

namespace App\Http\Controllers;

use App\Models\Objects\Commerce;
use App\Models\Params\CommerceType;
use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Category;
use App\Models\Params\City;

class CommerceController extends Controller
{
    public function index(Request $request) {

        $with = [
            'city', 'commerce', 'images'
        ];

        $default_where = [
            ['published', true]
        ];

        if($request['onmap'] == 1){$paginate = 2000;} else {$paginate = 20;}

        $city_id = 1;
        $category = Category::where('type', 'commerce')->first();
        $objects = ObjectController::searchFilter(6, $request, 'commerce', $paginate);
        //dd($objects);
        $cities = City::belgorod();
        $commerce_types = CommerceType::all();
        $request->query->add(['category' => '6']);

        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if($request['onmap'] == 1){
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.search', compact('objects', 'commerce_types', 'cities', 'city_id', 'category', 'request'));
        } else {
            return view('template.common.search.mobile', compact('objects', 'commerce_types', 'cities', 'city_id', 'category', 'request'));
        }

    }

    public function show($id) {

        $object = BaseObject::where([
            ['id', $id],
            ['category_id', 6],
            ['published', true]
        ])
            ->with(['images', 'commerce', 'autor', 'agency'])
            ->firstOrFail();
        $meta = 'commerce';
        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if(request('onmap') == 1){
                $cities = City::belgorod();
                $objects[0] = $object;
                $category = $object->category()->first();
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.index', compact('object', 'meta'));
        } else {
            return view('template.common.object.mobile', compact('object', 'meta'));
        }
    }
}
