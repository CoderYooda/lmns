<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Params\City;

class CityController extends Controller
{
    public static function getCityNameById($id){
        if($id == 0){
            return 'по Белгородской области';
        }
        $cityname = City::where('id', $id)->first()->name;
        if($cityname == 'Область'){
            return 'по Белгородской области';
        } else {
            $city = City::where('id', $id)->first();
            return 'в ' . $city->socr . '. ' . $city->name;
        }

    }
}
