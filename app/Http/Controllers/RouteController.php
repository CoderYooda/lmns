<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApartmentController;

class RouteController extends Controller
{
    public function index($route, Request $request){

        $routearray = explode('-', $route);

        if($request['rent'] == NULL) {
            if (in_array('kupit', $routearray, true)) {
                if($request['rent'] == NULL) {
                    $request->query->add(['rent' => '0']);
                }
            }else if(in_array('arenda', $routearray, true)){
                if($request['rent'] == NULL) {
                    $request->query->add(['rent' => '1']);
                }
            } else {
                if($request['rent'] == NULL) {
                    $request->query->add(['rent' => '2']);
                }
            }
        }

        if (in_array('belgorod', $routearray, true)) {if ($request['city'] == NULL) {$request->query->add(['city' => '1']);}}
        if (in_array('oblast', $routearray, true)) {if ($request['city'] == NULL) {$request->query->add(['city' => '0']);}}

        if (in_array('kvartiry', $routearray, true)) {

                if (in_array('1komnatnyu', $routearray, true)) {if ($request['rooms_one'] == NULL) {$request->query->add(['rooms_one' => '1']);}}
                if (in_array('2komnatnyu', $routearray, true)) {if ($request['rooms_two'] == NULL) {$request->query->add(['rooms_two' => '1']);}}
                if (in_array('3komnatnyu', $routearray, true)) {if ($request['rooms_three'] == NULL) {$request->query->add(['rooms_three' => '1']);}}
                if (in_array('4komnatnyu', $routearray, true)) {if ($request['rooms_four'] == NULL) {$request->query->add(['rooms_four' => '1']);}}

            $ap = new ApartmentController();
            return $ap->index($request);

        } elseif (in_array('komnatu', $routearray, true)) {

            if (in_array('do10metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                $request->query->add(['square_to' => '10']);
            }}
            if (in_array('ot10do15metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                $request->query->add(['square_from' => '10']);
                $request->query->add(['square_to' => '15']);
            }}
            if (in_array('ot15do20metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                $request->query->add(['square_from' => '15']);
                $request->query->add(['square_to' => '20']);
            }}
            if (in_array('ot20metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                $request->query->add(['square_from' => '20']);
            }}
            $ap = new RoomController();
            return $ap->index($request);
        } elseif (in_array('uchastok', $routearray, true)) {

            if (in_array('do10sotok', $routearray, true)) {if ($request['square_sot_from'] == NULL) {
                $request->query->add(['square_sot_to' => '10']);
            }}
            if (in_array('ot10do15sotok', $routearray, true)) {if ($request['square_sot_from'] == NULL) {
                $request->query->add(['square_sot_from' => '10']);
                $request->query->add(['square_sot_to' => '15']);
            }}
            if (in_array('ot15do20sotok', $routearray, true)) {if ($request['square_sot_from'] == NULL) {
                $request->query->add(['square_sot_from' => '15']);
                $request->query->add(['square_sot_to' => '20']);
            }}
            if (in_array('ot20sotok', $routearray, true)) {if ($request['square_sot_from'] == NULL) {
                $request->query->add(['square_sot_from' => '20']);
            }}

            $ap = new LandController();
            return $ap->index($request);

        }

        elseif (in_array('dom', $routearray, true)) {
                if (in_array('do100metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                    $request->query->add(['square_to' => '100']);
                }}
                if (in_array('ot100do150metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                    $request->query->add(['square_from' => '100']);
                    $request->query->add(['square_to' => '150']);
                }}
                if (in_array('ot150do2000metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                    $request->query->add(['square_from' => '120']);
                    $request->query->add(['square_to' => '200']);
                }}
                if (in_array('ot200metrov', $routearray, true)) {if ($request['square_from'] == NULL) {
                    $request->query->add(['square_from' => '200']);
                }}
                $ap = new HouseController();
                return $ap->index($request);
        } else {
            abort(404);
        }
    }
}
