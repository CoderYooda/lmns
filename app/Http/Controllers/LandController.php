<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Category;
use App\Models\Params\City;

class LandController extends Controller
{
    public function index(Request $request) {

        $category = Category::where('type', 'land')->first();

        if($request['onmap'] == 1){$paginate = 2000;} else {$paginate = 20;}

        $objects = ObjectController::searchFilter(4, $request, 'land', $paginate);

        $cities = City::belgorod();
        $request->query->add(['category' => '4']);

        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if($request['onmap'] == 1){
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.search', compact('objects', 'cities', 'category', 'request'));
        } else {
            return view('template.common.search.mobile', compact('objects', 'cities', 'category', 'request'));
        }
    }

    public function show($id) {
        $object = BaseObject::where([
            ['id', $id],
            ['category_id', 4],
            ['published', true]
        ])
            ->with(['images', 'land', 'autor', 'agency'])
            ->firstOrFail();
        $meta = 'land';
        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if(request('onmap') == 1){
                $cities = City::belgorod();
                $objects[0] = $object;
                $category = $object->category()->first();
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.index', compact('object', 'meta'));
        } else {
            return view('template.common.object.mobile', compact('object', 'meta'));
        }
    }
}
