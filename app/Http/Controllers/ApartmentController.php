<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Params\CommerceType;
use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Params\City;
use App\Http\Controllers\ObjectController;

class ApartmentController extends Controller
{
    public function index(Request $request) {

        $city_id = 1;
        $category = Category::where('type', 'apartment')->first();

        if($request['onmap'] == 1){$paginate = 2000;} else {$paginate = 20;}

        $objects = ObjectController::searchFilter(1, $request, 'apartment', $paginate);
        $cities = City::belgorod();
        $request->query->add(['category' => '1']);
        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if($request['onmap'] == 1){
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.search', compact('objects', 'cities', 'city_id', 'category', 'request'));
        } else {
            return view('template.common.search.mobile', compact('objects', 'cities', 'city_id', 'category', 'request'));
        }
    }

    public function show($id) {
        $object = BaseObject::where([
            ['id', $id],
            ['category_id', 1],
            ['published', true]
        ])
            ->with(['images', 'apartment', 'autor', 'agency'])
            ->firstOrFail();
        $meta = 'apartment';

        $browser = parent::checkAgent();

        if($browser->isDesktop()) {
            if(request('onmap') == 1){
                $cities = City::belgorod();
                $objects[0] = $object;
                $category = $object->category()->first();
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.index', compact('object', 'meta'));
        } else {
            return view('template.common.object.mobile', compact('object', 'meta'));
        }
    }
}
