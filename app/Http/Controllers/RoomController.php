<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Category;
use App\Models\Params\City;

class RoomController extends Controller
{
    public function index(Request $request) {

        $with = [
            'city', 'room', 'images'
        ];

        $default_where = [
            ['published', true]
        ];
        $city_id = 1;
        $category = Category::where('type', 'room')->first();

        if($request['onmap'] == 1){$paginate = 2000;} else {$paginate = 20;}

        $objects = ObjectController::searchFilter(2, $request, 'room', $paginate);

        $cities = City::belgorod();
        $request->query->add(['category' => '2']);


        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if($request['onmap'] == 1){
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.search', compact('objects', 'cities', 'city_id', 'category', 'request'));
        } else {
            return view('template.common.search.mobile', compact('objects', 'cities', 'city_id', 'category', 'request'));
        }
    }

    public function show($id) {
        $object = BaseObject::where([
            ['id', $id],
            ['category_id', 2],
            ['published', true]
        ])
            ->with(['images', 'room', 'autor', 'agency'])
            ->firstOrFail();
        $meta = 'room';
        $browser = parent::checkAgent();
        if($browser->isDesktop()) {
            if(request('onmap') == 1){
                $cities = City::belgorod();
                $objects[0] = $object;
                $category = $object->category()->first();
                return view('template.front.common.object.map', compact('objects', 'cities', 'category'));
            }
            return view('template.front.common.object.index', compact('object', 'meta'));
        } else {
            return view('template.common.object.mobile', compact('object', 'meta'));
        }
    }
}
