<?php

namespace App\Http\Controllers;

use App\Models\Objects\Room;
use Illuminate\Http\Request;
use App\Models\Objects\BaseObject;
use App\Models\Params\City;
use App\Models\Category;
use App\Models\Params\CommerceType;


class ObjectController extends Controller
{

    public static function getSimilarObjects($category_id, $current){
        $objects = BaseObject::with('category','images')
            ->where('category_id', $category_id)
            ->where('id', '!=', $current)
            ->paginate(8);
        return view('template.front.common.object.similar', compact('objects'));
    }

    public function searchResults(Request $request){

        $browser = parent::checkAgent();

        $category = Category::where('id', $request['category'])->first();

        $elements_on_page = 20;
        if($request['onmap'] == 1){
            $elements_on_page = 2000;
        }

        $objects = BaseObject::with('category','images')
            ->orderBy('created_at', 'DESC')
            ->where('category_id' , $request['category'])

            ->where(function($q) use($request, $category){

                self::rentSorting($request, $q);

                self::citySorting($request, $q);
                if($request['category'] != NULL){//Квартиры
                    self::metaSorting($request, $q, $category->type);
                }
            })
            ->paginate($elements_on_page);
        $cities = City::belgorod();
        $commerce_types = CommerceType::all();
        if($browser->isDesktop()) {
            if($request['onmap'] == 1){
                return view('template.front.common.object.map', compact('objects', 'cities', 'commerce_types', 'category'));
            }
            return view('template.front.common.object.search', compact('objects', 'cities', 'commerce_types', 'category'));
        } else {
            return view('template.common.search.mobile', compact('objects', 'cities', 'commerce_types', 'category'));
        }
    }

    public static function priceFilter($query, $table, $alter)
    {
        if(request('cost-from') == NULL && request('cost-to') == NULL) {
            return;
        }
        $query->where(function($query) use ($table, $alter){
            if(request('cost-to') == NULL) {
                $query->whereBetween(
                    $table, [request('cost-from'), 9999999999999]
                );
                if($table != null && $alter != null) {
                    $query->orWhereBetween(
                        $alter, [request('cost-from'), 9999999999999]
                    );
                }
            }

            if(request('cost-from') == NULL) {
                $query->whereBetween(
                    $table, [0, request('cost-to')]
                );

                if($table != null && $alter != null) {
                    $query->orWhereBetween(
                        $alter,  [0, request('cost-to')]
                    );
                }
            }

            if(request('cost-from') != NULL && request('cost-to') != NULL){
                $query->whereBetween(
                    $table, [request('cost-from') , request('cost-to')]
                );
                if($table != null && $alter != null) {
                    $query->orWhereBetween(
                        $alter, [request('cost-from') , request('cost-to')]
                    );
                }
            }
        });
    }

    public static function searchFilter($category_id, $request, $category, $pages = null){

        if($pages == null){
            $paginate = 20;
        } else {
            $paginate = $pages;
        }

        $objects = BaseObject::with('category','images')
            ->where('category_id' , $category_id)

            ->where(function($q) use($request){
                self::rentSorting($request, $q);
                self::citySorting($request, $q);
            })
            ->where(function($q) use($request, $category){
                $q->whereHas($category, function($q) use ($request, $category){
                    if($category == 'land') {
                        self::squareSorting($q, 'land_square');
                        self::squareSotSorting($q, 'land_square');

                    } else {
                        self::squareSorting($q, 'total_square');
                        self::squareSotSorting($q, 'total_square');
                    }
                    self::readinessSorting($request, $q, $category);
                    self::roomsSorting($request, $q, $category);
                    self::commerceTypeSorting($request, $q, $category);
                    self::floorSorting($q, 'floor');
                });
            })
            ->orderBy('created_at', 'DESC')
            ->paginate($paginate);
        return $objects;
    }

    private static function rentSorting($request, $q){

        if($request['rent'] != NULL && $request['rent'] != 2){

            if ($request['rent'] != NULL && $request['rent'] == 0) {
                $q->where('rented', 0);
                self::priceFilter($q, 'price', null);
            }
            if ($request['rent'] != NULL && $request['rent']) {
                $q->where('rented', 1);
                self::priceFilter($q, 'rent_price', null);
            }
        }

        if($request['rent'] == NULL || $request['rent'] == 2){
            self::priceFilter($q, 'rent_price', 'price');
        }

    }

    private static function citySorting($request, $q){
        if($request['city'] != NULL && $request['city'] != 0 && $request['city']){
            $q->whereHas('city',function($q) use ($request){
                $q->where('id', $request['city']);
            });
        }
    }

    private static function metaSorting ($request, $q, $category){

        $q->whereHas($category, function($q) use ($request, $category){
            if($category == 'land') {
                self::squareSorting($q, 'land_square');
                self::squareSotSorting($q, 'land_square');

            } else {
                self::squareSorting($q, 'total_square');
                self::squareSotSorting($q, 'total_square');
            }

            self::readinessSorting($request, $q, $category);
            self::roomsSorting($request, $q, $category);
            self::commerceTypeSorting($request, $q, $category);
            self::floorSorting($q, 'floor');
        });
    }

    private static function readinessSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'readiness')){
            return;
        }
        if($request['vtorich'] != NULL && $request['vtorich'] || $request['novostroi'] != NULL && $request['novostroi']){
            $q->where(function($q) use ($request){
                $q->where('id', NULL);

                $q->orWhereHas('readiness', function($q) use ($request){
                    $q->where('id', NULL);
                    if($request['vtorich'] != NULL && $request['vtorich']){
                        $q->orWhere('id', 1);
                    }
                    if($request['novostroi'] != NULL && $request['novostroi']){
                        $q->orWhere('id', 2);
                    }
                });
            });
        }
    }

    private static function commerceTypeSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'type')){
            return;
        }
        if($request['commece_type'] != NULL && $request['commece_type'] != 0){
            $q->whereHas('type', function($q) use ($request){
                $q->where('id', $request['commece_type']);
            });
        }
    }

    private static function squareSorting($q, $table){
        if(request('square_from') == NULL && request('square_to') == NULL) {
            return;
        }
            //dd(request('square_from'));
        $q->where(function($query) use ($table){

            if(request('square_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('square_from'), 999999999]
                );
            }

            if(request('square_from') == NULL) {
                $query->whereBetween(
                    $table, [0, (float)request('square_to')]
                );
            }

            if(request('square_from') != NULL && request('square_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('square_from') , (float)request('square_to')]
                );
            }
        });
    }

    private static function squareSotSorting($q, $table){
        if(request('square_sot_from') == NULL && request('square_sot_to') == NULL) {
            return;
        }

        $q->where(function($query) use ($table){

            if(request('square_sot_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('square_sot_from'), 999999999]
                );
            }

            if(request('square_sot_from') == NULL) {
                $query->whereBetween(
                    $table, [0, (float)request('square_sot_to')]
                );
            }

            if(request('square_sot_from') != NULL && request('square_sot_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('square_sot_from') , (float)request('square_sot_to')]
                );
            }
        });
    }

    private static function roomsSorting($request, $q, $category){
        $class = '\App\Models\Objects\\' . ucfirst(strtolower($category));
        if(!method_exists (new $class(), 'apartment_type')){
            return;
        }
        $q->where(function($q) use ($request){
            if($request['rooms_study'] != NULL && $request['rooms_study']){
                $q->whereHas('apartment_type', function($q) use ($request){
                    $q->where('id', 2);
                });
            }

            if($request['rooms_sp'] != NULL && $request['rooms_sp']){
                $q->whereHas('apartment_type', function($q) use ($request){
                    $q->where('id', 4);
                });
            }

            if($request['rooms_one'] != NULL || $request['rooms_two'] != NULL || $request['rooms_three'] != NULL || $request['rooms_four'] != NULL){
                $q->where('id', NULL);
                $q->orWhere(function($q) use ($request){
                    if($request['rooms_one'] != NULL && $request['rooms_one']){$q->orWhere('rooms', 1);}
                    if($request['rooms_two'] != NULL && $request['rooms_two']){$q->orWhere('rooms', 2);}
                    if($request['rooms_three'] != NULL && $request['rooms_three']){$q->orWhere('rooms', 3);}
                    if($request['rooms_four'] != NULL && $request['rooms_four']){$q->orWhere('rooms','>', 3);}
                });
            }
        });
    }

    private static function floorSorting($q, $table){

        if(request('floor_from') == NULL && request('floor_to') == NULL) {
            return;
        }

        $q->where(function($query) use ($table){

            if(request('floor_to') == NULL) {
                $query->whereBetween(
                    $table, [(float)request('floor_from'), PHP_FLOAT_MAX]
                );
            }

            if(request('floor_from') == NULL) {
                $query->whereBetween(
                    $table, [PHP_FLOAT_MIN, (float)request('floor_to')]
                );
            }

            if(request('floor_from') != NULL && request('floor_to') != NULL){
                $query->whereBetween(
                    $table, [(float)request('floor_from') , (float)request('floor_to')]
                );
            }
        });
    }

    public static function getTotalCount(){
        $count =  BaseObject::all()->count();
        return $count;
    }

    public static function getCount($cat_id, $rent = null, $rooms = null){

        $category = Category::where('id', $cat_id)->first();

        $count =  BaseObject::with($category->type)
            ->where('category_id' , $cat_id)
            ->where(function($q) use($cat_id, $rent, $rooms, $category){
                if($rent !== NULL && $rent !== 2){

                    if ($rent !== NULL && $rent === 0) {

                        $q->where('rented', 0);
                    }
                    if ($rent !== NULL && $rent === 1) {

                        $q->where('rented', 1);
                    }

                }
                $q->whereHas($category->type, function($q) use ($rooms){
                    if($rooms != NULL){
                        if($rooms == 4){
                            $q->where('rooms', '>=',  $rooms);
                        } else {
                            $q->where('rooms', $rooms);
                        }
                    }
                });
            })->count();
        return $count;
    }

    public static function getCountHouse($cat_id, $rent = null, $square = null){

        $category = Category::where('id', $cat_id)->first();

        $count =  BaseObject::with($category->type)
            ->where('category_id' , $cat_id)
            ->where(function($q) use($cat_id, $rent, $square, $category){
                if($rent !== NULL && $rent !== 2){

                    if ($rent !== NULL && $rent === 0) {

                        $q->where('rented', 0);
                    }
                    if ($rent !== NULL && $rent === 1) {

                        $q->where('rented', 1);
                    }

                }
                $q->whereHas($category->type, function($q) use ($square){
                    if($square != NULL){
                        $q->whereBetween('total_square', [$square, $square + 50]);
                    }
                });
            })->count();

        return $count;
    }

    public static function getCountBySquare($cat_id, $rent = null, $square_ot = null, $square_do = null){

        $category = Category::where('id', $cat_id)->first();

        $count =  BaseObject::with($category->type)
            ->where('category_id' , $cat_id)
            ->where(function($q) use($cat_id, $rent, $square_ot,$square_do, $category){
                if($rent !== NULL && $rent !== 2){

                    if ($rent !== NULL && $rent === 0) {

                        $q->where('rented', 0);
                    }
                    if ($rent !== NULL && $rent === 1) {

                        $q->where('rented', 1);
                    }

                }
                $q->whereHas($category->type, function($q) use ($square_ot, $square_do, $category){

                    if($category->id == 4){

                        $q->whereBetween('land_square', [$square_ot, $square_do]);
                    } else {

                        $q->whereBetween('total_square', [$square_ot, $square_do]);
                    }
                });
            })->count();

        return $count;
    }
}
