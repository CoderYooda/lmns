<?php

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'TestController@index2')->name('test');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/apartments', 'ApartmentController@index')->name('apartments');
Route::get('/commercials', 'CommerceController@index')->name('commerces');
Route::get('/rooms', 'RoomController@index')->name('rooms');
Route::get('/garages', 'GarageController@index')->name('garages');
Route::get('/lands', 'LandController@index')->name('lands');
Route::get('/houses', 'HouseController@index')->name('houses');

Route::get('/apartment/{id}', 'ApartmentController@show')->name('apartment');
Route::get('/room/{id}', 'RoomController@show')->name('room');
Route::get('/house/{id}', 'HouseController@show')->name('house');
Route::get('/land/{id}', 'LandController@show')->name('land');
Route::get('/garage/{id}', 'GarageController@show')->name('garage');
Route::get('/commerce/{id}', 'CommerceController@show')->name('commerce');

Route::get('/search', 'ObjectController@searchResults')->name('search');

Route::post('/mapdata', 'MapController@loadObjects')->name('loadMapaData');

//Route::get('/map', 'MapController@map')->name('Map');

Route::post('/messtoagent', 'AgentController@messageToAgent')->name('messageToAgent');

Route::group(['prefix' => '/admin','middleware' => ['auth']], function () {
    Route::get('/', 'Admin\AdminController@dashboard')->name('DashBoard');
    Route::get('/agency', 'Admin\AgencyController@index')->name('Agency');
    Route::get('/agency/settings', 'Admin\AgencyController@settings')->name('AgencySettings');
    Route::get('/agency/filterobjects', 'Admin\AgencyController@filterobjects')->name('filterObjects');
    Route::post('/agency/settings', 'Admin\AgencyController@saveSettings')->name('AgencySettingsSave');

    Route::post('/agency/createuser', 'Admin\AgencyController@createUser')->name('createUser');
    Route::post('/agency/removeuser', 'Admin\AgencyController@removeUser')->name('removeUser');

    Route::get('/export', 'Admin\AgencyController@exportPage')->name('exportPage');
    Route::get('/export/generate', 'Admin\ExportController@exportGenerate')->name('exportGenerate');
    Route::post('/export/generateExportFile', 'Admin\ExportController@generateExportFile')->name('generateExportFile');

    Route::group(['prefix' => '/objects'], function () {
        Route::get('/create', 'Admin\ObjectController@getBaseForm')->name('getBaseForm');
        Route::post('/store', 'Admin\ObjectController@saveObject')->name('saveObject');
        Route::post('/switchexport', 'Admin\ObjectController@switchExport')->name('switchExport');
        Route::get('/', 'Admin\ObjectController@list')->name('listObjects');
    });

    Route::group(['prefix' => '/object/{id}'], function () {
        Route::get('/edit', 'Admin\ObjectController@editObject')->name('editObject');
        Route::post('/remove', 'Admin\ObjectController@removeObject')->name('removeObject');
    });

    Route::group(['prefix' => '/profile_{id}'], function () {
        Route::get('/', 'Admin\ProfileController@profile')->name('Profile');
        Route::get('/edit', 'Admin\ProfileController@edit')->name('EditProfile');
        Route::post('/edit', 'Admin\ProfileController@save')->name('ProfileSave');
    });

    Route::get('/secret/online', 'Admin\ProfileController@online')->name('online');

    Route::get('/exclusive', 'Admin\ObjectController@exclusiveList')->name('listExclusive');

    Route::post('/image_upload', 'Stock\ImageController@upload')->name('uploadImage');
    Route::post('/crop_image', 'Stock\ImageController@cropImage')->name('CropImage');
    Route::post('/remove_image', 'Stock\ImageController@remove')->name('removeImage');
    Route::post('/rotate_image', 'Stock\ImageController@rotateImg')->name('rotateImg');
    Route::post('/setindexes', 'Stock\ImageController@setIndexes')->name('setIndexes');

    Route::post('/password/change', 'Admin\ProfileController@changePassword')->name('ChangePassword');

    Route::get('/settings', 'Admin\AdminController@settings')->name('settings');
    Route::post('/streets', 'StreetController@getStreetsByCityId')->name('streets');

    Route::post('/storeagent', 'AgentController@storeagent')->name('storeAgent');
    Route::post('/storepassword', 'AgentController@storepassword')->name('storePassword');

    Route::get('/getfromsession_{parametr}', 'Admin\SessionController@readFromSession')->name('getsession');
    Route::post('/storetosession', 'Admin\SessionController@saveToSession')->name('storetosession');

});



Route::get('/sitemap.xml', 'HelperController@generateSitemap')->name('Sitemap');
Route::get('/{route}', 'RouteController@index')->name('route');
