<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть подтвержден и иметь более 6-ти символов',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отправилии Вам почту с инструкциями!',
    'token' => 'Токен смены пароля неверен.',
    'user' => "Система не может найти пользователя с таким e-mail.",

];
