require('choices.js/public/assets/scripts/choices.min');

import Choices from 'choices.js/public/assets/scripts/choices.min';

function Parallax(options){
    options = options || {};
    this.nameSpaces = {
        wrapper: options.wrapper || 'body',
        layers: options.layers || '.parallax-layer',
        deep: options.deep || 'data-parallax-deep'
    };
    this.init = function() {
        var self = this,
            parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
        for(var i = 0; i < parallaxWrappers.length; i++){
            (function(i){
                parallaxWrappers[i].addEventListener('mousemove', function(e){
                    var x = e.clientX,
                        layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);

                    for(var j = 0; j < layers.length; j++){
                        (function(j){
                            var deep = layers[j].getAttribute(self.nameSpaces.deep),
                                disallow = layers[j].getAttribute('data-parallax-disallow'),
                                itemX = (disallow && disallow === 'x') ? 0 : x / deep;
                            if(disallow && disallow === 'both') return;
                            //console.log(parseFloat(layers[j].style.backgroundPositionX) + itemX);
                            layers[j].style.backgroundPositionX  =  'calc(50% + ' + itemX + 'px)';
                        })(j);
                    }
                })
            })(i);
        }
    };
    this.init();
    return this;
}

window.addEventListener('load', function(){
    new Parallax();
    var category = document.getElementById('category');
    if(category) {
        var cat_select = new Choices(category, {
                itemSelectText: 'Выбрать',
                searchEnabled: false,
                classNames: {},
            }
        );
    }
    var ckind = document.getElementById('commece_type');
    if(ckind) {
        const ckind_select = new Choices(ckind, {
                itemSelectText: '',
                searchEnabled: false,
                classNames: {},
            }
        );
    }
    var cities = document.getElementById('cities');
    const cities_select = new Choices(cities, {
            itemSelectText: '',
            placeholderValue: 'Поиск...',
            noResultsText: 'Не найдено',
            shouldSort: false,
            searchEnabled: true,
            classNames: {
            },
        }
    );

    var commercetype = document.getElementById('commerce_type');

    const commerce_type = new Choices(commercetype, {
            itemSelectText: '',
            placeholderValue: 'Поиск...',
            noResultsText: 'Не найдено',
            searchEnabled: true,
            classNames: {
            },
        }
    );



    if(category && cat_select) {
        cat_select.passedElement.element.addEventListener('change', function (event) {
            //document.getElementById('filter-head').reset();
            var filter = document.querySelector('.filter-outer');
            filter.classList.forEach(function (elem) {
                if (~elem.indexOf("type")) {
                    filter.classList.remove(elem);
                }
            });
            filter.classList.add('type-' + event.detail.value);
        }, false);
    }
});

var costs = document.getElementsByClassName('cost');

for(var j = 0; j < costs.length; j++){
    phones[j] = function(event) {
        var target = event.target;
        target.value = target.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    };
}

