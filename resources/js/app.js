
require('./bootstrap');

require('jquery/dist/jquery.min');

require('select2/dist/js/select2.full');

require('jquery-ui-sortable-npm/jquery-ui-sortable');

require('bootstrap-datepicker/dist/js/bootstrap-datepicker');

require('./settings');

require('ymaps');

var debounce = require('debounce');

var Inputmask = require('inputmask');

window.searchOnMap = debounce(function(){
    searchGeocoder();
}, 600);







$( document ).ready(function() {


    var im = new Inputmask("9 999 999 99 99", {
        placeholder: "_ ___ ___ __ __",
        definitions: {
            "0": {
                validator: "[0-9\uFF11-\uFF19]"
            }
        }
    });
    im.mask("input[name='phone']");

    var price = new Inputmask("( 999){+|1}", {
        positionCaretOnClick: "radixFocus",
        autoUnmask: true,
        removeMaskOnSubmit: true,
        radixPoint: ",",
        _radixDance: true,
        numericInput: true,
        placeholder: "0",
        definitions: {
            "0": {
                validator: "[0-9\uFF11-\uFF19]"
            }
        }
    });

    price.mask(".price_mask");

    $(".select-init").select2({
        minimumResultsForSearch: 20,
        dropdownCssClass: 'bigdrop'
    });


    $('.datepicker-here').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        firstDay: 1,
            });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.checkMap = function(event){
    if($('#geopoint').val() == ''){
        $('html,body').animate({
            scrollTop: $("#map").offset().top - 200
        }, 'fast');
        $('#map').addClass('redrays');
        event.preventDefault();
    }
};


