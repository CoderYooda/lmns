
require('../bootstrap');
require('jquery/dist/jquery.min');

require('select2/dist/js/select2.full');

$( document ).ready(function() {

});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.toggleMainMenu = function(){
    $('#main_menu').toggleClass('show');
}


$(".select-init").select2({
    minimumResultsForSearch: 20,
    dropdownCssClass: 'bigdrop'
});

$('#category').on('select2:select', function (e) {
    var data = e.params.data;
    $('#filter-head').removeClassWild('type-*');
    $('#filter-head').addClass('type-'+e.params.data.id);
    console.log(data);
});



(function($) {
    $.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
})(jQuery);
