
require('../bootstrap');
import Slick from 'slick-carousel';
/*var jQuery = $;
import fancybox from '@fancyapps/fancybox';*/

/*$.fancybox = fancybox;*/
window.jQuery = $;
require ('@fancyapps/fancybox');
require('select2/dist/js/select2.full');


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#form_apartment").on('submit', function() {

    let fields = [
        'price_from', 'price_to', 'square_from', 'square_to', 'floor_from', 'floor_to'
    ];

    for (let i = 0; i < fields.length; ++i) {

        let $el = $(`#${fields[i]}`);

        if (!$el.val().length) {

            $el.prop('disabled', true);
        }
    }

    return true;
});

$(".open-phone").on('click', function(){
    $(".phone-toggle").slideToggle(500);
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).html('Скрыть контакты');
    } else {
        $(this).html('Показать контакты');
    }
});


$("#room .room-item").on('click', function(){
    $("#room .room-item.active").removeClass("active");
    $(this).addClass("active");
});
$("#house .house-item").on('click', function(){
    $("#house .house-item.active").removeClass("active");
    $(this).addClass("active");
});






$(document).ready(function() {





    $(".select-init").select2({
        dropdownAutoWidth : true,
        width: 'auto',
        minimumResultsForSearch: 20,
        dropdownCssClass: 'bigdrop'
    });

    $('#filter-wraper').removeClassWild('type-*');
    $('#filter-wraper').addClass('type-'+$('#category').val());




    // if($('#sold-input').prop("checked")){
    //     $('#sold-tab').addClass('active');
    //     $('#rent-tab').removeClass('active');
    // }else if($('#rent-input').prop("checked")){
    //     $('#rent-tab').addClass('active');
    //     $('#sold-tab').removeClass('active');
    // }else{
    //     $('#sold-tab').addClass('active');
    //     $("#sold-input").prop("checked", true);
    // }
});

$('#category').on('select2:select', function (e) {
    var data = e.params.data;
    $('#filter-wraper').removeClassWild('type-*');
    $('#filter-wraper').addClass('type-'+e.params.data.id);
    console.log(data);
});

// $("#filter-head #sold-tab").on('click', function(){
//     $(this).addClass('active');
//     $('#rent-tab').removeClass('active');
//     $("#sold-input").prop("checked", true);
//     $("#rent-input").prop("checked", false);
// });
//
// $("#filter-head #rent-tab").on('click', function(){
//     $(this).addClass('active');
//     $('#sold-tab').removeClass('active');
//     $("#sold-input").prop("checked", false);
//     $("#rent-input").prop("checked", true);
// });


(function($) {
    $.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
})(jQuery);
