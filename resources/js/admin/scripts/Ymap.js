

class Ymap{

    init() {
        window.map = new ymaps.Map("map", {
            center: [50.597467, 36.588849],
            zoom: 10,
            controls: ['zoomControl']
        });

        if(issetobject){
            window.myPlacemark = yandexmmaps.createPlacemark(object_point);
            map.geoObjects.add(window.myPlacemark);
            map.setCenter(object_point, 16, {
                checkZoomRange: true
            });
        }

        map.events.add('click', function (e) {
            var coords = e.get('coords');
            console.log(coords);
            //yandexmmaps.geocoding(coords);
            if (window.myPlacemark) {
                window.myPlacemark.geometry.setCoordinates(coords);
                window.myPlacemark.properties.set('iconCaption', $('input[name="title"]').val());
            } else {
                window.myPlacemark = yandexmmaps.createPlacemark(coords);
                map.geoObjects.add(window.myPlacemark);
            }
            $('#geopoint').val(coords);
            yandexmmaps.addDragEvent(window.myPlacemark);
        });
        if (window.myPlacemark) {
            yandexmmaps.addDragEvent(window.myPlacemark);
        }
    };

    createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/images/map64.png',
            iconImageSize: [64, 64],
            iconImageOffset: [-32,-64],
            draggable: true
        });
        map.geoObjects.add(window.myPlacemark);
    }

    geocoding(coords){
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            $('#street').val(firstGeoObject.getThoroughfare());
            $('#house_number').val(firstGeoObject.getPremiseNumber());
        });
    };

    addDragEvent(Placemark){
        Placemark.events.add('dragend', function (e) {
            var coords = Placemark.geometry.getCoordinates();
            $('#geopoint').val(coords);
            //yandexmmaps.geocoding(coords);
        });
    }

    searchGeocoder(){
        var city = $('#city option:selected').text();

        if (city == "Не указано"){
            city = '';
        }
        var house = $('#house_number').val();
        var street = $('#street').val();
        var searchstring = 'Белгородская область ' + ' ' + city + ' ' + street + ' ' + house;
        ymaps.geocode(searchstring, {
            results: 1
        }).then(function (res) {

            var firstGeoObject = res.geoObjects.get(0),
                coords = firstGeoObject.geometry.getCoordinates(),
                bounds = firstGeoObject.properties.get('boundedBy');

            map.setBounds(bounds, {
                checkZoomRange: true
            });

            if (window.myPlacemark) {
                window.myPlacemark.geometry.setCoordinates(coords);
                window.myPlacemark.properties.set('iconCaption', $('input[name="title"]').val());
            } else {
                window.myPlacemark = yandexmmaps.createPlacemark(coords);
                $('#map').removeClass('redrays');
                map.geoObjects.add(window.myPlacemark);
            }

            $('#geopoint').val(coords);
            $('#geopoint').val(coords);
            yandexmmaps.addDragEvent(window.myPlacemark);
        });
    };
}

export default Ymap;
