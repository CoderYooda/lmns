

class Agency{

    createRequest() {
        var request;
        if (window.XMLHttpRequest) request = new XMLHttpRequest();
        else if (window.ActiveXObject) {
            try {
                request = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e){}
            try {
                request = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (e){}
        }
        return request;
    }
    saveSettings(form, e){
        e.preventDefault();
        $.ajax({
            url: $( form ).attr('action'),
            type: $( form ).attr('method'),
            data:  $( form ).serialize(),
            success: function(resp){
                notie.alert({ text: 'Настройки сохранены' });
                $('.tp_rofile').show();
            },
        });
    }

    createAgent(form, e){
        console.log(1);
        $('.invalid-feedback').remove();
        $('input').removeClass('is-invalid');
        $(form).find('button').attr('disabled', true);
        e.preventDefault();
        $.ajax({
            url: $( form ).attr('action'),
            type: $( form ).attr('method'),
            data:  $( form ).serialize(),
            success: function(resp){
                console.log(resp);
                $.each(resp[0], function( index, value ) {
                    var err = '<span class="invalid-feedback" style="margin-bottom: 16px" role="alert"> <strong>'+value+'</strong>';
                    $('input[name='+index+']').after(err);
                    $('input[name='+index+']').addClass('is-invalid');
                });
                if(resp.status == 'success'){
                    $('#m').modal('hide');
                }
                $(form).find('button').attr('disabled', false);
            },
        });
    }

    deleteAgent(form, e){
        $.ajax({
            url: $( form ).attr('action'),
            type: $( form ).attr('method'),
            data:  $( form ).serialize(),
            success: function(resp){
                $('*[data-id="item-'+resp.agent.id+'"]').remove();
            },
        });
    }

    checkSquare(elem){
        if(elem.value == '' || elem.value == 'undefined'){
            $('#square_from').prop('disabled', true);
            $('#square_to').prop('disabled', true);
        } else {
            $('#square_from').prop('disabled', false);
            $('#square_to').prop('disabled', false);
        }
        console.log(elem.value);
    }

    applyFilter(form, e){
        e.preventDefault();
        //$('#filtered_objects').addClass('preload');
        pages.setUrl(window.location.search);
        var container = $('#filtered_objects');

        $.ajax({
            url: $( form ).attr('action'),
            type: $( form ).attr('method'),
            data:  $( form ).serialize(),
            success: function(resp){
                container.html(resp.data);
                $('#filtered_objects').removeClass('preload');
                //$('input[name=square_max]').val(resp.params.square_max);
            },
        });
    }

    setParam(elem, e){
        this.applyFilter($(elem).closest('form'), e);
    }



}

export default Agency;
