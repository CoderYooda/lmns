

class Pages{
    ajaxLoad(url, e){
        e.preventDefault();
        $('.ajax_preloder').addClass('preload');
        this.setUrl();
        $.ajax({
            url: url,
            type: 'GET',
            success: function(resp){
                var container = $(resp.destination);
                container.html(resp.data);
                $('.ajax_preloder').removeClass('preload');
                //pages.setUrl(url);
            },
        });
    }

    setUrl(url){
        var query = (url || '?').substr(1),
            map = {};
        query.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function (match, key, value) {
            map[key] = value;
        });

        $( '#object-filter' ).serialize().replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function (match, key, value) {
            map[key] = value;
        });

        var str = "";
        for (var key in map) {
            if (str != "") {
                str += "&";
            }
            str += key + "=" + encodeURIComponent(map[key]);
        }
        //history.pushState(null, null, window.location.origin + window.location.pathname + '?' + str);
    }
}

export default Pages;
