

class Upload{

    removeImage(elem){

        var elem = $(elem).closest('.image-contracter');
        console.log(elem.data('id'));
        $.ajax({
            url:'/admin/remove_image',
            data: {id:elem.data('id')},
            dataType:'json',
            async:false,
            type:'post',
            success:function(response){
                elem.remove();
            },
        });
    }

    rotateImage(elem){
        var elem = $(elem).closest('.image-contracter');
        $.ajax({
            url:'/admin/rotate_image',
            data: {id:elem.data('id')},
            dataType:'json',
            async:false,
            type:'post',
            success:function(response){
                $('#'+response.id).find('img').attr('src', response.thumb_url);
            },
        });
    }


    images(url, indexesurl) {

        var progressBar = $('#progressbar');
        $.ajax({
            url:url,
            data:new FormData($("#interform")[0]),
            async: true,
            cache: false,
            type:'post',
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#imginfo').html('');
                progressBar.slideDown('fast');
                progressBar.find('.progress-bar').css({width: "0%"});
                progressBar.find('.progress-bar').text("Загрузка 0%");
            },
            xhr: function()
            {
                var xhr = new window.XMLHttpRequest();
                // прогресс загрузки на сервер
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        // делать что-то...
                        var percents = percentComplete * 100
                        progressBar.find('.progress-bar').css({width: percents+"%" });
                        progressBar.find('.progress-bar').text("Загрузка "+parseInt(percents)+"%");
                        if(percents == 100){
                            progressBar.find('.progress-bar').text("Загрузка завершена, обработка изображений...");
                        }
                    }
                }, false);
                return xhr;
            },
            success:function(response){

                $.each(response, function (index, value) {
                    var elem = '' +
                    '<div data-id="'+ value.id +'" class="col-6 col-sm-4 col-md-3 image-contracter">'+
                        '<div id="'+ value.id +'" class="box mt-2 mb-2 overflow-hidden">'+
                            '<input type="hidden" name="pic[]" value="'+ value.id +'">'+
                            '<img src="'+ value.thumb_url +'" alt="" class="w-100">'+
                            '<div class="p-2"><div class="text-xs">'+
                                '<i onclick="upload.rotateImage(this)" class="fa-button fa fa-undo"></i>'+
                                '<i onclick="upload.removeImage(this)" class="fa-button fa fa-trash"></i>'+
                            '</div></div>'+
                        '</div>'+
                    '</div>';

                    $('#images').append(elem);
                });
                sortable('#images', {
                    forcePlaceholderSize: true,
                    placeholderClass: 'col-6 col-sm-4 col-md-3 sortable-placeholder'
                });
                sortable('#images')[0].addEventListener('sortstart', function(e) {
                    $('#images').addClass('dragged');
                });

                sortable('#images')[0].addEventListener('sortstop', function(e) {
                    var serial = sortable('#images', 'serialize');

                    var sort = [];

                    serial[0].children.forEach(function(elem){
                        sort.push($(elem).data('id'));
                    });

                    $.ajax({
                        url:'/admin/setindexes',
                        data: {id:sort},
                        dataType:'json',
                        async:true,
                        type:'post',
                        success:function(response){
                            $('#images').removeClass('dragged');
                        },
                    });
                });

                progressBar.hide();
                progressBar.find('.progress-bar').css({width: "0%" });

            },
            error:function(response){
                if(response == 'undefined'){
                    $('#preloader').removeClass('show');
                } else {
                    var mess = '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
                        '<strong>Внимание!</strong> ' + response.responseJSON.message +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div>';
                    $('#imginfo').html(mess);
                    $('#preloader').removeClass('show');
                }
                progressBar.hide();
            }
        });
    }
}

export default Upload;
