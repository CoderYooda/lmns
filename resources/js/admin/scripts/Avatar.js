
var Croppr = require('croppr');

class Avatar{
    uploadImage(input){
        var form = input.closest('form');

        var data = new FormData();
        $.each($('#file_upload')[0].files, function(i, file) {
            data.append('image[]', file);
        });


        $.ajax({
            url: $( form ).attr('action'),
            type: $( form ).attr('method'),
            data:  data,
            success: function(response){
                $('#croppr-container').empty();
                response = response[0];
                var crop = document.createElement('img');
                crop.setAttribute("src", response.url);
                crop.setAttribute("id", 'croppr');

                document.getElementById('croppr-container').appendChild(crop);

                $('#pick-button').hide();
                $('#save-button').show();
                $('#another-button').show();

                // var crcontainer = document.getElementsByClassName('croppr-container');
                // while(crcontainer.length > 0){
                //     crcontainer[0].parentNode.removeChild(crcontainer[0]);
                // }



                // document.getElementById('upload_form_modal').setAttribute("style", "display:none;");
                // document.getElementById('crop_form_modal').setAttribute("style", "display:block;");
                //document.getElementById('croppr').setAttribute("src", response.url);



                var croppr = new Croppr('#croppr', {
                    startSize: [90, 90],
                    minSize: [50, 50, 'px'],
                    aspectRatio: 1,
                    onCropStart: function(){
                        // document.getElementById('crop_form_modal').classList.add('moving');
                        // document.getElementById('modal-container').classList.add('freeze');
                        // clearTimeout(window.freezeTimer);
                    },
                    onCropEnd: function(value) {
                        window.cropdata = {'url':response.url, 'filename':response.filename, 'coords' : value};
                        // document.getElementById('crop_form_modal').classList.remove('moving');
                        // window.freezeTimer = setTimeout(function() { document.getElementById('modal-container').classList.remove('freeze') }, 3000);
                    },
                    onInitialize: function(instance) {
                        window.cropdata = {'url':response.url, 'filename':response.filename, 'coords' : instance.getValue()};
                    }
                });
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    uploadAvatar(id){
        document.getElementById(id).click();
    }

    cropImage(cropdata){

        $.ajax({
            url: '/admin/crop_image',
            type: 'POST',
            data:  cropdata,
            success: function(response){
                //console.log(response.avatar.url);
                $('.user-thumb').attr('src', response.avatar.url);
                $('.tp_rofile').show();
            },
        });
    }
}
export default Avatar;
