class Object{


    remove(form, e){
        e.preventDefault();
        var als = confirm('Вы уверены?');
        if(als){
            $.ajax({
                url: $( form ).attr('action'),
                type: $( form ).attr('method'),
                data:  $( form ).serialize(),
                success: function(resp){
                    if(resp.status == 'success'){
                        $(form).find('button').tooltip('dispose');
                        $(form).closest('.obj-container').remove();
                    }
                },
            });
        }
    }

    loadStreets(city_id, url, e){
        e.preventDefault();
        var current = $('#street_help').val();
        $.ajax({
            url: url,
            type: 'POST',
            data:  {id:city_id, current:current},
            success: function(resp){
                $("#street").empty()
                $('#street').append(resp.data);
            },
        });
    }
}

export default Object;
