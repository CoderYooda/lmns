
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// $(window).bind('popstate', function(event){
//     console.log(location);
//     pages.ajaxLoad(location.search, event);
// });

$( document ).ajaxComplete(function(event, request, settings) {

    var req = request.responseJSON;
    if(req != undefined) {
        if (req.status == 'success' && req.mess == undefined) {
            notie.alert({type: 'success', text: req.message});
            $("input").removeClass('is-invalid');
        }

        if (req.status == 'error'  && req.mess == undefined) {

            if (typeof req.message == 'object') {
                $.each(req.message, function (key, elem) {
                    $("input[name='" + key + "']").addClass('is-invalid');
                    $.each(elem, function (i, mess) {
                        notie.alert({type: 'error', text: mess});
                    });
                });
            } else {
                notie.alert({type: 'error', text: req.message});
            }
        }
    }
});


$('#city').on('change', function(){
    console.log($(this).val());
    if($(this).val() != 51428) {
        $('#area').val('0');
        $('#area').prop("disabled", true);
    } else{
        $('#area').prop("disabled", false);
    }
});



$('input[name="folded"]').on('click', function(){
    setTimeout(function(){
        $('.form-control').each(function() {
            var formGroup = $(this),
                formgroupWidth = formGroup.parent('div').width();
            formGroup.siblings('.select2-container').css('width', formgroupWidth);
        });
    }, 20);
});

require('ymaps');
var debounce = require('debounce');

$('select').on('change', function(){
    if(typeof map !== 'undefined'){
        setTimeout(function(){
            map.container.fitToViewport();
        }, 2);
    }
});

import Agency from './scripts/Agency.js';
import Server from './scripts/Server.js';
import Profile from './scripts/Profile.js';
import Session from './scripts/Session.js';
import Avatar from './scripts/Avatar.js';
import Ymap from './scripts/Ymap.js';
import Upload from './scripts/Upload.js';
import Object from './scripts/Object.js';
import Pages from './scripts/Pages.js';


window.agency = new Agency();
window.profile = new Profile();
window.session = new Session();
window.avatar = new Avatar();
window.server = new Server();
window.yandexmmaps = new Ymap();
window.upload = new Upload();
window.object = new Object();
window.pages = new Pages();

var Inputmask = require('inputmask');

window.searchGeocoder = debounce(function(){
    yandexmmaps.searchGeocoder();
}, 600);


$( document ).ready(function() {

    var price = new Inputmask("( 999){+|1}", {
        positionCaretOnClick: "radixFocus",
        autoUnmask: true,
        removeMaskOnSubmit: true,
        radixPoint: ",",
        _radixDance: true,
        numericInput: true,
        placeholder: "0",
        definitions: {
            "0": {
                validator: "[0-9\uFF11-\uFF19]"
            }
        }
    });

    $('.select_cat_first').on('click', function(e){
        if($(this).find('input').is(':disabled')){
            alert('Выберите тип объекта');
        }
    });

    price.mask(".price_mask");

    var im = new Inputmask("+9(999)999-99-99", {
        placeholder: "_ ___ ___ __ __",
        definitions: {
            "0": {
                validator: "[0-9\uFF11-\uFF19]"
            }
        }
    });
    im.mask("input[name='phone']");

    checkProps();
    rentedProps();

    $(document.body).on("change","#rentedon",function(){
        rentedProps();
    });
    $(document.body).on("change","#rentedoff",function(){
        rentedProps();
    });

    $(document.body).on("change","#exlusive",function(){
        checkProps();
    });
    $(document.body).on("change","#writed",function(){
        checkProps();
    });


});


// window.onpopstate = function(event) {
//     console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
//     agency.paginate(document.location, event)
//     //event.preventDefault();
//     //event.stopImmediatePropagation();
// };

function checkProps(){
    if($('#exlusive').prop('checked')){
        $('#under_exclusive').show();
    } else {
        $('#writed').prop('checked', false);
        $('#under_exclusive').hide();
    }

    if($('#writed').prop('checked')){
        $('#under_writed').show();
    } else {
        $('#exlusive_date').val('');
        $('#under_writed').hide();
    }
}

function rentedProps(){
    if($('#rentedon').prop('checked')){
        $('#rented-box').show();
        $('#selded-box').hide();
        $('#rent_price').prop('required',true);
        $('#price').prop('required',false);
    }

    if($('#rentedoff').prop('checked')){
        $('#selded-box').show();
        $('#rented-box').hide();
        $('#price').prop('required',true);
        $('#rent_price').prop('required',false);
    }
}

$('#content-main').on('click', '.ajax a', function(e){
    pages.ajaxLoad($(this).attr('href'), e);
});


