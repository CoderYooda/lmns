//$.fn.datepicker.dates['en'] = {
    //days: ["пон", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    //daysShort: ["пон", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    //daysMin: ["Вс", "Пон", "Вт", "Ср", "Чт", "Пт", "Суб"],
   // months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "123"],
   // monthsShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
   // weekStart: 1
//};

$.fn.datepicker.dates['en'] = {
    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    daysMin: ["Вс", "Пон", "Вт", "Ср", "Чт", "Пт", "Суб"],
    months: ["Янвварь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    monthsShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
    today: "Today",
    clear: "Clear",
    format: "mm/dd/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 1
};
