@extends('layouts.admin')

@section('content')
    <div id="object-list" class="pb-5 my-5">
        <div class="container">
            <h1 style="font-size: 26px; margin-bottom: 20px">Персональные настройки</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="settings-wrapper">
                        <div class="settings-options">

                            <a href="{{route('settings', ['personal' => true])}}" class="settings-item @if(request('personal')) active @endif">
                                <div>
                                    <i class="material-icons">bookmark</i>
                                    <span>Личная информация</span>
                                </div>
                            </a>

                            <a href="{{route('settings', ['password' => true])}}" class="settings-item @if(request('password')) active @endif">
                                <div>
                                    <i class="material-icons">bookmark</i>
                                    <span>Аккаунт</span>
                                </div>
                            </a>
                        </div>
                        <div class="option-window w-100" style="padding: 0 15px;">
                            @if(request('password'))
                                <div>
                                    @if (\Session::has('message'))
                                        <div class="alert alert-success" role="alert">
                                            {!! \Session::get('message') !!}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <form action="{{route('storePassword')}}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{$agent->id}}" name="id">
                                        <div class="form-group">
                                            <label for="password" class="col-form-label text-md-right">Новый пароль</label>
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>


                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="password-confirm">Подтверждение пароля</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </form>
                                </div>
                            @else
                                <div>
                                    @if (\Session::has('message'))
                                        <div class="alert alert-success" role="alert">
                                            {!! \Session::get('message') !!}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <form action="{{route('storeAgent')}}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{$agent->id}}" name="id">

                                        <div class="form-group">
                                            <label for="lastname">Фамилия</label>
                                            <input value="{{$agent->lastname}}" required name="lastname" class="form-control" id="lastname" placeholder="Введите вашу фамилию">
                                            @if ($errors->has('lastname'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="firstname">Имя</label>
                                            <input value="{{$agent->firstname}}" required name="firstname" class="form-control" id="firstname" placeholder="Введите ваше имя">
                                            @if ($errors->has('firstname'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('firstname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="midname">Отчество</label>
                                            <input value="{{$agent->midname}}" required name="midname" class="form-control" id="midname" placeholder="Введите ваше отчество">
                                            @if ($errors->has('midname'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('midname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Номер телефона</label>
                                            <input value="{{$agent->phone}}" name="phone" required class="form-control" id="phone" placeholder="Введите номер телефона">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">E-mail</label>
                                            <input value="{{$agent->email}}" type="email" required name="email" class="form-control" id="email" placeholder="Введите ваш email">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>



                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .settings-wrapper {
            width: 100%;
            height: 100%;
            position: relative;
            display: flex;
            flex-direction: row;
            flex-grow: 1;
        }
        .settings-options {
            background-color: #fff;
            width: 370px;
            border-right: 2px solid rgba(153,161,182,.16);
        }
        .settings-item {
            display: block;
            border-bottom: 1px solid rgba(153,161,182,.24);
            margin-right: -2px;
        }
        .settings-item div {
            display: flex;
            flex-direction: row;
            padding: 20px 24px;
        }
        .settings-item div span {
            color: #525252;
            font-family: "Open Sans";
            font-size: 14px;
            line-height: 22px;
            letter-spacing: .44px;
            white-space: nowrap;
            margin: auto 0;
        }
        .settings-item.active, .settings-item:hover {
            text-decoration: none;
            background-image: linear-gradient(to left,rgba(100,149,254,.08) 15%,rgba(253,253,253,.08) 78%);
            border-right: 2px solid #6495fe;
        }
        .settings-item.active div i, .settings-item:hover div i {
            color: #fff;
            box-shadow: 0 1px 10px rgba(100,149,254,.5);
            background-color: #6495fe;
        }
        .settings-item div i {
            padding: 12px;
            border-radius: 50%;
            font-size: 16px;
            color: #99a1b6;
            background-color: #f7f7f7;
            margin: auto 16px auto 0;
        }
    </style>
@endpush
