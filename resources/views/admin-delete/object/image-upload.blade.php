<div class="row py-3 mb-4 bg-grey">
    <div class="col-sm-12" id="imginfo"></div>
    <div class="col-md-8">
        <div id="images">
            @if(isset($object))
                @foreach($object->images as $pic)
                    <div id="{{$pic->id}}" data-id="{{$pic->id}}" class="picbox"><span class="rotate"><i class="fa fa-undo" aria-hidden="true"></i></span><span class="delete"><i class="fa fa-times" aria-hidden="true"></i></span><img src="{{$pic->thumb_url}}"><input type="hidden" name="pic[]" value="{{$pic->id}}"></div>
                @endforeach
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <input type="file" name="image[]" style="height: auto;" class="form-control" multiple="multiple" id="image">
    </div>
</div>
