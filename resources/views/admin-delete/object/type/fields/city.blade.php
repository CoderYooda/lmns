<p class="label">Город</p>
<select data-plugin="select2" data-option="{}" name="city_id" id="city" class="form-control select-init" required>
    <option value="">Не указано</option>
    @foreach($params->city as $city)
        <option value="{{$city->id}}" @if(isset($object) && $object->city_id == $city->id)selected="selected"@endif>{{$city->title}}</option>
    @endforeach
</select>
{{--onchange="searchOnMap()"--}}
