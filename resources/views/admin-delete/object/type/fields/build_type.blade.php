<p class="label">Тип здания</p>
<select name="build_type_id" id="build_type_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->build_type as $build_type)
        <option value="{{$build_type->id}}"
                @if(isset($object) && $object->apartment->build_type_id == $build_type->id)selected="selected"@endif
        >{{$build_type->title}}</option>
    @endforeach
</select>
