<p class="label">Отопление</p>
<select name="heatings[]" id="heating" class="form-control select-init" multiple="multiple">
    <option value="">Не указано</option>
    @foreach($params->heating as $heating)
        <option value="{{$heating->id}}"
                @if(isset($object))
                @foreach($object->apartment->heatings->pluck('id') as $h)
                @if($h == $heating->id)
                selected="selected"
            @endif
            @endforeach
            @endif
        >{{$heating->title}}</option>
    @endforeach
</select>
