<p class="label">Готовность</p>
<select name="readiness_id" id="readiness" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->readiness as $readiness)
        <option value="{{$readiness->id}}"
                @if(isset($object) && $object->apartment->readiness_id == $readiness->id)selected="selected"@endif
        >{{$readiness->title}}</option>
    @endforeach
</select>
