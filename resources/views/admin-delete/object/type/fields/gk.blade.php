<p class="label">Жилищный комплекс</p>
<select name="gk_id" id="zh_complex" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->gk as $gk)
        <option @if(isset($object) && $object->apartment->gk_id == $gk->id)selected="selected"@endif value="{{$gk->id}}">{{$gk->title}}</option>
    @endforeach
</select>
