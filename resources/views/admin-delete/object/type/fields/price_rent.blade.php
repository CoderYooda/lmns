<div class="row" style="padding: 20px; padding-top: 0px;">
    <div class="col-sm-6">
        <div class="form-check">
            <input class="form-check-input" type="radio" name="rented" id="rentedon" style="width: auto;" value="1" @if(isset($object) && $object->rented)checked="checked"@endif>
            <label class="form-check-label" for="rentedon">
                Аренда
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-check">
            <input class="form-check-input" type="radio" name="rented" id="rentedoff" style="width: auto;" value="0" @if(isset($object) && !$object->rented)checked="checked"@endif @if(!isset($object)) checked="checked" @endif>
            <label class="form-check-label" for="rentedoff">
                Продажа
            </label>
        </div>
    </div>
</div>

<div id="rented-box">
    <p class="label">Цена в месяц</p>
    <input id="rent_price" maxlength="10" value="@if(isset($object)){{$object->rent_price}}@endif" name="rent_price" placeholder="Цена в месяц" class="mb-2 price_mask">
</div>

<div id="selded-box">
    <p class="label">Цена р.</p>
    <input id="price" maxlength="14" value="@if(isset($object)){{$object->price}}@endif"  name="price" placeholder="Цена р." class="mb-2 price_mask">
</div>

<p class="label">MLS</p>
    <input min="0" id="mls" maxlength="10" value="@if(isset($object)){{$object->mls}}@endif" name="mls"  placeholder="MLS" class="mb-2 price_mask">
