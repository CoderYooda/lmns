<p class="label">Материал стен</p>
<select name="wall_materials[]" id="wall_materials" class="form-control select-init" multiple="multiple">
    <option value="">Не указано</option>
    @foreach($params->apartment_wall_material as $wall_material)
        <option value="{{$wall_material->id}}"
                @if(isset($object))
                    @foreach($object->{$object->category->type}->wall_materials->pluck('id') as $wm)
                        @if($wm == $wall_material->id)
                        selected="selected"
                        @endif
                    @endforeach
                @endif
        >{{$wall_material->title}}</option>
    @endforeach
</select>
