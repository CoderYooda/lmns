<p class="label">Доп. параметры</p>
<input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox" @if(isset($object) && $object->notary)checked="checked"@endif>
<label for="notarius" class="css-label mb-0">Нотариус</label>

<input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox" @if(isset($object) && $object->custody)checked="checked"@endif>
<label for="custody" class="css-label mb-0">Опека</label>

<input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox" @if(isset($object) && $object->burden)checked="checked"@endif>
<label for="obremenenie" class="css-label mb-0">Обременение</label>

<input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox" @if(isset($object) && $object->exlusive)checked="checked"@endif>
<label for="exlusive" class="css-label mb-0">Эксклюзив</label>
<div id="under_exclusive" style="display: none;">
    <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox" @if(isset($object) && $object->writed)checked="checked"@endif>
    <label for="writed" class="css-label mb-0">Письменно</label>
    <div id="under_writed" style="display: none;">
        <input type="text" id="exlusive_date" name="exlusive_date" class="datepicker-here"  value="@if(isset($object)){{$object->exlusive_date}}@endif" data-position="top center" placeholder="Эксклюзив дата" />
    </div>
</div>
