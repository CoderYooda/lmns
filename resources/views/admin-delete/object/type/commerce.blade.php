<div class="body-creat-object">
    <div class="container">111
        <?php if(isset($object)){$object->commercemeta = $object->commerce->where('object_id',$object->id)->with('wall_materials', 'build_comforts', 'specials', 'adds','doing', 'gruz', 'kind', 'purpose', 'sklad', 'tools', 'type')->first();} ?>
            <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="commerce">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row bg-grey">
                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-6 pr-1">
                                    @include('template.admin.common.object.type.fields.floor')
                                </div>
                                <div class="col-6 pl-1">
                                    @include('template.admin.common.object.type.fields.n_of_floor')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.commerce_type')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('template.admin.common.object.type.fields.year')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height: 100%; min-height: 300px"></div>
                </div>


                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin-delete.object.type.fields.price_rent')
                        </div>
                        <div class="col-md-6">
                            @include('admin-delete.object.type.fields.dop_params')
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">

                        <div class="col-md-6 mb-2">
                            <p class="label">Состояние</p>
                            <select name="state_id" id="sostoyanie" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->state as $state)
                                    <option value="{{$state->id}}"
                                            @if(isset($object) && $object->commerce->state_id == $state->id)selected="selected"@endif
                                    >{{$state->title}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-6 mb-2">
                            <p class="label">Вид деятельности</p>
                            <select name="doing_id" id="doing_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->commerce_doing as $doing)
                                    <option value="{{$doing->id}}"
                                            @if(isset($object) && $object->commerce->doing_id == $doing->id)selected="selected"@endif
                                    >{{$doing->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6 mb-2">
                            <p class="label">Погруз - разгруз</p>
                            <select name="gruz_id" id="gruz_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->commerce_gruz as $gruz)
                                    <option value="{{$gruz->id}}"
                                            @if(isset($object) && $object->commerce->gruz_id == $gruz->id)selected="selected"@endif
                                    >{{$gruz->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6 mb-2">
                            <p class="label">Вид хранения</p>
                            <select name="sklad_id" id="sklad_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->commerce_sklad as $sklad)
                                    <option value="{{$sklad->id}}"
                                            @if(isset($object) && $object->commerce->sklad_id == $sklad->id)selected="selected"@endif
                                    >{{$sklad->title}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-6">
                            <p class="label">Кол-во помещений</p>
                            <input id="rooms" value="@if(isset($object)){{$object->commerce->rooms}}@endif" name="rooms" type="number" placeholder="Кол-во помещений" class="mb-2" required>
                        </div>

                        <div class="col-md-6 mb-2">
                            <p class="label">Вид здания</p>
                            <select name="kind_id" id="kind_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->commerce_kind as $commerce_kind)
                                    <option value="{{$commerce_kind->id}}"
                                            @if(isset($object) && $object->commerce->kind_id == $commerce_kind->id)selected="selected"@endif
                                    >{{$commerce_kind->title}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-12 mb-2">
                            <p class="label">Назначение здания</p>
                            <select name="purpose_id" id="tip_kvartiri" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->commerce_purpose as $commerce_purpose)
                                    <option value="{{$commerce_purpose->id}}"
                                            @if(isset($object) && $object->commerce->purpose_id == $commerce_purpose->id)selected="selected"@endif
                                    >{{$commerce_purpose->title}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-md-4">
                            <p class="label">S Объекта</p>
                            <input id="obshaya" value="@if(isset($object)){{$object->commerce->total_square}}@endif" name="total_square" type="number" placeholder="S Объекта м²" class="mb-2" required>
                        </div>

                        <div class="col-md-4">
                            <p class="label">S Участка</p>
                            <input id="zhilaya" value="@if(isset($object)){{$object->commerce->sector_square}}@endif" name="sector_square" type="number" placeholder="S Участка м²" class="mb-2">
                        </div>

                        <div class="col-md-4">
                            <p class="label">Высота</p>
                            <input id="zhilaya" value="@if(isset($object)){{$object->commerce->height}}@endif" name="height" type="number" placeholder="Высота в м" class="mb-2">
                        </div>



                    </div>
                </div>
            </div>
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')


            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse" data-target="#ac_blag_zdaniya">
                                    Благоустройство здания и территории
                                </button>
                            </div>
                            <div id="ac_blag_zdaniya" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_build_comfort as $build_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="build_comforts[]" value="{{$build_comfort->id}}" id="build_comfort{{$build_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->commerce->build_comforts->pluck('id') as $b)
                                                       @if($b == $build_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="build_comfort{{$build_comfort->id}}" class="css-label">{{$build_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Особенности
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse " data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_special as $specials)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="specials[]" value="{{$specials->id}}" id="specials{{$specials->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->commerce->specials->pluck('id') as $l)
                                                       @if($l == $specials->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="specials{{$specials->id}}" class="css-label">{{$specials->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_adds">
                                    Доп характеристики
                                </button>
                            </div>
                            <div id="ac_adds" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_adds as $commerce_add)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="adds[]" value="{{$commerce_add->id}}" id="add{{$commerce_add->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->commerce->adds->pluck('id') as $c)
                                                       @if($c == $commerce_add->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="add{{$commerce_add->id}}" class="css-label">{{$commerce_add->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_tools">
                                    Оборудование
                                </button>
                            </div>
                            <div id="ac_tools" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_tools as $commerce_tool)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="tools[]" value="{{$commerce_tool->id}}" id="tool{{$commerce_tool->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->commerce->tools->pluck('id') as $c)
                                                       @if($c == $commerce_tool->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="tool{{$commerce_tool->id}}" class="css-label">{{$commerce_tool->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
    </div>
</div>

