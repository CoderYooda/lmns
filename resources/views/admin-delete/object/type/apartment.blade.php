
        <?php if(isset($object)){$object->apartmentmeta = $object->apartment->where('object_id',$object->id)->with('wall_materials', 'repairs', 'layouts', 'heatings')->first();} ?>
            <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="apartment">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row ">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    @include('admin-delete.object.type.fields.floor')
                                </div>
                                <div class="col-md-6 pl-1">
                                    @include('admin-delete.object.type.fields.n_of_floor')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.gk')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.build_type')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.readiness')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    @include('admin-delete.object.type.fields.year')
                                </div>
                                <div class="col-md-6 pl-1">
                                    @include('admin-delete.object.type.fields.quarter')
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.heatings')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.hot_water')
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height:570px;width: 100%; max-height: 570px;"></div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin-delete.object.type.fields.price_rent')
                        </div>
                        <div class="col-md-6">
                            @include('admin-delete.object.type.fields.dop_params')
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <p class="label">Тип квартиры</p>
                            <select name="apartment_type_id" id="tip_kvartiri" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->apartment_type as $apartment_type)
                                    <option value="{{$apartment_type->id}}"
                                            @if(isset($object) && $object->apartment->apartment_type_id == $apartment_type->id)selected="selected"@endif
                                    >{{$apartment_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-2">
                            <p class="label">Ремонт</p>
                            <select name="state_id" id="sostoyanie" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->state as $state)
                                    <option value="{{$state->id}}"
                                            @if(isset($object) && $object->apartment->state_id == $state->id)selected="selected"@endif
                                    >{{$state->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{--<div class="col-md-3">--}}
                            {{--<p class="label">S Комнат</p>--}}
                            {{--<input id="komnat" name="kitchen_square" type="number" placeholder="Комнат м²" class="mb-2">--}}
                        {{--</div>--}}

                        <div class="col-md-4">
                            @include('admin-delete.object.type.fields.total_square')
                        </div>

                        <div class="col-md-4">
                            @include('admin-delete.object.type.fields.live_square')
                        </div>

                        <div class="col-md-4">
                            @include('admin-delete.object.type.fields.kitchen_square')
                        </div>
                        <div class="col-md-6">
                            @include('admin-delete.object.type.fields.room_count')
                        </div>

                        <div class="col-md-6">
                            <p class="label">Изол. комнат</p>
                            <select name="rooms_isolation_id" id="rooms_isolation_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->rooms_isolation as $rooms_isolation)
                                    <option value="{{$rooms_isolation->id}}"
                                            @if(isset($object) && $object->apartment->rooms_isolation_id == $rooms_isolation->id)selected="selected"@endif
                                    >{{$rooms_isolation->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <p class="label">Сан. узел</p>
                            <select name="wc_id" id="wc" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->wc as $wc)
                                    <option value="{{$wc->id}}"
                                            @if(isset($object) && $object->apartment->wc_id == $wc->id)selected="selected"@endif
                                    >{{$wc->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <p class="label">Кол-во с.у.</p>
                            <input id="kol" value="@if(isset($object)){{$object->apartment->wc_num}}@endif" name="wc_num" type="number" placeholder="Кол." class="mb-2">
                        </div>
                        <div class="col-md-3">
                            <p class="label">Балконов</p>
                            <input min="0" id="blyad" value="@if(isset($object)){{$object->apartment->balconies}}@endif" name="balconies" type="number" placeholder="Б/Л" class="mb-2">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')

            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Планировка
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->layout as $layout)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="layouts[]" value="{{$layout->id}}" id="layout{{$layout->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->apartment->layouts->pluck('id') as $l)
                                                       @if($l == $layout->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="layout{{$layout->id}}" class="css-label">{{$layout->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_remont">
                                    Ремонт
                                </button>
                            </div>
                            <div id="ac_remont" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->repairs as $repair)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="repairs[]" value="{{$repair->id}}" id="repair{{$repair->id}}" class="css-checkbox"
                                                    @if(isset($object))
                                                    @foreach($object->apartment->repairs->pluck('id') as $r)
                                                    @if($r == $repair->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="repair{{$repair->id}}" class="css-label">{{$repair->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
