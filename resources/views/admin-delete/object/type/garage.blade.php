<div class="body-creat-object">
    <div class="container">
        <?php if(isset($object)){$object->garage = $object->garage->where('object_id',$object->id)->with('wall_materials','comforts', 'specials', 'gk', 'garage_type', 'state')->first();} ?>
        <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="garage">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.area')
                        </div>

                        <div class="col-md-6 mb-3">
                            <p class="label">Комплекс</p>
                            <select name="gk_id" id="gk_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->gk as $gk)
                                    <option value="{{$gk->id}}" @if(isset($object) && $object->garage->gk_id == $gk->id)selected="selected"@endif>{{$gk->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Кооператив</p>
                            <select name="sgk_id" id="sgk_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->sgk as $sgk)
                                    <option value="{{$sgk->id}}" @if(isset($object) && $object->garage->sgk == $sgk->id)selected="selected"@endif>{{$sgk->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">№ Дома \ Гаража</p>
                            <input id="house_number" value="@if(isset($object)){{$object->garage->house_number}}@endif" name="house_number" type="text" placeholder="№ Дома \ Гаража">
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Тип гаража</p>
                            <select name="garage_type_id" id="district" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->garage_type as $garage_type)
                                    <option value="{{$garage_type->id}}" @if(isset($object) && $object->garage->garage_type_id == $garage_type->id)selected="selected"@endif>{{$garage_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <p class="label">Этаж</p>
                                    <input id="floor" name="floor" value="@if(isset($object)){{$object->garage->floor}}@endif" type="number" min="0" placeholder="Этаж" required>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <p class="label">Этажность</p>
                                    <input id="floors" name="n_of_floor" value="@if(isset($object)){{$object->garage->n_of_floor}}@endif" type="number" min="0" placeholder="Этажность" required>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height: 570px;width: 100%; max-height: 570px;"></div>
                </div>


                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row" style="padding: 20px; padding-top: 0px;">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedon" style="width: auto;" value="1" @if(isset($object) && $object->rented)checked="checked"@endif >
                                        <label class="form-check-label" for="rentedon">
                                            Аренда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedoff" style="width: auto;" value="0" @if(isset($object) && !$object->rented)checked="checked"@endif @if(!isset($object)) checked="checked" @endif>
                                        <label class="form-check-label" for="rentedoff">
                                            Продажа
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="rented-box">
                                <p class="label">Цена в месяц</p>
                                <input id="rent_price" value="@if(isset($object)){{$object->rent_price}}@endif" name="rent_price" type="number" min="0" placeholder="Цена в месяц" class="mb-2">
                            </div>
                            <div id="selded-box">
                                <p class="label">Цена р.</p>
                                <input id="price" value="@if(isset($object)){{$object->price}}@endif"  name="price" type="number" min="0" placeholder="Цена р." class="mb-2">
                            </div>

                            <p class="label">MLS</p>
                            <input id="mls" value="@if(isset($object)){{$object->mls}}@endif" name="mls" type="number" min="0" placeholder="MLS" class="mb-2">

                        </div>
                        <div class="col-md-6">
                            <p class="label">Доп. параметры</p>
                            <input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox" @if(isset($object) && $object->notary)checked="checked"@endif>
                            <label for="notarius" class="css-label mb-0">Нотариус</label>

                            <input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox" @if(isset($object) && $object->custody)checked="checked"@endif>
                            <label for="custody" class="css-label mb-0">Опека</label>

                            <input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox" @if(isset($object) && $object->burden)checked="checked"@endif>
                            <label for="obremenenie" class="css-label mb-0">Обременение</label>

                            <input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox" @if(isset($object) && $object->exlusive)checked="checked"@endif>
                            <label for="exlusive" class="css-label mb-0">Эксклюзив</label>
                            <div id="under_exclusive" style="display: none;">
                                <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox" @if(isset($object) && $object->writed)checked="checked"@endif>
                                <label for="writed" class="css-label mb-0">Письменно</label>
                                <div id="under_writed" style="display: none;">
                                    <input type="text" id="exlusive_date" name="exlusive_date" class="datepicker-here"  value="@if(isset($object)){{$object->exlusive_date}}@endif" data-position="top center" placeholder="Эксклюзив дата" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <p class="label">Материал стен</p>
                            <select name="wall_materials[]" id="wall_materials" class="form-control select-init" multiple="multiple">
                                <option value="">Не указано</option>
                                @foreach($params->wall_material as $wall_material)
                                    <option value="{{$wall_material->id}}"
                                            @if(isset($object))
                                            @foreach($object->garage->wall_materials->pluck('id') as $wm)
                                            @if($wm == $wall_material->id)
                                            selected="selected"
                                        @endif
                                        @endforeach
                                        @endif
                                    >{{$wall_material->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-2">
                            <p class="label">Состояние</p>
                            <select name="state_id" id="sostoyanie" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->state as $state)
                                    <option value="{{$state->id}}"
                                            @if(isset($object) && $object->garage->state_id == $state->id)selected="selected"@endif
                                    >{{$state->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <p class="label">S Общая</p>
                            <input id="obshaya" value="@if(isset($object)){{$object->garage->total_square}}@endif" name="total_square" type="number" step="0.1" min="0" placeholder="S общая" class="mb-2" required>
                        </div>

                        <div class="col-md-6">
                            <p class="label">S участка</p>
                            <input id="obshaya" value="@if(isset($object)){{$object->garage->sector_square}}@endif" name="sector_square" type="number" step="0.1" min="0" placeholder="S участка" class="mb-2">
                        </div>


                        <div class="col-md-4">
                            <p class="label">Высота</p>
                            <input id="height" value="@if(isset($object)){{$object->garage->height}}@endif" name="height" type="number" min="0" step="0.1" placeholder="высота" class="mb-2">
                        </div>

                        <div class="col-md-4">
                            <p class="label">Ширина</p>
                            <input id="width" value="@if(isset($object)){{$object->garage->width}}@endif" name="width" type="number" min="0" step="0.1" placeholder="ширина" class="mb-2">
                        </div>

                        <div class="col-md-4">
                            <p class="label">Длинна</p>
                            <input id="length" value="@if(isset($object)){{$object->garage->length}}@endif" name="length" type="number" min="0" step="0.1" placeholder="длинна" class="mb-2">
                        </div>


                    </div>
                </div>
            </div>
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')

            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Планировка
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->garage_special as $specials)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="specials[]" value="{{$specials->id}}" id="specials{{$specials->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->garage->specials->pluck('id') as $l)
                                                       @if($l == $specials->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="specials{{$specials->id}}" class="css-label">{{$specials->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_blag_kvartiri">
                                    Благоустройство
                                </button>
                            </div>
                            <div id="ac_blag_kvartiri" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->garage_comfort as $garage_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="comforts[]" value="{{$garage_comfort->id}}" id="garage_comfort{{$garage_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->garage->comforts->pluck('id') as $c)
                                                       @if($c == $garage_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="garage_comfort{{$garage_comfort->id}}" class="css-label">{{$garage_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
    </div>
</div>

