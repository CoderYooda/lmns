<div class="body-creat-object">
    <div class="container">
        <?php if (isset($object)) {
            $object->landmeta = $object->land->where('object_id', $object->id)->with('comforts')->first();
        } ?>
        <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="land">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            @include('admin-delete.object.type.fields.city')
                        </div>
                        <div class="col-md-4 mb-3">
                            @include('admin-delete.object.type.fields.area')
                        </div>
                        <div class="col-4 mb-3">
                            <p class="label">S Участка в сотках</p>
                            <input id="landarea" value="@if(isset($object)){{$object->land->land_square}}@endif"
                                   name="land_square" type="number" min="0" max="10000" step="0.1" placeholder="Участок (сотки)" class="mb-2" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.house_num')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row" style="padding: 20px; padding-top: 0px;">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedon"
                                               style="width: auto;" value="1"
                                               @if(isset($object) && $object->rented)checked="checked"@endif>
                                        <label class="form-check-label" for="rentedon">
                                            Аренда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedoff"
                                               style="width: auto;" value="0"
                                               @if(isset($object) && !$object->rented)checked="checked"
                                               @endif @if(!isset($object)) checked="checked" @endif>
                                        <label class="form-check-label" for="rentedoff">
                                            Продажа
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="rented-box">
                                <p class="label">Цена в месяц</p>
                                <input id="rent_price" value="@if(isset($object)){{$object->rent_price}}@endif"
                                       name="rent_price" type="number" min="0" placeholder="Цена в месяц" class="mb-2">
                            </div>
                            <div id="selded-box">
                                <p class="label">Цена р.</p>
                                <input id="price" value="@if(isset($object)){{$object->price}}@endif" name="price"
                                       type="number" min="0" placeholder="Цена р." class="mb-2">
                            </div>


                            <p class="label">MLS</p>
                            <input id="mls" value="@if(isset($object)){{$object->mls}}@endif" name="mls" type="number" min="0"
                                   placeholder="MLS" class="mb-2">
                        </div>
                        <div class="col-md-6">
                            <p class="label">Доп. параметры</p>
                            <input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox"
                                   @if(isset($object) && $object->notary)checked="checked"@endif>
                            <label for="notarius" class="css-label mb-0">Нотариус</label>

                            <input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox"
                                   @if(isset($object) && $object->custody)checked="checked"@endif>
                            <label for="custody" class="css-label mb-0">Опека</label>

                            <input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox"
                                   @if(isset($object) && $object->burden)checked="checked"@endif>
                            <label for="obremenenie" class="css-label mb-0">Обременение</label>

                            <input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox"
                                   @if(isset($object) && $object->exlusive)checked="checked"@endif>
                            <label for="exlusive" class="css-label mb-0">Эксклюзив</label>
                            <div id="under_exclusive" style="display: none;">
                                <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox"
                                       @if(isset($object) && $object->writed)checked="checked"@endif>
                                <label for="writed" class="css-label mb-0">Письменно</label>
                                <div id="under_writed" style="display: none;">
                                    <input type="text" id="exlusive_date" name="exlusive_date" class="datepicker-here"
                                           value="@if(isset($object)){{$object->exlusive_date}}@endif"
                                           data-position="top center" placeholder="Эксклюзив дата"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height: 570px;width: 100%; max-height: 570px;"></div>
                </div>

            </div>
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')


            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse"
                                        data-target="#ac_blag_land">
                                    Благоустройство участка
                                </button>
                            </div>
                            <div id="ac_blag_land" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->land_comfort as $land_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="comforts[]" value="{{$land_comfort->id}}"
                                                       id="land_comfort{{$land_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->landmeta->comforts->pluck('id') as $c)
                                                       @if($c == $land_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="land_comfort{{$land_comfort->id}}"
                                                       class="css-label">{{$land_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
    </div>
</div>

