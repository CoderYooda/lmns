<div class="body-creat-object">
    <div class="container">
        <?php if(isset($object)){$object->room = $object->room->where('object_id',$object->id)->with('wall_materials', 'specials', 'comforts', 'build_comforts')->first();} ?>

        <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="room">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <p class="label">Этаж</p>
                                    <input id="floor" name="floor" value="@if(isset($object)){{$object->room->floor}}@endif" type="number" min="0" placeholder="Этаж" required>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <p class="label">Этажность</p>
                                    <input id="floors" name="n_of_floor" value="@if(isset($object)){{$object->room->n_of_floor}}@endif" type="number" min="0" placeholder="Этажность" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Тип здания</p>
                            <select name="build_type_id" id="build_type_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->build_type as $build_type)
                                    <option value="{{$build_type->id}}"
                                            @if(isset($object) && $object->room->build_type_id == $build_type->id)selected="selected"@endif
                                    >{{$build_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Год</p>
                            <input id="year" value="@if(isset($object)){{$object->room->year}}@endif" name="year" type="number" min="0" placeholder="Год">
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height: 570px;width: 100%;max-height: 570px;"></div>
                </div>


                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row" style="padding: 20px; padding-top: 0px;">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedon" style="width: auto;" value="1" @if(isset($object) && $object->rented)checked="checked"@endif >
                                        <label class="form-check-label" for="rentedon">
                                            Аренда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedoff" style="width: auto;" value="0" @if(isset($object) && !$object->rented)checked="checked"@endif @if(!isset($object)) checked="checked" @endif>
                                        <label class="form-check-label" for="rentedoff">
                                            Продажа
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="rented-box">
                                <p class="label">Цена в месяц</p>
                                <input id="rent_price" value="@if(isset($object)){{$object->rent_price}}@endif" name="rent_price" type="number" min="0" placeholder="Цена в месяц" class="mb-2">
                            </div>
                            <div id="selded-box">
                                <p class="label">Цена р.</p>
                                <input id="price" value="@if(isset($object)){{$object->price}}@endif"  name="price" type="number" min="0" placeholder="Цена р." class="mb-2">
                            </div>
                            <p class="label">MLS</p>
                            <input id="mls" value="@if(isset($object)){{$object->mls}}@endif" name="mls" type="number" min="0" placeholder="MLS" class="mb-2">
                        </div>
                        <div class="col-md-6">
                            <p class="label">Доп. параметры</p>
                            <input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox" @if(isset($object) && $object->notary)checked="checked"@endif>
                            <label for="notarius" class="css-label mb-0">Нотариус</label>

                            <input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox" @if(isset($object) && $object->custody)checked="checked"@endif>
                            <label for="custody" class="css-label mb-0">Опека</label>

                            <input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox" @if(isset($object) && $object->burden)checked="checked"@endif>
                            <label for="obremenenie" class="css-label mb-0">Обременение</label>

                            <input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox" @if(isset($object) && $object->exlusive)checked="checked"@endif>
                            <label for="exlusive" class="css-label mb-0">Эксклюзив</label>
                            <div id="under_exclusive" style="display: none;">
                                <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox" @if(isset($object) && $object->writed)checked="checked"@endif>
                                <label for="writed" class="css-label mb-0">Письменно</label>
                                <div id="under_writed" style="display: none;">
                                    <input type="text" id="exlusive_date" name="exlusive_date" class="datepicker-here"  value="@if(isset($object)){{$object->exlusive_date}}@endif" data-position="top center" placeholder="Эксклюзив дата" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <p class="label">Тип комнаты</p>
                            <select name="room_type_id" id="tip_kvartiri" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->room_type as $room_type)
                                    <option value="{{$room_type->id}}"
                                            @if(isset($object) && $object->room->room_type_id == $room_type->id)selected="selected"@endif
                                    >{{$room_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-2">
                            <p class="label">Состояние</p>
                            <select name="state_id" id="sostoyanie" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->state as $state)
                                    <option value="{{$state->id}}"
                                            @if(isset($object) && $object->room->state_id == $state->id)selected="selected"@endif
                                    >{{$state->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <p class="label">N. комнат</p>
                            <input id="rooms" value="@if(isset($object)){{$object->room->rooms}}@endif" name="rooms" type="number" min="0" placeholder="Кол-во комнат" class="mb-2">
                        </div>
                        {{--<div class="col-md-3">--}}
                        {{--<p class="label">S Комнат</p>--}}
                        {{--<input id="komnat" name="kitchen_square" type="number" min="0" placeholder="Комнат м²" class="mb-2">--}}
                        {{--</div>--}}
                        <div class="col-md-3">
                            <p class="label">S Общая</p>
                            <input id="obshaya" value="@if(isset($object)){{$object->room->total_square}}@endif" name="total_square" type="number" min="0" placeholder="Общая м²" class="mb-2" required>
                        </div>
                        <div class="col-md-3">
                            <p class="label">S комнат</p>
                            <input id="zhilaya" value="@if(isset($object)){{$object->room->room_square}}@endif" name="room_square" type="number" min="0" placeholder="Жилая м²" class="mb-2">
                        </div>
                        <div class="col-md-3">
                            <p class="label">N. человек</p>
                            <input id="rooms" value="@if(isset($object)){{$object->room->count_people}}@endif" name="count_people" type="number" min="0" placeholder="Кол-во человек" class="mb-2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')

            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse" data-target="#ac_blag_kvartiri">
                                    Особенности комнаты
                                </button>
                            </div>
                            <div id="ac_blag_kvartiri" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->room_specials as $room_special)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="specials[]" value="{{$room_special->id}}" id="room_specials{{$room_special->id}}" class="css-checkbox"
                                                    @if(isset($object))
                                                       @foreach($object->room->specials->pluck('id') as $c)
                                                       @if($c == $room_special->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="room_specials{{$room_special->id}}" class="css-label">{{$room_special->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Благоустройство комнаты
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->room_comfort as $room_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="comforts[]" value="{{$room_comfort->id}}" id="layout{{$room_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->room->comforts->pluck('id') as $l)
                                                       @if($l == $room_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="layout{{$room_comfort->id}}" class="css-label">{{$room_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#ac_blag_zdaniya">
                                    Благоустройство здания и территории
                                </button>
                            </div>
                            <div id="ac_blag_zdaniya" class="collapse" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->build_comfort as $build_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="build_comforts[]" value="{{$build_comfort->id}}" id="build_comfort{{$build_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->room->build_comforts->pluck('id') as $b)
                                                       @if($b == $build_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="build_comfort{{$build_comfort->id}}" class="css-label">{{$build_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
    </div>
</div>

