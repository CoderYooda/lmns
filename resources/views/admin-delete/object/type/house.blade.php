<div class="body-creat-object">
    <div class="container">
        <?php if(isset($object)){$object->housemeta = $object->house->where('object_id',$object->id)->with('wall_materials','comforts', 'repairs', 'build_comforts', 'layouts', 'heatings')->first();} ?>
        <form id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="house">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('admin-delete.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="label">Этажность</p>
                                    <input id="floors" name="n_of_floor" value="@if(isset($object)){{$object->house->n_of_floor}}@endif" type="number" min="0" placeholder="Этажность" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Тип объекта</p>
                            <select name="build_type_id" id="build_type_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->house_build_type as $build_type)
                                    <option value="{{$build_type->id}}"
                                            @if(isset($object) && $object->house->build_type_id == $build_type->id)selected="selected"@endif
                                    >{{$build_type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="label">Год постройки</p>
                                    <input id="year" value="@if(isset($object)){{$object->house->year}}@endif" name="year" type="number" min="0" placeholder="Год">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            <p class="label">Отопление</p>
                            <select name="heatings[]" id="heating" class="form-control select-init" multiple="multiple">
                                <option value="">Не указано</option>
                                @foreach($params->heating as $heating)
                                    <option value="{{$heating->id}}"
                                            @if(isset($object))
                                            @foreach($object->house->heatings->pluck('id') as $h)
                                            @if($h == $heating->id)
                                            selected="selected"
                                        @endif
                                        @endforeach
                                        @endif
                                    >{{$heating->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <p class="label">Водоснабжение</p>
                            <select name="hot_water_system_id" id="water_supply" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->hot_water_system as $hot_water_system)
                                    <option value="{{$hot_water_system->id}}"
                                            @if(isset($object) && $object->house->hot_water_system_id == $hot_water_system->id)selected="selected"@endif
                                    >{{$hot_water_system->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <p class="label">Укажите точку обьекта</p>
                    <div id="map" style="height: 570px;width: 100%; max-height: 570px;"></div>
                </div>


                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row" style="padding: 20px; padding-top: 0px;">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedon" style="width: auto;" value="1" @if(isset($object) && $object->rented)checked="checked"@endif>
                                        <label class="form-check-label" for="rentedon">
                                            Аренда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="rented" id="rentedoff" style="width: auto;" value="0" @if(isset($object) && !$object->rented)checked="checked"@endif @if(!isset($object)) checked="checked" @endif>
                                        <label class="form-check-label" for="rentedoff">
                                            Продажа
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div id="rented-box">
                                <p class="label">Цена в месяц</p>
                                <input id="rent_price" value="@if(isset($object)){{$object->rent_price}}@endif" name="rent_price" type="number" min="0" placeholder="Цена в месяц" class="mb-2">
                            </div>
                            <div id="selded-box">
                                <p class="label">Цена р.</p>
                                <input id="price" value="@if(isset($object)){{$object->price}}@endif"  name="price" type="number" min="0" placeholder="Цена р." class="mb-2">
                            </div>


                            <p class="label">MLS</p>
                            <input id="mls" value="@if(isset($object)){{$object->mls}}@endif" name="mls" type="number" min="0" placeholder="MLS" class="mb-2">
                        </div>
                        <div class="col-md-6">
                            <p class="label">Доп. параметры</p>
                            <input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox" @if(isset($object) && $object->notary)checked="checked"@endif>
                            <label for="notarius" class="css-label mb-0">Нотариус</label>

                            <input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox" @if(isset($object) && $object->custody)checked="checked"@endif>
                            <label for="custody" class="css-label mb-0">Опека</label>

                            <input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox" @if(isset($object) && $object->burden)checked="checked"@endif>
                            <label for="obremenenie" class="css-label mb-0">Обременение</label>

                            <input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox" @if(isset($object) && $object->exlusive)checked="checked"@endif>
                            <label for="exlusive" class="css-label mb-0">Эксклюзив</label>
                            <div id="under_exclusive" style="display: none;">
                                <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox" @if(isset($object) && $object->writed)checked="checked"@endif>
                                <label for="writed" class="css-label mb-0">Письменно</label>
                                <div id="under_writed" style="display: none;">
                                    <input type="text" id="exlusive_date" name="exlusive_date" class="datepicker-here"  value="@if(isset($object)){{$object->exlusive_date}}@endif" data-position="top center" placeholder="Эксклюзив дата" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <p class="label">Ремонт</p>
                            <select name="state_id" id="sostoyanie" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->state as $state)
                                    <option value="{{$state->id}}"
                                            @if(isset($object) && $object->house->state_id == $state->id)selected="selected"@endif
                                    >{{$state->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{--<div class="col-md-3">--}}
                        {{--<p class="label">S Комнат</p>--}}
                        {{--<input id="komnat" name="kitchen_square" type="number" min="0" placeholder="Комнат м²" class="mb-2">--}}
                        {{--</div>--}}
                        <div class="col-md-6">
                            <p class="label">Кол-во комнат</p>
                            <input id="rooms" value="@if(isset($object)){{$object->house->rooms}}@endif" name="rooms" type="number" min="0" max="100" placeholder="Кол-во комнат" class="mb-2">
                        </div>

                        <div class="col-md-3">
                            <p class="label">S Общая</p>
                            <input id="obshaya" value="@if(isset($object)){{$object->house->total_square}}@endif" name="total_square" type="number" min="0" max="100000" placeholder="Общая м²" class="mb-2" required>
                        </div>

                        <div class="col-md-3">
                            <p class="label">S жилая</p>
                            <input id="zhilaya" value="@if(isset($object)){{$object->house->live_square}}@endif" name="live_square" type="number" min="0" max="100000" placeholder="Жилая м²" class="mb-2">
                        </div>

                        <div class="col-md-3">
                            <p class="label">S кухня</p>
                            <input id="kuhnya" value="@if(isset($object)){{$object->house->kitchen_square}}@endif" name="kitchen_square" type="number" min="0" max="100000" placeholder="Кухня м²" class="mb-2">
                        </div>
                        <div class="col-md-3">
                            <p class="label">S участка</p>
                            <input id="land_square" value="@if(isset($object)){{$object->house->land_square}}@endif" name="land_square" type="number" min="0" max="100000" placeholder="Участок м²" class="mb-2">
                        </div>


                        <div class="col-md-6">
                            <p class="label">Изол. комнат</p>
                            <select name="rooms_isolation_id" id="rooms_isolation_id" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->rooms_isolation as $rooms_isolation)
                                    <option value="{{$rooms_isolation->id}}"
                                            @if(isset($object) && $object->house->rooms_isolation_id == $rooms_isolation->id)selected="selected"@endif
                                    >{{$rooms_isolation->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <p class="label">Кол-во балконов</p>
                            <input id="blyad" value="@if(isset($object)){{$object->house->balconies}}@endif" name="balconies" type="number" min="0" placeholder="Кол-во балконов" class="mb-2">
                        </div>
                        <div class="col-md-6">
                            <p class="label">Сан. узел</p>
                            <select name="wc_id" id="wc" class="form-control select-init">
                                <option value="">Не указано</option>
                                @foreach($params->wc as $wc)
                                    <option value="{{$wc->id}}"
                                            @if(isset($object) && $object->house->wc_id == $wc->id)selected="selected"@endif
                                    >{{$wc->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <p class="label">Кол-во сан. узлов</p>
                            <input id="kol" value="@if(isset($object)){{$object->house->wc_num}}@endif" name="wc_num" type="number" min="0" placeholder="Кол-во сан. узлов" class="mb-2">
                        </div>

                    </div>
                </div>
            </div>
            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    @include('admin-delete.object.type.fields.description')
                </div>
            </div>

            @include('admin-delete.object.image-upload')


            <div class="row py-3 mb-4 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-accordion" type="button" data-toggle="collapse" data-target="#ac_blag_kvartiri">
                                    Благоустройство Дома \ Дачи \ Участка
                                </button>
                            </div>
                            <div id="ac_blag_kvartiri" class="collapse show" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->house_comfort as $house_comfort)
                                            <div class="col-md-3">
                                                <input type="checkbox" name="comforts[]" value="{{$house_comfort->id}}" id="house_comfort{{$house_comfort->id}}" class="css-checkbox"
                                                       @if(isset($object))
                                                       @foreach($object->house->comforts->pluck('id') as $c)
                                                       @if($c == $house_comfort->id)
                                                       checked="checked"
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >
                                                <label for="house_comfort{{$house_comfort->id}}" class="css-label">{{$house_comfort->title}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="checkMap(event)" class="property-button" type="submit">Сохранить</button>
        </form>
    </div>
</div>

