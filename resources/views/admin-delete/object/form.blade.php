@extends('layouts.admin')

@section('content')

    <div id="dashboard" class="pb-5 mb-5">
        <div id="object-list-selection" class="creat-object">
            <div class="container">
                @if(isset($object->id) && $object->id != null)
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="my-3">Редактирование объекта</h1>
                        </div>
                    </div>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="my-3">Создание объекта
                        @if(isset($_GET['type']))
                                ({{app\Models\Category::where('type', $_GET['type'])->first()->title}})
                            @endif
                        </h1>
                    </div>
                    @if(!isset($_GET['type']))
                    <div class="col-md-12">
                        <div class="row block-wraper">
                            @foreach($categories as $category)
                                <div class="col-md-4 selection-item">
                                    <a href="{{route('getBaseForm', ['type' => $category->type])}}"
                                       class="block-wraper-white">
                                        <div class="fon-wraper">
                                            <div class="black-fon">
                                                <p>{{$category->title}}</p>
                                            </div>
                                            <img src="/img/mask.png" alt="">
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
                @endif
            </div>
        </div>



        <div id="ajax-container">
            @if(isset($_GET['type']))
                @include("admin-delete.object.type." . $_GET['type'])
            @elseif(isset($object->category->type))
                @include("admin-delete.object.type." . $object->category->type)
            @endif
        </div>
    </div>

@endsection

@push('styles')
    <style type="text/css">
        #sortable-1 { list-style-type: none; margin: 0;
            padding: 0; width: 25%; }
        #sortable-1 li { margin: 0 3px 3px 3px; padding: 0.4em;
            padding-left: 1.5em; font-size: 17px; height: 16px; }
        .default {
            background: #cedc98;
            border: 1px solid #DDDDDD;
            color: #333333;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>

        @if(isset($_GET['type']) || isset($object))
        ymaps.ready(function(){
            window.map = new ymaps.Map("map", {
                center: [50.597467, 36.588849],
                zoom: 10,
                controls: ['zoomControl']
            });

            @if(isset($object))
                window.myPlacemark = new ymaps.Placemark([{{$object->geopoint}}], {
                hintContent: '{!! $object->title !!}'
            }, {
            });
            map.geoObjects.add(window.myPlacemark);
            map.setCenter([{{$object->geopoint}}], 16, {
                checkZoomRange: true
            });
            @else
                window.myPlacemark;
            @endif

            map.events.add('click', function (e) {
                var coords = e.get('coords');

                geocoding(coords);

                // Если метка уже создана – просто передвигаем ее.
                if (window.myPlacemark) {
                    //map.geoObjects.removeAll();
                    window.myPlacemark.geometry.setCoordinates(coords);
                    window.myPlacemark.properties.set('iconCaption', $('input[name="title"]').val());
                } else {
                    window.myPlacemark = createPlacemark(coords);
                    $('#map').removeClass('redrays');
                    map.geoObjects.add(window.myPlacemark);
                }

                $('#geopoint').val(coords);
                addDragEvent(window.myPlacemark);
            });
            if (window.myPlacemark) {
                addDragEvent(window.myPlacemark);
            }



            window.createPlacemark = function(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: $('input[name="title"]').val()
                }, {
                    preset: 'islands#violetDotIconWithCaption',
                    draggable: true
                });
                map.geoObjects.add(window.myPlacemark);
            }


        });
        @endif

            window.searchGeocoder = function(){
            var city = $('#city option:selected').text();

            if (city == "Не указано"){
                city = '';
            }

            var house = $('#house_number').val();
            var street = $('#street').val();

            var searchstring = 'Белгородская область ' + ' ' + city + ' ' + street + ' ' + house;
            ymaps.geocode(searchstring, {
                results: 1
            }).then(function (res) {

                var firstGeoObject = res.geoObjects.get(0),
                    coords = firstGeoObject.geometry.getCoordinates(),
                    bounds = firstGeoObject.properties.get('boundedBy');

                map.setBounds(bounds, {
                    checkZoomRange: true
                });

                if (window.myPlacemark) {
                    window.myPlacemark.geometry.setCoordinates(coords);
                    window.myPlacemark.properties.set('iconCaption', $('input[name="title"]').val());
                } else {
                    window.myPlacemark = createPlacemark(coords);
                    $('#map').removeClass('redrays');
                    map.geoObjects.add(window.myPlacemark);
                }

                $('#geopoint').val(coords);
                $('#geopoint').val(coords);
                addDragEvent(window.myPlacemark);
            });
        };



        window.geocoding = function(coords){
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                $('#street').val(firstGeoObject.getThoroughfare());
                $('#house_number').val(firstGeoObject.getPremiseNumber());
            });
        };

        window.addDragEvent = function(Placemark){
            Placemark.events.add('dragend', function (e) {
                //map.geoObjects.removeAll();
                $('#map').removeClass('redrays');
                var coords = Placemark.geometry.getCoordinates();
                $('#geopoint').val(coords);
                Placemark.properties.set('iconCaption', $('input[name="title"]').val());
                geocoding(coords);
            });
        }

        window.sortImages = function(Order){
            var newOrder = $('#images').sortable('toArray');
            $.ajax({
                url:'{{route('setIndexes')}}',
                data: {id:newOrder},
                dataType:'json',
                async:false,
                type:'post',
                success:function(response){
                    console.log('Отсортировано!');
                },
            });
        }

        $( document ).ready(function() {

            checkProps();
            rentedProps();

            $(document.body).on("change","#rentedon",function(){
                rentedProps();
            });
            $(document.body).on("change","#rentedoff",function(){
                rentedProps();
            });

            $(document.body).on("change","#exlusive",function(){
                checkProps();
            });
            $(document.body).on("change","#writed",function(){
                checkProps();
            });

            $("#images").sortable({
                revert: 100,
                update: function(event, ui) {
                    window.newOrder = $(this).sortable('toArray');
                    sortImages(window.newOrder);
                }
            });

            $(document.body).on("change","#image",function(){
                $('#preloader').addClass('show');
                $.ajax({
                    url:'{{route('uploadImage')}}',
                    data:new FormData($("#interform")[0]),
                    dataType:'json',
                    async:true,
                    type:'post',
                    processData: false,
                    contentType: false,
                    success:function(response){

                        $.each(response, function (index, value) {
                            var elem = '<div id="'+ value.id +'" data-id="'+ value.id +'" class="picbox"><span class="rotate"><i class="fa fa-undo" aria-hidden="true"></i></span><span class="delete"><i class="fa fa-times" aria-hidden="true"></i></span><img src="'+ value.thumb_url +'"><input type="hidden" name="pic[]" value="'+ value.id +'"></div>';
                            $('#images').append(elem);
                        });
                        $('#preloader').removeClass('show');
                        sortImages(window.newOrder);
                    },
                    error:function(response){
                        if(response == 'undefined'){
                            $('#preloader').removeClass('show');
                        } else {
                            var mess = '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
                                '<strong>Внимание!</strong> ' + response.responseJSON.message +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                '</div>';
                            $('#imginfo').html(mess);
                            $('#preloader').removeClass('show');
                        }
                    }
                });
                var newOrder = $('#images').sortable('toArray');
                $.ajax({
                    url:'{{route('setIndexes')}}',
                    data: {id:newOrder},
                    dataType:'json',
                    async:false,
                    type:'post',
                    success:function(response){
                        console.log('Отсортировано!');
                    },
                });
            });
            $(document.body).on("click",".delete",function(){
                console.log();
                var elem = $(this).parent();
                $.ajax({
                    url:'{{route('removeImage')}}',
                    data: {id:elem.data('id')},
                    dataType:'json',
                    async:false,
                    type:'post',
                    success:function(response){
                        elem.remove();
                    },
                });
            });
            $(document.body).on("click",".rotate",function(){

                var elem = $(this).parent();
                console.log(elem.data('id'));
                $.ajax({
                    url:'{{route('rotateImg')}}',
                    data: {id:elem.data('id')},
                    dataType:'json',
                    async:false,
                    type:'post',
                    success:function(response){
                        $('#'+response.id).find('img').attr('src', response.thumb_url);
                    },
                });
            });
        });


        $(document.body).on("change","#select_object_type",function(){
            window.location.href = "{{route('getBaseForm')}}?type=" + this.value;
            {{--$.ajax({--}}
                {{--method: 'GET',--}}
                {{--url: '{{route('getInnerFormByType')}}',--}}
                {{--data: {'category' : this.value},--}}
                {{--success: function(data){--}}
                    {{--$('#ajax-container').html(data);--}}

                {{--},--}}
                {{--dataType: 'html'--}}
            {{--});--}}

        });
        function checkProps(){
            if($('#exlusive').prop('checked')){
                $('#under_exclusive').show();
            } else {
                $('#writed').prop('checked', false);
                $('#under_exclusive').hide();
            }

            if($('#writed').prop('checked')){
                $('#under_writed').show();
            } else {
                $('#exlusive_date').val('');
                $('#under_writed').hide();
            }
        }

        function rentedProps(){
            if($('#rentedon').prop('checked')){
                $('#rented-box').show();
                $('#selded-box').hide();
                $('#rent_price').prop('required',true);
                $('#price').prop('required',false);
            }

            if($('#rentedoff').prop('checked')){
                $('#selded-box').show();
                $('#rented-box').hide();
                $('#price').prop('required',true);
                $('#rent_price').prop('required',false);
            }
        }


    </script>

@endpush
