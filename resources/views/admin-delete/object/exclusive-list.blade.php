@extends('layouts.admin')

@section('content')
    <div class="main-content">
        <div id="object-list" class="pb-5 my-5">
            <div class="container">
                <h1 style="font-size: 26px; margin-bottom: 20px">Эксклюзивные объекты</h1>
                <div class="row">
                    <div class="col-md-4">
                        <form action="{{ route('listExclusive' ) }}" method="GET">
                            <div class="object-filter mb-4">
                                <h3>Фильтр</h3>
                                <select id="typefilter" name="type" class="form-control">
                                    <option value="">Тип сделки</option>
                                    <option value="rented" @if(request('type') == 'rented')selected @endif>Аренда</option>
                                    <option value="selded" @if(request('type') == 'selded')selected @endif>Продажа</option>
                                </select>
                                <select name="category" id="select_object_type" class="form-control">
                                    <option value="">Тип объекта</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if(request('category') == $category->id)selected @endif>{{$category->title}}</option>
                                    @endforeach
                                </select>
                                <select name="area" id="district" class="form-control">
                                    <option value="">Район</option>
                                    @foreach($areas as $area)
                                        <option value="{{$area->id}}" @if(request('area') == $area->id) selected @endif>{{$area->title}}</option>
                                    @endforeach
                                </select>

                                <div class="row" id="pricefilter" style="display: none">
                                    <div class="col-md-6">
                                        <input id="cena_from" name="price_from" type="number" value="{{request('price_from')}}"
                                               placeholder="Цена (от) р">
                                    </div>
                                    <div class="col-md-6">
                                        <input id="cena_to" name="price_to" type="number" value="{{request('price_to')}}" placeholder="Цена (до) р">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-form w-100">Фильтровать</button>
                                @if(request('category') || request('area') || request('price_from') || request('price_to') || request('year') != null || request('type') != null)
                                    <a href="{{route('listExclusive')}}" style="margin-top: 15px; color:white" class="btn btn-form w-100">Сбросить</a>
                                @endif
                            </div>
                        </form>
                    </div>

                    <div class="col-md-8">
                        <div class="object-list-wraper">
                            <div class="table-property-list">
                                @if($objects->count() < 1)
                                    <div class="row table-list-item">
                                        <div class="col-md-12" style="text-align: center;width: 100%;line-height: 100px;">
                                            Результатов нет,
                                            <a href="{{route('listExclusive')}}">сбросить фильтр</a>
                                        </div>
                                    </div>
                                @endif
                                @foreach($objects as $object)
                                    <div class="row table-list-item">
                                        <div class="col-md-3">
                                            <div class="apartment-image">
                                                @if($object->images->first() != null)
                                                    <img src="{{$object->images->first()->thumb_url}}" alt="apartment">
                                                @else
                                                    <img src="/img/noimage.jpg" alt="apartment">
                                                @endif
                                                <div class="badge-wrapper">

                                                    @if($object->published)
                                                    <div class="small-badge new"></div>
                                                    @endif

                                                    @if($object->exlusive)
                                                        <div class="small-badge popular"></div>
                                                    @endif

                                                    @if($object->custody)
                                                        <div class="small-badge ipoteka"></div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="name-wrap">
                                                <h2 style="margin-bottom: 15px; margin-top: 5px;"><a target="_blank" href="{{ route($object->category->type, $object->id) }}">{!! $object->title!!}</a></h2>
                                                <p class="address">Агент: {{$object->autor->lastname}} {{$object->autor->firstname}}</p>
                                                <p class="address">{{$object->city->title}}, ул.{{$object->street}}, д.{{$object->{$object->category->type}->house_number}}</p>
                                            </div>
                                            <div class="property-info body">

                                                <div class="bad">Площадь
                                                    @if($object->category->type == 'land')
                                                        <span>{{$object->{$object->category->type}->total_square}} сот.</span>
                                                    @else
                                                        <span>{{$object->{$object->category->type}->total_square}} м.кв</span>
                                                    @endif
                                                </div>


                                                @if($object->category->type != 'land' && $object->category->type != 'house')
                                                    <div class="bath">Этаж<span>{{$object->{$object->category->type}->floor}}</span></div>
                                                @endif

                                                @if($object->category->type != 'land')
                                                    <div class="sq">Этажность<span>{{$object->{$object->category->type}->n_of_floor}}</span></div>
                                                @endif

                                                @if($object->rented)
                                                    <div class="perweek">Тип<span>Аренда</span></div>
                                                @else
                                                    <div class="perweek">Тип<span>Продажа</span></div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3 apartment-details-wraper">
                                            <div class="apartment-details">
                                                @if($object->rented)
                                                    <div class="price">{{number_format($object->rent_price, 0, '.', ' ')}} в мес.</div>
                                                @else
                                                    <div class="price">{{number_format($object->price, 0, '.', ' ')}} р.</div>
                                                @endif
                                                @if($object->created_id == Auth::user()->id)
                                                    <a href="{{route('editObject' , $object->id )}}" class="details-button">Редактировать</a>
                                                    @else
                                                    <a href="#" class="details-button">Смотреть</a>
                                                        {{--TODO Ссылка на полный объект--}}
                                                @endif
                                            </div>
                                            @if($object->created_id == Auth::user()->id)
                                                <a href="{{route('removeObject' , $object->id )}}" class="details-button dellink"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            @endif
                                        </div>
                                    </div>

                                @endforeach
                                    @if($objects->count() > 0)
                                        <div class="col-md-12">
                                            <div class="legend">
                                                <div class="legend-item"><span class="new"></span>Опубликовано</div>
                                                <div class="legend-item"><span class="popular"></span>Эксклюзивный</div>
                                                <div class="legend-item"><span class="ipoteka"></span>Опека</div>
                                            </div>
                                        </div>
                                    @endif
                            </div>
                            @if ($objects instanceof \Illuminate\Pagination\LengthAwarePaginator)
                                <div class="row pagination-bottom" style="margin-top: 15px;">
                                    <div class="col-md-12">
                                        {{$objects->appends(request()->all())->links()}}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $( document ).ready(function() {
            typeSelected();
        });

        $(document.body).on("change","#typefilter",function(){
            typeSelected();
        });

        {{--$(document.body).on("change","#exlusive",function(){--}}
            {{--if(this.checked){--}}
                {{--window.location.href = "{{ route('listObjects' , ['exlusive' => true]) }}";--}}
            {{--} else {--}}
                {{--window.location.href = "{{ route('listObjects') }}";--}}
            {{--}--}}

        {{--});--}}

        function typeSelected(){

            if($('#typefilter').val() != ''){
                $('#pricefilter').show();
            } else {
                $('#pricefilter').hide();
            }
        }
    </script>
@endpush
