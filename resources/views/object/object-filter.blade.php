<div class="col-md-4">
    <button id="mapbtn" style="width: 100%;margin-bottom: 15px;" onclick="loadMap({{ $category->id }}, {{request('rent')}})" class="btn btn-primary">
        Показать на карте
    </button>
    <form id="form_apartment" action="{{route($category->type.'s')}}">
        <input type="hidden" name="category" value="{{ $category->id }}">
        <div class="form-group">
            <label for="city">Город</label>
            <select name="city" id="city" class="form-control">
                <option value="0" @if( $category->id != NULL && request('city') == 0 )selected="selected"@endif>Любой</option>
                @foreach($cities as $city)
                    <option value="{{ $city->id }}"
                        {{ Request::input('city') == $city->id ? 'selected' : '' }}>{{ $city->title }}</option>
                @endforeach
            </select>
        </div>
        @if($category->id == 6)
            <div class="form-group">
                <label for="commerce_type">Тип</label>
                <select name="commerce_type" id="commerce_type" class="form-control">
                    <option value="0" @if( request('commerce_type') != NULL && request('commerce_type') == 0 )selected="selected"@endif>Любой</option>
                    @foreach($commerce_types as $commerce_type)
                        <option value="{{ $commerce_type->id }}"
                            {{ Request::input('commerce_type') == $commerce_type->id ? 'selected' : '' }}>{{ $commerce_type->title }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="form-group count-room">
            <input style="" id="all-input" type="radio" name="rent" value="2" @if(request('rent') == NULL || request('rent') == 2 ) checked @endif>
            <label for="all-input">Все</label>
            <input style="" id="sold-input" type="radio" name="rent" value="0" @if(request('rent') != NULL && request('rent') == 0 ) checked @endif>
            <label for="sold-input">Купить</label>
            <input style="" id="rent-input" type="radio" name="rent" value="1" @if(request('rent') != NULL && request('rent') == 1 ) checked @endif>
            <label for="rent-input">Арендовать</label>
        </div>
        @if($category->id == 1)
            <div class="form-group count-room">
                <input style="" id="novostroi-input" type="checkbox" name="novostroi" value="1" {{ Request::filled('novostroi') && request('novostroi') == 1  ? 'checked' : '' }}>
                <label for="novostroi-input">Новостройка</label>
                <input style="" id="vtorich-input" type="checkbox" name="vtorich" value="1" {{ Request::filled('vtorich') && request('vtorich') == 1 ? 'checked' : '' }}>
                <label for="vtorich-input">Вторичка</label>
            </div>

            <div class="form-group count-room">

                <div>
                    <label class="title">Комнат</label>
                </div>

                <input id="rooms_study"
                       name="rooms_study"
                       value="1"
                       type="checkbox"
                    {{ Request::filled('rooms_study') ? 'checked' : '' }}
                >
                <label for="rooms_study">Студия</label>

                <input id="rooms_one"
                       name="rooms_one"
                       value="1"
                       type="checkbox"
                    {{ Request::filled('rooms_one') ? 'checked' : '' }}
                >
                <label for="rooms_one">1</label>

                <input id="rooms_two"
                       name="rooms_two"
                       value="2"
                       type="checkbox"
                    {{ Request::filled('rooms_two') ? 'checked' : '' }}
                >
                <label for="rooms_two">2</label>

                <input id="rooms_three"
                       name="rooms_three"
                       value="3"
                       type="checkbox"
                    {{ Request::filled('rooms_three') ? 'checked' : '' }}
                >
                <label for="rooms_three">3</label>

                <input id="rooms_four"
                       name="rooms_four"
                       type="checkbox"
                    {{ Request::filled('rooms_four') ? 'checked' : '' }}
                >
                <label for="rooms_four">4+</label>

            </div>
        @endif
        <div class="form-group row">
            <div class="col-md-6">
                <label for="cost-from">Цена, от</label>
                <input type="number"
                       id="cost-from"
                       name="cost-from"
                       class="form-control"
                       value="{{ Request::input('cost-from', '') }}"
                >
            </div>
            <div class="col-md-6">
                <label for="cost-to">до</label>
                <input type="number"
                       id="cost-to"
                       name="cost-to"
                       class="form-control"
                       value="{{ Request::input('cost-to', '') }}"
                >
            </div>
        </div>
        @if($category->id == 2 || request('category') == 3)
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="square_from">Площадь, от</label>
                    <input type="number"
                           id="square_from"
                           name="square_from"
                           class="form-control"
                           value="{{ Request::input('square_from', '') }}"
                    >
                </div>
                <div class="col-md-6">
                    <label for="square_to">до</label>
                    <input type="number"
                           id="square_to"
                           name="square_to"
                           class="form-control"
                           value="{{ Request::input('square_to', '') }}"
                    >
                </div>
            </div>
        @endif
        @if($category->id == 4)
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="square-sot-from">Площадь (сот), от</label>
                    <input type="number"
                           id="square-sot-from"
                           name="square_sot_from"
                           class="form-control"
                           value="{{ Request::input('square_sot_from', '') }}"
                    >
                </div>
                <div class="col-md-6">
                    <label for="square-sot-to">до</label>
                    <input type="number"
                           id="square-sot-to"
                           name="square_sot_to"
                           class="form-control"
                           value="{{ Request::input('square_sot_to', '') }}"
                    >
                </div>
            </div>
        @endif
        @if($category->id != 3 && request('category') != 4)
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="floor_from">Этаж, от</label>
                    <input type="number"
                           id="floor_from"
                           name="floor_from"
                           class="form-control"
                           value="{{ Request::input('floor_from', '') }}"
                    >
                </div>
                <div class="col-md-6">
                    <label for="floor_to">до</label>
                    <input type="number"
                           id="floor_to"
                           name="floor_to"
                           class="form-control"
                           value="{{ Request::input('floor_to', '') }}"
                    >
                </div>
            </div>
        @endif
        <div class="text-center">
            <button type="submit" class="btn btn-primary w-100">Найти</button>
        </div>

    </form>
</div>
