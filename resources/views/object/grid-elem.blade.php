@if(isset($col) && $col)
<div class="col-sm-4 col-lg-4">
@else
<div class="col-sm-4 col-lg-6">
@endif
    <div class="object-wraper">
        <p class="object-title">
            <a href="{{ route($object->category->type, $object->id) }}">
                {!! $object->title  !!}
            </a>
        </p>
        <div class="object-address pt-2 pb-3">
            <span class="address"><i class="fa fa-map-marker mr-1"></i> {{ $object->city->title }}, {{ $object->street }}
                @if($object->{$object->category->type}->house_number != null),  д {{$object->{$object->category->type}->house_number}} @endif
            </span>
        </div>
        <div class="object-image">
            @if($object->images->first() != NULL)
                <img src="{{ asset($object->images->first()->thumb_url) }}" alt="{{ $object->city->title }}">
            @else
                <img src="{{ asset('/img/noimage.jpg') }}" alt="{{ $object->city->title }}">
            @endif
        </div>
        <div class="object-info">
            <div class="object-price">
                @if($object->rented)
                    <p class="price-big mb-0">{{ number_format($object->rent_price, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i> / мес</p>
                @else
                    <p class="price-big mb-0">{{ number_format($object->price, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i></p>
                @endif
            </div>
            <div class="icons">
                <a href="{{ route($object->category->type, $object->id) }}" class="heart">Подробнее</a>
            </div>
        </div>
    </div>
</div>
