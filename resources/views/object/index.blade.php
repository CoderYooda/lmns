@extends('layouts.main')

@section('title', strip_tags($object->title) . ' | ' . config('app.name', 'Laravel'))

@section('content')
	<section id="object">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="mt-4 mb-3">{!! $object->title !!}</h1>
				</div>
				<div class="col-md-8">
                    @if(count($object->images) > 0)
					<div class="img-wraper wrapper-box">
                        <div class="fotorama"
                             data-allowfullscreen="true"
                             data-keyboard="true"
                             data-nav="thumbs"
                             data-width="700" data-ratio="700/467" data-max-width="100%">
                            @foreach($object->images as $image)
                            <img src="{{ asset($image->url) }}">
                            @endforeach
                        </div>
					</div>
                    @endif
					<div class="details-wraper wrapper-box">
						<div class="d-flex align-items-center justify-content-between mb-3">
							<h2 class="title-2 mb-0">Характеристики объекта</h2>
							<p class=" mb-0">опубликовано {{$object->created_at->format('d.m H:i')}}</p>
						</div>
						<div class="row">
                        @include('template.common.object.parametrs.items')
						</div>
					</div>
                    @if($object->description != NULL)
                        <div class="description-wraper wrapper-box">
                            <h2 class="title-2">Описание объекта</h2>
                            <p>{{$object->description}}</p>
                        </div>
                    @endif

					<div class="map-wraper wrapper-box" style="min-height: 400px;position: relative">
                        <div id="map" style="height: 100%;width: 100%; height: 570px;"></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="sidebar-price sidebar-box">
                        @if($object->rented)
                            <p class="title-2 mb-0">{{number_format($object->rent_price)}} <i class="fa fa-rub" aria-hidden="true"></i> / мес.</p>
                        @else
                            <p class="title-2 mb-0">{{number_format($object->price)}} <i class="fa fa-rub" aria-hidden="true"></i></p>
                            @if($object->price_m != NULL)
                                <p style="color: #9a9a9a;font-size: 14px;" class="title-2 mb-0">{{number_format($object->price_m)}}  <i class="fa fa-rub" aria-hidden="true"></i> за м<sup>2</sup></p>
                            @endif
                        @endif
					</div>
                    <div class="sidebar-agent sidebar-box">
                        <div class="object-manager">
                            <div class="manager-wrap align-items-center">
                                <div class="manager-data">
                                    <span class="manager-name ml-0">{{$object->agency->title}}</span>
                                    <span style="display: block;" class="ml-0">Агентство недвижимости</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (!\Session::has('success'))
                    <button type="button" onclick="$(this).remove()" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" type="submit" class="btn btn-search w-100">Показать номер</button>
                    @endif
                    <div class="collapse  @if (\Session::has('success')) show @endif" id="collapseExample">
                        <div  class="sidebar-agent sidebar-box">
                            <div class="object-manager">
                                <div class="manager-wrap align-items-center">
                                    {{--<div class="manager-icon mr-2">--}}
                                        {{--<a href="#chat">--}}
                                            {{--<img src="{{ asset('/img/noava.png') }}" alt="Агент">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="manager-data">
                                        <span class="manager-name ml-0">{{$object->autor->lastname}}</span>
                                        <span style="display: block;color: #8c8c8c;font-size: 16px;" class="manager-name ml-0">{{$object->autor->firstname . ' ' . $object->autor->midname}}</span>
                                    </div>
                                </div>
                                <div class="information-column">
                                    <span><i class="fa fa-phone"></i></span>
                                    <a href="tel:{{$object->autor->phone}}" class="values">{{$object->autor->phone}}</a>
                                </div>
                                @if (\Session::has('success'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        {!! \Session::get('success') !!}
                                    </div>
                                @endif
                                <div class="form-wraper">
                                    <h2 style="margin-top: 0!important" class="title-2 mt-3 mb-2">Написать агенту</h2>
                                    <form action="{{route('messageToAgent')}}" method="post">
                                        <input type="hidden" name="agent" value="{{$object->autor->id}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <input type="text" id="af_name" name="name" value='' placeholder="Ваше имя" class="form-control mb-0" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <input type="email" id="af_email" name="email" value="" placeholder="Ваш E-mail" class="form-control mb-0" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <input type="text" id="af_phone" name="phone" value="" placeholder="Ваш Телефон" required class="form-control mb-0" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <textarea id="af_message" name="message" class="form-control mb-0" rows="5" required placeholder="Сообщение" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <div class="controls">
                                                        <button type="submit" class="btn btn-search w-100">Отправить</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
    <link href="{{asset('lib/fotorama/fotorama.css')}}" rel="stylesheet">
    <script src="{{asset('lib/fotorama/fotorama.js')}}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(function(){
            // Указывается идентификатор HTML-элемента.
            var map = new ymaps.Map("map", {
                center: [{{$object->geopoint}}],
                zoom: 16,
                controls: ['zoomControl', 'searchControl']
            });



                myPlacemark = new ymaps.Placemark([{{$object->geopoint}}], {
                hintContent: '{{$object->title}}'

            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                //iconLayout: 'default#image'
            });
            map.geoObjects.add(myPlacemark);



        });
    </script>
@endpush
