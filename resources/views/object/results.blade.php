@extends('layouts.main')

@section('title')
    @if(request('rent') != NULL && request('rent') == 0)Продажа@endif
    @if(request('rent') != NULL && request('rent') == 1)Аренда@endif
    @if(request('rent') == NULL || request('rent') == 2)Продажа и аренда@endif
    <?php switch (Request::input('category')) {
        case 1:echo "квартир";break;
        case 2:echo "комнат";break;
        case 3:echo "коттеджей";break;
        case 4:echo "земельных участков";break;
        case 5:echo "гаражей";break;
        case 6:echo "коммерческой недвижимости";break;}?>
    {{\App\Http\Controllers\Support\CityController::getCityNameById(Request::input('city'))}}
@endsection
@section('content')

    <div class="container mt-3">

        <h1 class="mb-4" style="font-size: 28px;">
            @if(request('rent') != NULL && request('rent') == 0)Продажа@endif
            @if(request('rent') != NULL && request('rent') == 1)Аренда@endif
            @if(request('rent') == NULL || request('rent') == 2)Продажа и аренда@endif
            <?php switch (Request::input('category')) {
                case 1:echo "квартир";break;
                case 2:echo "комнат";break;
                case 3:echo "коттеджей";break;
                case 4:echo "земельных участков";break;
                case 5:echo "гаражей";break;
                case 6:echo "коммерческой недвижимости";break;}?>
            {{\App\Http\Controllers\Support\CityController::getCityNameById(Request::input('city'))}}</h1>

    @include('chunks.map')

        <div class="row mb-4">


            <div class="col-md-8">
                <div class="row">
                    {{--@include('chunks.objects_grid')--}}
                    @forelse($objects as $object)
                        @include('object.grid-elem', ['col' => 4])
                    @empty

                        <div class="col-md-12">
                            По вашему запросу ничего не найдено :(
                        </div>

                    @endforelse

                    <div class="col-md-12">
                        {{ $objects->appends(Request::all())->links() }}

                    </div>
                </div>
            </div>

            @include('object.object-filter')

        </div>

    </div>

@endsection

@push('scripts')
    @include('chunks.mapscript')
@endpush
