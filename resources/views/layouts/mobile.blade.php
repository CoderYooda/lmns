<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta name="yandex-verification" content="2b73412deddfdbe2" />
    <link rel="shortcut icon" href="{{ asset("favicon.ico") }}">

    <title>@yield('title') | {{env('APP_NAME', 'Недвижимость')}}</title>
    <meta name="keywords" content="Недвижимость, нд31, продажа, аренда, снять, купить, собственника, агентство, риэлторы, новостройки в Белгороде" />
    <meta name="description" content="@yield('description')">

    <link rel="canonical" href="{{ Request::url() }}">

    <link href="{{ asset(mix('css/mobile.css')) }}" rel="stylesheet">

    @stack('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

    <main>

        @include('template.parts.header.mobile')

        <div class="main-content">

            @yield('content')

        </div>

        @include('template.parts.footer.mobile')

    </main>

    <script src="{{asset('js/mobile.js') }}"></script>

    @stack('scripts')
    <!— Yandex.Metrika counter —>
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(52188475, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52188475" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!— /Yandex.Metrika counter —>
</body>
</html>
