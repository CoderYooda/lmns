<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="@yield('description')">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900%7COpen+Sans:400,800%7CRaleway:400,800,900%7CRoboto:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<!-- доделать форму + -->

<body>
<div class="top-bar">
    <div class="container">
        <div class="contacts-wrapper">
            <span class="contacts">
                <span class="name">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>Call Us Now:</span>
                </span>
                <span class="info">
                    987 654 3210
                </span>
            </span>
            <span class="contacts">
                <span class="name">
                    <i class="fa fa-skype" aria-hidden="true"></i>
                    <span>Chat us:</span>
                </span>
                <span class="info">
                    area.skype
                </span>
            </span>
        </div>

        <div class="wrapper">
            <span class="language">
                <i class="fa fa-globe" aria-hidden="true"></i>
                English (UK)
            </span>
            <span class="units">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                Sq ft
            </span>
            <ul class="currency-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle currency" data-toggle="dropdown">
                        <i class="fa fa-gbp" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-usd" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-eur" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-rub" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="header">
    <div class="container">
        <div class="wrapper">
            <a href="home1.html" class="logo">
                <img src="images/logo.png" alt="logo">
            </a>
            <button type="button" data-toggle="collapse" data-target="#header" aria-expanded="false"
                    class="navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <div class="menu-list">
                <div id="header" class="navbar-collapse collapse">
                    <ul>
                        <li>
                            <a href="home1.html">
                                home
                            </a>
                            <ul class="sub-menu small">
                                <li>
                                    <ul>
                                        <li>
                                            Home pages
                                        </li>
                                        <li>
                                            <a href="home1.html">
                                                Home 1
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home2.html">
                                                Home 2
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home3.html">
                                                Home 3
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home4.html">
                                                Home 4
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home5.html">
                                                Home 5
                                            </a>
                                        </li>

                                        <li>
                                            <a href="intro_page.html">
                                                Home 6
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home7.html">
                                                Home 7
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home8.html">
                                                Home 8
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Pages
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <ul>
                                        <li>
                                            Properties
                                        </li>
                                        <li>
                                            <a href="listing_sidebar_standart.html">
                                                Listing table
                                            </a>
                                        </li>
                                        <li>
                                            <a href="listings_sidebar_grid.html">
                                                Listing grid
                                            </a>
                                        </li>
                                        <li>
                                            <a href="listings_half_screen_table.html">
                                                Listing with map
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_property_1.html">
                                                Single
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_property_sub_property.html">
                                                Single fullscreen
                                            </a>
                                        </li>
                                        <li>
                                            <a href="compare_page.html">
                                                Compare properties
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <ul>
                                        <li>
                                            Agencies
                                        </li>
                                        <li>
                                            <a href="agencies.html">
                                                Listing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_agency.html">
                                                Single
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_agency_fullscreen.html">
                                                Single fullscreen
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <ul>
                                        <li>
                                            Agents
                                        </li>
                                        <li>
                                            <a href="agents.html">
                                                Listing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="agent_grid_fs.html">
                                                Listing grid
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_agent.html">
                                                Single
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_agent_fullscreen.html">
                                                Single fullscreen
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="destanations">
                                    <ul>
                                        <li>
                                            Blog
                                        </li>
                                        <li>
                                            <a href="blog_archive.html">
                                                Listing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="blog_single_post.html">
                                                Single
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="destanations">
                                    <ul>
                                        <li>
                                            FAQ
                                        </li>
                                        <li>
                                            <a href="faq_page.html">
                                                Listing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="single_question_page.html">
                                                Single question
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="account.html">
                                Account
                            </a>
                            <ul class="sub-menu small">
                                <li>
                                    <ul>
                                        <li>
                                            Account pages
                                        </li>
                                        <li>
                                            <a href="account.html#dashboard">
                                                Dashboard
                                            </a>
                                        </li>
                                        <li>
                                            <a href="account.html#submit">
                                                Submit property
                                            </a>
                                        </li>
                                        <li>
                                            <a href="account.html#property">
                                                My property
                                            </a>
                                        </li>
                                        <li>
                                            <a href="account.html#favorites">
                                                Favorites
                                            </a>
                                        </li>
                                        <li>
                                            <a href="account.html#messages">
                                                Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="account.html#settings">
                                                Settings
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Other pages
                            </a>
                            <ul class="sub-menu small">
                                <li>
                                    <ul>
                                        <li>
                                            Other pages
                                        </li>
                                        <li>
                                            <a href="contacts2_page.html">
                                                Contacts
                                            </a>
                                        </li>
                                        <li>
                                            <a href="contacts_page.html">
                                                Contacts with sidebar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="coming_soon.html">
                                                Coming soon
                                            </a>
                                        </li>
                                        <li>
                                            <a href="error_page.html">
                                                Error
                                            </a>
                                        </li>
                                        <li>
                                            <a href="maintenance.html">
                                                Maintenance
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="subscribe-buttons">
                <button class="sign-in js-sign-in">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    <span>sign in</span>
                </button>
                <button class="registration">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span>registration</span>
                </button>
            </div>
        </div>
    </div>
</div>
@yield('content')
<div class="footer">
    <div class="container">
        <div class="row footer-border">
            <div class="col-md-7">
                <div class="popular-destanations">
                    <h3>Popular destanations:</h3>
                    <ul class="places">
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        about us
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        agents
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        agencies
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        team
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        property status
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        property type
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        FAQ
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        error page
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        offline page
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        comming soon
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Taxonomy grid
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <h3>Stay updated property news :</h3>
                    <input type="email" placeholder="Enter your e-mail">
                    <button type="button">subscribe</button>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="row contacts">
                    <div class="col-md-12 col-sm-6">
                        <h3>Contact us:</h3>
                        <div class="address">303 Malaga, Kendrick Landing Suite 131</div>
                        <dl>
                            <dt>Phone:</dt>
                            <dd>+ 987 654 3210</dd>
                            <dt>Skype:</dt>
                            <dd>area.skype</dd>
                            <dt>E-mail:</dt>
                            <dd><a href="#">info@hotmail.com</a></dd>
                        </dl>
                    </div>
                    <div class="col-md-12 col-sm-6">
                        <h3>Follow us:</h3>
                        <div class="social-wrap">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-bottom">
            <div class="col-sm-6">
                <div class="copyright">© 2017 <a href="#">Area</a> — Real Estate Agency Sketch Template</div>
            </div>
            <div class="col-sm-6 text-right">
                <a href="#">Site map</a>
                <a href="#">Privacy policy</a>
                <a href="#">Terms & Conditions</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>





{{--<!DOCTYPE html>--}}
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
{{--<head>--}}
    {{--<meta charset="utf-8">--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

    {{--<!-- CSRF Token -->--}}
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

    {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}

    {{--<!-- Scripts -->--}}
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    {{--<!-- Fonts -->--}}
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    {{--<!-- Styles -->--}}
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
{{--</head>--}}
{{--<body>--}}
    {{--<div id="app">--}}
        {{--<nav class="navbar navbar-expand-md navbar-light navbar-laravel">--}}
            {{--<div class="container">--}}
                {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
                    {{--{{ config('app.name', 'Laravel') }}--}}
                {{--</a>--}}
                {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
                    {{--<span class="navbar-toggler-icon"></span>--}}
                {{--</button>--}}

                {{--<div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
                    {{--<!-- Left Side Of Navbar -->--}}
                    {{--<ul class="navbar-nav mr-auto">--}}

                    {{--</ul>--}}

                    {{--<!-- Right Side Of Navbar -->--}}
                    {{--<ul class="navbar-nav ml-auto">--}}
                        {{--<!-- Authentication Links -->--}}
                        {{--@guest--}}
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                            {{--</li>--}}
                            {{--@if (Route::has('register'))--}}
                                {{--<li class="nav-item">--}}
                                    {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                        {{--@else--}}
                            {{--<li class="nav-item dropdown">--}}
                                {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                                    {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                                {{--</a>--}}

                                {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                                    {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                        {{--{{ __('Logout') }}--}}
                                    {{--</a>--}}

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--@csrf--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--@endguest--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</nav>--}}

        {{--<main class="py-4">--}}
            {{--@yield('content')--}}
        {{--</main>--}}
    {{--</div>--}}
{{--</body>--}}
{{--</html>--}}
