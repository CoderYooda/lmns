<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from h-area.torbara.com/home5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 04 Dec 2018 06:22:10 GMT -->
<head>
    {{--<meta charset="utf-8">--}}
    {{--<meta http-equiv="X-UA-Compatible" content="IE=edge"/>--}}
    {{--<meta name="description" content=""/>--}}
    {{--<meta name="keywords" content=""/>--}}
    {{--<meta name="viewport" content="width=device-width,initial-scale=1">--}}
    {{--<title>DEMO Area HTML</title>--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900%7COpen+Sans:400,800%7CRaleway:400,800,900%7CRoboto:400,900" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    {{--<link rel="icon" href="favicon.ico" type="image/x-icon">--}}
    {{--<link href="css-min/area.min.css" rel="stylesheet" type="text/css"/>--}}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900%7COpen+Sans:400,800%7CRaleway:400,800,900%7CRoboto:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | Администратор</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')

</head>

<!-- доделать форму + -->

<body>

<header class="header">
    <div class="menu-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('home')}}" target="_blank" class="logo"><img src="{{asset('/img/logo.png')}}" alt="Недвижка31"></a>
                </div>
                <div class="col-md-9 text-right">
                    <ul class="main-menu mb-0">
                        @if(Auth::user()->hasAgency())
                            <li><a href="{{route('Agency')}}" title="Моё агентство">Моё агентство</a></li>
                        @endif
                        <li><a href="{{route('listObjects')}}" title="Мои объекты">Мои объекты</a></li>
                        <li><a href="{{route('listExclusive')}}" title="Эксклюзив">Эксклюзив</a></li>
                        <li><a href="{{route('getBaseForm')}}" title="Создать объект">Создать объект</a></li>
                        <li><a href="{{route('settings')}}" title="Создать объект">Настройки</a></li>
                    </ul>
                    <div class="login">
                        {{--<a href="#" title="Пользователь">--}}
                            {{--<img src="{{asset('/img/userpic.png')}}" alt="user">--}}
                            <span>
                                <a href="{{ route('logout')}}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="sign-in js-sign-in">
                                    {{--<i class="fa fa-sign-out" aria-hidden="true"></i>--}}
                                    <span>Выход</span>
                                </a>
                             </span>
                        <form id="logout-form" action="{{ route('logout')}}" method="POST" style="display: none;">
                            {{ csrf_field()}}
                        </form>
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-content">
    @yield('content')
</div>
<div class="footer">
    <div class="container">
        <div class="row footer-bottom">
            <div class="col-sm-6">
                <div class="copyright">© 2019 <a href="/">{{ config('app.name', 'Laravel') }}</a></div>
            </div>
            <div class="col-sm-6 text-right">
                <a href="https://webstyle.top/">Сделано в Webstyle</a>
            </div>
        </div>
    </div>
</div>
<div class="loader" id="preloader">
    <div class="l_main">
        <div class="l_square"><span></span><span></span><span></span></div>
        <div class="l_square"><span></span><span></span><span></span></div>
        <div class="l_square"><span></span><span></span><span></span></div>
        <div class="l_square"><span></span><span></span><span></span></div>
    </div>
</div>
@stack('scripts')
@stack('styles')
</body>
</html>
