<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>@yield('title', 'Недвижка31')</title>

    <link rel="shortcut icon" href="{{ asset("favicon.ico") }}">

    @yield('SEO')

    <link rel="canonical" href="{{ Request::url() }}">

    @yield('options')

    <link href="{{ asset(mix('css/main.css')) }}" rel="stylesheet">

    @stack('styles')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="author" content="tiber1um@yandex.ru">
</head>
<body>

    <main>

        @include('chunks.header')

        <div class="main-content">

            @yield('content')

        </div>

        @include('chunks.footer')

    </main>

    <script src="{{ asset(mix('js/main.js')) }}"></script>

    @stack('scripts')

</body>
</html>
