
@forelse($objects as $object)

    <div class="col-6">
        <div class="object-wraper">
            <p class="object-title">
                {{--<a href="{{ route('apartment', $object->id) }}">--}}
                    {{--{{ $object->title }}--}}
                {{--</a>--}}
            </p>
            <div class="object-address pt-2 pb-3">
                <i class="fa fa-map-marker mr-1"></i>
                <span class="address">г. {{ $apartment->city->title }}, {{ $apartment->street }}, д. {{ $apartment->apartment->house_number }}</span>
            </div>
            <div class="object-image">
                <img src="{{ asset($apartment->images->first()->thumb_url) }}" alt="{{ $apartment->city->title }}">
            </div>
            <div class="object-info">
                <div class="object-price">
                    <p class="price-big mb-0">{{ number_format($apartment->price, 0, '', ' ') }} ₽</p>
                    <p class="price-small mb-0">{{ number_format($apartment->price_m, 0, '', ' ') }} ₽ за м<sup>2</sup></p>
                </div>
                <!--↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ АНАТОЛИЙ! НЕ ВЗДУМАЙ УДАЛИТЬ ЭТОТ КОМЕНТАРИЙ ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓-->
                <!-- <div class="icons">
                    <a href="#" class="clone"><i class="fa fa-clone"></i></a>
                    <a href="#" class="heart"><i class="fa fa-heart-o"></i></a>
                </div> -->
            </div>

        </div>
    </div>
@empty

    <div class="col-md-12">
        По вашему запросу ничего не найдено :(
    </div>

@endforelse
