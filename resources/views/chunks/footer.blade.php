<div class="footer py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 bl">
                <ul class="footer-menu text-right mb-0">
                    <li><a href="{{ route('apartments') }}">Квартиры</a></li>
                    <li><a href="{{ route('rooms') }}">Комнаты</a></li>
                    <li><a href="{{ route('houses') }}">Дома</a></li>
                    <li><a href="{{ route('lands') }}">Участки</a></li>
                    <li><a href="{{ route('garages') }}">Гаражи</a></li>
                    <li><a href="{{ route('commerces') }}">Комм. недвижимость</a></li>
                </ul>
            </div>
            <div class="col-md-3 bl">
                <a href="{{route('home')}}" target="_blank">
                    <img src="{{ asset('/img/logo.png') }}" alt="Недвижка31">
                </a>
            </div>
            <div class="col-md-3 bl">
                <a href="https://webstyle.top/" target="_blank">
                    <img src="{{ asset('/images/webstyle-logo.png') }}" alt="Создание и продвижение сайтов">
                </a>
            </div>
        </div>
    </div>
</div>
