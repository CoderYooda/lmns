<div class="object-wraper pt-0">
    <div class="object-image">
        <img src="{{ asset('/img/object.png') }}" alt="image">
    </div>
	<p class="object-title">Просторная квартира без окон</p>
	<div class="object-info">
        <div class="object-price">
            <p class="price-big mb-0">2 500 333 р</p>
            <p class="price-small mb-0">73 000/м<sup>2</sup></p>
        </div>
        <!---↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ АНАТОЛИЙ! НЕ ВЗДУМАЙ УДАЛИТЬ ЭТОТ КОМЕНТАРИЙ ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓-->
        <!-- <div class="icons">
            <a href="#" class="clone"><i class="fa fa-clone"></i></a>
            <a href="#" class="heart"><i class="fa fa-heart-o"></i></a>
        </div> -->
    </div>
</div>