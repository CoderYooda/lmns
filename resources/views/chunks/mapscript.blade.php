<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    window.maploaded = false;
    ymaps.ready(function () {
        myMap = new ymaps.Map('map', {
            center: [50.597467, 36.588849],
            zoom: 12,
            behaviors: ['default', 'scrollZoom']
        }, {
            searchControlProvider: 'yandex#search'
        });
    });

    window.loadMap = function(category, type){
        window.category = category;
        window.type = type;
        if(maploaded){
            $('#mapCollapse').collapse('toggle');
        } else {
            $('#mapCollapse').collapse('show');
        }
    };
    $('#mapCollapse').on('hidden.bs.collapse', function(){
        $('#mapCollapse').addClass('cloack');
        $('#mapbtn').text('Показать на карте');
    });
    $('#mapCollapse').on('shown.bs.collapse', function () {
        $('#mapbtn').text('Закрыть карту');
        $.ajax({
            url: '{{route('loadMapaData')}}',
            method: 'POST',
            data: {category: category, type: type},
            success: function (data) {
                window.maploaded = true;
                var clusterer = new ymaps.Clusterer({
                        preset: 'islands#invertedVioletClusterIcons',
                        groupByCoordinates: false,
                        clusterDisableClickZoom: true,
                        clusterHideIconOnBalloonOpen: false,
                        geoObjectHideIconOnBalloonOpen: false
                    }),
                    getPointData = function (index) {
                        return {
                            balloonContentHeader: '<font size=3><b><a target="_blank" href="' + index.link + '">' + index.title + '</a></b></font>',
                            balloonContentBody: ' <img class="inmap-img" src="' + index.thumb + '" alt="' + index.title + '">',
                            balloonContentFooter: '<span>'+ index.price +'</span><strong style="float:right;"><a href="' + index.link + '" class="heart">Подробнее</a></strong>',
                            clusterCaption: index.title
                        };
                    },
                    getPointOptions = function () {
                        return {
                            preset: 'islands#violetIcon'
                        };
                    },
                    points = data.objects,
                    geoObjects = [];

                for (var i = 0, len = points.length; i < len; i++) {
                    geoObjects[i] = new ymaps.Placemark(points[i].point, getPointData(points[i]), getPointOptions());
                }

                clusterer.options.set({
                    gridSize: 80,
                    clusterDisableClickZoom: true
                });
                clusterer.add(geoObjects);
                myMap.geoObjects.add(clusterer);

                myMap.setBounds(clusterer.getBounds(), {
                    checkZoomRange: true, zoomMargin: 50
                }).then(
                    function () {
                        if (myMap.getZoom() > 11) myMap.setZoom(11);
                        console.log(1);
                        $('#mapCollapse').removeClass('cloack');
                    })
            },
            error: function (data) {
                $('#mapCollapse').collapse('hide');
            }
        });
    });


</script>
