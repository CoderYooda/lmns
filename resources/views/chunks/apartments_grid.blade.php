
@forelse($apartments as $apartment)

    <div class="col-6">
        <div class="object-wraper">
            <p class="object-title">
                <a href="{{ route('apartment', $apartment->id) }}">
                    {!! $apartment->title  !!}
                </a>
            </p>
            <div class="object-address pt-2 pb-3">
                <i class="fa fa-map-marker mr-1"></i>
                <span class="address">г. {{ $apartment->city->title }}, {{ $apartment->street }}, д. {{ $apartment->apartment->house_number }}</span>
            </div>
            <div class="object-image">
                <img src="{{ asset($apartment->images->first()->thumb_url) }}" alt="{{ $apartment->city->title }}">
            </div>
            <div class="object-info">
                <div class="object-price">
                    <p class="price-big mb-0">{{ number_format($apartment->price, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i></p>
                    <p class="price-small mb-0">{{ number_format($apartment->price_m, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i> за м<sup>2</sup></p>
                </div>
            </div>

        </div>
    </div>
@empty

    <div class="col-md-12">
        По вашему запросу ничего не найдено :(
    </div>

@endforelse
