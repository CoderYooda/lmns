<header class="header">
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="p-0 m-0">
						{{--<li><a href="#">Вступить в ассоциацию</a></li>--}}
						{{--<li><a href="#">Блог</a></li>--}}
						<li class="float-right">
                            @if(Auth::guest())
							<a href="{{ route('login') }}">
								<i class="fa fa-user"></i>
								Вход для агентов
							</a>
                            @else
                            <a href="{{ route('DashBoard') }}">
                                <i class="fa fa-user"></i>
                                Панель управления
                            </a>
                            @endif
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<a href="{{ route('home') }}" class="logo"><img src="{{ asset('/img/logo-full.svg') }}" style="height: 55px;" alt="Недвижка31"></a>
				</div>
				<div class="col-md-8">
					<ul class="main-menu text-right mb-0">
						<li><a href="{{ route('apartments') }}">Квартиры</a></li>
						<li><a href="{{ route('rooms') }}">Комнаты</a></li>
						<li><a href="{{ route('houses') }}">Дома</a></li>
						<li><a href="{{ route('lands') }}">Участки</a></li>
						<li><a href="{{ route('garages') }}">Гаражи</a></li>
						<li><a href="{{ route('commerces') }}">Комм. недвижимость</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
