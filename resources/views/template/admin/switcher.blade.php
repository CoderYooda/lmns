<div id="setting">
    <div class="setting rounded-bottom" id="theme">
        <div class="box-body">
            <span class="mb-3 d-block text-xs hidden-folded">Настройка темы</span>
            <p id="settingLayout">
                <label class="md-check my-1 d-block">
                    <input type="checkbox" name="container">
                    <i></i>
                    <span>Бокс контент</span>
                </label>
            </p>
            <p>Палитра:</p>
            <p>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="primary">
                    <i class="primary"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="accent">
                    <i class="accent"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="warn">
                    <i class="warn"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="info">
                    <i class="info"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="success">
                    <i class="success"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="warning">
                    <i class="warning"></i>
                </label>
                <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                    <input type="radio" name="theme" value="danger">
                    <i class="danger"></i>
                </label>
            </p>
            <div class="row no-gutters">
                <div class="col">
                    <p>Лого</p>
                    <p>
                        <label class="radio radio-inline m-0 mr-1 ui-check">
                            <input type="radio" name="brand" value="dark-white">
                            <i class="light"></i>
                        </label>
                        <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                            <input type="radio" name="brand" value="dark">
                            <i class="dark"></i>
                        </label>
                    </p>
                </div>
                <div class="col mx-2">
                    <p>Меню</p>
                    <p>
                        <label class="radio radio-inline m-0 mr-1 ui-check">
                            <input type="radio" name="aside" value="white">
                            <i class="light"></i>
                        </label>
                        <label class="radio radio-inline m-0 mr-1 ui-check ui-check-color">
                            <input type="radio" name="aside" value="dark">
                            <i class="dark"></i>
                        </label>
                    </p>
                </div>
                <div class="col">
                    <p>Тема</p>
                    <div class="clearfix">
                        <label class="radio radio-inline ui-check">
                            <input type="radio" name="bg" value="">
                            <i class="light"></i>
                        </label>
                        <label class="radio radio-inline ui-check ui-check-color">
                            <input type="radio" name="bg" value="dark">
                            <i class="dark"></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
