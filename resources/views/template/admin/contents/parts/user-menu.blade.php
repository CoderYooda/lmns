<!-- User dropdown menu -->
<li class="dropdown d-flex align-items-center">
    <a href="#" data-toggle="dropdown" class="d-flex align-items-center">
	    	        <span class="avatar w-32">
                        @if(Auth::user()->Avatar()->first() != null)
                            <img class="user-thumb" src="{{Auth::user()->Avatar()->first()->thumb_url}}" alt="{{Auth::user()->lastname}} {{Auth::user()->firstname}}">
                        @else
                            <img class="user-thumb" src="{{asset('img/noava.png')}}" alt="Нет аватара">
                        @endif
	    	        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-right w pt-0 mt-2 animate fadeIn">
        <a class="dropdown-item" href="{{route('Profile', Auth::user()->id)}}">
            <span>Профиль</span>
        </a>
        <a class="dropdown-item" href="{{route('EditProfile', Auth::user()->id)}}">
            <span>Настройки</span>
        </a>
        {{--<a class="dropdown-item" href="app.inbox.html">--}}
            {{--<span class="float-right"><span class="badge info">6</span></span>--}}
            {{--<span>Нужна помощь!</span>--}}
        {{--</a>--}}
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
        <form id="logout-form" action="{{ route('logout')}}" method="POST" style="display: none;">
            {{ csrf_field()}}
        </form>
    </div>
</li>
