@extends('template.admin.layouts.2')

@section('content')
<div id="content" class="app-content box-shadow-2 box-radius-2" role="main">
    <!-- Header -->
    <div class="content-header white  box-shadow-2" id="content-header">
        <div class="navbar navbar-expand-lg">
            <!-- btn to toggle sidenav on small screen -->
            <a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
            </a>
            {{--<a href="{{URL::previous()}}" class="back-btn"><i class="fa fa-backward"></i></a>--}}
            <!-- Page title -->
            <div class="navbar-text nav-title flex" id="pageTitle">

                @yield('pagename')
            </div>

            <ul class="nav flex-row order-lg-2">
                @include('template.admin.contents.parts.user-menu')
            </ul>
            <!-- Navbar collapse -->
        </div>
        <div id="create_object" class="collapse text-center">
            <div style="width: 100%;" >
                <div class="create-object-container">
                    <div class="brick-row row">
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'apartment'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/apartment.svg')}}" alt="." class=""></div>
                                    <span class="text-md _500 block">Квартира</span>
                                </div>
                            </a>
                        </div>
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'room'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/room.svg')}}" alt="." class=""></div>
                                    <span class="text-md _500 block">Комната</span>
                                </div>
                            </a>
                        </div>
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'house'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/house.svg')}}" alt="." class=""></div>
                                    <span class="text-md _500 block">Дом</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="brick-row row">
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'land'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/land.svg')}}" alt="." class=""></div>
                                    <span class="text-md _500 block">Участок</span>
                                </div>
                            </a>
                        </div>
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'garage'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/garage.svg')}}" alt="." class=""></div>
                                    <span class="text-md _500 block">Гараж</span>
                                </div>
                            </a>
                        </div>
                        <div class="brick-col col-sm-6 col-lg-4">
                            <a href="{{route('getBaseForm', ['type' => 'commerce'])}}">
                                <div class="p-2">
                                    <div class="round_prev"><img src="{{asset('assets/images/svg/commerce.svg')}}" alt="." class=""></div>
                                    <span href="#" class="text-md _500 block">Коммерция</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <button onclick="$('#content-main').toggleClass('backdrop');" data-toggle="collapse" data-target="#create_object" class="close-selecter">Закрыть</button>
        </div>
    </div>
    <!-- Main -->
    <div class="content-main" id="content-main">

        @yield('main')

    </div>
    <!-- Footer -->
    <div class="content-footer white " id="content-footer">
        <div class="d-flex p-3">
            <span class="text-sm text-muted flex">&copy; Все права защищены. Webstyle</span>
            <div class="text-sm text-muted">Версия {{ config('app.version', '2.0.0') }}</div>
        </div>
    </div>
</div>
@endsection
