<div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
    <div class="sidenav modal-dialog dk white">
        <!-- sidenav top -->
        <div class="navbar bg">
            <!-- brand -->
            <a href="{{route('DashBoard')}}" class="navbar-brand">
                <span class="hidden-folded d-inline">{{ config('app.name', 'Laravel') }}</span>
            </a>
            <!-- / brand -->
        </div>

        <!-- Flex nav content -->
        <div class="flex hide-scroll">
            <div class="scroll">
                <div class="nav-stacked nav-active-theme" data-nav>
                    <ul class="nav bg">
                        <li class="nav-header">
                            <div class="py-3">
                                <button onclick="$('#aside').modal('hide'); $('#content-main').toggleClass('backdrop');" data-toggle="collapse" data-target="#create_object" class="btn btn-sm success theme-accent btn-block">
                                    <i class="fa fa-fw fa-plus"></i>
                                    <span class="hidden-folded d-inline">Новый объект</span>
                                </button>
                            </div>
                            <span class="text-xs hidden-folded">Главное меню</span>
                        </li>
                        {{--<li class="{{ request()->is('admin') ? 'active' : '' }}">--}}
                            {{--<a href="{{route('DashBoard')}}">--}}
		                  {{--<span class="nav-icon">--}}
		                    {{--<i class="fa fa-dashboard"></i>--}}
		                  {{--</span>--}}
                                {{--<span class="nav-text">Главная</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        <li class="{{ request()->is('home/') ? 'active' : '' }}">
                            <a target="_blank" href="{{route('home')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-reply"></i>
		                  </span>
                                <span class="nav-text">На сайт</span>
                            </a>
                        </li>
                        @if(Auth::user()->hasAgency())
                            <li class="{{ request()->is('admin/agency*') ? 'active' : '' }}">
                                <a href="{{route('Agency')}}">
                              <span class="nav-icon">
                                <i class="fa fa-users"></i>
                              </span>
                                    <span class="nav-text">Моё агентство</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('admin/export*') ? 'active' : '' }}">
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                        <i class="fa fa-cloud-upload"></i>
                                    </span>
                                    <span class="nav-text">Экспорт</span>
                                </a>
                                <ul class="nav-sub">
                                    <li class="{{ request()->is('admin/export/generate') ? 'active' : '' }}">
                                        <a href="{{route('exportGenerate')}}">
                                            <span class="nav-text">Автовыгрузка</span>
                                        </a>
                                    </li>
                                    <li class="{{ request()->is('admin/export') ? 'active' : '' }}">
                                        <a href="{{route('exportPage')}}">
                                            <span class="nav-text">Объекты</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li class="{{ request()->is('admin/profile_'.Auth::user()->id.'*') ? 'active' : '' }}">
                            <a href="{{route('Profile', Auth::user()->id)}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-address-book"></i>
		                  </span>
                                <span class="nav-text">Мой профиль</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('admin/exclusive*') ? 'active' : '' }}">
                            <a href="{{route('listExclusive')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-diamond"></i>
		                  </span>
                                <span class="nav-text">Эксклюзив</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('admin/objects/create*') ? 'active' : '' }}">
                            <a>
		                  <span class="nav-caret">
		                    <i class="fa fa-caret-down"></i>
		                  </span>
                                <span class="nav-icon">
		                     <i class="fa fa-fw fa-plus"></i>
		                  </span>
                                <span class="nav-text">Новый объект</span>
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'apartment'])}}">
                                        <span class="nav-text">Квартира</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'room'])}}">
                                        <span class="nav-text">Комната</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'house'])}}">
                                        <span class="nav-text">Дом</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'land'])}}">
                                        <span class="nav-text">Участок</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'garage'])}}">
                                        <span class="nav-text">Гараж</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('getBaseForm', ['type' => 'commerce'])}}">
                                        <span class="nav-text">Коммерция</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    @include('template.admin.switcher')
                </div>
            </div>
        </div>

        <!-- sidenav bottom -->
        <div class="no-shrink lt">
            <div class="nav-fold">
                @include('template.admin.contents.parts.bottom-user-menu')
                <div class="hidden-folded flex p-2-3 bg">
                    <div class="d-flex p-1">
                        <a href="javascript:void(0)" class="flex text-nowrap">
                        </a>
                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="px-2" data-toggle="tooltip" title="Выход">
                            <i class="fa fa-power-off text-muted"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
