<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Laravel') }} | Администратор</title>
    <meta name="description" content="Responsive, Bootstrap, BS4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{asset('img/logo.svg')}}">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('img/logo.svg')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- style -->

    <link rel="stylesheet" href="{{asset('libs/font-awesome/css/font-awesome.min.css')}}" type="text/css" />

    <!-- build:css ../assets/css/app.min.css -->
    <link rel="stylesheet" href="{{asset('libs/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/admin.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}" type="text/css" />
    <!-- endbuild -->
</head>
<body>


<div class="app" id="app">

@include('template.admin.asides.2')

@yield('content')

</div>



{{--@include('template.admin.switcher')--}}

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
<script src="{{asset('libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- core -->
<script src="{{asset('libs/pace-progress/pace.min.js')}}"></script>
<script src="{{asset('libs/pjax/pjax.js')}}"></script>

<script src="{{asset('libs/notie/dist/notie.min.js')}}"></script>

<script src="{{asset('assets/js/lazyload.config.js')}}"></script>
<script src="{{asset('assets/js/lazyload.js')}}"></script>
<script src="{{asset('assets/js/plugin.js')}}"></script>
<script src="{{asset('assets/js/nav.js')}}"></script>
<script src="{{asset('assets/js/scrollto.js')}}"></script>
<script src="{{asset('assets/js/toggleclass.js')}}"></script>
<script src="{{asset('assets/js/theme.js')}}"></script>
<script src="{{asset('assets/js/ajax.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>

<script src="{{asset('js/admin.js')}}"></script>

@stack('scripts')
<!-- endbuild -->
</body>
</html>
