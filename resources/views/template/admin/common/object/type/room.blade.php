<div class="body-creat-object mt-3 mt-lg-0 mb-3 mb-lg-0">
    <div class="container">
        <?php if(isset($object)){$object->room = $object->room->where('object_id',$object->id)->with('wall_materials', 'specials', 'comforts', 'build_comforts')->first();} ?>
        <form onsubmit="$(this).find('.save_butt').attr('disabled', true);" id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="room">
            <input type="hidden" name="watermark" value="1">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row bg-grey">
                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-6 pr-1">
                                    @include('template.admin.common.object.type.fields.floor')
                                </div>
                                <div class="col-6 pl-1">
                                    @include('template.admin.common.object.type.fields.n_of_floor')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.build_type')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.year')
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-3 map-container">
                    <div id="map" style="height: 100%; min-height: 300px"></div>
                </div>


                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-lg-6 col-12 mb-3">
                            @include('template.admin.common.object.type.fields.price_rent')
                        </div>
                        <div class="col-lg-6 col-12">
                            @include('template.admin.common.object.type.fields.dop_params')
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.room_type')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.state')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.room_count')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.total_square')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.rooms_square')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.people_count')
                        </div>
                    </div>
                </div>
            </div>

            <div class="row bg-grey">
                <div class="col-md-12 mb-3">
                    @include('template.admin.common.object.type.fields.description')
                </div>
            </div>

            @include('template.admin.common.object.type.fields.image-upload')

            <div class="row mb-3 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">

                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_blag_kvartiri">
                                    Особенности комнаты
                                </button>
                            </div>
                            <div id="ac_blag_kvartiri" class="collapse show b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->room_specials as $room_special)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="specials[]" value="{{$room_special->id}}" id="room_specials{{$room_special->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->room->specials->pluck('id') as $c)
                                                               @if($c == $room_special->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$room_special->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Благоустройство комнаты
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->room_comfort as $room_comfort)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="comforts[]" value="{{$room_comfort->id}}" id="layout{{$room_comfort->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->room->comforts->pluck('id') as $l)
                                                               @if($l == $room_comfort->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$room_comfort->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_blag_zdaniya">
                                    Благоустройство здания и территории
                                </button>
                            </div>
                            <div id="ac_blag_zdaniya" class="collapse b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->build_comfort as $build_comfort)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="build_comforts[]" value="{{$build_comfort->id}}" id="build_comfort{{$build_comfort->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->room->build_comforts->pluck('id') as $b)
                                                               @if($b == $build_comfort->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$build_comfort->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @include('template.admin.common.object.type.fields.save_button')
        </form>
    </div>
</div>

