<div class="body-creat-object mt-3 mt-lg-0 mb-3 mb-lg-0">
    <div class="container">
        <?php if(isset($object)){$object->commercemeta = $object->commerce->where('object_id',$object->id)->with('wall_materials', 'build_comforts', 'specials', 'adds','doing', 'gruz', 'kind', 'purpose', 'sklad', 'tools', 'type')->first();} ?>
        <form onsubmit="$(this).find('.save_butt').attr('disabled', true);" id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="commerce">
            <input type="hidden" name="watermark" value="1">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row bg-grey">
                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.house_num')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.wall_materials')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-6 pr-1">
                                    @include('template.admin.common.object.type.fields.floor')
                                </div>
                                <div class="col-6 pl-1">
                                    @include('template.admin.common.object.type.fields.n_of_floor')
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.commerce_type')
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('template.admin.common.object.type.fields.year')
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.state')
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.doing')
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.gruz')
                        </div>

                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.sklad')
                        </div>

                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-3 map-container">
                    <div id="map" style="height: 100%; min-height: 300px"></div>
                </div>

                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-lg-6 col-12 mb-3">
                            @include('template.admin.common.object.type.fields.price_rent')
                        </div>
                        <div class="col-lg-6 col-12">
                            @include('template.admin.common.object.type.fields.dop_params')
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="row">

                        <div class="col-md-6">
                            @include('template.admin.common.object.type.fields.comm_room_count')
                        </div>

                        <div class="col-md-6 mb-2">
                            @include('template.admin.common.object.type.fields.kind')
                        </div>


                        <div class="col-md-12 mb-2">
                            @include('template.admin.common.object.type.fields.purpose')
                        </div>


                        <div class="col-md-4">
                            <p class="label"><span class="badge warning pos-rlt mr-2">!</span>S Объекта</p>
                            <input id="obshaya" min="0" max="100000" step="0.1" value="@if(isset($object)){{$object->commerce->total_square}}@endif" name="total_square" type="number" placeholder="S Объекта м²" class="form-control mb-2" required>
                        </div>

                        <div class="col-md-4">
                            <p class="label">S Участка</p>
                            <input  min="0" max="100000" step="0.1" value="@if(isset($object)){{$object->commerce->sector_square}}@endif" name="sector_square" type="number" placeholder="S Участка м²" class="form-control mb-2">
                        </div>

                        <div class="col-md-4">
                            <p class="label">Высота</p>
                            <input id="zhilaya" min="0" max="100000" step="0.1" value="@if(isset($object)){{$object->commerce->height}}@endif" name="height" type="number" placeholder="Высота в м" class="form-control mb-2">
                        </div>



                    </div>
                </div>
            </div>

            <div class="row bg-grey">
                <div class="col-md-12 mb-3">
                    @include('template.admin.common.object.type.fields.description')
                </div>
            </div>

            @include('template.admin.common.object.type.fields.image-upload')


            <div class="row mb-3 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_blag_zdaniya">
                                    Благоустройство здания и территории
                                </button>
                            </div>
                            <div id="ac_blag_zdaniya" class="collapse show b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_build_comfort as $build_comfort)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="build_comforts[]" value="{{$build_comfort->id}}" id="build_comfort{{$build_comfort->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->commerce->build_comforts->pluck('id') as $b)
                                                               @if($b == $build_comfort->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$build_comfort->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_planirovka">
                                    Особенности
                                </button>
                            </div>

                            <div id="ac_planirovka" class="collapse b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_special as $specials)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="specials[]" value="{{$specials->id}}" id="specials{{$specials->id}}" class="css-checkbox"
                                                                @if(isset($object))
                                                                @foreach($object->commerce->specials->pluck('id') as $l)
                                                                @if($l == $specials->id)
                                                                checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$specials->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_adds">
                                    Доп характеристики
                                </button>
                            </div>
                            <div id="ac_adds" class="collapse b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_adds as $commerce_add)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="adds[]" value="{{$commerce_add->id}}" id="add{{$commerce_add->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->commerce->adds->pluck('id') as $c)
                                                               @if($c == $commerce_add->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$commerce_add->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse" data-target="#ac_tools">
                                    Оборудование
                                </button>
                            </div>
                            <div id="ac_tools" class="collapse b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->commerce_tools as $commerce_tool)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="tools[]" value="{{$commerce_tool->id}}" id="tool{{$commerce_tool->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->commerce->tools->pluck('id') as $c)
                                                               @if($c == $commerce_tool->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$commerce_tool->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @include('template.admin.common.object.type.fields.save_button')
        </form>
    </div>
</div>

