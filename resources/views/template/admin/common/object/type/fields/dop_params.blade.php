
<div id="dops">
    <p class="label">Доп. параметры</p>
    <p>
        <label class="md-switch w-100">
            <input type="checkbox" value="1" name="notary" id="notarius" class="css-checkbox" @if(isset($object) && $object->notary)checked="checked"@endif>
            <i class="blue"></i>
            <span class="pull-left">Нотариус</span>
        </label>
    </p>
    <p>
        <label class="md-switch w-100">
            <input type="checkbox" value="1" name="custody" id="custody" class="css-checkbox" @if(isset($object) && $object->custody)checked="checked"@endif>
            <i class="blue"></i>
            Опека
        </label>
    </p>

    <p>
        <label class="md-switch w-100">
            <input type="checkbox" value="1" name="burden" id="obremenenie" class="css-checkbox" @if(isset($object) && $object->burden)checked="checked"@endif>
            <i class="blue"></i>
            Обременение
        </label>
    </p>

    <p>
        <label class="md-switch w-100">
            <input type="checkbox" value="1" name="exlusive" id="exlusive" class="css-checkbox" @if(isset($object) && $object->exlusive)checked="checked"@endif>
            <i class="blue"></i>
            Эксклюзив
        </label>
    </p>
    <div id="under_exclusive" style="display: none;">
        <p>
            <label class="md-switch w-100">
                <input type="checkbox" value="1" name="writed" id="writed" class="css-checkbox" @if(isset($object) && $object->writed)checked="checked"@endif>
                <i class="blue"></i>
                Письменно
            </label>
        </p>
        <p>
            <div class='form-group'>
                <div id="under_writed" style="display: none; position:relative">
                    <input type="text" data-plugin="datepicker" data-option="{container:'#content-main'}" class="form-control datepicker-here" id="exlusive_date" name="exlusive_date" value="@if(isset($object)){{$object->exlusive_date}}@endif" data-position="top center" placeholder="Эксклюзив дата" />
                </div>
            </div>
        </p>
    </div>
</div>
