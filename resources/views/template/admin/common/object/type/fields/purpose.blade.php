<p class="label">Назначение здания</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="purpose_id" id="tip_kvartiri" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_purpose as $commerce_purpose)
        <option value="{{$commerce_purpose->id}}"
                @if(isset($object) && $object->commerce->purpose_id == $commerce_purpose->id)selected="selected"@endif
        >{{$commerce_purpose->title}}</option>
    @endforeach
</select>
