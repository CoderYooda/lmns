<p class="label"><span class="badge warning pos-rlt mr-2">!</span>Материал стен</p>
<select data-plugin="select2" data-option="{ placeholder: 'Не указано',  minimumResultsForSearch: 8}" name="wall_materials[]" id="wall_materials" class="form-control select-init" required multiple="multiple">
    @foreach($params->apartment_wall_material as $wall_material)
        <option value="{{$wall_material->id}}"
                @if(isset($object))
                    @foreach($object->{$object->category->type}->wall_materials->pluck('id') as $wm)
                        @if($wm == $wall_material->id)
                        selected="selected"
                        @endif
                    @endforeach
                @endif
        >{{$wall_material->title}}</option>
    @endforeach
</select>
