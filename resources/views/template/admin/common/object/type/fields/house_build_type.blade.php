<p class="label">Тип объекта</p>
<select data-plugin="select2" data-option="{ minimumResultsForSearch: 8}" name="build_type_id" id="build_type_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->house_build_type as $build_type)
        <option value="{{$build_type->id}}"
                @if(isset($object) && $object->house->build_type_id == $build_type->id)selected="selected"@endif
        >{{$build_type->title}}</option>
    @endforeach
</select>
