<p class="label"><span class="badge warning pos-rlt mr-2">!</span>Город</p>
<select onchange="searchGeocoder();object.loadStreets(this.value, '{{ route('streets') }}', event)" data-plugin="select2" data-option="{}" name="city_id" id="city" class="form-control select-init" required>
    @foreach($params->city as $city)
        <option value="{{$city->id}}" @if(isset($object) && $object->city_id == $city->id)selected="selected"@endif>{{$city->socr}}. {{$city->name}}</option>
    @endforeach
</select>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        object.loadStreets($('#city').val(), '{{ route('streets') }}', event)
    });
</script>
{{--onchange="searchOnMap()"--}}
