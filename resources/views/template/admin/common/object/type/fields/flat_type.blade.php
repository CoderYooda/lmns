<p class="label">Тип квартиры</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="apartment_type_id" id="tip_kvartiri" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->apartment_type as $apartment_type)
        <option value="{{$apartment_type->id}}"
                @if(isset($object) && $object->apartment->apartment_type_id == $apartment_type->id)selected="selected"@endif
        >{{$apartment_type->title}}</option>
    @endforeach
</select>
