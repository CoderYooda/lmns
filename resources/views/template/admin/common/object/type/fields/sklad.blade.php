<p class="label">Вид хранения</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="sklad_id" id="sklad_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_sklad as $sklad)
        <option value="{{$sklad->id}}"
                @if(isset($object) && $object->commerce->sklad_id == $sklad->id)selected="selected"@endif
        >{{$sklad->title}}</option>
    @endforeach
</select>
