<p class="label">Ремонт</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="state_id" id="sostoyanie" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->state as $state)
        <option value="{{$state->id}}"
                @if(isset($object) && $object->{$object->category->type}->state_id == $state->id)selected="selected"@endif
        >{{$state->title}}</option>
    @endforeach
</select>
