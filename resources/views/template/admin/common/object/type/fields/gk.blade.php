<p class="label">Жилищный комплекс</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}"  name="gk_id" id="zh_complex" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->gk as $gk)
        <option @if(isset($object) && $object->{$object->category->type}->gk_id == $gk->id)selected="selected"@endif value="{{$gk->id}}">{{$gk->title}}</option>
    @endforeach
</select>
