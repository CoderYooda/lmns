<p class="label">Кооператив</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}"  name="sgk_id" id="sgk_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->sgk as $sgk)
        <option value="{{$sgk->id}}" @if(isset($object) && $object->garage->sgk == $sgk->id)selected="selected"@endif>{{$sgk->title}}</option>
    @endforeach
</select>
