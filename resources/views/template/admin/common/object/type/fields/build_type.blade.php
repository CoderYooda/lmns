<p class="label">Тип здания</p>
<select data-plugin="select2" data-option="{ minimumResultsForSearch: 8}" name="build_type_id" id="build_type_id" class="form-control select-init">
    @foreach($params->build_type as $build_type)
        <option value="{{$build_type->id}}"
                @if(isset($object) && $object->{$object->category->type}->build_type_id == $build_type->id)selected="selected"@endif
        >{{$build_type->title}}</option>
    @endforeach
</select>
