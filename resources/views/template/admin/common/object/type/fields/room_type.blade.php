<p class="label">Тип комнаты</p>
<select data-plugin="select2" data-option="{ minimumResultsForSearch: 8}"  name="room_type_id" id="tip_kvartiri" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->room_type as $room_type)
        <option value="{{$room_type->id}}"
                @if(isset($object) && $object->room->room_type_id == $room_type->id)selected="selected"@endif
        >{{$room_type->title}}</option>
    @endforeach
</select>
