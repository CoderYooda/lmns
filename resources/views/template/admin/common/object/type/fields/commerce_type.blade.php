<p class="label">Тип коммерции</p>
<select data-plugin="select2" data-option="{ placeholder: 'Не указано',  minimumResultsForSearch: 8}" name="commerce_type_id" id="commerce_type_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_type as $commerce_type)
        <option value="{{$commerce_type->id}}"
                @if(isset($object) && $object->commerce->commerce_type_id == $commerce_type->id)selected="selected"@endif
        >{{$commerce_type->title}}</option>
    @endforeach
</select>
