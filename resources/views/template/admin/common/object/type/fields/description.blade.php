<p class="label">Описание объекта</p>
<textarea data-plugin="summernote" data-option="{
        lang: 'ru-RU',
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['paragraph']]
        ]
      }" class="form-control summernote" style="width: 100%; padding: 10px; max-width: 100%; " name="description" id="" cols="30" maxlength="5000" rows="4">@if(isset($object)){{$object->description}}@endif</textarea>
