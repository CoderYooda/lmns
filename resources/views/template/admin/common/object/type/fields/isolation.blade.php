<p class="label">Изол. комнат</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="rooms_isolation_id" id="rooms_isolation_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->rooms_isolation as $rooms_isolation)
        <option value="{{$rooms_isolation->id}}"
                @if(isset($object) && $object->{$object->category->type}->rooms_isolation_id == $rooms_isolation->id)selected="selected"@endif
        >{{$rooms_isolation->title}}</option>
    @endforeach
</select>
