<div class="row">
    <div class="col-6">
        <div class="radio">
            <label class="ui-check">
                <input class="form-check-input" type="radio" name="rented" id="rentedon" style="width: auto;" value="1" @if(isset($object) && $object->rented)checked="checked"@endif>
                <i class="dark-white"></i>
                Аренда
            </label>
        </div>
    </div>
    <div class="col-6">
        <div class="radio">
            <label class="ui-check">
                <input class="form-check-input" type="radio" name="rented" id="rentedoff" style="width: auto;" value="0" @if(isset($object) && !$object->rented)checked="checked"@endif @if(!isset($object)) checked="checked" @endif>
                <i class="dark-white"></i>
                Продажа
            </label>
        </div>
    </div>
</div>

<div id="rented-box" class="mb-3">
    <p class="label">Цена в месяц</p>
    <input class="form-control price_mask" id="rent_price" maxlength="10" value="@if(isset($object)){{$object->rent_price}}@endif" name="rent_price" placeholder="Цена в месяц" class="mb-2 price_mask">
</div>

<div id="selded-box" class="mb-3">
    <p class="label">Цена р.</p>
    <input class="form-control price_mask" id="price" maxlength="14" value="@if(isset($object)){{$object->price}}@endif"  name="price" placeholder="Цена р." class="mb-2 price_mask">
</div>

<p class="label" class="mb-3">MLS</p>
    <input class="form-control price_mask" min="0" id="mls" maxlength="10" value="@if(isset($object)){{$object->mls}}@endif" name="mls"  placeholder="MLS" class="mb-2 price_mask">
