<p class="label">Тип гаража</p>
<select data-plugin="select2" data-option="{ minimumResultsForSearch: 8}"  name="garage_type_id" id="district" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->garage_type as $garage_type)
        <option value="{{$garage_type->id}}" @if(isset($object) && $object->garage->garage_type_id == $garage_type->id)selected="selected"@endif>{{$garage_type->title}}</option>
    @endforeach
</select>
