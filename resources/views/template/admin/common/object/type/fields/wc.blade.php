<p class="label">Сан. узел</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="wc_id" id="wc" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->wc as $wc)
        <option value="{{$wc->id}}"
                @if(isset($object) && $object->{$object->category->type}->wc_id == $wc->id)selected="selected"@endif
        >{{$wc->title}}</option>
    @endforeach
</select>
