<p class="label"><span class="badge warning pos-rlt mr-2">!</span>Улица</p>
@if(isset($object))
    <input id="street_help" type="hidden" name="street_help" value="{{$object->street}}">
@else
    <input id="street_help" type="hidden" name="street_help" value="">
@endif
<select onchange="searchGeocoder()" data-plugin="select2" data-option="{}" name="street" id="street" class="form-control select-init" required>
</select>
{{--<input onkeydown="searchGeocoder()" class="form-control" id="street" maxlength="60" value="@if(isset($object)){{$object->street}}@endif" name="street" type="text" placeholder="Улица" required>--}}
