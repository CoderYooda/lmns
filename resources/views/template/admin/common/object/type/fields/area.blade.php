<p class="label">Район</p>
<select data-plugin="select2" data-option="{ minimumResultsForSearch: 20}" name="area_id" id="area" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->area as $area)
        <option value="{{$area->id}}" @if(isset($object) && $object->area_id == $area->id)selected="selected"@endif>{{$area->title}}</option>
    @endforeach
</select>
