<p class="label">Вид деятельности</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="doing_id" id="doing_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_doing as $doing)
        <option value="{{$doing->id}}"
                @if(isset($object) && $object->commerce->doing_id == $doing->id)selected="selected"@endif
        >{{$doing->title}}</option>
    @endforeach
</select>
