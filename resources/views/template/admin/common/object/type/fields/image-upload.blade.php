<div class="row bg-grey">
    <div class="col-sm-12" id="imginfo">

    </div>
    <div class="col-md-8 mb-3">
        <div id="images" class="row row-xs" data-plugin="html5sortable">
            @if(isset($object))
                @foreach($object->images as $pic)
                <div data-id="{{$pic->id}}" class="col-6 col-sm-4 col-md-3 image-contracter">
                    <div id="{{$pic->id}}" class="box mt-2 mb-2 overflow-hidden">
                        <input type="hidden" name="pic[]" value="{{$pic->id}}">
                        <img src="{{$pic->thumb_url}}" alt="" class="w-100">
                        <div class="p-2"><div class="text-xs">
                                <i onclick="upload.rotateImage(this)" class="fa-button fa fa-undo"></i>
                                <i onclick="upload.removeImage(this)" class="fa-button fa fa-trash"></i>
                        </div></div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>
    <div class="col-md-4 mb-3">
        <div class="box mb-3  d-none d-lg-block">
            <div class="box-header">
                <h3>Информация</h3>
            </div>
            <div class="box-divider"></div>
            <div class="box-body">
                Максимальное кол-во файлов : 20<br>
                Максимальный размер файла : 5мб<br>
                Доступные форматы .jpg .png .gif<br>
            </div>
        </div>
        <button type="button" onclick="$('#image-butt').trigger('click')" class="btn btn-sm success theme-accent btn-block">Выбрать фото</button>
        <input onchange="upload.images('{{route('uploadImage')}}', '{{route('setIndexes')}}')" type="file" name="image[]" style="height: auto;" class="hide" multiple="multiple" id="image-butt">
    </div>
    <div class="col-12 mb-3" id="progressbar" style="display: none;">
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
</div>
