<p class="label">Готовность</p>
<select data-plugin="select2" data-option="{ placeholder: 'Не указано',  minimumResultsForSearch: 20}" name="readiness_id" id="readiness" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->readiness as $readiness)
        <option value="{{$readiness->id}}"
                @if(isset($object) && $object->{$object->category->type}->readiness_id == $readiness->id)selected="selected"@endif
        >{{$readiness->title}}</option>
    @endforeach
</select>
