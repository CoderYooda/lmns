<p class="label"><span class="badge warning pos-rlt mr-2">!</span>S Участка в сотках</p>
<input id="landarea" maxlength="6" value="@if(isset($object)){{$object->land->land_square}}@endif" name="land_square" type="number" min="0" max="10000" step="0.1" placeholder="Участок (сотки)" class="form-control mb-2" required>
