<p class="label">Вид здания</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="kind_id" id="kind_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_kind as $commerce_kind)
        <option value="{{$commerce_kind->id}}"
                @if(isset($object) && $object->commerce->kind_id == $commerce_kind->id)selected="selected"@endif
        >{{$commerce_kind->title}}</option>
    @endforeach
</select>
