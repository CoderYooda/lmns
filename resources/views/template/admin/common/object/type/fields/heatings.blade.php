<p class="label">Отопление</p>
<select data-plugin="select2" data-option="{ placeholder: 'Не указано',  minimumResultsForSearch: 8}" name="heatings[]" id="heating" class="form-control select-init" multiple="multiple">
    @foreach($params->heating as $heating)
        <option value="{{$heating->id}}"
                @if(isset($object))
                @foreach($object->{$object->category->type}->heatings->pluck('id') as $h)
                @if($h == $heating->id)
                selected="selected"
            @endif
            @endforeach
            @endif
        >{{$heating->title}}</option>
    @endforeach
</select>
