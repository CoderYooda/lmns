<p class="label">Погруз - разгруз</p>
<select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" name="gruz_id" id="gruz_id" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->commerce_gruz as $gruz)
        <option value="{{$gruz->id}}"
                @if(isset($object) && $object->commerce->gruz_id == $gruz->id)selected="selected"@endif
        >{{$gruz->title}}</option>
    @endforeach
</select>
