<p class="label">Водоснабжение</p>
<select data-plugin="select2" data-option="{ placeholder: 'Не указано',  minimumResultsForSearch: 8}" name="hot_water_system_id" id="water_supply" class="form-control select-init">
    <option value="">Не указано</option>
    @foreach($params->hot_water_system as $hot_water_system)
        <option value="{{$hot_water_system->id}}"
                @if(isset($object) && $object->{$object->category->type}->hot_water_system_id == $hot_water_system->id)selected="selected"@endif
        >{{$hot_water_system->title}}</option>
    @endforeach
</select>
