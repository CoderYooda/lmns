<div class="col-md-4 mb-3">
    <p class="label">Высота</p>
    <input id="height" value="@if(isset($object)){{$object->{$object->category->type}->height}}@endif" name="height" type="number" min="0" step="0.1" placeholder="высота" class="form-control mb-2">
</div>

<div class="col-md-4 mb-3">
    <p class="label">Ширина</p>
    <input id="width" value="@if(isset($object)){{$object->{$object->category->type}->width}}@endif" name="width" type="number" min="0" step="0.1" placeholder="ширина" class="form-control mb-2">
</div>

<div class="col-md-4 mb-3">
    <p class="label">Длинна</p>
    <input id="length" value="@if(isset($object)){{$object->{$object->category->type}->length}}@endif" name="length" type="number" min="0" step="0.1" placeholder="длинна" class="form-control mb-2">
</div>
