<div class="body-creat-object mt-3 mt-lg-0 mb-3 mb-lg-0">
    <div class="container">
        <?php if (isset($object)) {
            $object->landmeta = $object->land->where('object_id', $object->id)->with('comforts')->first();
        } ?>
        <form onsubmit="$(this).find('.save_butt').attr('disabled', true);" id="interform" action="{{route('saveObject')}}" method="POST">
            @csrf
            <input type="hidden" name="type" value="land">
            <input type="hidden" name="watermark" value="1">
            <input type="hidden" name="id" value="@if(isset($object)){{$object->id}}@endif">
            <input type="hidden" id="geopoint" name="geopoint" value="@if(isset($object)){{$object->geopoint}}@endif">
            <div class="row bg-grey">
                <div class="col-lg-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.city')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.area')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.street')
                        </div>
                        <div class="col-md-6 mb-3">
                            @include('template.admin.common.object.type.fields.house_num')
                        </div>

                        <div class="col-12 mb-3">
                            @include('template.admin.common.object.type.fields.land_sot_square')
                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-lg-6 col-12 mb-3">
                                    @include('template.admin.common.object.type.fields.price_rent')
                                </div>
                                <div class="col-lg-6 col-12">
                                    @include('template.admin.common.object.type.fields.dop_params')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-3 map-container">
                    <div id="map" style="height: 100%; min-height: 300px"></div>
                </div>

            </div>

            <div class="row bg-grey">
                <div class="col-md-12 mb-3">
                    @include('template.admin.common.object.type.fields.description')
                </div>
            </div>

            @include('template.admin.common.object.type.fields.image-upload')


            <div class="row mb-3 bg-grey">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="box mb-2">
                            <div class="box-header">
                                <button class="btn btn-sm success theme-accent" type="button" data-toggle="collapse"
                                        data-target="#ac_blag_land">
                                    Благоустройство участка
                                </button>
                            </div>
                            <div id="ac_blag_land" class="collapse show b-t" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($params->land_comfort as $land_comfort)
                                            <div class="col-md-3">
                                                <p class="pb-2 pt-2">
                                                    <label class="md-check">
                                                        <input type="checkbox" name="comforts[]" value="{{$land_comfort->id}}"
                                                               id="land_comfort{{$land_comfort->id}}" class="css-checkbox"
                                                               @if(isset($object))
                                                               @foreach($object->landmeta->comforts->pluck('id') as $c)
                                                               @if($c == $land_comfort->id)
                                                               checked="checked"
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        >
                                                        <i class="blue"></i>
                                                        {{$land_comfort->title}}
                                                    </label>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('template.admin.common.object.type.fields.save_button')
        </form>
    </div>
</div>

