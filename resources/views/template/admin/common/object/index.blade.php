@extends('template.admin.contents.2')

@section('pagename')
    @if(isset($object))
        Редактирование ( {!!$object->title!!} )
    @else
        Создание объекта
        @if( request('type') != NULL )
            ({{app\Models\Category::where('type', $_GET['type'])->first()->title}})
        @endif
    @endif
@endsection

@section('main')
    <div class="p-lg-3" >
        @if(isset($_GET['type']))
            @include("template.admin.common.object.type." . $_GET['type'])
        @elseif(isset($object->category->type))
            @include("template.admin.common.object.type." . $object->category->type)
        @endif
    </div>
@endsection

@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>

        @if(isset($object))
            window.issetobject = true;
            window.object_point = [{{$object->geopoint}}];
        @else
            window.issetobject = false;
        @endif
        ymaps.ready(function(){
            yandexmmaps.init();
        });
     </script>
@endpush
