@extends('template.admin.contents.2')

@section('pagename', 'Эксклюзив')

@section('main')


<div class="row">
    <div class="col-12">
        <div class="padding" data-plugin="range_slider" style="padding-bottom: 0;padding-top: 1rem;">
            <form style="position: relative" id="object-filter" onsubmit="agency.applyFilter(this, event)" action="{{route('listExclusive')}}" method="GET">
                <div class="filter-preload"></div>
                <input name="price_max" type="hidden" value="{{$filter_params['price_max']}}">
                <input name="rent_price_max" type="hidden" value="{{$filter_params['rent_price_max']}}">
                <input name="square_max" type="hidden" value="{{$filter_params['square_max']}}">


                <input name="price_from" type="hidden" value="{{request('price_from')}}">
                <input name="price_to" type="hidden" value="{{request('price_to')}}">

                <input name="square_from" type="hidden" value="{{request('square_from')}}">
                <input name="square_to" type="hidden" value="{{request('square_to')}}">

                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="form-group" style="height: 40px;line-height: 62px;">
                            <div class="row">
                                <div class="col-6">
                                    <div class="radio">
                                        <label class="ui-check">
                                            <input
                                                onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=rent_price_max]').val());
                                                                    updateSlider();
                                                                    agency.setParam(this, event);"
                                                type="radio" name="type" value="rented" @if(request('type') == 'rented') checked @endif>
                                            <i class="dark-white"></i>
                                            Аренда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="radio">
                                        <label class="ui-check">
                                            <input
                                                onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=price_max]').val());
                                                                    updateSlider();
                                                                    agency.setParam(this, event);"
                                                type="radio" name="type" value="selded" @if(request('type') == 'selded' || request('type') == null) checked @endif>
                                            <i class="dark-white"></i>
                                            Продажа
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="form-group">
                            <input id="slider" type="text" class="form-control mb-3">
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="form-group" style="height: 45px;line-height: 62px;">
                            <select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" onchange="updateSlider();agency.setParam(this, event);" name="category"  class="form-control">
                                <option value="">Тип объекта</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if(request('category') == $category->id)selected @endif>{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="form-group">
                            <input id="square-slider" type="text" class="form-control mb-3">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="padding">
            <div id="filtered_objects" class="row row-sm ajax_preloder" style="position: relative;">
                @include('template.admin.common.agency.objects.collection',['redirect_url' => route('listExclusive')])
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
@endpush
