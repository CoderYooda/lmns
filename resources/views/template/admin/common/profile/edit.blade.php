@extends('template.admin.contents.2')

@section('pagename', 'Настройка пользователя')

@section('main')
        <div class="d-sm-flex">
            <div class="w w-auto-xs light bg bg-auto-sm b-r">
                <div class="py-3">
                    <div class="nav-active-border left b-primary">
                        <ul class="nav flex-column nav-sm">
                            <li class="nav-item">
                                <a onclick="session.saveToSession('personal_edit_link', 'personal')" class="nav-link @if(session('personal_edit_link') == 'personal' || session('personal_edit_link') == null) active @endif" href="#" data-toggle="tab" data-target="#tab-1">Личная информация</a>
                            </li>
                            <li class="nav-item">
                                <a onclick="session.saveToSession('personal_edit_link', 'contacts')" class="nav-link @if(session('personal_edit_link') == 'contacts') active @endif" href="#" data-toggle="tab" data-target="#tab-2">Контакты</a>
                            </li>
                            <li class="nav-item">
                                <a onclick="session.saveToSession('personal_edit_link', 'picture')" class="nav-link @if(session('personal_edit_link') == 'picture') active @endif" href="#" data-toggle="tab" data-target="#tab-3">Изображение</a>
                            </li>
                            <li class="nav-item">
                                <a onclick="session.saveToSession('personal_edit_link', 'password')" class="nav-link @if(session('personal_edit_link') == 'password') active @endif" href="#" data-toggle="tab" data-target="#tab-4">Пароль</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col p-0">
                <div class="tab-content pos-rlt">
                    <div class="tab-pane @if(session('personal_edit_link') == 'personal' || session('personal_edit_link') == null) active @endif" id="tab-1">
                        <div class="p-4 b-b _600">Личная информация</div>
                        <form onsubmit="agency.saveSettings(this, event);" action="{{route('ProfileSave', $user->id)}}" method="POST"  role="form" class="p-4 col-md-6">
                            @csrf
                            <input type="hidden" value="{{$user->id}}" name="user_id">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="lastname" id="lastname" value="{{$user->lastname}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="firstname" id="firstname" value="{{$user->firstname}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="midname" id="midname" value="{{$user->midname}}" class="form-control">
                            </div>
                            <a href="{{route('Profile', $user->id)}}" style="display: none;" class="tp_rofile btn btn-fw success theme-accent" >В профиль</a>
                            <button type="submit" class="btn primary">Сохранить</button>
                        </form>
                    </div>
                    <div class="tab-pane @if(session('personal_edit_link') == 'contacts') active @endif" id="tab-2">
                        <div class="p-4 b-b _600">Контакты</div>
                        <form onsubmit="agency.saveSettings(this, event);" action="{{route('ProfileSave', $user->id)}}" method="POST"  role="form" class="p-4 col-md-6">
                            <input type="hidden" value="{{$user->id}}" name="user_id">
                            <div class="form-group">
                                <label>Электронная почта</label>
                                <input type="text" name="email" id="email" value="{{$user->email}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" name="phone" id="phone" value="{{$user->phone}}" class="form-control">
                            </div>
                            <a href="{{route('Profile', $user->id)}}" style="display: none;" class="tp_rofile btn btn-fw success theme-accent" >В профиль</a>
                            <button type="submit" class="btn primary">Сохранить</button>
                        </form>
                    </div>
                    <div class="tab-pane @if(session('personal_edit_link') == 'picture') active @endif" id="tab-3">

                        <div class="p-4 b-b _600">Изображение</div>

                        <div class="p-4">
                            <div>

                                <form action="{{route('uploadImage')}}" id="upload_form" method="POST" enctype="multipart/form-data" style="display: none;">
                                    <input id="file_upload" type="file" name="file" class="form-control" onchange="avatar.uploadImage(this)">
                                </form>

                                <div class="panel box no-border mb-2">
                                    <div class="box-header p-y-sm">
                                        Фотография - олицетворение вашего профиля.<br>Вы можете загрузить изображение в формате JPG, BMP или PNG.
                                    </div>
                                </div>

                                <div class="form-group" id="croppr-container"></div>

                                <div class="button-wrapper text-center form-group">
                                    <a href="{{route('Profile', $user->id)}}" style="display: none;" class="tp_rofile btn btn-fw success theme-accent" >В профиль</a>
                                    <button id="pick-button" class="btn btn-fw primary" onclick="avatar.uploadAvatar('file_upload')">Выбрать файл</button>
                                    <button data-redirect-url="{{route('Profile', $user->id)}}" style="display: none;" id="save-button" class="btn btn-fw primary" onclick="avatar.cropImage(window.cropdata);">Сохранить</button>
                                    <button style="display: none;" id="another-button" class="btn btn-fw primary" onclick="avatar.uploadAvatar('file_upload')">Другое фото</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane @if(session('personal_edit_link') == 'password') active @endif" id="tab-4">
                        <div class="p-4 b-b _600">Смена пароля</div>
                        <form onsubmit="server.saveForm(this, event);" action="{{route('ChangePassword')}}" method="POST"  role="form" class="p-4 col-md-6">
                            <input  type="hidden" value="{{$user->id}}" name="user_id">
                            <div class="form-group">
                                <label>Старый пароль</label>
                                <input type="password" name="password" id="password" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Новый пароль</label>
                                <input type="password" name="new_password" id="new_password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Подтверждение</label>
                                <input type="password" name="new_password_confirmation" class="form-control">
                            </div>
                            <a href="{{route('Profile', $user->id)}}" style="display: none;" class="tp_rofile btn btn-fw success theme-accent" >В профиль</a>
                            <button type="submit" class="btn primary mt-2">Сменить пароль</button>
                        </form>
                        {{--<div class="p-4">--}}
                            {{--<p class="mt-4"><strong>Вы можете</strong></p>--}}
                            {{--<button type="submit" class="btn danger m-t" data-toggle="modal" data-target="#modal">Удалить учетную запись</button>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- .modal -->
        <div id="modal" class="modal fade animate black-overlay" data-backdrop="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content flip-y">
                    <div class="modal-body text-center">
                        <p class="py-3 mt-3"><i class="fa fa-remove text-warning fa-3x"></i></p>
                        @if($user->objects()->get()->count())
                        <p>Передать свои обьекты:</p>
                            @else
                            <p>Вы уверены?</p>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn white" data-dismiss="modal">Я передумал</button>
                        <button type="button" class="btn danger" data-dismiss="modal">Уверен</button>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
        <!-- / .modal -->
@endsection

@push('scripts')
@endpush
