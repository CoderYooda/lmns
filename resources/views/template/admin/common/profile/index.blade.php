@extends('template.admin.contents.2')

@section('pagename', 'Профиль')

@section('main')
    <div class="p-3 red">
        <div class=" _600 text-sm">
            <span class="text-info mr-2">ВНИМАНИЕ!</span> В связи с переходом на базу городов и улиц "ФИАС" требуется обновить информацию о местонахождении Ваших объектов. <br>( Указать город и улицу )
        </div>
    </div>
<div>
    <div class="item">
        <div class="item-bg">
            <img src="../assets/images/a0.jpg" alt="." class="blur opacity-3">
        </div>
        <div class="p-4">
            <div class="row mt-3">
                <div class="col-sm-7">
                    <div class="media">
                        <a href="#">
              <span class="avatar w-96" @if($user->isOnline())data-toggle="tooltip" title="" data-original-title="Сейчас онлайн"@endif>
                  @if($user->Avatar()->first() != null)
                      <img src="{{$user->Avatar()->first()->url}}" alt="{{$user->lastname}} {{$user->firstname}}">
                      @else
                      <img src="{{asset('img/noava.png')}}" alt="Нет аватара">
                  @endif
                      @if($user->isOnline())
                          <i class="on"></i>
                      @endif
              </span>
                        </a>
                        <div class="media-body mx-3 mb-2">
                            <h4 class="username">{{$user->lastname}} {{$user->firstname}}</h4>
                            <p class="text-muted">
                                <span class="m-r">
                                    {{--{{Auth::user()->roles()->first()->display_name}}--}}
                                    {{--<i class="fa fa-at mr-2"></i>--}}
                                </span>
                                <small>{{$user->toAgency()->first()->title}}</small></p>
                            @if($user->id == Auth::user()->id)
                            <div class="block clearfix mb-3">
                                <a href="{{route('EditProfile', $user->id)}}" class="btn btn-sm rounded btn-outline b-success">Редактировать</a>
                            </div>
                                @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">

                </div>
            </div>
        </div>
    </div>
    <div class="white bg b-b px-3">
        <div class="row">
            <div class="col-sm-6 order-sm-2">
                <div class="py-3 text-center text-sm-right">
                    <a href="#" class="d-inline-block px-3 text-center">
                        <span class="text-md d-block">{{$user->objects_count}}</span>
                        <small class="text-xs text-muted">Объектов</small>
                    </a>
                    <a href="#" class="d-inline-block b-l px-3 text-center">
                        <span class="text-md d-block">0</span>
                        <small class="text-xs text-muted">Клиентов</small>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 order-sm-1">
                <div class="py-4 clearfix nav-active-theme">
                    <ul class="nav nav-pills nav-sm">
                        <li class="nav-item">
                            <a class="nav-link @if(session('profile_tab_link') == 'objects' || session('profile_tab_link') == null) active @endif" href="#" data-toggle="tab" data-target="#tab_1">Объекты</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('profile_tab_link') == 'activity') active @endif" href="#" data-toggle="tab" data-target="#tab_2">Активность</a>
                        </li>
                        @if($user->id == Auth::user()->id)
                            <li class="nav-item">
                                <a class="nav-link @if(session('profile_tab_link') == 'clients') active @endif" href="#" data-toggle="tab" data-target="#tab_3">Клиенты</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-12">
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="tab_1">
                        <div class="padding" data-plugin="range_slider" style="padding-bottom: 0;padding-top: 1rem;">
                            <form style="position: relative" id="object-filter" onsubmit="agency.applyFilter(this, event)" action="{{route('Profile', $user->id)}}" method="GET">
                                <div class="filter-preload"></div>
                                <input name="price_max" type="hidden" value="{{$filter_params['price_max']}}">
                                <input name="rent_price_max" type="hidden" value="{{$filter_params['rent_price_max']}}">
                                <input name="square_max" type="hidden" value="{{$filter_params['square_max']}}">


                                <input name="price_from" type="hidden" value="{{request('price_from')}}">
                                <input name="price_to" type="hidden" value="{{request('price_to')}}">

                                <input name="square_from" type="hidden" value="{{request('square_from')}}">
                                <input name="square_to" type="hidden" value="{{request('square_to')}}">

                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group" style="height: 40px;line-height: 62px;">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="radio">
                                                        <label class="ui-check">
                                                            <input
                                                                onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=rent_price_max]').val());
                                                                    updateSlider();
                                                                    agency.setParam(this, event);"
                                                                   type="radio" name="type" value="rented" @if(request('type') == 'rented') checked @endif>
                                                            <i class="dark-white"></i>
                                                            Аренда
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="radio">
                                                        <label class="ui-check">
                                                            <input
                                                                onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=price_max]').val());
                                                                    updateSlider();
                                                                    agency.setParam(this, event);"
                                                                type="radio" name="type" value="selded" @if(request('type') == 'selded' || request('type') == null) checked @endif>
                                                            <i class="dark-white"></i>
                                                            Продажа
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="form-group">
                                            <input id="slider" type="text" class="form-control mb-3">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group" style="height: 45px;line-height: 62px;">
                                            <select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" onchange="updateSlider();agency.setParam(this, event);" name="category"  class="form-control">
                                                <option value="">Тип объекта</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if(request('category') == $category->id)selected @endif>{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="form-group">
                                            <input id="square-slider" type="text" class="form-control mb-3">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="padding">
                            <div id="filtered_objects" class="row row-sm ajax_preloder" style="position: relative;">
                                @include('template.admin.common.agency.objects.collection',['type'=>'/e'])
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_2">
                        <div class="padding">
                            @if($user->activities()->count())
                                <div class="streamline">
                                    @foreach($user->activities()->limit(20)->get() as $activity)
                                        <div class="sl-item
                                    @if(
                                    $activity->slug == 'edit_profile_password' ||
                                    $activity->slug == 'create_object'
                                    )
                                            b-success @endif
                                        @if(
                                        $activity->slug == 'update_object'
                                        )
                                            b-primary @endif
                                        @if(
                                        $activity->slug == 'delete_object'
                                        )
                                            b-danger @endif

                                            ">
                                            <div class="sl-content">
                                                <div class="sl-date text-muted">{{$activity->created_at->format('d.m H:i')}}</div>
                                                <p>{!! $activity->title !!}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <div class="box p-3">{{$user->firstname}} пока не проявлял(а) активности</div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_3">
                        {!! \App\Http\Controllers\Admin\ClientController::getClientListByUser($user) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
@endpush
