@if($objects->count() < 1)
    <div class="box w-100">
        <div class="p-3">
            Результатов нет, <a href="javascript:void(0)" onclick="javascript:location.reload();" class="text-md mt-3 d-block">Cбросить фильтр</a>
        </div>
    </div>
@endif
@foreach($objects as $object)
    @include('template.admin.common.agency.objects.single')
@endforeach
<div class="form-group col-12">
    <div class="ajax">{{$objects->appends(request()->input())->links()}}</div>
</div>
