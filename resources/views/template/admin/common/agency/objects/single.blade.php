<div class="obj-container col-sm-4 col-md-4">
    <div class="box">

        <a href="{{ route($object->category->type, $object->id) }}" style="display: block" class="item r-t">
            <div class="item-media item-media-21by9">
                @if($object->images->first() != null)
                    <span class="item-media-content" style="background-image: url({{$object->images->first()->thumb_url}})"></span>
                @else
                    <span class="item-media-content" style="background-image: url(/img/noimage.jpg)"></span>
                @endif
            </div>
            <div class="bottom gd-overlay px-3 py-2">
                @if($object->rented)
                    <span class="text-md block text-white">{{number_format($object->rent_price)}} в мес.</span>
                @else
                    <span class="text-md block text-white">{{number_format($object->price)}} р.</span>
                @endif
                    @if($object->exlusive)
                    <span class="badge badge-pill danger">Эксклюзив</span>
                    @endif
            </div>
            <div class="top item-overlay text-right px-2">
                @if($object->isAdminOfObject(Auth::user()) || $object->isAutor(Auth::user()))
                    <form onsubmit="object.remove(this, event)" action="{{ route('removeObject', $object->id)}}" method="POST">
                        {{ csrf_field()}}
                        <button type="submit" class="text-md p-1" data-toggle="tooltip" title="Удалить">
                            <i class="fa fa-close d-inline"></i>
                            <i class="fa fa-close text-danger d-none"></i>
                        </button>
                    </form>
                @endif
            </div>
        </a>
        <div class="p-3">
            <div class="text-muted mb-2">
                @if($object->rented)
                    <span class="float-right"><span class="badge badge-sm info">Аренда</span></span>
                @else
                    <span class="float-right"><span class="badge badge-sm warning">Продажа</span></span>
                @endif
                <span class="mr-2">Опубликовано {{$object->created_at->format('d.m H:i')}}</span>
            </div>
            <div class="mb-2"><a href="{{ route($object->category->type, $object->id) }}" class="_800">{!! $object->title !!}</a></div>
            <span class="text-ellipsis">Агент: {{$object->autor->lastname}} {{$object->autor->firstname}}</span>
            <span class="text-ellipsis">Адрес: {{$object->city->socr}} {{$object->city->name}}, {{$object->street}}, д.{{$object->{$object->category->type}->house_number}}</span>
            @if($object->mls != NULL)
                <span class="text-ellipsis">MLS: {{number_format($object->mls)}} р.</span>
            @else
                <span class="text-ellipsis">MLS: не указано</span>
            @endif
            <div class="mt-2">
                <a target="_blank" href="{{ route($object->category->type, $object->id) }}" class="btn btn-xs white">Смотреть</a>
                @if($object->isAdminOfObject(Auth::user()) || $object->isAutor(Auth::user()))
                    <a target="_blank" href="{{route('editObject' , $object->id )}}" class="btn btn-xs white ml-2">Редактировать</a>
                @endif
            </div>
        </div>
    </div>
</div>
