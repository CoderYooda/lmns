@extends('template.admin.contents.2')

@section('pagename', 'Настройки "'. $agency->title .'"')

@section('main')
<div>
    <div class="d-sm-flex">
        <div class="w w-auto-xs light bg bg-auto-sm b-r">
            <div class="py-3">
                <div class="nav-active-border left b-primary">
                    <ul class="nav flex-column nav-sm">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab-1">Общие</a>
                        </li>
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link " href="#" data-toggle="tab" data-target="#tab-2">Изображение</a>--}}
                        {{--</li>--}}
                    </ul>
                </div>
            </div>
        </div>
        <div class="col p-0">
            <div class="tab-content pos-rlt">
                <div class="tab-pane active" id="tab-1">
                    <div class="p-4 b-b _600">Настройки агентства</div>
                    <form onsubmit="agency.saveSettings(this, event)" action="{{route('AgencySettingsSave')}}" method="POST" role="form" class="p-4 col-md-6">
                        <input type="hidden" name="agency_id" value="{{$agency->id}}">
                        <div class="form-group">
                            <label>Название агентства</label>
                            <input type="text" name="title" value="{{$agency->title}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Телефон агентства</label>
                            <input type="text" name="phone" value="{{$agency->phone}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>E-mail агентства</label>
                            <input type="text" name="email" value="{{$agency->email}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Адрес агентства</label>
                            <input type="text" name="address" value="{{$agency->address}}" class="form-control">
                        </div>
                        <button type="submit" class="btn primary mt-2">Сохранить</button>
                    </form>
                </div>
                <div class="tab-pane" id="tab-2">
                    <div class="p-4 b-b _600">Настройки изображения</div>
                    <form role="form" class="p-4 col-md-6">
                        <div class="form-group">
                            <label>Client ID</label>
                            <input type="text" disabled class="form-control" value="d6386c0651d6380745846efe300b9869">
                        </div>
                        <div class="form-group">
                            <label>Secret Key</label>
                            <input type="text" disabled class="form-control" value="3f9573e88f65787d86d8a685aeb4bd13">
                        </div>
                        <div class="form-group">
                            <label>App Name</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>App URL</label>
                            <input type="text" class="form-control">
                        </div>
                        <button type="submit" class="btn primary m-t">Сохранить</button>
                    </form>
                </div>
                <div class="tab-pane" id="tab-3">
                    <div class="p-4 b-b _600">Emails</div>
                    <form role="form" class="p-4 col-md-6">
                        <p>E-mail me whenever</p>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone posts a comment
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone follow me
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone send me a message
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone invite me to group
                            </label>
                        </div>
                        <button type="submit" class="btn primary mt-2">Update</button>
                    </form>
                </div>
                <div class="tab-pane" id="tab-4">
                    <div class="p-4 b-b _600">Notifications</div>
                    <form role="form" class="p-4 col-md-6">
                        <p>Notice me whenever</p>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone seeing my profile page
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone follow me
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone send me a message
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="ui-check">
                                <input type="checkbox"><i class="dark-white"></i> Anyone invite me to group
                            </label>
                        </div>
                        <button type="submit" class="btn primary mt-2">Update</button>
                    </form>
                </div>
                <div class="tab-pane" id="tab-5">
                    <div class="p-4 b-b _600">Security</div>
                    <div class="p-4">
                        <div class="clearfix">
                            <form role="form" class="col-md-6 p-0">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>New Password Again</label>
                                    <input type="password" class="form-control">
                                </div>
                                <button type="submit" class="btn primary mt-2">Update</button>
                            </form>
                        </div>

                        <p class="mt-4"><strong>Delete account?</strong></p>
                        <button type="submit" class="btn danger m-t" data-toggle="modal" data-target="#modal">Delete Account</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .modal -->
    <div id="modal" class="modal fade animate black-overlay" data-backdrop="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content flip-y">
                <div class="modal-body text-center">
                    <p class="py-3 mt-3"><i class="fa fa-remove text-warning fa-3x"></i></p>
                    <p>Are you sure to delete your account?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn white" data-dismiss="modal">No</button>
                    <button type="button" class="btn danger" data-dismiss="modal">Yes</button>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <!-- / .modal -->
</div>
@endsection
