@extends('template.admin.contents.2')

@section('pagename', 'Автовыгрузка')

@section('main')

        <div class="d-sm-flex" data-plugin="exportObjects">
            <div class="w w-auto-xs light bg bg-auto-sm b-r">
                <div class="py-3">
                    <div class="nav-active-border left b-primary">
                        <ul class="nav flex-column nav-sm">
                            <li class="nav-item">
                                <a  class="nav-link active" href="#" data-toggle="tab" data-target="#tab-1">Афи</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#" data-toggle="tab" data-target="#tab-2">Циан</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#" data-toggle="tab" data-target="#tab-3">Яндекс</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#" data-toggle="tab" data-target="#tab-4">Моя реклама</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#" data-toggle="tab" data-target="#tab-5">Из рук в руки</a>
                            </li>
                            <li class="nav-item">
                                <a  class="nav-link" href="#" data-toggle="tab" data-target="#tab-6">Юла</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col p-0">
                <div class="tab-content pos-rlt">
                    <div class="tab-pane active" id="tab-1">
                        <div class="p-4 b-b _600">Генерация XML файла выгрузки объектов</div>
                        <div class="row p-3">
                            <div class="col-sm-12 col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="item">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ссылка на файл</label>
                                                <input class="form-control" id="afilink" placeholder="Нажмите кнопку 'Генерация' для создания файла">
                                            </div>
                                        </div>
                                        <p>
                                            <a onclick="window.generate({{Auth::user()->ownedAgency()->first()->id}}, 'afi', '{{route('generateExportFile')}}')" href="#" class="btn btn-sm btn-rounded success">Генерация</a>
                                            <a onclick="window.copyBuffer('afilink')" href="#" class="btn btn-sm btn-rounded warn">Скопировать в буфер</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="p-4 b-b _600">Генерация XML файла выгрузки объектов на ЦИАН</div>
                        <div class="row p-3">
                            <div class="col-sm-12 col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="item">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ссылка на файл</label>
                                                <input class="form-control" id="cianlink" placeholder="Нажмите кнопку 'Генерация' для создания файла">
                                            </div>
                                        </div>
                                        <p>
                                            <a onclick="window.generate({{Auth::user()->ownedAgency()->first()->id}}, 'cian', '{{route('generateExportFile')}}')" href="#" class="btn btn-sm btn-rounded success">Генерация</a>
                                            <a onclick="window.copyBuffer('cianlink')" href="#" class="btn btn-sm btn-rounded warn">Скопировать в буфер</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-3">
                        <div class="p-4 b-b _600">Генерация XML файла выгрузки объектов на Яндекс</div>
                        <div class="row p-3">
                            <div class="col-sm-12 col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="item">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Ссылка на файл</label>
                                                <input class="form-control" id="yandexlink" placeholder="Нажмите кнопку 'Генерация' для создания файла">
                                            </div>
                                        </div>
                                        <p>
                                            <a onclick="window.generate({{Auth::user()->ownedAgency()->first()->id}}, 'yandex', '{{route('generateExportFile')}}')" href="#" class="btn btn-sm btn-rounded success">Генерация</a>
                                            <a onclick="window.copyBuffer('yandexlink')" href="#" class="btn btn-sm btn-rounded warn">Скопировать в буфер</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-4">
                        <div class="p-4 b-b _600">В разработке</div>
                    </div>
                    <div class="tab-pane" id="tab-5">
                        <div class="p-4 b-b _600">В разработке</div>
                    </div>
                    <div class="tab-pane" id="tab-6">
                        <div class="p-4 b-b _600">В разработке</div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
@endpush
