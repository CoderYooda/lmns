@extends('template.admin.contents.2')

@section('pagename', 'Экспорт объектов')

@section('main')

    <div class="d-flex flex" data-plugin="export" style="height: calc(100vh - 107px);">
        <div class="fade aside aside-sm b-r" id="content-aside">
            <div class="modal-dialog d-flex flex-column w-md light lt" id="user-nav">
                <div class="navbar white no-radius box-shadow pos-rlt">
                    <a href="{{route('exportGenerate')}}" class="btn btn-fw success w-100">Автовыгрузка</a>
                </div>
                <div class="scrollable hover">
                    <div class="sidenav mt-2">
                        <nav class="nav-border b-primary" data-nav>
                            <ul class="nav">
                                <li class="nav-header hidden-folded mt-2">
                                    <span class="d-block pt-1 text-sm text-muted _600">Типы недвижимости</span>
                                </li>
                                <li class="active">
                                    <a href="#all">
                                        <span class="nav-text">Все</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#flat">
                                        <span class="nav-text">Квартира</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#room">
                                        <span class="nav-text">Комната</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#house">
                                        <span class="nav-text">Дом, Дача, Коттедж</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#land">
                                        <span class="nav-text">Земельный участок</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#garage">
                                        <span class="nav-text">Гараж/Машиноместо</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#commercial">
                                        <span class="nav-text">Коммерческая недвижимость</span>
                                    </a>
                                </li>
                                <li class="nav-header hidden-folded mt-2">
                                    <span class="d-block pt-1 text-sm text-muted _600">Агенты</span>
                                </li>
                                @foreach($agents as $agent)
                                    <li>
                                        <a href="#agent{{$agent->id}}">
                                            <span class="nav-text">{{$agent->lastname}} {{$agent->firstname}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- / -->
            </div>
        </div>
        <input type="hidden" id="exportUrl" value="{{route('switchExport')}}">
        <div class="d-flex flex" id="content-body" >
            <div class="d-flex flex-column flex" id="user-list">
                <div class="navbar white no-radius box-shadow pos-rlt">
                    <form class="flex">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm search" placeholder="Поиск" required>
                            <span class="input-group-append">
                                                <button class="btn btn-default btn-sm no-shadow" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                        </div>
                    </form>
                    <button class="btn btn-sm white ml-1 sort" data-sort="item-title" data-toggle="tooltip" title="сортировка">
                        <i class="fa fa-sort"></i>
                    </button>
                    <a data-toggle="modal" data-target="#content-aside" data-modal class="ml-1 d-md-none">
                                        <span class="btn btn-sm btn-icon primary">
                                            <i class="fa fa-th"></i>
                                        </span>
                    </a>
                </div>
                <div class="d-flex flex scroll-y">
                    <div class="d-flex flex-column flex white lt">
                        <div class="scroll-y">
                            <div class="list hide">
                                @foreach($objects as $object)
                                <div class="list-item" data-id="item-1">
                                    <span class="w-40 avatar circle grey">
                                        @if($object->images->first() != NULL)
                                            <img src="{{asset($object->images()->first()->url)}}" style="height: 100%;border-radius: 0;object-fit: cover;">
                                        @else
                                            <img title="Нет изображения" src="{{ asset('/img/noimage.jpg') }}">
                                        @endif
                                    </span>
                                    <div class="list-body">
                                        <a target="_blank" href="{{ route($object->category->type, $object->id) }}" class="item-title _500">{!!  $object->title !!} (@if($object->rented)Аренда@elseПродажа@endif)</a>
                                        <div class="item-except text-sm text-muted h-1x">
                                            #{{$object->id}} | {{$object->autor()->first()->lastname}} {{$object->autor()->first()->firstname}} | {{number_format($object->price, 0, '', ' ')}}р.
                                        </div>
                                        <div class="item-tag tag hide">
                                            Объект № {{$object->id}}, {{$object->autor()->first()->lastname}} {{$object->autor()->first()->firstname}}, {{$object->category()->first()->title}}, Все
                                        </div>
                                    </div>
                                    <label class="md-switch"  style="margin-top: 10px!important;">
                                        <input class="expo_switch" type="checkbox" data-id="{{$object->id}}" name="export[{{$object->id}}]" @if($object->export) checked @endif>
                                        <i class="blue"></i>
                                    </label>
                                </div>
                                @endforeach
                            </div>
                            <div class="no-result hide">
                                <div class="p-4 text-center">
                                    Нет объектов
                                </div>
                            </div>
                        </div>
                        <div class="p-3 b-t mt-auto">
                            <div class="d-flex align-items-center">
                                <div class="flex">
                                    <div class="pagination pagination-xs">
                                    </div>
                                </div>
                                <div>
                                    <span class="text-muted">Всего:</span>
                                    <span id="count"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{--<script src="{{asset('assets/js/app/export.js')}}"></script>--}}
@endpush
