@extends('template.admin.contents.2')

@section('pagename', 'Агентство')

@section('main')
<div class="dark">
    <div class="p-4 p-md-5">
        <div class="row">
            <div class="col-md-8">
                <h1 class="display-6 l-s-n-1x"><span class="text-muted _300"></span><span class="text-theme _500">{{$agency->title}}</span></h1>
                <div>
                    <span>{{$agency->email}}, <i class="fa fa-map-marker mx-2"></i>{{$agency->address}}</span>
                </div>
                <div class="mt-5">
                    <a href="{{route('AgencySettings')}}" class="btn btn-outline b-a">Редактировать агентство <i class="fa fa-long-arrow-right ml-2"></i></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="d-flex my-3 text-center">
                    <div class="lt p-3 px-4 r-l b-a">
                        <span class="text-muted">Объектов</span>
                        <div class="text-white text-lg">{{$agency->objects_count}}</div>
                    </div>
                    <div class="lt p-3 px-4 r-r b-a no-b-l">
                        <span class="text-muted">Агентов</span>
                        <div class="text-theme text-lg">{{$agency->agents_count}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-active-border b-theme px-4 px-md-5">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab_1">
                    Объекты агентства
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tab" data-target="#tab_2">
                    Контакты
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('exportPage')}}" >
                    Экспорт
                </a>
            </li>
        </ul>
    </div>
</div>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_1">
                    <div class="padding" style="padding-bottom: 0;padding-top: 1rem;">
                        <form style="position: relative" id="object-filter" onsubmit="agency.applyFilter(this, event)" action="{{route('Agency')}}" method="GET">
                            <input name="price_max" type="hidden" value="{{$filter_params['price_max']}}">
                            <input name="rent_price_max" type="hidden" value="{{$filter_params['rent_price_max']}}">
                            <input name="square_max" type="hidden" value="{{$filter_params['square_max']}}">


                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <div class="form-group" >
                                        <label for="square_from">Тип сделки</label>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="radio">
                                                    <label class="ui-check">
                                                        <input
                                                            class="price_mask"
                                                            onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=rent_price_max]').val());
                                                                    agency.setParam(this, event);"
                                                            type="radio" name="type" value="rented" @if(request('type') == 'rented') checked @endif>
                                                        <i class="dark-white"></i>
                                                        Аренда
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="radio">
                                                    <label class="ui-check">
                                                        <input
                                                            class="price_mask"
                                                            onchange="
                                                                    $('input[name=price_from]').val(0);
                                                                    $('input[name=price_to]').val($('input[name=price_max]').val());
                                                                    agency.setParam(this, event);"
                                                            type="radio" name="type" value="selded" @if(request('type') == 'selded' || request('type') == null) checked @endif>
                                                        <i class="dark-white"></i>
                                                        Продажа
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-8">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Цена от</label>
                                            <input onchange="agency.setParam(this, event);" type="text" class="form-control price_mask" id="price_from" name="price_from" placeholder="Цена от" value="{{request('price_from')}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">Цена до</label>
                                            <input onchange="agency.setParam(this, event);" type="text" class="form-control price_mask" id="price_to" name="price_to" placeholder="Цена до" value="{{request('price_to')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="select_cat">Тип объекта</label>
                                        <select data-plugin="select2" data-option="{minimumResultsForSearch: 8}" id="select_cat" onchange="agency.checkSquare(this);agency.setParam(this, event);" name="category"  class="form-control">
                                            <option value="">Все объекты</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if(request('category') == $category->id)selected @endif>{{$category->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-row select_cat_first">
                                        <div class="form-group col-md-6">
                                            <label for="square_from">Площадь от</label>
                                            <input disabled onchange="agency.setParam(this, event);" type="number" class="form-control" id="square_from" name="square_from" placeholder="Площадь от" value="{{request('square_from')}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="square_to">Площадь до</label>
                                            <input disabled onchange="agency.setParam(this, event);" type="number" class="form-control" id="square_to" name="square_to" placeholder="Площадь до" value="{{request('square_to')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-2">
                                    <label for="square_from"></label>
                                    <button type="submit" class="btn primary filter-bob">Фильтровать</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="padding">
                        <div id="filtered_objects" class="row row-sm ajax_preloder" style="position: relative;">
                            @include('template.admin.common.agency.objects.collection')
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_2">
                    <div class="d-flex flex" data-plugin="user">
                        <div class="fade aside aside-sm b-r" id="content-aside">
                            <div class="modal-dialog d-flex flex-column w-md light lt" id="user-nav">
                                <div class="navbar white no-radius box-shadow pos-rlt">
                                    <span class="text-md">Контакты</span>
                                </div>
                                <div class="scrollable hover">
                                    <div class="sidenav mt-2">
                                        <nav class="nav-border b-primary" data-nav>
                                            <ul class="nav">
                                                <li class="active">
                                                    <a href="#all">
                                                        <span class="nav-badge">
                                                            <b class="badge badge-sm badge-pill warn">{{$agency->users()->get()->count()}}</b>
                                                        </span>
                                                        <span class="nav-text">Все</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#administrator">
                                                        <span class="nav-badge">
                                                            <b class="badge badge-sm badge-pill primary">{{$agency->admins()->get()->count()}}</b>
                                                        </span>
                                                        <span class="nav-text">Администраторы</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#agent">
                                                        <span class="nav-badge">
                                                            <b class="badge badge-sm badge-pill primary">{{$agency->agents()->get()->count()}}</b>
                                                        </span>
                                                        <span class="nav-text">Агенты</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#client">
                                                        <span class="nav-text">Клиенты</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <!-- / -->
                                <!-- footer -->
                                <div class="p-2 mt-auto p-3">
                                    <div class="input-group">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#m" class="btn btn-sm success theme-accent btn-block">
                                            <i class="fa fa-fw fa-plus"></i>
                                            <span class="hidden-folded d-inline">Создать агента</span>
                                        </a>
                                    </div>
                                </div>
                                <!-- / -->
                            </div>
                        </div>
                        <div id="m" class="modal black-overlay" data-backdrop="true">
                            <div class="modal-dialog animate">
                                <form onsubmit="agency.createAgent(this, event)" action="{{route('createUser')}}" method="POST">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Добавление агента</h5>
                                        </div>
                                        <div class="modal-body text-center p-lg">

                                                @csrf
                                                <div class="form-group row">
                                                    <label for="firstname" class="col-sm-2 col-form-label d-none d-sm-block">Имя</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" id="firstname" name="firstname" type="text" value="{{old('firstname')}}" placeholder="Имя" required>
                                                    </div>
                                                </div>
                                            <div class="form-group row">
                                                <label for="lastname" class="col-sm-2 col-form-label d-none d-sm-block">Фамилия</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" id="lastname" name="lastname" type="text" value="{{old('lastname')}}" placeholder="Фамилия" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="midname" class="col-sm-2 col-form-label d-none d-sm-block">Отчество</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" id="midname" name="midname" type="text" value="{{old('midname')}}" placeholder="Отчество" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="phone" class="col-sm-2 col-form-label d-none d-sm-block">Телефон</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" type="text" value="{{old('phone')}}" placeholder="Телефон" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-2 col-form-label d-none d-sm-block">Email</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" type="email" value="{{old('email')}}" placeholder="E-mail" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password" class="col-sm-2 col-form-label d-none d-sm-block">Пароль</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" type="password" value="{{old('password')}}" placeholder="Пароль" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-form w-100">Создать</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </form>
                            </div>
                        </div>
                        <div class="d-flex flex" id="content-body" >
                            <div class="d-flex flex-column flex" id="user-list">
                                <div class="navbar white no-radius box-shadow pos-rlt">
                                    <form class="flex">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm search" placeholder="Поиск" required>
                                            <span class="input-group-append">
                                                <button class="btn btn-default btn-sm no-shadow" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </form>
                                    <button class="btn btn-sm white ml-1 sort" data-sort="item-title" data-toggle="tooltip" title="Sort">
                                        <i class="fa fa-sort"></i>
                                    </button>
                                    <a data-toggle="modal" data-target="#content-aside" data-modal class="ml-1 d-md-none">
                                        <span class="btn btn-sm btn-icon primary">
                                            <i class="fa fa-th"></i>
                                        </span>
                                    </a>
                                    </a>
                                </div>
                                <div class="d-flex flex scroll-y">
                                    <div class="d-flex flex-column flex white lt">
                                        <div class="scroll-y">
                                            <div class="list hide">
                                                @foreach($agency->users()->withPivot('status')->get() as $agent)
                                                <div class="list-item" data-id="item-{{$agent->id}}">
                                                    <span class="w-40 avatar circle grey">
                                                        @if($agent->isOnline())
                                                            <i class="on b-white avatar-right"></i>
                                                        @endif
                                                        @if($agent->Avatar()->first() != null)
                                                            <img src="{{$agent->Avatar()->first()->thumb_url}}" alt="{{$agent->lastname}} {{$agent->firstname}}">
                                                        @else
                                                            <img src="{{asset('img/noava.png')}}" alt="Нет аватара">
                                                        @endif
                                                    </span>

                                                    <div class="list-body">
                                                        <a href="{{route('Profile', $agent->id)}}" class="item-title _500">{{$agent->lastname}} {{$agent->firstname}}</a>


                                                        <div class="item-except text-sm text-muted h-1x">
                                                            {{$agent->email}}
                                                        </div>
                                                        <div class="item-tag tag hide">
                                                            @if($agent->pivot->status == 'manager')
                                                                Администраторы
                                                            @elseif($agent->pivot->status == 'agent')
                                                                Агенты
                                                            @elseif($agent->pivot->status == 'client')
                                                                Клиенты
                                                            @endif, Все
                                                        </div>
                                                    </div>
                                                    @if($agent->id != Auth::user()->id)
                                                        <div>
                                                            <div class="item-action dropdown">
                                                                <a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                                                <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                                                    <a class="dropdown-item" onclick="agency.deleteAgent($(this).siblings('form'), event);">
                                                                        <i class="fa fa-trash"></i>
                                                                        Удалить агента
                                                                    </a>
                                                                    <form action="{{ route('removeUser')}}" method="POST" style="display: none;">
                                                                        @csrf
                                                                        <input type="hidden" name="agent_id" value="{{$agent->id}}">
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                     @endif
                                                </div>
                                                    @endforeach

                                            </div>
                                            <div class="no-result hide">
                                                <div class="p-4 text-center">
                                                    Нет результатов
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-3 b-t mt-auto">
                                            <div class="d-flex align-items-center">
                                                <div class="flex">
                                                    <div class="pagination pagination-xs">
                                                    </div>
                                                </div>
                                                <div>
                                                    <span class="text-muted">Всего:</span>
                                                    <span id="count"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column b-l" style="width: 50px">
                                        <div class="scrollable hover">
                                            <div class="text-center text-sm py-3 d-flex flex-column" id="filter">
                                                <a href="javascript:void(0);">А</a>
                                                <a href="javascript:void(0);">Б</a>
                                                <a href="javascript:void(0);">В</a>
                                                <a href="javascript:void(0);">Г</a>
                                                <a href="javascript:void(0);">Д</a>
                                                <a href="javascript:void(0);">Е</a>
                                                <a href="javascript:void(0);">Ж</a>
                                                <a href="javascript:void(0);">З</a>
                                                <a href="javascript:void(0);">И</a>
                                                <a href="javascript:void(0);">К</a>
                                                <a href="javascript:void(0);">Л</a>
                                                <a href="javascript:void(0);">М</a>
                                                <a href="javascript:void(0);">Н</a>
                                                <a href="javascript:void(0);">О</a>
                                                <a href="javascript:void(0);">П</a>
                                                <a href="javascript:void(0);">Р</a>
                                                <a href="javascript:void(0);">С</a>
                                                <a href="javascript:void(0);">Т</a>
                                                <a href="javascript:void(0);">У</a>
                                                <a href="javascript:void(0);">Ф</a>
                                                <a href="javascript:void(0);">Х</a>
                                                <a href="javascript:void(0);">Ц</a>
                                                <a href="javascript:void(0);">Ч</a>
                                                <a href="javascript:void(0);">Ш</a>
                                                <a href="javascript:void(0);">Э</a>
                                                <a href="javascript:void(0);">Ю</a>
                                                <a href="javascript:void(0);">Я</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{--<div class="col-sm-4 col-lg-3">--}}
            {{--<div>--}}
                {{--<div class="box">--}}
                    {{--<div class="box-header">--}}
                        {{--<h3>Who to follow</h3>--}}
                    {{--</div>--}}
                    {{--<div class="box-divider"></div>--}}
                    {{--<ul class="list no-border">--}}
                        {{--<li class="list-item">--}}
                            {{--<a href="#">--}}
                    {{--<span class="w-40 avatar">--}}
                      {{--<img src="../assets/images/a4.jpg" alt="...">--}}
                      {{--<i class="on bottom"></i>--}}
                    {{--</span>--}}
                            {{--</a>--}}
                            {{--<div class="list-body">--}}
                                {{--<div><a href="#">Chris Fox</a></div>--}}
                                {{--<small class="text-muted text-ellipsis">Designer, Blogger</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<a href="#">--}}
                    {{--<span class="w-40 avatar">--}}
                      {{--<img src="../assets/images/a5.jpg" alt="...">--}}
                      {{--<i class="on bottom"></i>--}}
                    {{--</span>--}}
                            {{--</a>--}}
                            {{--<div class="list-body">--}}
                                {{--<div><a href="#">Mogen Polish</a></div>--}}
                                {{--<small class="text-muted text-ellipsis">Writter, Mag Editor</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<a href="#">--}}
                    {{--<span class="w-40 avatar">--}}
                      {{--<img src="../assets/images/a6.jpg" alt="...">--}}
                      {{--<i class="busy bottom"></i>--}}
                    {{--</span>--}}
                            {{--</a>--}}
                            {{--<div class="list-body">--}}
                                {{--<div><a href="#">Joge Lucky</a></div>--}}
                                {{--<small class="text-muted text-ellipsis">Art director, Movie Cut</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<a href="#">--}}
                    {{--<span class="w-40 avatar">--}}
                      {{--<img src="../assets/images/a7.jpg" alt="...">--}}
                      {{--<i class="away bottom"></i>--}}
                    {{--</span>--}}
                            {{--</a>--}}
                            {{--<div class="list-body">--}}
                                {{--<div><a href="#">Folisise Chosielie</a></div>--}}
                                {{--<small class="text-muted text-ellipsis">Musician, Player</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<a href="#">--}}
                    {{--<span class="w-40 circle green avatar">--}}
                      {{--P--}}
                      {{--<i class="away bottom"></i>--}}
                    {{--</span>--}}
                            {{--</a>--}}
                            {{--<div class="list-body">--}}
                                {{--<div><a href="#">Peter</a></div>--}}
                                {{--<small class="text-muted text-ellipsis">Musician, Player</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="box">--}}
                    {{--<div class="box-header">--}}
                        {{--<h2>Latest Tweets</h2>--}}
                    {{--</div>--}}
                    {{--<div class="box-divider"></div>--}}
                    {{--<ul class="list">--}}
                        {{--<li class="list-item">--}}
                            {{--<div class="list-body">--}}
                                {{--<p>Wellcome <a href="#" class="text-primary">@Drew Wllon</a> and play this web application template, have fun1 </p>--}}
                                {{--<small class="d-block text-muted"><i class="fa fa-fw fa-clock-o"></i> 2 minuts ago</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<div class="list-body">--}}
                                {{--<p>Morbi nec <a href="#" class="text-primary">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>--}}
                                {{--<small class="d-block text-muted"><i class="fa fa-fw fa-clock-o"></i> 1 hour ago</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="list-item">--}}
                            {{--<div class="list-body">--}}
                                {{--<p><a href="#" class="text-primary">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis</p>--}}
                                {{--<small class="d-block text-muted"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</small>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

@endsection

@push('scripts')
    <script src="{{asset('assets/js/app/user.js')}}"></script>
@endpush
