@extends('template.admin.contents.2')

@section('pagename', 'Панель управления')

@section('main')
    <div class="padding">
        <div class="row">
            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="d-flex">
                        <span class="text-muted">Total revenue</span>
                    </div>
                    <div class="py-3 text-center text-lg text-success">
                        $35,340
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">Income</span>
                        <span><i class="fa fa-caret-up text-success"></i> 12%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="d-flex">
                        <span class="text-muted">Order status</span>
                    </div>
                    <div class="py-3 text-center text-lg text-danger">
                        2,560
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">New order</span>
                        <span><i class="fa fa-caret-down text-danger"></i> 5%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="d-flex">
                        <span class="text-muted">Income status</span>
                    </div>
                    <div class="py-3 text-center text-lg text-primary">
                        $6,190
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">New income</span>
                        <span><i class="fa fa-caret-up text-primary"></i> 1%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="d-flex">
                        <span class="text-muted">Customer status</span>
                    </div>
                    <div class="py-3 text-center text-lg">
                        3,320
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">New users</span>
                        <span><i class="fa fa-caret-down text-danger"></i> 11%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="py-3">
                        <div class="easypiechart" data-plugin="easyPieChart" data-option="{barColor: app.color.primary}" data-percent="35" data-size="90" data-scale-length="0">
                            <div>
                                <span class="text-primary">35%</span>
                                <div class="text-xs text-muted">rate</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">Bounce</span>
                        <span><i class="fa fa-caret-up text-success"></i> 5%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="py-3">
                        <div class="easypiechart" data-plugin="easyPieChart" data-option="{barColor: app.color.accent}" data-percent="35" data-size="90" data-scale-length="10">
                            <div>
                                <span class="text-accent">65%</span>
                                <div class="text-xs text-muted">growth</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">World market</span>
                        <span><i class="fa fa-caret-up text-success"></i> 10%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="py-3">
                        <div class="easypiechart" data-plugin="easyPieChart" data-option="{barColor: app.color.warn}" data-percent="75" data-line-width="5" data-size="90" data-rotate="45" data-scale-length="0">
                            <div>
                                <span class="text-warn">55%</span>
                                <div class="text-xs text-muted">turn</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">Advertise</span>
                        <span><i class="fa fa-caret-up text-success"></i> 5%</span>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-lg-3">
                <div class="box p-3">
                    <div class="py-3">
                        <div class="easypiechart" data-plugin="easyPieChart" data-option="{barColor: app.color.accent}" data-percent="20" data-line-width="10" data-size="90" data-scale-length="0" data-line-cap="square">
                            <div>
                                <span class="text-accent">20%</span>
                                <div class="text-xs text-muted">avarage</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <span class="flex text-muted">Clicks</span>
                        <span><i class="fa fa-caret-up text-success"></i> 10%</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
          <span class="avatar w-40 text-center rounded primary">
            <span class="fa fa-dollar"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">75 <span class="text-sm">Sales</span></a></h4>
                        <small class="text-muted">6 waiting payment.</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
          <span class="avatar w-40 text-center rounded info theme">
            <span class="fa fa-female"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">40 <span class="text-sm">Orders</span></a></h4>
                        <small class="text-muted">38 Shipped.</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
          <span class="avatar w-40 text-center rounded success">
            <span class="fa fa-bookmark"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">6k+ <span class="text-sm">Members</span></a></h4>
                        <small class="text-muted">632 VIPs.</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
          <span class="avatar w-40 text-center rounded warning">
            <span class="fa fa-comment"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">69 <span class="text-sm">Comments</span></a></h4>
                        <small class="text-muted">5 approved.</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item primary theme">
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">2k+ <span class="text-sm">Posts</span></a></h4>
                        <small class="text-muted">32 waiting approval</small>
                    </div>
                    <span class="avatar w-40 text-center circle dk">
            <span class="fa fa-pencil"></span>
          </span>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item success">
          <span class="avatar w-40 text-center lt">
            <span class="fa fa-pie-chart"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">2k+ <span class="text-sm">Posts</span></a></h4>
                        <small class="text-muted">32 waiting approval</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">123 <span class="text-sm">Taska</span></a></h4>
                        <small class="text-muted">32 unfinished</small>
                    </div>
                    <span class="avatar w-40 text-center circle primary theme">
            <span class="fa fa-tasks"></span>
          </span>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="box list-item">
          <span class="avatar w-40 text-center circle warn">
            <span class="fa fa-file"></span>
          </span>
                    <div class="list-body">
                        <h4 class="m-0 text-md"><a href="#">349 <span class="text-sm">Files</span></a></h4>
                        <small class="text-muted">23 uploaded</small>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
