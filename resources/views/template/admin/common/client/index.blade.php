<!-- ############ Main START-->
<div class="d-flex flex" data-plugin="user">
    <div class="fade aside aside-sm b-r" id="content-aside">
        <div class="modal-dialog d-flex flex-column w-md light lt" id="user-nav">
            <div class="navbar white no-radius box-shadow pos-rlt">
                <span class="text-md"><button>123</button></span>
            </div>
            <div class="scrollable hover">
                <div class="sidenav mt-2">
                    <nav class="nav-border b-primary" data-nav>
                        <ul class="nav">
                            @foreach($categories as $cat)
                            <li>
                                <a href="#{{$cat->type}}">
                                    <span class="nav-text">{{$cat->title}}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        <ul class="nav">
                            <li class="nav-header hidden-folded mt-2">
                                <span class="d-block pt-1 text-sm text-muted _600">Tags</span>
                            </li>
                            <li>
                                <a href="#client">
                                                        <span class="nav-badge">
				                    	<b class="badge badge-xs primary">&nbsp;</b>
				                  	</span>
                                    <span class="nav-text">Clients</span>
                                </a>
                            </li>
                            <li>
                                <a href="#supplier">
                                                        <span class="nav-badge">
				                    	<b class="badge badge-xs warn">&nbsp;</b>
				                  	</span>
                                    <span class="nav-text">Suppliers</span>
                                </a>
                            </li>
                            <li>
                                <a href="#competitor">
                                                        <span class="nav-badge">
				                    	<b class="badge badge-xs">&nbsp;</b>
				                  	</span>
                                    <span class="nav-text">Competitors</span>
                                </a>
                            </li>
                            <li>
                                <a href="#corp">
                                                        <span class="nav-badge">
				                    	<b class="badge badge-xs success">&nbsp;</b>
				                  	</span>
                                    <span class="nav-text">Corps</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- / -->
            <!-- footer -->
            <div class="p-2 mt-auto p-3">
                <div class="input-group">
                    <input type="text" class="form-control form-control-sm" id="newField" placeholder="New group" required>
                    <span class="input-group-append">
	                  <button class="btn btn-default btn-sm no-shadow" type="button" id="newBtn">
	                    <i class="fa fa-plus text-muted"></i>
	                  </button>
	                </span>
                </div>
            </div>
            <!-- / -->
        </div>
    </div>
    <div class="d-flex flex" id="content-body">
        <div class="d-flex flex-column flex" id="user-list">
            <div class="navbar white no-radius box-shadow pos-rlt">
                <form class="flex">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-sm search" placeholder="Search" required>
                        <span class="input-group-append">
		                	<button class="btn btn-default btn-sm no-shadow" type="button"><i class="fa fa-search"></i></button>
		              	</span>
                    </div>
                </form>
                <button class="btn btn-sm white ml-1 sort" data-sort="item-title" data-toggle="tooltip" title="Sort">
                    <i class="fa fa-sort"></i>
                </button>
                <a data-toggle="modal" data-target="#content-aside" data-modal class="ml-1 d-md-none">
                                        <span class="btn btn-sm btn-icon primary">
			      		<i class="fa fa-th"></i>
			        </span>
                </a>
            </div>
            <div class="d-flex flex scroll-y">
                <div class="d-flex flex-column flex white lt">
                    <div class="scroll-y">
                        <div class="list hide">
                            @foreach($user->clients()->get() as $client)
                            <div class="list-item " data-id="item-5">
                                                    <span class="w-40 avatar circle blue-grey">
			      			        <i class="on b-white avatar-right"></i>
			      			          R
			      			      </span>
                                <div class="list-body">
                                    <a href="app.user.detail.html#item-5" class="item-title _500">{{$client->name}}</a>
                                    <div class="item-except text-sm text-muted h-1x">
                                        radionomy@gmail.com
                                    </div>
                                    <div class="item-tag tag hide">
                                        {{$client->category()->first()->title}}
                                    </div>
                                </div>
                                <div>
                                    <div class="item-action dropdown">
                                        <a href="#" data-toggle="dropdown" class="text-muted">
                                            <i class="fa fa-fw fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right text-color" role="menu">
                                            <a class="dropdown-item">
                                                <i class="fa fa-tag"></i>
                                                Action
                                            </a>
                                            <a class="dropdown-item">
                                                <i class="fa fa-pencil"></i>
                                                Another action
                                            </a>
                                            <a class="dropdown-item">
                                                <i class="fa fa-reply"></i>
                                                Something else here
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item">
                                                <i class="fa fa-ellipsis-h"></i>
                                                Separated link
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="no-result hide">
                            <div class="p-4 text-center">
                                No Results
                            </div>
                        </div>
                    </div>
                    <div class="p-3 b-t mt-auto">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <div class="pagination pagination-xs">
                                </div>
                            </div>
                            <div>
                                <span class="text-muted">Total:</span>
                                <span id="count"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column b-l" style="width: 50px">
                    <div class="scrollable hover">
                        <div class="text-center text-sm py-3 d-flex flex-column" id="filter">
                            <a href="javascript:void(0);">А</a>
                            <a href="javascript:void(0);">Б</a>
                            <a href="javascript:void(0);">В</a>
                            <a href="javascript:void(0);">Г</a>
                            <a href="javascript:void(0);">Д</a>
                            <a href="javascript:void(0);">Е</a>
                            <a href="javascript:void(0);">Ж</a>
                            <a href="javascript:void(0);">З</a>
                            <a href="javascript:void(0);">И</a>
                            <a href="javascript:void(0);">К</a>
                            <a href="javascript:void(0);">Л</a>
                            <a href="javascript:void(0);">М</a>
                            <a href="javascript:void(0);">Н</a>
                            <a href="javascript:void(0);">О</a>
                            <a href="javascript:void(0);">П</a>
                            <a href="javascript:void(0);">Р</a>
                            <a href="javascript:void(0);">С</a>
                            <a href="javascript:void(0);">Т</a>
                            <a href="javascript:void(0);">У</a>
                            <a href="javascript:void(0);">Ф</a>
                            <a href="javascript:void(0);">Х</a>
                            <a href="javascript:void(0);">Ц</a>
                            <a href="javascript:void(0);">Ч</a>
                            <a href="javascript:void(0);">Ш</a>
                            <a href="javascript:void(0);">Э</a>
                            <a href="javascript:void(0);">Ю</a>
                            <a href="javascript:void(0);">Я</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ############ Main END-->
