<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <meta name="yandex-verification" content="9ed09c9e96942bff" />
    <meta name="yandex-verification" content="2b73412deddfdbe2" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{env('APP_NAME', 'Недвижимость')}}</title>
    <meta name="keywords" content="Недвижимость, нд31, продажа, аренда, снять, купить, собственника, агентство, риэлторы, новостройки в Белгороде" />
    <meta name="description" content="@yield('description')">

    <meta property="og:site_name" content="{{env('APP_NAME', 'Недвижимость')}}">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:type" content="article">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:url" content="@yield('url')">
    <meta property="og:image" content="@yield('image')">

    <link href="{{ mix('css/front.css') }}" rel="stylesheet">
    @yield('canonical')
    @stack('styles')
</head>
<body>
<div class="main-container">
    <div class="topbar-outer dark">
        <div class="topbar content-width" style="opacity: 1;">
            <div class="table fullheight">
                <div class="table-cell fullheight middle">
                    <div class="logo">
                        <a href="{{url('/')}}"><img alt="" src="{{asset('/img/logo-full.svg')}}" height="45">
                        </a>
                    </div>
                </div>
            </div>
            <div class="topnav">

            </div>
            <ul class="topnav">
                <li><a href="{{ route('apartments') }}" class="@if(request()->route()->getName() == 'apartments')current @endif">Квартиры</a>
                </li>
                <li><a href="{{ route('rooms') }}"  class="@if(request()->route()->getName() == 'rooms')current @endif">Комнаты</a>
                </li>
                <li><a href="{{ route('houses') }}" class="@if(request()->route()->getName() == 'houses')current @endif">Дома</a>
                </li>
                <li><a href="{{ route('lands') }}" class="@if(request()->route()->getName() == 'lands')current @endif">Участки</a>
                </li>
                <li><a href="{{ route('garages') }}" class="@if(request()->route()->getName() == 'garages')current @endif">Гаражи</a>
                </li>
                <li><a href="{{ route('commerces') }}" class="@if(request()->route()->getName() == 'commerces')current @endif">Ком.недвижимость</a>
                </li>
                <li>
                    @if(Auth::guest())
                        <a href="{{ route('login') }}">
                            <img alt="" src="{{asset('/img/user.svg')}}">
                        </a>
                    @else
                        <a href="{{ route('DashBoard') }}">
                            <i class="fa fa-user"></i>
                            Панель управления
                        </a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
    <div style="margin-top: 100px;">
        @yield('content')
    </div>
</div>
<footer>
    <div class="footer content-width">
        <div class="logo">
            <a href="{{url('/')}}"><img alt="" src="{{asset('/img/logo-full.svg')}}" height="45">
            </a>
        </div>
        <ul class="botnav">
            <li><a href="{{ route('apartments') }}" class="current">Квартиры</a>
            </li>
            <li><a href="{{ route('rooms') }}">Комнаты</a>
            </li>
            <li><a href="{{ route('houses') }}" class="drop">Дома</a>
            </li>
            <li><a href="{{ route('lands') }}" class="drop">Участки</a>
            </li>
            <li><a href="{{ route('garages') }}">Гаражи</a>
            </li>
            <li><a href="{{ route('commerces') }}">Ком.недвижимость</a>
            </li>
            <li>
                <div style="padding: 10px 0;">
                    <!-- Yandex.Metrika informer -->
                    <a href="https://metrika.yandex.ru/stat/?id=52188475&amp;from=informer"
                       target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/52188475/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                                           style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="52188475" data-lang="ru" /></a>
                    <!-- /Yandex.Metrika informer -->

                    <!-- Yandex.Metrika counter -->
                    <script type="text/javascript" >
                        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

                        ym(52188475, "init", {
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true
                        });
                    </script>
                    <noscript><div><img src="https://mc.yandex.ru/watch/52188475" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                    <!-- /Yandex.Metrika counter -->
                </div>
            </li>
        </ul>
        <div class="madeby">

            <a href="https://webstyle.top">
                <img alt="" src="{{asset('/img/ws.svg')}}" height="38">
            </a>
        </div>
    </div>
</footer>
<script src="{{ mix('js/front.js') }}"></script>
@stack('scripts')
<!— Yandex.Metrika counter —>
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52188475, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52188475" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!— /Yandex.Metrika counter —>
</body>
</html>
