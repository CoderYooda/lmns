@extends('template.front.layouts.desktop')

@section('title', 'Продажа и аренда недвижимости в Белгороде и области')

@section('description', 'Крупнейший портал недвижимоти в городе Белгород. Позволяет легко купить продать обменять как жилую так и коммерческую недвижимость' )

@section('content')
    <form id="filter-head" action="{{route('search')}}" method="GET">
        <div class="home-back">
            <div class="home-outer content-width">
                <div class="ban-bot-outer ">
                    <div class="head-buttons">
                        <input style="" id="sold-input" type="radio" name="rent" value="0" checked><label for="sold-input">Купить</label><input style="" id="rent-input" type="radio" name="rent" value="1"><label for="rent-input">Снять</label>
                    </div>
                    <h1>Недвижимость в Белгороде и Белгородской области</h1>
                </div>
{{--                <button name="onmap" value="1" class="map-link">--}}
{{--                    <span class="">--}}
{{--                        <img src="{{asset('img/svg/point.svg')}}"  alt="">--}}
{{--                        <div class="look-on-map">Смотреть</br> на карте</div>--}}
{{--                    </span>--}}
{{--                </button>--}}
                <button class="map-link onmap" type="submit" name="onmap" value="1">
                    <div class="">
                        <img src="{{asset('img/onmap.png')}}"  alt="">
                    </div>
                </button>
            </div>
            {{--<div data-parallax-disallow="y" id="velo" class="velo parallax-layer parallax-layer__3" ></div>--}}
            {{--<div id="light" class="light parallax-layer parallax-layer__4" data-parallax-deep="50"></div>--}}
            {{--<div id="lamp" class="lamp parallax-layer parallax-layer__4" data-parallax-deep="50"></div>--}}
            {{--<div id="trees" class="trees parallax-layer parallax-layer__6" data-parallax-deep="20"></div>--}}
        </div>
        <div class="filter-block">
            <div class="filter-outer content-width type-1">
                <div id="select" class="f-elem">
                    <div class="incont">
                        <div class="headb np">
                            <select name="category" id="category" class="form-control select-init" style="visibility: hidden">
                                <option value="1" @if( request('category') != NULL && request('category') == 1 )selected="selected"@endif>Квартиры</option>
                                <option value="2" @if( request('category') != NULL && request('category') == 2 )selected="selected"@endif>Комнаты</option>
                                <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дома</option>
                                <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дачи</option>
                                <option value="4" @if( request('category') != NULL && request('category') == 4 )selected="selected"@endif>Участки</option>
                                <option value="5" @if( request('category') != NULL && request('category') == 5 )selected="selected"@endif>Гаражи</option>
                                <option value="6" @if( request('category') != NULL && request('category') == 6 )selected="selected"@endif>Коммерческая недвижимсоть</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="c-kind" class="f-elem">
                    <div class="incont">
                        <div class="headb">
                            <span class="headname">Назначение</span>
                            <select name="commece_type" id="commerce_type" class="form-control select-init" style="visibility: hidden">
                                <option value="0" @if( request('commece_type') != NULL && request('commece_type') == 0 )selected="selected"@endif>Любое</option>
                                @foreach($commerce_type as $type)
                                    <option value="{{$type->id}}" @if( request('commece_type') != NULL && request('commece_type') == $type->id )selected="selected"@endif>{{$type->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div id="type" class="f-elem" style="min-width: 140px;">
                    <div class="incont">
                        <div>
                            <div class="headb">Тип</div>

                            <label class="elem form-check">
                                <input type="checkbox" class="checkbox form-check-input" name="vtorich" id="vtorich" value="1">
                                <span class="checkbox-custom"></span>
                                <span class="form-check-label">Вторичка</span>
                            </label>
                            <label class="elem form-check">
                                <input type="checkbox" class="checkbox form-check-input" name="novostroi" id="novostroi" value="1">
                                <span class="checkbox-custom"></span>
                                <span class="form-check-label">Новостройка</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="rooms" class="f-elem" style="min-width: 200px;">
                    <div class="incont">
                        <div>
                            <div class="headb">Комнат</div>
                            <div>
                                <label class="elem pr-10 d-inline form-check">
                                    <input name="rooms_one" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">1 </span>
                                </label>
                                <label class="elem pr-10 d-inline form-check">
                                    <input name="rooms_two" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">2 </span>
                                </label>
                                <label class="elem pr-10 d-inline form-check">
                                    <input name="rooms_three" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">3 </span>
                                </label>
                                <label class="elem pr-10 d-inline form-check">
                                    <input  name="rooms_four" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">4+ </span>
                                </label>
                            </div>
                            <div>
                                <label class="elem pr-12 d-inline form-check">
                                    <input name="rooms_study" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">Студия</span>
                                </label>
                                <label class="elem d-inline form-check">
                                    <input name="rooms_sp" type="checkbox" class="checkbox form-check-input">
                                    <span class="checkbox-custom"></span>
                                    <span class="form-check-label">Св. план.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="square" class="f-elem" style="min-width: 148px;">
                    <div class="incont">
                        <div>
                            <div class="headb">Площадь</div>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">от</span>
                                <input name="square_from" type="number" class="" style="width: 73px">
                            </label>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">до</span>
                                <input name="square_to" type="number" class="" style="width: 73px">
                                <div class="suffix form-check-label">м<sup>2</sup></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="square_sot" class="f-elem" style="min-width: 148px;">
                    <div class="incont">
                        <div>
                            <div class="headb">Площадь</div>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">от</span>
                                <input name="square_sot_from" type="number" class="" style="width: 73px">
                            </label>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">до</span>
                                <input name="square_sot_to" type="number" class="" style="width: 73px">
                                <div class="suffix form-check-label"><sup>соток</sup></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="cost" class="f-elem" style="min-width: 165px;">
                    <div class="incont">
                        <div>
                            <div class="headb">Цена</div>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">от</span>
                                <input name="cost-from" type="number" class="" style="width: 100px">
                            </label>
                            <label class="elem form-check">
                                <span class="prefix form-check-label">до</span>
                                <input name="cost-to" type="number" class="" style="width: 100px">
                                <div class="suffix form-check-label">Р</div>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="place" class="f-elem" style="width: 40%;">
                    <div class="incont">
                        <div style="width: 100%;">
                            <div class="headb">
                                <span class="headname">Населенный пункт</span>
                                {{--<label class="elem form-check" style="margin-top: 37px">--}}
                                {{--<input type="text" class="" style="width: 100%;" placeholder="Населенный пункт, адрес, улица">--}}
                                {{--</label>--}}
                                <select name="city" id="cities" class="form-control select-init" style="visibility: hidden">
                                    <option value="0" @if( request('category') != NULL && request('city') == 0 )selected="selected"@endif>Любой</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" @if( request('category') != NULL && request('city') == $city->id )selected="selected"@endif>{{$city->socr}}. {{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="content-width under-searcher" >
                <div class="under-block">
                    <span>Сейчас на сайте {{App\Http\Controllers\ObjectController::getTotalCount()}} предложений недвижимости</span>
                </div><div class="under-block" style="text-align: right;">
                    <button type="submit" class="blue-btn">Показать</button>
                </div>
            </div>
        </div>
    </form>
    <div>
        <div class="content-width pre-outer">
            <div class="elem col-3">
                <div class="prejudes">
                    <img src="{{asset('img/svg/flats.svg')}}" alt="">
                    <span class="title">
                        Квартиры
                    </span>
                    <ul class="lister-rent">
                        <li>
                            <a title="Однокомнатные квартиры" href="/1komnatnyu-kvartiry" class="name">1 - комнатные</a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCount(1, null, 1)}}</span>
                        </li>
                        <li>
                            <a title="Двукомнатные квартиры" href="/2komnatnyu-kvartiry" class="name">2 - комнатные</a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCount(1, null, 2)}}</span>
                        </li>
                        <li>
                            <a title="Трехкомнатные квартиры" href="/3komnatnyu-kvartiry" class="name">3 - комнатные</a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCount(1, null, 3)}}</span></li>
                        <li>
                            <a title="Четырехкомнатные квартиры" href="/4komnatnyu-kvartiry" class="name">4+ - комнат</a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCount(1, null, 4)}}</span>
                        </li>
                    </ul>
                </div>
            </div><div class="elem col-3">
                <div class="prejudes">
                    <img src="{{asset('img/svg/rooms.svg')}}" alt="">
                    <span class="title">
                        Комнаты
                    </span>
                    <ul class="lister-rent">
                        <li>
                            <a title="Комнаты до 10 квадратных метров" href="/komnatu-do10metrov" class="name">До 10 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, null, 0, 10)}}</span>
                        </li>
                        <li>
                            <a title="Комнаты от 10 до 15 квадратных метров" href="/komnatu-ot10do15metrov" class="name">От 10 до 15 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, null, 10, 15)}}</span>
                        </li>
                        <li>
                            <a title="Комнаты от 15 до 20 квадратных метров" href="/komnatu-ot15do20metrov" class="name">От 15 до 20 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, null, 15, 20)}}</span></li>
                        <li>
                            <a title="Комнаты от 20 квадратных метров" href="/komnatu-ot20metrov" class="name">От 20 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, null, 20, 1000)}}</span>
                        </li>
                    </ul>
                </div>
            </div><div class="elem col-3">
                <div class="prejudes">
                    <img src="{{asset('img/svg/houses.svg')}}" alt="">
                    <span class="title">
                        Дома
                    </span>
                    <ul class="lister-rent">
                        <li>
                            <a title="Дома от 100 квадратных метров" href="/dom-do100metrov" class="name">До 100 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, null, 0, 100)}}</span>
                        </li>
                        <li>
                            <a title="Дома от 100 до 150 квадратных метров" href="/dom-ot100do150metrov" class="name">От 100 до 150 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, null, 100, 150)}}</span>
                        </li>
                        <li>
                            <a title="Дома от 150 до 200 квадратных метров" href="/dom-ot150do200metrov" class="name">От 150 до 200 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, null, 150, 200)}}</span></li>
                        <li>
                            <a title="Дома от 200 квадратных метров" href="/dom-ot200metrov" class="name">От 200 м<sup>2</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, null, 200, 10000)}}</span>
                        </li>
                    </ul>
                </div>
            </div><div class="elem col-3">
                <div class="prejudes">
                    <img src="{{asset('img/svg/areas.svg')}}" alt="">
                    <span class="title">
                        Участки
                    </span>
                    <ul class="lister-rent">
                        <li>
                            <a href="/uchastok-do10sotok" class="name">До 10 <sup>сот</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, null, 0, 10)}}</span>
                        </li>
                        <li>
                            <a href="/uchastok-ot10do15sotok" class="name">От 10 до 15 <sup>сот</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, null, 10, 15)}}</span>
                        </li>
                        <li>
                            <a href="/uchastok-ot15do20sotok" class="name">От 15 до 20 <sup>сот</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, null, 15, 20)}}</span></li>
                        <li>
                            <a href="/uchastok-ot20sotok" class="name">От 20 <sup>сот</sup></a>
                            <span class="count">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, null, 20, 9000)}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="content-width pre-outer">
            <h2>Новые объявления</h2>
            <div class="product-block">
                @forelse($lastes as $object)
                    @include('template.front.common.object.grid-elem', ['col' => 5])
                @empty
                    <div class="col-md-12">
                        Новых объектов пока что нет.
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
