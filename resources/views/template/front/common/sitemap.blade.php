<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($categories as $category)
        <url>
            <loc>{{ route($category->type . 's') }}</loc>
            <lastmod>{{ \App\Models\Objects\BaseObject::where('category_id', ($category->id))->orderBy('updated_at', 'DESC')->first()->updated_at->toDateString() }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.7</priority>
        </url>
    @endforeach
    @foreach($objects as $object)
        <url>
            <loc>{{ route($object->category->type, $object->id) }}</loc>
            <lastmod>{{ $object->updated_at->toDateString() }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
