@extends('template.front.layouts.desktop')

@section('canonical')
    @if(Request::input('category') != NULL)
     <link rel="canonical" href="{{ \Request::url() }}" />
    @endif
@endsection

@if(Request::input('category') != NULL)

    @section('description', \App\Models\Category::where('id', Request::input('category'))->first()->generateDescription() )
    @section('title', \App\Models\Category::where('id', Request::input('category'))->first()->generateTitle())
    @section('url', \Request::url())

@endif

@section('content')

    <div>
        <div class="content-width">
            <h1 class="mb-4" style="font-size: 28px;">
                @if(request('rent') != NULL && request('rent') == 0)Продажа@endif
                @if(request('rent') != NULL && request('rent') == 1)Аренда@endif
                @if(request('rent') == NULL || request('rent') == 2)Продажа и аренда@endif
                <?php switch (Request::input('category')) {
                    case 1:echo "квартир";break;
                    case 2:echo "комнат";break;
                    case 3:echo "коттеджей";break;
                    case 4:echo "земельных участков";break;
                    case 5:echo "гаражей";break;
                    case 6:echo "коммерческой недвижимости";break;}?>
                {{\App\Http\Controllers\Support\CityController::getCityNameById(Request::input('city'))}}</h1>
                <div class="objects_box">
                    <div class="product-block">
                            @forelse($objects as $object)
                                @include('template.front.common.object.grid-elem', ['col' => 4])
                            @empty
                                <div class="col-md-12">
                                    По вашему запросу ничего не найдено :(
                                </div>
                            @endforelse
                    </div>
                    <div>
                        {{ $objects->appends(Request::all())->links() }}

                    </div>
                </div>
                @include('template.front.common.object.filter')
        </div>
    </div>
@endsection

@push('scripts')
@endpush
