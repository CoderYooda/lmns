<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ mix('css/front.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.min.css" rel="stylesheet">
    <style>
        .product-pre {
            width: 100%!important;
            padding: 0 15px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body class="map">
<div>
    <div class="map-box">
        <div class="objects_box" style="height: 100vh;">
            <div class="product-block nano">
                <div class="nano-content">
                    {{--@include('chunks.objects_grid')--}}
                    @forelse($objects as $object)
                        @include('template.front.common.object.grid-elem', ['col' => 4])
                    @empty
                    <div class="no-objects">По данным критериям объектов нет</div>


                    @endforelse
                </div>
            </div>
        </div>
        <div class="fullmap" id="map" ></div>
        {{--@include('template.front.common.object.filter')--}}
        <div class="object-filter">
            <form id="form_apartment" >
                <a href="{{Request::url() . '?' . http_build_query(array_merge(\Request::query(), ['onmap' => '0']))}}" class="map-btn" type="submit" style="background: transparent; display: block;">
                    <div class="map-link" style="width: 140px;height: 123px;">
                        <span class="button-shine">
                            <img style="padding-top: 26px;" src="{{asset('img/svg/hamb.svg')}}"  alt="">
                            <div class="look-on-map">Назад</div>
                        </span>
                    </div>
                </a>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    $(".nano").nanoScroller();


    ymaps.ready(function(){




        // Указывается идентификатор HTML-элемента.
        var map = new ymaps.Map("map", {
            center: [50.616463, 36.583558],
            zoom: 16,
            controls: ['zoomControl', 'searchControl']
        });

        objectManager = new ymaps.ObjectManager();
        //map.controls.get('zoomControl').options.set({size: 'small'});
        // Загружаем GeoJSON файл, экспортированный из Конструктора карт.
        $.getJSON('/geo/test.geojson')
            .done(function (geoJson) {
                geoJson.features[0].geometry.coordinates[0].forEach(function (obj) {
                    map.geoObjects
                        .add(new ymaps.Placemark(obj, {
                            balloonContent: 'цвет <strong>воды пляжа бонди</strong>'
                        }, {
                            preset: 'islands#icon',
                            iconColor: '#0095b6'
                        }));
                });
                // Добавляем описание объектов в формате JSON в менеджер объектов.
                objectManager.add(geoJson);
                // Добавляем объекты на карту.
                map.geoObjects.add(objectManager);
            });


        // Добавляем многоугольник на карту.



        @if(count($objects) > 0)
        var clusterer = new ymaps.Clusterer({
                preset: 'islands#invertedVioletClusterIcons',
                groupByCoordinates: false,
                clusterDisableClickZoom: true,
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false
            }),
            getPointData = function (index) {
                return {
                    balloonContentHeader: '<font size=3><b><a target="_blank" href="' + index.link + '">' + index.title + '</a></b></font>',
                    balloonContentBody: ' <img class="inmap-img" src="' + index.thumb + '" alt="' + index.title + '">',
                    balloonContentFooter: '<span>'+ index.price +'</span><strong style="float:right;"><a href="' + index.link + '" class="heart">Подробнее</a></strong>',
                    clusterCaption: index.title
                };
            },
            getPointOptions = function () {
                return {
                    iconLayout: 'default#image',
                    iconImageHref: '/img/svg/mp.svg',
                    iconImageSize: [47, 77],
                    iconImageOffset: [-23.5,-71]
                };
            },
            points = [
                @foreach($objects as $object)
                    @if($object->geopoint != NULL)
                        {
                            point:[{{$object->geopoint}}],
                            name:'{{$object->title}}',
                            link:'{{route($object->category->type, $object->id)}}',
                            price:'@if($object->rented){{ number_format($object->rent_price, 0, '', ' ') }} р / мес @else{{ number_format($object->price, 0, '', ' ') }} р@endif',
                            thumb:'@if($object->images->first() != NULL) {{asset($object->images->first()->thumb_url)}} @else {{ asset('/img/noimage.jpg') }} @endif',
                            title:'{!!$object->title!!}'
                        },
                    @endif
                @endforeach
            ],
            geoObjects = [];

        for (var i = 0, len = points.length; i < len; i++) {
            geoObjects[i] = new ymaps.Placemark(points[i].point, getPointData(points[i]), getPointOptions());
        }

        clusterer.options.set({
            gridSize: 80,
            clusterDisableClickZoom: true
        });
        clusterer.add(geoObjects);
        map.geoObjects.add(clusterer);

        map.setBounds(clusterer.getBounds(), {
            checkZoomRange: true, zoomMargin: 50
        }).then(
            function () {
                if (map.getZoom() > 11) map.setZoom(11);
                $('#mapCollapse').removeClass('cloack');
            })
        // clusterer.options.set({
        //     gridSize: 80,
        //     clusterDisableClickZoom: true
        // });
        // clusterer.add(geoObjects);
        // myMap.geoObjects.add(clusterer);
        //
        // myMap.setBounds(clusterer.getBounds(), {
        //     checkZoomRange: true, zoomMargin: 50
        // }).then(
        //     function () {
        //         if (myMap.getZoom() > 11) myMap.setZoom(11);
        //         console.log(1);
        //         $('#mapCollapse').removeClass('cloack');
        //     })
        $('.map .product-pre a').on('click', function(e){
            e.preventDefault();
            //
            var coords = $(this).parent('.product-pre').data('coords').split(',');
            coords[0] = parseFloat(coords[0]);
            coords[1] = parseFloat(coords[1]);
            map.panTo(
                // Координаты нового центра карты
                coords, {
                    checkZoomRange: true,
                    flying: true
                }
            ).then(function () {
                map.setZoom(19, {
                    duration: 1000
                });
            });

        });
        @endif
    });
</script>
</body>
</html>
