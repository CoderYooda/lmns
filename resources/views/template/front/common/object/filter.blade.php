<div class="object-filter">

    {{--<button id="mapbtn" style="width: 100%;margin-bottom: 15px;" onclick="loadMap({{ $category->id }}, {{request('rent')}})" class="btn btn-primary">--}}
        {{--Показать на карте--}}
    {{--</button>--}}

    <form id="form_apartment" action="{{route($category->type.'s')}}">

        <button class="map-btn" type="submit" name="onmap" value="1">
            <div class="map-link">
                <img src="{{asset('img/onmap.png')}}"  alt="">
            </div>
        </button>

        <div class="filter-form">
            <h3 style="margin-bottom: 15px;"><img src="{{asset('img/filtre.svg')}}" alt="">
                <span>Фильтр</span>
            </h3>
            <input type="hidden" name="category" value="{{ $category->id }}">

            @if($category->id == 6)
                <div class="form-group">
                    <span class="headname">Тип</span>
                    <select name="commece_type" id="commece_type" class="form-control">
                        <option value="0" @if( request('commece_type') != NULL && request('commece_type') == 0 )selected="selected"@endif>Любой</option>
                        @foreach($commerce_types as $commerce_type)
                            <option value="{{ $commerce_type->id }}"
                                {{ Request::input('commece_type') == $commerce_type->id ? 'selected' : '' }}>{{ $commerce_type->title }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <hr>
            <label class="elem form-check">
                <input type="radio" class="checkbox form-check-input" name="rent" id="all-input" value="2" @if(request('rent') == '' || request('rent') == 2 ) checked @endif>
                <span class="checkbox-custom"></span>
                <span class="form-check-label">Все</span>
            </label>

            <label class="elem form-check">
                <input type="radio" class="checkbox form-check-input" name="rent" id="sold-input" value="0" @if(request('rent') == '0' ) checked @endif>
                <span class="checkbox-custom"></span>
                <span class="form-check-label">Купить</span>
            </label>

            <label class="elem form-check">
                <input type="radio" class="checkbox form-check-input" name="rent" id="rent-input" value="1" @if(request('rent') == '1' ) checked @endif>
                <span class="checkbox-custom"></span>
                <span class="form-check-label">Снять</span>
            </label>

            <hr>

            <div class="elem">
                <span class="headname">Населенный пункт</span>
                <select name="city" id="cities" class="form-control">
                    <option value="0" @if( $category->id != NULL && request('city') == 0 )selected="selected"@endif>Любой</option>
                    @foreach($cities as $city)
                        <option value="{{ $city->id }}"
                            {{ Request::input('city') == $city->id ? 'selected' : '' }}>{{$city->socr}}. {{$city->name}}</option>
                    @endforeach
                </select>
            </div>

            <hr>
            {{--<div class="form-group count-room">--}}
                {{--<input style="" id="all-input" type="radio" name="rent" value="2" @if(request('rent') == NULL || request('rent') == 2 ) checked @endif>--}}
                {{--<label for="all-input">Все</label>--}}
                {{--<input style="" id="sold-input" type="radio" name="rent" value="0" @if(request('rent') != NULL && request('rent') == 0 ) checked @endif>--}}
                {{--<label for="sold-input">Купить</label>--}}
                {{--<input style="" id="rent-input" type="radio" name="rent" value="1" @if(request('rent') != NULL && request('rent') == 1 ) checked @endif>--}}
                {{--<label for="rent-input">Арендовать</label>--}}
            {{--</div>--}}
            @if($category->id == 1)
                <span class="headname">Тип</span>
                <div style="display: inline-block">
                    <label class="elem form-check">
                        <input type="checkbox" class="checkbox form-check-input" name="novostroi" id="novostroi-input" value="1" {{ Request::filled('novostroi') && request('novostroi') == 1  ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">Новостройка</span>
                    </label>

                    <label class="elem form-check">
                        <input type="checkbox" class="checkbox form-check-input" name="vtorich" id="vtorich-input" value="1" {{ Request::filled('vtorich') && request('vtorich') == 1 ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">Вторичка</span>
                    </label>
                </div>
             <hr>
                <span class="headname">Комнатность</span>
                    <label class="elem pr-10 d-inline form-check">
                        <input name="rooms_one" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_one') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">1 </span>
                    </label>
                    <label class="elem pr-10 d-inline form-check">
                        <input name="rooms_two" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_two') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">2 </span>
                    </label>
                    <label class="elem pr-10 d-inline form-check">
                        <input name="rooms_three" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_three') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">3 </span>
                    </label>
                    <label class="elem pr-10 d-inline form-check">
                        <input  name="rooms_four" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_four') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">4+ </span>
                    </label>

                    <label class="elem pr-12 d-inline form-check">
                        <input name="rooms_study" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_study') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">Студия</span>
                    </label>
                    <label class="elem d-inline form-check">
                        <input name="rooms_sp" type="checkbox" class="checkbox form-check-input" {{ Request::filled('rooms_sp') ? 'checked' : '' }}>
                        <span class="checkbox-custom"></span>
                        <span class="form-check-label">Св. план.</span>
                    </label>
                <hr>

            @endif

            <span class="headname">Цена</span>
            <div style="display: inline-block">
                <label class="elem form-check">
                    <span class="prefix form-check-label">от</span>
                    <input name="cost-from" type="number" class="" value="{{ Request::input('cost-from', '') }}" style="width: 120px">
                </label>
                <label class="elem form-check">
                    <span class="prefix form-check-label">до</span>
                    <input name="cost-to" type="number" class="" value="{{ Request::input('cost-to', '') }}" style="width: 120px">
                    <div class="suffix form-check-label">Р</div>
                </label>
            </div>

            <hr>


            @if($category->id == 1 || $category->id == 2 || $category->id == 3 || $category->id == 5)

                <span class="headname">Площадь</span>
                <div style="display: inline-block">
                    <label class="elem form-check">
                        <span class="prefix form-check-label">от</span>
                        <input name="square_from" type="number" class="" value="{{ Request::input('square_from', '') }}" style="width: 120px">
                    </label>
                    <label class="elem form-check">
                        <span class="prefix form-check-label">до</span>
                        <input name="square_to" type="number" class="" value="{{ Request::input('square_to', '') }}" style="width: 120px">
                        <div class="suffix form-check-label">м<sup>2</sup></div>
                    </label>
                </div>

            @endif
            @if($category->id == 4)

                <span class="headname">Площадь (сот)</span>
                <div style="display: inline-block">
                    <label class="elem form-check">
                        <span class="prefix form-check-label">от</span>
                        <input name="square_sot_from" type="number" class="" value="{{ Request::input('square_sot_from', '') }}" style="width: 120px">
                    </label>
                    <label class="elem form-check">
                        <span class="prefix form-check-label">до</span>
                        <input name="square_sot_to" type="number" class="" value="{{ Request::input('square_sot_to', '') }}" style="width: 120px">
                        <div class="suffix form-check-label"><sup>сот</sup></div>
                    </label>
                </div>

            @endif
            @if($category->id != 3 && request('category') != 4)

                <span class="headname">Этаж</span>
                <div style="display: inline-block">
                    <label class="elem form-check">
                        <span class="prefix form-check-label">от</span>
                        <input name="floor_from" type="number" class="" value="{{ Request::input('floor_from', '') }}" style="width: 120px">
                    </label>
                    <label class="elem form-check">
                        <span class="prefix form-check-label">до</span>
                        <input name="floor_to" type="number" class="" value="{{ Request::input('floor_to', '') }}" style="width: 120px">
                        <div class="suffix form-check-label"></div>
                    </label>
                </div>

            @endif
            <div class="text-center">
                <button type="submit" class="find-butt">Найти</button>
            </div>
        </div>
    </form>
</div>
