@extends('template.front.layouts.desktop')

@section('title', str_replace("&nbsp;", " ", strip_tags($object->title)))

@section('description', $object->generateDescription())

@section('url', route($object->category->type, $object->id))

@if(count($object->images) > 0)
    @section('image', asset($object->images->first()->url))
@endif

@section('content')
	<section id="object">
		<div class="content-width">
            <div class="single_object objects_box">
                <h1 class="">{!! $object->title !!}</h1>
                <div class="city_bread">
                    <div class="elem">
                        Белгородская область
                    </div>
                    @if($object->city()->first() != null)
                        <div class="elem">
                            {{ $object->city->socr  }}. {{ $object->city->name }}
                        </div>
                    @endif
                    @if($object->area()->first() != null)
                        <div class="elem">
                            {{ucfirst($object->area()->first()->title)}}
                        </div>
                    @endif
                    @if($object->street != null)
                        <div class="elem">
                            {{mb_convert_case($object->street, MB_CASE_TITLE, "UTF-8")}} {{ $object->{$meta}->house_number }}
                        </div>
                    @endif
                </div>
                <div class="photo_container">
                    @if(count($object->images) > 0)
                        <div class="img-wraper wrapper-box">
                            <div class="fotorama"
                                 data-allowfullscreen="true"
                                 data-keyboard="true"
                                 data-nav="thumbs"
                                 data-width="920" data-ratio="920/467" data-max-width="100%">
                                @foreach($object->images as $image)
                                    <img src="{{ asset($image->url) }}">
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                <div class="main_params">
                    @include('template.front.common.object.square')
                    @if($object->{$meta}->floor != NULL && $object->{$meta}->n_of_floor != NULL)
                        <div class="elem">
                            <div class="headtitle">Этаж</div>
                            <div class="elmets">
                                <div class="elzet nominal">{{$object->{$meta}->floor . ' / ' . $object->{$meta}->n_of_floor}}</div>
                            </div>
                        </div>
                    @endif
                    @if($object->{$meta}->year != NULL)
                        <div class="elem">
                            <div class="headtitle">Год постройки</div>
                            <div class="elmets">
                                <div class="elzet nominal">@if($object->{$meta}->qr != null){{$object->{$meta}->qr}} / @endif{{$object->{$meta}->year}}</div>
                            </div>
                        </div>
                    @endif
                    {{--<div class="elem">--}}
                        {{--<div class="headtitle">Номер объекта</div>--}}
                        {{--<div class="elmets">--}}
                            {{--<div class="elzet nominal">{{$object->id}}</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--@if(method_exists($object->{$meta}, 'wc') &&  $object->{$meta}->wc_id != null)--}}
                        {{--<div class="elem">--}}
                            {{--<div class="headtitle">Санузел</div>--}}
                            {{--<div class="elmets">--}}
                                {{--<div class="elzet nominal" style="font-size: 14px;font-weight: bold;">{{$object->{$meta}->wc()->first()->title}}</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                </div>

                @if($object->description != NULL)
                    <div class="description-wraper">
                        <p>{!!$object->description!!}</p>
                    </div>
                @endif

                @include('template.front.common.object.parametrs')
                {!! App\Http\Controllers\ObjectController::getSimilarObjects($object->category_id, $object->id) !!}

            </div>
            <div class="object-filter">

                @if($object->rented)
                    <p class="title-2 full-cost mb-0">{{number_format($object->rent_price, 0, '.', ' ')}} <i class="fa fa-rub" aria-hidden="true"></i><span style="font-size: 12px"> / мес.</span></p>
                @else
                    <p class="title-2 full-cost  mb-0">{{number_format($object->price, 0, '.', ' ')}} <i class="fa fa-rub" aria-hidden="true"></i></p>
                    @if($object->price_m != NULL)
                        <p class="title-2 m-squad mb-0">{{number_format($object->price_m, 0, '.', ' ')}}  р/м<sup>2</sup></p>
                    @endif
                @endif
                <div class="in-params">
                    <form id="form_apartment" action="">
                        <button class="map-btn" type="submit" name="onmap" value="1">
                            <div class="map-link">
                                <img src="{{asset('img/onmap.png')}}"  alt="">
                            </div>
                        </button>
                    </form>
                    <div id="wrapper">
                        <div class="collapse  @if (\Session::has('success')) show @endif filter-form" id="collapseExample">
                            <div  class="sidebar-agent sidebar-box">
                                <div class="object-manager">
                                    <div class="manager-wrap align-items-center">
                                        <div class="manager-data">
                                            <div class="agency-name ml-0">{{$object->agency->title}}</div>
                                            <hr>
                                            @if($object->autor->Avatar()->first() != null)
                                                <div class="manager-ava ml-0"><img src="{{$object->autor->Avatar()->first()->url}}" alt=""></div>
                                            @else
                                                <div class="manager-ava ml-0"><img src="{{ asset('/img/noava.png') }}" alt=""></div>
                                            @endif
                                            <span class="manager-name ml-0">{{$object->autor->lastname}}<br>{{$object->autor->firstname . ' ' . $object->autor->midname}}</span>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" onclick="this.innerHTML = '{{$object->autor->phone}}'; this.href = 'tel:{{$object->autor->phone}}'" class="blue-btn w-100 href">Показать номер</a>
                                    {{--<div class="information-column">--}}
                                        {{--<span><i class="fa fa-phone"></i></span>--}}
                                        {{--<a href="tel:{{$object->autor->phone}}" class="values">{{$object->autor->phone}}</a>--}}
                                    {{--</div>--}}
                                    @if (\Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            {!! \Session::get('success') !!}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>
@endsection

@push('styles')
    <link href="{{asset('lib/fotorama/fotorama.css')}}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="{{asset('lib/fotorama/fotorama.js')}}"></script>
    <script>
        $(window).scroll(function(){
            var offset = 0;
            var sticky = false;
            var top = $(window).scrollTop();

            if ($("#wrapper").offset().top - 123 < top) {
                $("#collapseExample").addClass("sticky");
                sticky = true;
            } else {
                $("#collapseExample").removeClass("sticky");
            }
        });
    </script>
@endpush
