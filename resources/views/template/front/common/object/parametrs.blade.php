<div class="parametrs">

    {{--<div class="elem">--}}
        {{--<span class="details-title">Номер объекта:</span>--}}
        {{--<div class="details-data">{{$object->id}}</div>--}}
    {{--</div>--}}
    @if(method_exists($object->{$meta}, 'wall_materials') && $object->{$meta}->wall_materials->count() > 0)
        <div class="elem">
            <span class="details-title">Материал стен</span>
            <div class="details-data">
                {{$object->{$meta}->wmString()}}
            </div>
        </div>
    @endif
    @if($object->{$meta}->floor != NULL && $object->{$meta}->n_of_floor != NULL)
        <div class="elem">
            <span class="details-title">Этаж</span>
            <div class="details-data">
                {{$object->{$meta}->floor . '/' . $object->{$meta}->n_of_floor}}
            </div>
        </div>
    @endif
    @if($object->{$meta}->floor == NULL && $object->{$meta}->n_of_floor != NULL)
        <div class="elem">
            <span class="details-title">Этаж</span>
            <div class="details-data">
                {{$object->{$meta}->n_of_floor}}
            </div>
        </div>
    @endif
    @if($object->{$meta}->gk_id != NULL)
        <div class="elem">
            <span class="details-title">Жилищьный комплекс</span>
            <div class="details-data">
                {{$object->{$meta}->gk->first()->title}}
            </div>
        </div>
    @endif


    @if(method_exists($object->{$meta}, 'build_type') && $object->{$meta}->build_type_id != NULL)<div class="elem"><span class="details-title">Тип здания</span><div class="details-data">
            {{$object->{$meta}->build_type()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'readiness') &&  $object->{$meta}->readiness_id != NULL)<div class="elem"><span class="details-title">Готовность</span><div class="details-data">
            {{$object->{$meta}->readiness()->first()->title}}
        </div></div>@endif

    @if($object->{$meta}->year != NULL)<div class="elem"><span class="details-title">Год постройки:</span><div class="details-data">
            {{$object->{$meta}->year}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'heatings') && $object->{$meta}->heatings->count() > 0)<div class="elem"><span class="details-title">Отопление</span><div class="details-data">
            {{$object->{$meta}->heatingString()}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'hot_water_system') &&  $object->{$meta}->hot_water_system_id != NULL)<div class="elem"><span class="details-title">Водоснабжение</span><div class="details-data">
            {{$object->{$meta}->hot_water_system()->first()->title}}
        </div></div>@endif
    @if(method_exists($object->{$meta}, 'state') &&  $object->{$meta}->state_id != null)<div class="elem"><span class="details-title">Ремонт</span><div class="details-data">
            {{$object->{$meta}->state()->first()->title}}
        </div></div>@endif

    {{--@if($object->{$meta}->total_square != NULL)<div class="elem"><span class="details-title">Общая площадь:</span><div class="details-data">--}}
            {{--{{$object->{$meta}->total_square}} м<sup>2</sup>--}}
        {{--</div></div>@endif--}}

    {{--@if($object->{$meta}->live_square != NULL)<div class="elem"><span class="details-title">Жилая площадь:</span><div class="details-data">--}}
            {{--{{$object->{$meta}->live_square}} м<sup>2</sup>--}}
        {{--</div></div>@endif--}}

    {{--@if($object->{$meta}->kitchen_square != NULL)<div class="elem"><span class="details-title">Площадь кухни:</span><div class="details-data">--}}
            {{--{{$object->{$meta}->kitchen_square}} м<sup>2</sup>--}}
        {{--</div></div>@endif--}}

    {{--@if($object->{$meta}->land_square != NULL)<div class="elem"><span class="details-title">Площадь участка:</span><div class="details-data">--}}
            {{--{{$object->{$meta}->land_square}} сот.--}}
        {{--</div></div>@endif--}}

    {{--@if($object->{$meta}->room_square != NULL)<div class="elem"><span class="details-title">Площадь комнаты:</span><div class="details-data">--}}
            {{--{{$object->{$meta}->room_square}} м<sup>2</sup>--}}
        {{--</div></div>@endif--}}

    @if(method_exists($object->{$meta}, 'commerce_type') &&  $object->{$meta}->commerce_type_id != NULL)<div class="elem"><span class="details-title">Тип коммерции</span><div class="details-data">
            {{$object->{$meta}->commerce_type()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'doing') &&  $object->{$meta}->doing_id != NULL)<div class="elem"><span class="details-title">Вид деятельности</span><div class="details-data">
            {{$object->{$meta}->doing()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'gruz') &&  $object->{$meta}->gruz_id != NULL)<div class="elem"><span class="details-title">Погруз/разгруз</span><div class="details-data">
            {{$object->{$meta}->gruz()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'sklad') &&  $object->{$meta}->sklad_id != NULL)<div class="elem"><span class="details-title">Вид хранения</span><div class="details-data">
            {{$object->{$meta}->sklad()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'kind') &&  $object->{$meta}->kind_id != NULL)<div class="elem"><span class="details-title">Вид здания</span><div class="details-data">
            {{$object->{$meta}->kind()->first()->title}}
        </div></div>@endif

    {{--@if(method_exists($object->{$meta}, 'apartment_type') &&  $object->{$meta}->apartment_type_id != NULL ||--}}
        {{--method_exists($object->{$meta}, 'room_type') &&  $object->{$meta}->room_type_id != NULL ||--}}
        {{--$object->{$meta}->rooms != NULL ||--}}
        {{--method_exists($object->{$meta}, 'rooms_isolation') &&  $object->{$meta}->rooms_isolation_id != null ||--}}
        {{--method_exists($object->{$meta}, 'wc') &&  $object->{$meta}->wc_id != null ||--}}
        {{--$object->{$meta}->wc_num != NULL ||--}}
        {{--$object->{$meta}->count_people != NULL ||--}}
        {{--$object->{$meta}->balconies != NULL--}}
      {{--)--}}
    {{--@endif--}}

    @if(method_exists($object->{$meta}, 'apartment_type') &&  $object->{$meta}->apartment_type_id != NULL)<div class="elem"><span class="details-title">Тип квартиры</span><div class="details-data">
            {{$object->{$meta}->apartment_type()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'room_type') &&  $object->{$meta}->room_type_id != NULL)<div class="elem"><span class="details-title">Тип комнаты</span><div class="details-data">
            {{$object->{$meta}->room_type()->first()->title}}
        </div></div>@endif

    @if($object->{$meta}->rooms != NULL)<div class="elem"><span class="details-title">Комнат</span><div class="details-data">
            {{$object->{$meta}->rooms}}
        </div></div>@endif

    @if($object->{$meta}->count_people != NULL)<div class="elem"><span class="details-title">Кол - во человек</span><div class="details-data">
            {{$object->{$meta}->count_people}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'rooms_isolation') &&  $object->{$meta}->rooms_isolation_id != null)<div class="elem"><span class="details-title">Изолированность</span><div class="details-data">
            {{$object->{$meta}->rooms_isolation()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'wc') &&  $object->{$meta}->wc_id != null)<div class="elem"><span class="details-title">Санузел</span><div class="details-data">
            {{$object->{$meta}->wc()->first()->title}}
        </div></div>@endif

    @if($object->{$meta}->wc_num != NULL)<div class="elem"><span class="details-title">Санузлов</span><div class="details-data">
            {{$object->{$meta}->wc_num}}
        </div></div>@endif

    @if($object->{$meta}->balconies != NULL)<div class="elem"><span class="details-title">Балконов</span><div class="details-data">
            {{$object->{$meta}->balconies}}
        </div></div>@endif

    @if($object->{$meta}->height || $object->{$meta}->width || $object->{$meta}->length)

    @endif

    @if($object->{$meta}->height != NULL)<div class="elem"><span class="details-title">Высота</span><div class="details-data">
            {{$object->{$meta}->height}} м.
        </div></div>@endif
    @if($object->{$meta}->width != NULL)<div class="elem"><span class="details-title">Ширина</span><div class="details-data">
            {{$object->{$meta}->width}} м.
        </div></div>@endif
    @if($object->{$meta}->length != NULL)<div class="elem"><span class="details-title">Длинна</span><div class="details-data">
            {{$object->{$meta}->length}} м.
        </div></div>@endif

    @if($object->notary || $object->custody || $object->burden)

    @endif

    @if($object->notary)<div class="elem"><span class="details-title">Нотариус</span><div class="details-data">
            да
        </div></div>@endif
    @if($object->custody)<div class="elem"><span class="details-title">Опека</span><div class="details-data">
            да
        </div></div>@endif
    @if($object->burden)<div class="elem"><span class="details-title">Обременение</span><div class="details-data">
            да
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'purpose') &&  $object->{$meta}->purpose_id != NULL)<div class="elem fullwidth"><span class="details-title">Назначение здания</span><div class="details-data">
            {{$object->{$meta}->purpose()->first()->title}}
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'layouts') &&  $object->{$meta}->layouts->count() > 0)<div class="elem fullwidth"><span class="details-title">Планировка</span><div class="details-data">
            @foreach($object->{$meta}->layouts()->get() as $layout)@if ($loop->index != 0), @endif{{$layout->title}}@endforeach
        </div></div>@endif


    @if(method_exists($object->{$meta}, 'repairs') &&  $object->{$meta}->repairs->count() > 0)<div class="elem fullwidth"><span class="details-title">Ремонт</span><div class="details-data">
            @foreach($object->{$meta}->repairs()->get() as $repair)@if ($loop->index != 0), @endif{{$repair->title}}@endforeach
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'specials') &&  $object->{$meta}->specials->count() > 0)<div class="elem fullwidth"><span class="details-title">Особенности</span><div class="details-data">
            @foreach($object->{$meta}->specials()->get() as $special)@if ($loop->index != 0), @endif{{$special->title}}@endforeach
        </div></div>@endif

    @if(method_exists($object->{$meta}, 'build_comforts') &&  $object->{$meta}->build_comforts->count() > 0)<div class="elem fullwidth"><span class="details-title">Благоустройство здания</span><div class="details-data">
            @foreach($object->{$meta}->build_comforts()->get() as $build_comfort)@if ($loop->index != 0), @endif{{$build_comfort->title}}@endforeach
        </div></div>@endif
</div>
<div class="parametrs">
    @if(method_exists($object->{$meta}, 'comforts') &&  $object->{$meta}->comforts->count() > 0)<div class="fullwidth"><span class="details-title">Благоустройство</span><div class="details-data">
            @foreach($object->{$meta}->comforts()->get() as $comfort)@if ($loop->index != 0), @endif{{$comfort->title}}@endforeach
        </div></div>@endif
</div>
