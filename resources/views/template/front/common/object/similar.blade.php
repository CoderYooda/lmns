
@if($objects->count() > 0)
    <h2 style="    font-size: 22px;padding-top: 40px">Похожие объекты</h2>
<div class="objects_box">
    <div class="product-block">
        @forelse($objects as $object)
            @include('template.front.common.object.grid-elem', ['col' => 4])
        @empty
        @endforelse
    </div>
</div>
@endif
