<div class="product-pre col-{{$col}}" data-coords="{{$object->geopoint}}">
    <a title="{{ str_replace("&nbsp;", " ", strip_tags($object->title)) }}" href="{{ route($object->category->type, $object->id) }}">
        @if($object->images->first() != NULL)
            <img title="{{ str_replace("&nbsp;", " ", strip_tags($object->title)) }}" src="{{ asset($object->images->first()->thumb_url) }}" alt="{{ $object->city->title }}">
        @else
            <img title="Нет изображения" src="{{ asset('/img/noimage.jpg') }}" alt="{{ $object->city->title }}">
        @endif
    </a>
    <a title="{{ str_replace("&nbsp;", " ", strip_tags($object->title)) }}" href="{{ route($object->category->type, $object->id) }}"><h3>{!! $object->title  !!}</h3></a>
    <div class="phone-block">
        @if($object->rented)
            <span class="number">{{ number_format($object->rent_price, 0, '', ' ') }} <i class="union"></i> / мес</span>
        @else
            <span class="number">{{ number_format($object->price, 0, '', ' ') }} <i class="union"></i></span>
        @endif
        <a href="tel:{{$object->autor->phone}}" class="phonebutt">
            <div class="phone_short">
                {{$object->autor->phone}}
            </div>
        </a>

    </div>
    <span class="address">{{ $object->city->socr  }}. {{ $object->city->name }}, {{mb_strtoupper(mb_substr($object->street, 0, 1)) . mb_substr($object->street, 1)}}@if($object->{$object->category->type}->house_number != null), д&nbsp;{{$object->{$object->category->type}->house_number}} @endif
    </span>
</div>
