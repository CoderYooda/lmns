@if($object->{$meta}->total_square != NULL || $object->{$meta}->live_square != NULL || $object->{$meta}->kitchen_square != NULL || $object->{$meta}->room_square != NULL)
    <div class="elem">
        <div class="headtitle">Площадь м<sup>2</sup></div>
        <div class="elmets">
            @if($object->{$meta}->total_square != NULL)
                <div class="elzet icon" title="Общая площадь">
                    <img src="{{asset('/img/svg/shouse.svg')}}" alt="">
                </div>
                <div class="elzet nominal">{{(float)$object->{$meta}->total_square}}</div>
            @endif

            @if($object->{$meta}->live_square != NULL)
                <div class="elzet icon" title="Жилая площадь">
                    <img src="{{asset('/img/svg/slive.svg')}}" alt="">
                </div>
                <div class="elzet nominal">{{(float)$object->{$meta}->live_square}}</div>
            @endif

            @if($object->{$meta}->kitchen_square != NULL)
                <div class="elzet icon" title="Площадь кухни">
                    <img src="{{asset('/img/svg/skitchen.svg')}}" alt="">
                </div>
                <div class="elzet nominal">{{(float)$object->{$meta}->kitchen_square}}</div>
            @endif

            @if($object->{$meta}->room_square != NULL)
                <div class="elzet icon" title="Площадь комнат">
                    <img src="{{asset('/img/svg/room.svg')}}" alt="">
                </div>
                <div class="elzet nominal">{{(float)$object->{$meta}->room_square}}</div>
            @endif
        </div>
    </div>
@endif

@if($object->{$meta}->land_square != NULL)
    <div class="elem">
        <div class="headtitle">Площадь участка</div>
        <div class="elmets">
            <div class="elzet icon">
                <img src="{{asset('/img/svg/land.svg')}}" alt="">
            </div>
            <div class="elzet nominal">{{(float)$object->{$meta}->land_square}} соток</div>
        </div>
    </div>
@endif
