@extends('layouts.mobile')

@section('title', str_replace("&nbsp;", " ", strip_tags($object->title)))

@section('content')
	<section id="object">
		<div class="container">
            <div class="mt-3 object-wraper">
                <h1 class="mt-2 results_header object-info">{!! $object->title !!}</h1>
                <p class="mb-3 published object-info">опубликовано {{$object->created_at->format('d.m H:i')}}</p>
                @if(count($object->images) > 0)
                    <div class="img-wraper wrapper-box">
                        <div class="fotorama"
                             data-allowfullscreen="true"
                             data-keyboard="true"
                             data-nav="thumbs"
                             data-width="700" data-ratio="700/467" data-max-width="100%">
                            @foreach($object->images as $image)
                                <img src="{{ asset($image->url) }}">
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

            <h2 class="title-2 mb-3">Характеристики объекта</h2>

            <div class="details-wraper object-wraper">
                <div class="elem price">
                    @if($object->rented)
                        <p class="details-title">{{number_format($object->rent_price)}} <i class="fa fa-rub" aria-hidden="true"></i> / мес.</p>
                    @else
                        <p class="details-title mb-0">{{number_format($object->price)}} <i class="fa fa-rub" aria-hidden="true"></i></p>
                        <p style="color: #9a9a9a;font-size: 14px;" class="details-data mb-0">{{number_format($object->price_m)}}  <i class="fa fa-rub" aria-hidden="true"></i> за м<sup>2</sup></p>
                    @endif
                </div>
            </div>
            <div class="details-wraper object-wraper">
                <div class="city_bread">
                    <div class="elem">
                        Белгородская область
                    </div>
                    @if($object->city()->first() != null)
                        <div class="elem">
                            {{ $object->city->socr  }}. {{ $object->city->name }}
                        </div>
                    @endif
                    @if($object->area()->first() != null)
                        <div class="elem">
                            {{ucfirst($object->area()->first()->title)}}
                        </div>
                    @endif
                    @if($object->street != null)
                        <div class="elem">
                            {{mb_convert_case($object->street, MB_CASE_TITLE, "UTF-8")}} {{ $object->{$meta}->house_number }}
                        </div>
                    @endif
                </div>
            </div>

            <div class="details-wraper object-wraper">
                @include('template.common.object.parametrs.items')
            </div>

            @if($object->description != NULL)
                <h2 class="title-2">Описание объекта</h2>
                <div class="details-wraper object-wraper">
                    <div class="description-wraper wrapper-box">
                        <div class="contenter">{!!  $object->description !!}</div>
                    </div>
                </div>
            @endif

            <div class="details-wraper object-wraper">
                <div class="map-wraper wrapper-box" style="height: 400px;position: relative">
                    <div id="map" style="height: 400px;width: 100%; height: 400px;"></div>
                </div>
            </div>

            <div class="contact object-wraper" style="padding: 15px;">
                <div class="elem">
                    <span class="title-2 w-100 d-block">{{$object->agency->title}}</span>
                    <span class="published">Агентство недвижимости</span>
                </div>

                @if (!\Session::has('success'))
                    <button type="button" onclick="$(this).remove()" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" type="submit" class="btn btn-search w-100">Показать номер</button>
                @endif
                <div class="collapse  @if (\Session::has('success')) show @endif" id="collapseExample">

                    <div class="manager-data">
                        <span class="manager-name ml-0">{{$object->autor->lastname}}</span>
                        <span style="display: block;color: #8c8c8c;font-size: 16px;" class="manager-name ml-0">{{$object->autor->firstname . ' ' . $object->autor->midname}}</span>
                        <div class="information-column">
                            <a href="tel:{{$object->autor->phone}}" class="contact mb-2"><i class="fa fa-phone"></i> {{$object->autor->phone}}</a>

                            <a href="mailto:{{$object->autor->email}}" class="contact mb-2"><i class="fa fa-envelope"></i> {{$object->autor->email}}</a>
                        </div>
                    </div>
                    @if (\Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {!! \Session::get('success') !!}
                        </div>
                    @endif
                    <div class="form-wraper">
                        <h2 style="margin-top: 0!important" class="title-2 mt-3 mb-2">Написать агенту</h2>
                        <form action="{{route('messageToAgent')}}" method="post">
                            <input type="hidden" name="agent" value="{{$object->autor->id}}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" id="af_name" name="name" value='' placeholder="Ваше имя" class="form-control mb-0" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="email" id="af_email" name="email" value="" placeholder="Ваш E-mail" class="form-control mb-0" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input type="text" id="af_phone" name="phone" value="" placeholder="Ваш Телефон" required class="form-control mb-0" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <textarea id="af_message" name="message" class="form-control mb-0" rows="5" required placeholder="Сообщение" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="controls">
                                            <button type="submit" class="btn btn-search w-100">Отправить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</section>
@endsection

@push('scripts')
    <link href="{{asset('lib/fotorama/fotorama.css')}}" rel="stylesheet">
    <script src="{{asset('lib/fotorama/fotorama.js')}}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(function(){
            // Указывается идентификатор HTML-элемента.
            var map = new ymaps.Map("map", {
                center: [{{$object->geopoint}}],
                zoom: 16,
                controls: ['zoomControl', 'searchControl']
            });



                myPlacemark = new ymaps.Placemark([{{$object->geopoint}}], {
                hintContent: '{{$object->title}}'

            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                //iconLayout: 'default#image'
            });
            map.geoObjects.add(myPlacemark);



        });
    </script>
@endpush
