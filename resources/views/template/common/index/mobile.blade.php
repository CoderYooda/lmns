@extends('layouts.mobile')

@section('content')
    <div class="container">
        <form id="filter-head" class="type-1" action="{{route('search')}}" method="GET">
            <div class="tab_checkbox">
                <input id="sold-input" type="radio" name="rent" value="0" checked>
                <label for="sold-input">Купить</label>
                <input id="rent-input" type="radio" name="rent" value="1">
                <label for="rent-input">Снять</label>
            </div>
            <div class="filter_panel">
                <div class="filter_elem">
                    <select name="category" id="category" class="form-control select-init">
                        <option value="1" @if( request('category') != NULL && request('category') == 1 )selected="selected"@endif>Квартиру</option>
                        <option value="2" @if( request('category') != NULL && request('category') == 2 )selected="selected"@endif>Комнату</option>
                        <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дом</option>
                        <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дачу</option>
                        <option value="4" @if( request('category') != NULL && request('category') == 4 )selected="selected"@endif>Участок</option>
                        <option value="5" @if( request('category') != NULL && request('category') == 5 )selected="selected"@endif>Гараж</option>
                        <option value="6" @if( request('category') != NULL && request('category') == 6 )selected="selected"@endif>Комерческую недвижимсоть</option>
                    </select>
                </div>
                <div id="readiness" class="tab_pin filter_elem">
                    <input class="form-check-input" name="vtorich" type="checkbox" id="vtorich" value="1">
                    <label class="form-check-label" for="vtorich">Вторичка</label>
                    <input class="form-check-input" name="novostroi" type="checkbox" id="novostroi" value="1">
                    <label class="form-check-label" for="novostroi">Новостройка</label>
                </div>
                <div id="rooms" class=" filter_elem">
                    <label>Колличество комнат</label>
                    <div class="tab_pin">
                        <input id="rooms_study" name="rooms_study" value="1" type="checkbox">
                        <label class="ib" for="rooms_study">Студия</label>
                        <input id="rooms_one" name="rooms_one" value="1" type="checkbox">
                        <label class="ib" for="rooms_one">1</label>
                        <input id="rooms_two" name="rooms_two" value="1" type="checkbox">
                        <label class="ib" for="rooms_two">2</label>
                        <input id="rooms_three" name="rooms_three" value="1" type="checkbox">
                        <label class="ib" for="rooms_three">3</label>
                        <input id="rooms_four" name="rooms_four" value="1" type="checkbox">
                        <label class="ib" for="rooms_four">4+</label>
                    </div>
                </div>
                <div id="commece_type" class="filter_elem">
                    <label>Назначение</label>
                    <select name="commerce_type" id="commerce_type" class="form-control select-init">
                        <option value="0" @if( request('commerce_type') != NULL && request('commerce_type') == 0 )selected="selected"@endif>Любой</option>
                        @foreach($commerce_type as $type)
                            <option value="{{$type->id}}" @if( request('commerce_type') != NULL && request('commerce_type') == $type->id )selected="selected"@endif>{{$type->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div id="cost" class="filter_elem">
                    <label>Стоимость</label>
                    <div class="number_input">
                        <input min="0" max="14000000000" type="number" name="cost-from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="">
                        <input min="0" max="14000000000" type="number" name="cost-to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="">
                        <span class="unit"><i class="fa fa-rub" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div id="square_sot" class="filter_elem">
                    <label>Площадь</label>
                    <div class="number_input">
                        <input min="0" max="1000000" type="number" name="square_sot_from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="">
                        <input min="0" max="1000000" type="number" name="square_sot_to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="">
                        <span class="unit">соток</span>
                    </div>
                </div>
                <div id="square" class="filter_elem">
                    <label>Площадь</label>
                    <div class="number_input">
                        <input type="number" name="square_from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="">
                        <input type="number" name="square_to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="">
                        <span class="unit">м<sup>2</sup></span>
                    </div>
                </div>
                <div id="city" class="filter_elem">
                    <label>Город</label>
                    <select name="city" id="categories" class="form-control select-init">
                        @foreach($cities as $city)
                            <option value="{{$city->id}}" @if( request('category') != NULL && request('city') == $city->id )selected="selected"@endif>{{$city->socr}}. {{$city->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div id="submit_filter" class="filter_elem">
                    <button class="btn btn-search">Найти</button>
                </div>
            </div>
        </form>
    </div>

    <div class="container area-tabs">
        <ul class="prev_tab_container nav nav-tabs area-tab-header">
            <li class="active"><a class="active" data-toggle="tab" href="#panel1">Купить</a></li>
            <li><a data-toggle="tab" href="#panel2">Снять</a></li>
        </ul>
        <div class="tab-content">
            <div id="panel1" class="tab-pane fade in active show">
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/flat.jpg')}}" alt="Квартиры">
                    </div>
                    <div class="links">
                        <a href="/kupit-1komnatnyu-kvartiry" class="category"><span>1 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 1)}}</span></a>
                        <a href="/kupit-2komnatnyu-kvartiry" class="category"><span>2 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 2)}}</span></a>
                        <a href="/kupit-3komnatnyu-kvartiry" class="category"><span>3 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 3)}}</span></a>
                        <a href="/kupit-4komnatnyu-kvartiry" class="category"><span>4 и более комнат</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 4)}}</span></a>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/rooms.jpg')}}" alt="Комнаты">
                    </div>
                    <div class="links">
                        <a href="/kupit-komnatu-do10metrov" class="category"><span>Комнаты до 10м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 0, 10)}}</span></a>
                        <a href="/kupit-komnatu-ot10do15metrov" class="category"><span>Комнаты 10 - 15м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 10, 15)}}</span></a>
                        <a href="/kupit-komnatu-ot15do20metrov" class="category"><span>Комнаты 15 - 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 15, 20)}}</span></a>
                        <a href="/kupit-komnatu-ot20metrov" class="category"><span>Комнаты более 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 20, PHP_INT_MAX)}}</span></a>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/house.jpg')}}" alt="Квартиры">
                    </div>
                    <div class="links">
                        <a href="/kupit-dom-do100metrov" class="category"><span>Дома до 100м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 0, 100)}}</span></a>
                        <a href="/kupit-dom-ot100do150metrov" class="category"><span>Дома 100 - 150м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 100, 150)}}</span></a>
                        <a href="/kupit-dom-ot150do200metrov" class="category"><span>Дома 150 - 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 150, 200)}}</span></a>
                        <a href="/kupit-dom-ot200metrov" class="category"><span>Дома более 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 200, PHP_INT_MAX)}}</span></a>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/lands.jpg')}}" alt="Квартиры">
                    </div>
                    <div class="links">
                        <a href="/kupit-uchastok-do10sotok" class="category"><span>Участки до 10 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 0, 10)}}</span></a>
                        <a href="/kupit-uchastok-ot10do15sotok" class="category"><span>Участки 10-15 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 10, 15)}}</span></a>
                        <a href="/kupit-uchastok-ot15do20sotok" class="category"><span>Участки 15-20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 15, 20)}}</span></a>
                        <a href="/kupit-uchastok-ot20sotok" class="category"><span>Участки более 20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 20, PHP_INT_MAX)}}</span></a>
                    </div>
                </div>
            </div>
            <div id="panel2" class="tab-pane fade">
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/flat.jpg')}}" alt="Квартиры">
                    </div>
                    <div class="links">
                        <a href="/arenda-1komnatnyu-kvartiry" class="category"><span>1 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 1)}}</span></a>
                        <a href="/arenda-2komnatnyu-kvartiry" class="category"><span>2 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 2)}}</span></a>
                        <a href="/arenda-3komnatnyu-kvartiry" class="category"><span>3 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 3)}}</span></a>
                        <a href="/arenda-4komnatnyu-kvartiry" class="category"><span>4 и более комнат</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 4)}}</span></a>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/rooms.jpg')}}" alt="Комнаты">
                    </div>
                    <div class="links">
                        <a href="/arenda-komnatu-do10metrov" class="category"><span>Комнаты до 10м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 0, 10)}}</span></a>
                        <a href="/arenda-komnatu-ot10do15metrov" class="category"><span>Комнаты 10 - 15м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 10, 15)}}</span></a>
                        <a href="/arenda-komnatu-ot15do20metrov" class="category"><span>Комнаты 15 - 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 15, 20)}}</span></a>
                        <a href="/arenda-komnatu-ot20metrov" class="category"><span>Комнаты более 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 20, PHP_INT_MAX)}}</span></a>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="prev_pic">
                        <img src="{{asset('/images/house.jpg')}}" alt="Квартиры">
                    </div>
                    <div class="links">
                        <a href="/arenda-dom-do100metrov" class="category"><span>Дома до 100м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 0, 100)}}</span></a>
                        <a href="/arenda-dom-ot100do150metrov" class="category"><span>Дома 100 - 150м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 100, 150)}}</span></a>
                        <a href="/arenda-dom-ot150do200metrov" class="category"><span>Дома 150 - 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 150, 200)}}</span></a>
                        <a href="/arenda-dom-ot200metrov" class="category"><span>Дома более 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 200, PHP_INT_MAX)}}</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
