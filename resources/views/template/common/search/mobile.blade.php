@extends('layouts.mobile')

@section('content')

    <div class="container mt-3">

        <h1 class="mb-4 results_header" >
            @if(request('rent') != NULL && request('rent') == 0)Продажа@endif
            @if(request('rent') != NULL && request('rent') == 1)Аренда@endif
            @if(request('rent') == NULL || request('rent') == 2)Продажа и аренда@endif
            <?php switch (Request::input('category')) {
                case 1:echo "квартир";break;
                case 2:echo "комнат";break;
                case 3:echo "коттеджей";break;
                case 4:echo "земельных участков";break;
                case 5:echo "гаражей";break;
                case 6:echo "коммерческой недвижимсоти";break;}?>
            {{\App\Http\Controllers\Support\CityController::getCityNameById(Request::input('city'))}}</h1>

    @include('chunks.map')
        @if(count($objects) > 0)
        <button id="mapbtn" style="width: 100%;margin-bottom: 15px;" onclick="loadMap({{ $category->id }}, {{request('rent')}})" class="btn btn-primary">
            Показать на карте
        </button>
        @endif

        @forelse($objects as $object)
            @include('template.parts.object.mobile-grid')
        @empty

            <div class="not_found">
                По вашему запросу ничего не найдено :(
            </div>

        @endforelse

        {{ $objects->appends(Request::all())->links() }}


        @include('template.parts.filter.mobile')

    </div>

@endsection

@push('scripts')
    @include('chunks.mapscript')
@endpush
