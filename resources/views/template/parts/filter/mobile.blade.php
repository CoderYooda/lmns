<form id="form_apartment" action="{{route($category->type.'s')}}">
    <input type="hidden" name="category" value="{{ $category->id }}">
    <div class="filter_panel inpage">
        <div id="rent" class=" filter_elem">
            <div class="tab_pin">
                <input style="" id="all-input" type="radio" name="rent" value="2" @if(request('rent') == NULL || request('rent') == 2 ) checked @endif>
                <label for="all-input">Все</label>
                <input style="" id="sold-input" type="radio" name="rent" value="0" @if(request('rent') != NULL && request('rent') == 0 ) checked @endif>
                <label for="sold-input">Купить</label>
                <input style="" id="rent-input" type="radio" name="rent" value="1" @if(request('rent') != NULL && request('rent') == 1 ) checked @endif>
                <label for="rent-input">Арендовать</label>
            </div>
        </div>
        <div id="city" class="filter_elem">
            <label>Город</label>
            <select name="city" id="city" class="form-control select-init">
                <option value="0" @if( $category->id != NULL && request('city') == 0 )selected="selected"@endif>Любой</option>
                @foreach($cities as $city)
                    <option value="{{$city->id}}" @if( request('category') != NULL && request('city') == $city->id )selected="selected"@endif>{{$city->title}}</option>
                @endforeach
            </select>
        </div>
        @if($category->id == 6)
        <div id="commece_type" class="filter_elem">
            <label>Назначение</label>
            <select name="commerce_type" id="commerce_type" class="form-control select-init">
                <option value="0" @if( request('commerce_type') != NULL && request('commerce_type') == 0 )selected="selected"@endif>Любой</option>
                @foreach($commerce_types as $type)
                    <option value="{{$type->id}}" @if( request('commerce_type') != NULL && request('commerce_type') == $type->id )selected="selected"@endif>{{$type->title}}</option>
                @endforeach
            </select>
        </div>
        @endif
        @if($category->id == 1)
            <div id="readiness" class="tab_pin filter_elem">
                <input class="form-check-input" name="vtorich" type="checkbox" id="vtorich" value="1" {{ Request::filled('vtorich') && request('vtorich') == 1 ? 'checked' : '' }}>
                <label class="form-check-label" for="vtorich">Вторичка</label>
                <input class="form-check-input" name="novostroi" type="checkbox" id="novostroi" value="1" {{ Request::filled('novostroi') && request('novostroi') == 1  ? 'checked' : '' }}>
                <label class="form-check-label" for="novostroi">Новостройка</label>
            </div>
            <div id="rooms" class=" filter_elem">
                <label>Колличество комнат</label>
                <div class="tab_pin">
                    <input id="rooms_study" name="rooms_study" value="1" type="checkbox"
                        {{ Request::filled('rooms_study') ? 'checked' : '' }}
                    >
                    <label class="ib" for="rooms_study">Студия</label>
                    <input id="rooms_one" name="rooms_one" value="1" type="checkbox"
                        {{ Request::filled('rooms_one') ? 'checked' : '' }}
                    >
                    <label class="ib" for="rooms_one">1</label>
                    <input id="rooms_two" name="rooms_two" value="1" type="checkbox"
                        {{ Request::filled('rooms_two') ? 'checked' : '' }}
                    >
                    <label class="ib" for="rooms_two">2</label>
                    <input id="rooms_three" name="rooms_three" value="1" type="checkbox"
                        {{ Request::filled('rooms_three') ? 'checked' : '' }}
                    >
                    <label class="ib" for="rooms_three">3</label>
                    <input id="rooms_four" name="rooms_four" value="1" type="checkbox"
                        {{ Request::filled('rooms_four') ? 'checked' : '' }}
                    >
                    <label class="ib" for="rooms_four">4+</label>
                </div>
            </div>
        @endif
        <div id="cost" class="filter_elem">
            <label>Стоимость</label>
            <div class="number_input">
                <input min="0" max="14000000000" type="number" name="cost-from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="{{ Request::input('cost-from', '') }}">
                <input min="0" max="14000000000" type="number" name="cost-to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="{{ Request::input('cost-to', '') }}">
                <span class="unit"><i class="fa fa-rub" aria-hidden="true"></i></span>
            </div>
        </div>
        @if($category->id == 2 || request('category') == 3 || request('category') == 5 || request('category') == 6)
            <div id="square" class="filter_elem">
                <label>Площадь</label>
                <div class="number_input">
                    <input type="text" name="square_from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="{{ Request::input('square_from', '') }}">
                    <input type="text" name="square_to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="{{ Request::input('square_to', '') }}">
                    <span class="unit">м<sup>2</sup></span>
                </div>
            </div>
        @endif
        @if($category->id == 4)
            <div id="square_sot" class="filter_elem">
                <label>Площадь</label>
                <div class="number_input">
                    <input min="0" max="1000000" type="number" name="square_sot_from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="{{ Request::input('square_sot_from', '') }}">
                    <input min="0" max="1000000" type="number" name="square_sot_to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="{{ Request::input('square_sot_to', '') }}">
                    <span class="unit">соток</span>
                </div>
            </div>
        @endif
        @if($category->id != 3 && request('category') != 4)
            <div id="floor" class="filter_elem">
                <label>Этаж</label>
                <div class="number_input">
                    <input min="0" max="100" type="number" name="floor_from" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="от" value="{{ Request::input('floor_from', '') }}">
                    <input min="0" max="100" type="number" name="floor_to" inputmode="numeric" pattern="[0-9]*" class="form-control" placeholder="до" value="{{ Request::input('floor_to', '') }}">
                </div>
            </div>
        @endif
        <div id="submit_filter" class="text-center filter_elem">
            <button type="submit" class="btn btn-primary w-100">Фильтровать</button>
        </div>
    </div>


</form>
