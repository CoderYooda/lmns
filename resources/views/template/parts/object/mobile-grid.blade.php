<div class="object-wraper">

    <div class="object-image">
        @if($object->images->first() != NULL)
            <img src="{{ asset($object->images->first()->thumb_url) }}" alt="{{ $object->city->title }}">
        @else
            <img src="{{ asset('/img/noimage.jpg') }}" alt="{{ $object->city->title }}">
        @endif
    </div>
    <div class="object-info">

        <div class="elem">
            <a href="{{ route($object->category->type, $object->id) }}">
                {!! $object->title !!}
            </a>
        </div>


        <div class="elem">
        <span class="address"><i class="fa fa-map-marker mr-1"></i> {{ $object->city->title }}, {{ $object->street }}
            @if($object->{$object->category->type}->house_number != null),  д {{$object->{$object->category->type}->house_number}} @endif
        </span>
        </div>
        <div class="elem">
            @if($object->rented)
                <p class="price-big mb-0">{{ number_format($object->rent_price, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i> / мес</p>
            @else
                <p class="price-big mb-0">{{ number_format($object->price, 0, '', ' ') }} <i class="fa fa-rub" aria-hidden="true"></i></p>
            @endif
        </div>


        <div class="elem more">
            <a href="{{ route($object->category->type, $object->id) }}" class="heart">Подробнее</a>
        </div>
    </div>
</div>
