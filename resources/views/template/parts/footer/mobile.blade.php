<div class="footer">
    <div class="container">
        <ul class="list-inline">
            <li class="item" ><a href="{{ route('apartments') }}">Квартиры</a></li>
            <li class="item" ><a href="{{ route('rooms') }}">Комнаты</a></li>
            <li class="item" ><a href="{{ route('houses') }}">Дома</a></li>
            <li class="item" ><a href="{{ route('lands') }}">Участки</a></li>
            <li class="item" ><a href="{{ route('garages') }}">Гаражи</a></li>
            <li class="item" ><a href="{{ route('commerces') }}">Комм. недвижимость</a></li>
        </ul>
        <div class="webstyle_footer">
            <a href="https://webstyle.top/" target="_blank">
                <img src="{{ asset('/img/ws.svg') }}" alt="Создание и продвижение сайтов">
            </a>
        </div>
    </div>
</div>
