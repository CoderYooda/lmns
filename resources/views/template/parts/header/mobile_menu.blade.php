<div id="main_menu" class="hover_bar">
    <div class="menu_header" >
        <div onclick="toggleMainMenu()" class="button_close_menu"><i class="fa fa-times" aria-hidden="true"></i></div>
    </div>
    <div class="menu_body">
        <ul>
            <li onclick="toggleMainMenu()"><a href="{{ route('apartments') }}"><i class="fa fa-building-o" aria-hidden="true"></i>
                    Квартиры</a></li>
            <li onclick="toggleMainMenu()"><a href="{{ route('rooms') }}"><i class="fa fa-bed" aria-hidden="true"></i>
                    Комнаты</a></li>
            <li onclick="toggleMainMenu()"><a href="{{ route('houses') }}"><i class="fa fa-home" aria-hidden="true"></i>
                    Дома</a></li>
            <li onclick="toggleMainMenu()"><a href="{{ route('lands') }}"><i class="fa fa-square-o" aria-hidden="true"></i>
                    Участки</a></li>
            <li onclick="toggleMainMenu()"><a href="{{ route('garages') }}"><i class="fa fa-car" aria-hidden="true"></i>
                    Гаражи</a></li>
            <li onclick="toggleMainMenu()"><a href="{{ route('commerces') }}"><i class="fa fa-briefcase" aria-hidden="true"></i>
                    Комм. недвижимость</a></li>

            <li onclick="toggleMainMenu()" class="bot_sticked">
                @if(Auth::guest())
                    <a href="{{ route('login') }}">
                        Вход для агентов
                    </a>
                @else
                    <a href="{{ route('DashBoard') }}">
                        Панель управления
                    </a>
                @endif
            </li>
        </ul>
    </div>
</div>
