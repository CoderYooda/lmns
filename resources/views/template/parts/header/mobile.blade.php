@include('template.parts.header.mobile_menu')
<div class="m-header">
    <div class="menu_butt_hamb" onclick="toggleMainMenu()"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="logo_container">
        <a href="{{ route('home') }}" class="logo">
            <img class="logo_img" src="{{ asset('/img/logo.svg') }}" alt="{{ config('app.name', 'Laravel') }}">
            <span>{{ config('app.name', 'Laravel') }}</span>
        </a>
    </div>
</div>
