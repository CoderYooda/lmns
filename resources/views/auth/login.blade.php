@extends('layouts.login')

@section('content')


    <div id="content-body">
        <div class="py-5 text-center w-100">
            <div class="mx-auto w-xxl w-auto-xs">
                <div class="px-3">
                    <form method="POST" action="{{ route('login') }}" name="form">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>


                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="margin-bottom: 16px" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>
                        <div class="form-group">
                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                   placeholder="Пароль" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <label class="md-check">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i class="primary"></i> Запомнить меня
                            </label>
                        </div>
                        <button type="submit" class="btn primary">Вход</button>
                    </form>
                    <div class="my-4">
                        <a href="password/reset" class="text-primary _600">Забыли пароль?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
