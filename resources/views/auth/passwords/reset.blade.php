@extends('layouts.login')

@section('content')
    <div id="content-body">
        <div class="py-5 text-center w-100">
            <div class="mx-auto w-xxl w-auto-xs">
                <div class="px-3">
                    <div>
                        <h5>Забыли пароль?</h5>
                        <p class="text-muted my-3">
                            Введите ваш e-mail и мы вышлем инструкции для восстановления доступа.
                        </p>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-12 col-form-label">E-Mail</label>

                            <div class="col-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-12 col-form-label">Новый пароль</label>

                            <div class="col-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-12 col-form-label ">Подтверждение</label>

                            <div class="col-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    Восстановить пароль
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="py-4">
                        Вернуться к
                        <a href="{{route('login')}}" class="text-primary _600">входу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
