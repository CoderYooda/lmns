@extends('layouts.login')

@section('content')

    <div id="content-body">
        <div class="py-5 text-center w-100">
            <div class="mx-auto w-xxl w-auto-xs">
                <div class="px-3">
                    <div>
                        <h5>Забыли пароль?</h5>
                        <p class="text-muted my-3">
                            Введите ваш e-mail и мы вышлем инструкции для восстановления доступа.
                        </p>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-12 col-form-label">E-Mail Адрес</label>

                            <div class="col-md-12 col-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 col-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    Восстановить
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="py-4">
                        Вернуться к
                        <a href="{{route('login')}}" class="text-primary _600">входу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
