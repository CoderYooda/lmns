@extends('layouts.main')

@section('description', $object->generateDescription())

@section('content')

    <div class="home-baner mb-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 style="color: #fff;text-align: center;padding-bottom: 30px;text-shadow: 0 0 7px #022131;">Недвижимость в Белгороде и области</h1>
                    <form id="filter-head" action="{{route('search')}}" method="GET">
                        <div class="count-room">
                            <input style="" id="sold-input" type="radio" name="rent" value="0" checked>
                            <label for="sold-input">Купить</label>
                            <input style="" id="rent-input" type="radio" name="rent" value="1">
                            <label for="rent-input">Снять</label>
                        </div>

                    <div class="tab-content" >
                        <div class="tab-pane fade show active" id="sold" role="tabpanel" aria-labelledby="room-tab">
                            <div id="filter-wraper" class="filter-wraper type-1">
                                <div style="display: flex;height: 72px;">
                                    <div class="room-wraper mr-2">
                                        <label>Тип</label>
                                        <select name="category" id="category" class="form-control select-init">
                                                <option value="1" @if( request('category') != NULL && request('category') == 1 )selected="selected"@endif>Квартиру</option>
                                                <option value="2" @if( request('category') != NULL && request('category') == 2 )selected="selected"@endif>Комнату</option>
                                                <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дом</option>
                                                <option value="3" @if( request('category') != NULL && request('category') == 3 )selected="selected"@endif>Дачу</option>
                                                <option value="4" @if( request('category') != NULL && request('category') == 4 )selected="selected"@endif>Участок</option>
                                                <option value="5" @if( request('category') != NULL && request('category') == 5 )selected="selected"@endif>Гараж</option>
                                                <option value="6" @if( request('category') != NULL && request('category') == 6 )selected="selected"@endif>Комерческую недвижимсоть</option>
                                        </select>
                                    </div>
                                    <div class="readiness-wraper pr-2">
                                        <label>Готовность</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" name="vtorich" type="checkbox" id="vtorich" value="1">
                                            <label class="form-check-label" for="vtorich">Вторичка</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" name="novostroi" type="checkbox" id="novostroi" value="1">
                                            <label class="form-check-label" for="novostroi">Новостройка</label>
                                        </div>
                                    </div>
                                    <div class="rooms-wraper pr-2">
                                        <label>Комнатность</label>
                                        <div class="form-group count-room">
                                            <input id="rooms_study" name="rooms_study" value="1" type="checkbox">
                                            <label class="ib" for="rooms_study">Студия</label>
                                            <input id="rooms_one" name="rooms_one" value="1" type="checkbox">
                                            <label class="ib" for="rooms_one">1</label>
                                            <input id="rooms_two" name="rooms_two" value="1" type="checkbox">
                                            <label class="ib" for="rooms_two">2</label>
                                            <input id="rooms_three" name="rooms_three" value="1" type="checkbox">
                                            <label class="ib" for="rooms_three">3</label>
                                            <input id="rooms_four" name="rooms_four" value="1" type="checkbox">
                                            <label class="ib" for="rooms_four">4+</label>
                                        </div>
                                    </div>
                                    <div class="commece_type-wraper mr-2">
                                        <label>Назначение</label>
                                        <select name="commerce_type" id="commerce_type" class="form-control select-init">
                                            <option value="0" @if( request('commerce_type') != NULL && request('commerce_type') == 0 )selected="selected"@endif>Любой</option>
                                            @foreach($commerce_type as $type)
                                                <option value="{{$type->id}}" @if( request('commerce_type') != NULL && request('commerce_type') == $type->id )selected="selected"@endif>{{$type->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="cost-wraper pr-2">
                                        <label>Стоимость</label>
                                        <div class="row m-0">
                                            <div class="col-6 p-0">
                                                <input type="text" name="cost-from" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="от" value="">
                                                <span class="unit">₽</span>
                                            </div>
                                            <div class="col-6 p-0">
                                                <input type="text" name="cost-to" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="до" value="">
                                                <span class="unit">₽</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="square-sot-wraper pr-2">
                                        <label>Площадь (соток)</label>
                                        <div class="row m-0">
                                            <div class="col-6 p-0">
                                                <input type="text" name="square_sot_from" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="от" value="">
                                                <span class="unit">соток</span>
                                            </div>
                                            <div class="col-6 p-0">
                                                <input type="text" name="square_sot_to" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="до" value="">
                                                <span class="unit">соток</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="square-wraper pr-2">
                                        <label>Площадь</label>
                                        <div class="row m-0">
                                            <div class="col-6 p-0">
                                                <input type="text" name="square_from" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="от" value="">
                                                <span class="unit">м2</span>
                                            </div>
                                            <div class="col-6 p-0">
                                                <input type="text" name="square_to" inputmode="numeric" pattern="[0-9]*" class="input-filter" placeholder="до" value="">
                                                <span class="unit">м2</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="area-wraper">
                                        <label>Город</label>
                                        <select name="city" id="categories" class="form-control select-init">
                                            <option value="0" @if( request('category') != NULL && request('city') == 0 )selected="selected"@endif>Любой</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}" @if( request('category') != NULL && request('city') == $city->id )selected="selected"@endif>{{$city->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="button-wraper" style="float: right;    width: 100%;">
                                        <label class="invisible">Найти</label>
                                        <button class="btn btn-search">Найти</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="property">
        <div class="object-slider mb-5">
            <div class="container area-tabs">
                <ul class="nav nav-tabs area-tab-header">
                    <li class="active"><a class="active" data-toggle="tab" href="#panel1">Купить</a></li>
                    <li><a data-toggle="tab" href="#panel2">Снять</a></li>
                </ul>
                <div class="tab-content">
                    <div id="panel1" class="tab-pane fade in active show">
                        <div class="row m-0">
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/flat.jpg')}}" alt="Квартиры">
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/kupit-1komnatnyu-kvartiry" class="category"><span>1 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 1)}}</span></a>
                                                <a href="/kupit-2komnatnyu-kvartiry" class="category"><span>2 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 2)}}</span></a>
                                                <a href="/kupit-3komnatnyu-kvartiry" class="category"><span>3 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 3)}}</span></a>
                                                <a href="/kupit-4komnatnyu-kvartiry" class="category"><span>4 и более комнат</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 0, 4)}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/rooms.jpg')}}" alt="Комнаты">
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/kupit-komnatu-do10metrov" class="category"><span>Комнаты до 10м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 0, 10)}}</span></a>
                                                <a href="/kupit-komnatu-ot10do15metrov" class="category"><span>Комнаты 10 - 15м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 10, 15)}}</span></a>
                                                <a href="/kupit-komnatu-ot15do20metrov" class="category"><span>Комнаты 15 - 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 15, 20)}}</span></a>
                                                <a href="/kupit-komnatu-ot20metrov" class="category"><span>Комнаты более 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 0, 20, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/kupit-dom-do100metrov" class="category"><span>Дома до 100м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 0, 100)}}</span></a>
                                                <a href="/kupit-dom-ot100do150metrov" class="category"><span>Дома 100 - 150м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 100, 150)}}</span></a>
                                                <a href="/kupit-dom-ot150do200metrov" class="category"><span>Дома 150 - 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 150, 200)}}</span></a>
                                                <a href="/kupit-dom-ot200metrov" class="category"><span>Дома более 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 0, 200, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/house.jpg')}}" alt="Квартиры">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-md-6 p-0 col-md-push-6">
                                            <div class="links">
                                                <a href="/kupit-uchastok-do10sotok" class="category"><span>Участки до 10 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 0, 10)}}</span></a>
                                                <a href="/kupit-uchastok-ot10do15sotok" class="category"><span>Участки 10-15 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 10, 15)}}</span></a>
                                                <a href="/kupit-uchastok-ot15do20sotok" class="category"><span>Участки 15-20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 15, 20)}}</span></a>
                                                <a href="/kupit-uchastok-ot20sotok" class="category"><span>Участки более 20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 0, 20, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 p-0 col-md-pull-6">
                                            <img src="{{asset('/images/lands.jpg')}}" alt="Квартиры">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="panel2" class="tab-pane fade">
                        <div class="row m-0">
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/flat.jpg')}}" alt="Квартиры">
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/arenda-1komnatnyu-kvartiry" class="category"><span>1 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 1)}}</span></a>
                                                <a href="/arenda-2komnatnyu-kvartiry" class="category"><span>2 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 2)}}</span></a>
                                                <a href="/arenda-3komnatnyu-kvartiry" class="category"><span>3 - комнатные</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 3)}}</span></a>
                                                <a href="/arenda-4komnatnyu-kvartiry" class="category"><span>4 и более комнат</span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCount(1, 1, 4)}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/rooms.jpg')}}" alt="Комнаты">
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/arenda-komnatu-do10metrov" class="category"><span>Комнаты до 10м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 0, 10)}}</span></a>
                                                <a href="/arenda-komnatu-ot10do15metrov" class="category"><span>Комнаты 10 - 15м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 10, 15)}}</span></a>
                                                <a href="/arenda-komnatu-ot15do20metrov" class="category"><span>Комнаты 15 - 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 15, 20)}}</span></a>
                                                <a href="/arenda-komnatu-ot20metrov" class="category"><span>Комнаты более 20м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(2, 1, 20, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-sm-6 p-0">
                                            <div class="links">
                                                <a href="/arenda-dom-do100metrov" class="category"><span>Дома до 100м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 0, 100)}}</span></a>
                                                <a href="/arenda-dom-ot100do150metrov" class="category"><span>Дома 100 - 150м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 100, 150)}}</span></a>
                                                <a href="/arenda-dom-ot150do200metrov" class="category"><span>Дома 150 - 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 150, 200)}}</span></a>
                                                <a href="/arenda-dom-ot200metrov" class="category"><span>Дома более 200м<sup>2</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(3, 1, 200, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 p-0">
                                            <img src="{{asset('/images/house.jpg')}}" alt="Квартиры">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 p-0">
                                <div class="offer-item">
                                    <div class="row m-0">
                                        <div class="col-md-6 p-0 col-md-push-6">
                                            <div class="links">
                                                <a href="/arenda-uchastok-do10sotok" class="category"><span>Участки до 10 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 1, 0, 10)}}</span></a>
                                                <a href="/arenda-uchastok-ot10do15sotok" class="category"><span>Участки 10-15 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 1, 10, 15)}}</span></a>
                                                <a href="/arenda-uchastok-ot15do20sotok" class="category"><span>Участки 15-20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 1, 15, 20)}}</span></a>
                                                <a href="/arenda-uchastok-ot20sotok" class="category"><span>Участки более 20 <sup>сот</sup></span><span class="pull-right">{{\App\Http\Controllers\ObjectController::getCountBySquare(4, 1, 20, PHP_INT_MAX)}}</span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 p-0 col-md-pull-6">
                                            <img src="{{asset('/images/lands.jpg')}}" alt="Квартиры">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <h2>Новые объекты</h2>
            <div class="row">
                {{--@include('chunks.objects_grid')--}}
                @forelse($lastes as $object)
                    @include('object.grid-elem', ['col' => true])
                @empty
                    <div class="col-md-12">
                        Новых объектов пока что нет.
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
