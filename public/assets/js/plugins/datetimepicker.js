(function ($) {
	"use strict";

  var init = function(){

    $('#exlusive_date').datetimepicker({locale: 'ru'});
    $('#datetimepicker2').datetimepicker();
    $('#datetimepicker3').datetimepicker({locale: 'ru'});
    $('#datetimepicker4').datetimepicker({format: 'L'});
    $('#datetimepicker5').datetimepicker({format: 'LT'});
    $('#datetimepicker6').datetimepicker({
      disabledDates: [
          moment().add(1,'d'), moment().add(-1,'d')
      ]
    });
    $('#datetimepicker7').datetimepicker({
      daysOfWeekDisabled: [0, 6]
    });
    $('#datetimepicker8').datetimepicker({
      viewMode: 'years'
    });

    $('#datetimepicker9').datetimepicker();
    $('#datetimepicker10').datetimepicker({
        useCurrent: false
    });
    $("#datetimepicker9").on("change.datetimepicker", function (e) {
        $('#datetimepicker10').datetimepicker('minDate', e.date);
    });
    $("#datetimepicker10").on("change.datetimepicker", function (e) {
        $('#datetimepicker9').datetimepicker('maxDate', e.date);
    });

    $('#datetimepicker11').datetimepicker({
      format: 'DD/MM/YYYY',
      inline: true,
      sideBySide: true
    });

  }

  // for ajax to init again
  $.fn.datetimepicker.init = init;
$.fn.datepicker.dates['en'] = {
    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    daysMin: ["Вс", "Пон", "Вт", "Ср", "Чт", "Пт", "Суб"],
    months: ["Янвварь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    monthsShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
    today: "Today",
    clear: "Clear",
    format: "mm/dd/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 1
};

})(jQuery);
