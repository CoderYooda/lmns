window.generate = function(agency_id, service, link){
    $.ajax({
        url:link,
        data: {agency_id:agency_id, service:service},
        dataType:'json',
        async:true,
        type:'post',
        success:function(response){
            $('#' + service + 'link').val(response.url);
        },
    });
}

function copyBuffer(id) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('#'+id).val()).select();
    document.execCommand("copy");
    $temp.remove();
    notie.alert({type: 'success', text: 'Скопировано в буфер обмена'});
}
