(function ($) {
  "use strict";

  var init = function(){

      sortable('#images', {
          forcePlaceholderSize: true,
          placeholderClass: 'col-6 col-sm-4 col-md-3 sortable-placeholder'
      });
      sortable('#images')[0].addEventListener('sortstart', function(e) {
          $('#images').addClass('dragged');
      });

      sortable('#images')[0].addEventListener('sortstop', function(e) {
          var serial = sortable('#images', 'serialize');

          var sort = [];

          serial[0].children.forEach(function(elem){
              sort.push($(elem).data('id'));
          });

          $.ajax({
              url:'/admin/setindexes',
              data: {id:sort},
              dataType:'json',
              async:true,
              type:'post',
              success:function(response){
                  $('#images').removeClass('dragged');
              },
          });
      });
  }

  // for ajax to init again
  $.fn.html5sortable.init = init;

})(jQuery);
