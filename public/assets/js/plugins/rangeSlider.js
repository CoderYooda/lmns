(function ($) {
    "use strict";

    $(document).ready(function(){


       var form = $('#object-filter');
       var price_to = $('input[name=price_to]');
       var price_from = $('input[name=price_from]');


       console.log(price_from.val());


       var square_from = $('input[name=square_from]');
       var square_to = $('input[name=square_to]');

        if($('input[name=type]:checked').val() == 'selded'){

            var max = $('input[name=price_max]').val();
            var step = 10000;
        } else {

            var max =  $('input[name=rent_price_max]').val();
            var step = 500;
        }

        var sqmax = $('input[name=square_max]').val();

        $("#slider").ionRangeSlider({
            min: 0,
            max: max,
            from: price_from.val(),
            to: price_to.val(),
            step: step,
            type: "double",
            postfix: " ₽",
            hasGrid: true,
            onStart: function(){
                setTimeout(function(){
                    $('#object-filter').find('.filter-preload').fadeOut('slow');
                }, 300);

            },
            onFinish: function (obj) {
                price_to.val(obj.to_pretty.replace(/\s/g, ''));
                price_from.val(obj.from_pretty.replace(/\s/g, ''));
                form.submit();
            }
        });

        $("#square-slider").ionRangeSlider({
            min: 0,
            max: sqmax,
            from: square_from.val(),
            to: square_to.val(),
            step: 1,
            type: "double",
            postfix: " м<sup>2</sup>",
            hasGrid: true,
            disable: function () {
                if($('select[name=category]').val() === ''){
                    return true;
                } else {
                    return false;
                }
            },
            onFinish: function (obj) {
                square_to.val(obj.to_pretty.replace(/\s/g, ''));
                square_from.val(obj.from_pretty.replace(/\s/g, ''));
                form.submit();
            }
        });

        if($('select[name=category]').val() === ''){
            $("#square-slider").data("ionRangeSlider").update({disable:true});
        } else {
            $("#square-slider").data("ionRangeSlider").update({disable:false});
        }

        window.updateSlider = function(){

            if($('input[name=type]:checked').val() === 'selded'){
                var max = $('input[name=price_max]').val();
                var step = 10000;
                var from = 0;
                var to = max;
            } else {
                var max = $('input[name=rent_price_max]').val();
                var step = 500;
                var from = 0;
                var to = max;
            }

            var sqmax = $('input[name=square_max]').val();

            $("#slider").data("ionRangeSlider").update({
                min: 0,
                max: max,
                from: $('input[name=price_from]').val(),
                to: $('input[name=price_to]').val(),
                step: step
            });

            $("#square-slider").data("ionRangeSlider").update({
                min: 0,
                max: sqmax,
                from: $('input[name=square_from]').val(),
                to: $('input[name=square_to]').val()
            });

            if($('select[name=category]').val() === ''){
                $("#square-slider").data("ionRangeSlider").update({disable:true});
            } else {
                $("#square-slider").data("ionRangeSlider").update({disable:false});
            }

            if($('select[name=category]').val() === '4'){
                $("#square-slider").data("ionRangeSlider").update({postfix: " соток",});
            } else {
                $("#square-slider").data("ionRangeSlider").update({postfix: " м<sup>2</sup>",});
            }

            var price_to = $('input[name=price_to]');
            var price_from = $('input[name=price_from]');

            price_to.val($('input[name=price_to]').val());
            price_from.val($('input[name=price_from]').val());
        }
    });

})(jQuery);
