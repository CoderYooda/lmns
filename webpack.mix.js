const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/main/main.js', 'public/js')
    .sass('resources/sass/main/main.scss', 'public/css');

mix.js('resources/js/mobile/mobile.js', 'public/js')
    .sass('resources/sass/mobile/mobile.scss', 'public/css');

mix.js('resources/js/admin/admin.js', 'public/js')
    .sass('resources/sass/admin/admin.scss', 'public/css');

mix.js('resources/js/front/front.js', 'public/js')
    .sass('resources/sass/front/front.scss', 'public/css');


if( !mix.inProduction() ) {
    mix.webpackConfig({
        devtool:"inline-source-map",
    });
    mix.sourceMaps();

    mix.disableNotifications();
}

if(mix.inProduction() ) {

    mix.minify('public/js/app.min.js');
    mix.version();
}





